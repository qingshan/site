package com.zdsoft.p2p.front.mapper;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

import org.apache.ibatis.session.RowBounds;
import org.junit.Before;
import org.junit.Test;
import org.mybatis.spring.MyBatisSystemException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.BadSqlGrammarException;
import org.springframework.transaction.annotation.Transactional;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.plugins.pagination.Pagination;
import com.zdsoft.p2p.front.TestAbstract;
import com.zdsoft.p2p.front.domain.User;

public class UserMapperTest extends TestAbstract{
	
	@Autowired
	private UserMapper userMapper;
	
	@Before
	public void before(){
		assertNotNull(userMapper);
	}
	public User createUser(){
		Random r=new Random(System.currentTimeMillis());
		User user=new User("cqyhm","zz"+r.nextInt());
		user.setEffTime(new Date());
		user.setStateTime(new Date());
		user.setExpTime(new Date());
		user.setListSort(0);
		user.setVersion(0);
		user.setState("F0A");
		user.setCreateBy("system");
		user.setCreateTime(new Date());
		return user;
	}
	@Test
	@Transactional
	public void testInsert(){
		User user=createUser();
		//插入一条记录 ,没有指定属性值则为null
		assertTrue(userMapper.insert(user)==1);
		logger.debug("【userId={}】",user.getUserId());
	}
	@Test(expected=MyBatisSystemException.class)
	@Transactional
	public void testInsert1(){
		//实体对象如果为null,则会抛出异常MyBatisSystemException
		userMapper.insert(null);
	}
	@Test
	@Transactional
	public void testInsertSelective(){
		User user=createUser();
		//插入一条记录（选择字段， null 字段不插入） 
		assertTrue(userMapper.insertSelective(user)==1);	
		logger.debug("【userId={}】",user.getUserId());
	}
	
	@Test(expected=MyBatisSystemException.class)
	@Transactional
	public void testInsertSelective1(){
		//当需要保存的对象为null时,抛出异常：MyBatisSystemException
		userMapper.insertSelective(null);
	}
	@Test
	@Transactional
	public void testInsertBatch(){
		List<User> users=new ArrayList<User>();
		for (int i=0;i<5;i++) {
			User u=new User("cqyhm","杨华明"+i);
			u.setEffTime(new Date());
			u.setStateTime(new Date());
			u.setExpTime(new Date());
			u.setListSort(0);
			u.setVersion(0);
			u.setState("F0A");
			u.setCreateBy("system");
			u.setCreateTime(new Date());
			users.add(u);
		}
		//批量保存数据
		assertNotNull(userMapper.insertBatch(users)==5);
	}
	
	@Test(expected=MyBatisSystemException.class)
	@Transactional
	public void testInsertBatch1(){
		//当需要保存的对象为null时,抛出异常：MyBatisSystemException
		userMapper.insertBatch(null);
	}
	@Test(expected=BadSqlGrammarException.class)
	public void testInsertBatch2(){
		//当要保存的列表中无对象时,抛出异常：BadSqlGrammarException
		userMapper.insertBatch(new ArrayList<User>());
	}
	
	@Test
	@Transactional
	public void testdeleteById(){
		User user=createUser();
		//插入一条记录 ,没有指定属性值则为null
		assertTrue(userMapper.insert(user)==1);
		logger.debug("【userId={}】",user.getUserId());	
		assertTrue(userMapper.deleteById(user.getUserId())==1);
	}
	@Test
	@Transactional
	public void testdeleteById1(){
		//客户Id可以传入null值和""值
		assertTrue(userMapper.deleteById(null)==0);
	}
	@Test
	@Transactional
	public void deleteBatchIds(){
		List<User> users=new ArrayList<User>();
		for (int i=0;i<5;i++) {
			User u=new User("cqyhm","杨华明"+i);
			u.setEffTime(new Date());
			u.setStateTime(new Date());
			u.setExpTime(new Date());
			u.setListSort(0);
			u.setVersion(0);
			u.setState("F0A");
			u.setCreateBy("system");
			u.setCreateTime(new Date());
			users.add(u);
		}
		//批量保存数据
		assertNotNull(userMapper.insertBatch(users)==5);
		List<String> ids=new ArrayList<String>();
		for (User user : users) {
			ids.add(user.getUserId());
		}
		assertNotNull(userMapper.deleteBatchIds(ids)==5);
	}	
	@Test(expected=MyBatisSystemException.class)
	@Transactional
	public void deleteBatchIds1(){
		//删除的list为null时抛出异常：MyBatisSystemException
		userMapper.deleteBatchIds(null);
	}
	
	@Test(expected=BadSqlGrammarException.class)
	@Transactional
	public void deleteBatchIds2(){
		//删除的的Id列表中如果无数据,则抛出异常:BadSqlGrammarException
		userMapper.deleteBatchIds(new ArrayList<String>());
	}
	@Test
	@Transactional
	public void deleteSelective(){
		User user=createUser();
		//插入一条记录 ,没有指定属性值则为null
		assertTrue(userMapper.insert(user)==1);
		logger.debug("【userId={}】",user.getUserId());	
		assertTrue(userMapper.deleteSelective(user)==1);
	}
	@Test(expected=MyBatisSystemException.class)
	@Transactional
	public void deleteSelective1(){
		//当要删除的作为条件的对象为null时报异常:MyBatisSystemException
		userMapper.deleteSelective(null);
	}
	
	@Test
	@Transactional
	public void testUpdate(){
		User user=createUser();
		//插入一条记录 ,没有指定属性值则为null
		assertTrue(userMapper.insert(user)==1);
		
		User whereuser=new User();
		whereuser.setUserId(user.getUserId());
		whereuser.setUserCd(user.getUserCd());
		
		logger.debug("【userId={}】",user.getUserId());	
		user.setUserNm("琴瑟"+System.currentTimeMillis());
		logger.debug("条件:{}",whereuser);
		assertTrue(userMapper.update(user, whereuser)==1);
	}
	@Test
	@Transactional
	public void testUpdate1(){
		User user=createUser();
		//插入一条记录 ,没有指定属性值则为null
		assertTrue(userMapper.insert(user)==1);
		
		User whereuser=new User();
		whereuser.setUserId(user.getUserId());
		whereuser.setUserCd(user.getUserCd());
		
		logger.debug("【userId={}】",user.getUserId());	
		user.setUserNm("琴瑟"+System.currentTimeMillis());
		logger.debug("条件:{}",whereuser);
		//当条件实体对象为null时相当于修改时不带where条件
		assertTrue(userMapper.update(user, null)>=1);	
	}
	@Test
	@Transactional
	public void testUpdate2(){
		User user=createUser();
		//插入一条记录 ,没有指定属性值则为null
		assertTrue(userMapper.insert(user)==1);
		
		User whereuser=new User();
		whereuser.setUserId(user.getUserId());
		whereuser.setUserCd(user.getUserCd());
		
		logger.debug("【userId={}】",user.getUserId());	
		user.setUserNm("琴瑟"+System.currentTimeMillis());
		logger.debug("条件:{}",whereuser);
		//修改的对象为null时，等于把所有的属性都改为null(主键除外)
		assertTrue(userMapper.update(null, whereuser)==1);	
	}
	@Test
	@Transactional
	public void testUpdate3(){
		assertTrue(userMapper.update(null, null)>=1);	
	}
	@Test
	@Transactional
	public void testUpdateSelective(){
		User user=createUser();
		//插入一条记录 ,没有指定属性值则为null
		assertTrue(userMapper.insert(user)==1);
		
		User whereuser=new User();
		whereuser.setUserId(user.getUserId());
		whereuser.setUserCd(user.getUserCd());
		
		logger.debug("【userId={}】",user.getUserId());	
		user.setUserNm("琴瑟"+System.currentTimeMillis());
		logger.debug("条件:{}",whereuser);
		assertTrue(userMapper.updateSelective(user, whereuser)==1);
	}
	@Test
	@Transactional
	public void testUpdateSelective1(){
		User user=createUser();
		//插入一条记录 ,没有指定属性值则为null
		assertTrue(userMapper.insert(user)==1);
		
		User whereuser=new User();
		whereuser.setUserId(user.getUserId());
		whereuser.setUserCd(user.getUserCd());
		
		logger.debug("【userId={}】",user.getUserId());	
		user.setUserNm(null);
		
		logger.debug("条件:{}",whereuser);
		//当要修改对象中userNm=null时，不会修改该字段
		assertTrue(userMapper.updateSelective(user, null)>=1);	
	}
	
	@Test(expected=MyBatisSystemException.class)
	public void testUpdateSelectiv2(){
		userMapper.updateSelective(null, null);	
	}	
	@Test
	@Transactional
	public void testUpdateById(){
		User user=createUser();
		//插入一条记录 ,没有指定属性值则为null
		assertTrue(userMapper.insert(user)==1);
		
		logger.debug("【userId={}】",user.getUserId());	
		//user.setUserNm("琴瑟"+System.currentTimeMillis());
		user.setUserNm(null);
		
		assertTrue(userMapper.updateById(user)==1);
	}
	@Test
	public void testUpdateById1(){
		assertTrue(userMapper.updateById(null)==0);
	}
	
	@Test
	@Transactional
	public void testUpdateSelectiveById(){
		User user=createUser();
		//插入一条记录 ,没有指定属性值则为null
		assertTrue(userMapper.insert(user)==1);
		
		logger.debug("【userId={}】",user.getUserId());	
		//user.setUserNm("琴瑟"+System.currentTimeMillis());
		user.setUserNm(null);
		
		assertTrue(userMapper.updateSelectiveById(user)==1);
	}
	@Test(expected=MyBatisSystemException.class)
	public void testUpdateSelectiveById1(){
		assertTrue(userMapper.updateSelectiveById(null)==0);
	}
	
	@Test
	@Transactional
	public void testUpdateBatchById(){
		List<User> users=new ArrayList<User>();
		for(int i=0;i<5;i++){
			User user=createUser();
			//插入一条记录 ,没有指定属性值则为null
			users.add(user);
		}
		userMapper.insertBatch(users);
		List<User> nUsers=new ArrayList<User>();
		for (User u : users) {
			u.setUserNm(""+System.currentTimeMillis());
			u.setCreateBy(null);
			nUsers.add(u);
		}
		assertTrue(userMapper.updateBatchById(nUsers)==5);
		
	}
	@Test(expected=MyBatisSystemException.class)
	public void testUpdateBatchById1(){
		userMapper.updateBatchById(null);
	}
	@Test(expected=BadSqlGrammarException.class)
	public void testUpdateBatchById2(){
		userMapper.updateBatchById(new ArrayList<User>());
	}
	
	
	@Test
	public void testFinaAll(){
		List<User> users=userMapper.findAll();
		assertNotNull(users);
		logger.debug("{}",users);
	}
	
	@Test
	@Transactional
	public void testSelectById(){
		User user=createUser();
		//插入一条记录 ,没有指定属性值则为null
		assertTrue(userMapper.insert(user)==1);
		
		logger.debug("【userId={}】",user.getUserId());
		
		User u=userMapper.selectById(user.getUserId());
		assertNotNull(u);
		assertEquals(u.getUserCd(), user.getUserCd());
		assertEquals(u.getUserNm(), user.getUserNm());
		assertEquals(u.getUserId(), user.getUserId());
	}
	@Test
	@Transactional
	public void testSelectById1(){
		User u=userMapper.selectById("");
		assertNull(u);
	}
	
	@Test()
	@Transactional
	public void testSelectBatchIds(){
		List<User> users=new ArrayList<User>();
		for (int i=0;i<5;i++) {
			User u=new User("cqyhm","杨华明"+i);
			u.setEffTime(new Date());
			u.setStateTime(new Date());
			u.setExpTime(new Date());
			u.setListSort(0);
			u.setVersion(0);
			u.setState("F0A");
			u.setCreateBy("system");
			u.setCreateTime(new Date());
			users.add(u);
		}
		//批量保存数据
		assertNotNull(userMapper.insertBatch(users)==5);
		List<String> ids=new ArrayList<String>();
		for (User user : users) {
			ids.add(user.getUserId());
		}
		
		List<User> nUsers=userMapper.selectBatchIds(ids);
		assertNotNull(nUsers);
		assertTrue(nUsers.size()==5);
	}
	@Test()
	@Transactional
	public void testSelectBatchIds1(){
		List<String> ids=new ArrayList<String>();
		for(int i=0;i<5;i++){
			ids.add(""+i);
		}
		List<User> nUsers=userMapper.selectBatchIds(ids);
		assertNotNull(nUsers);
	}
	@Test(expected=MyBatisSystemException.class)
	@Transactional
	public void testSelectBatchIds2(){
		//Id列表不允许为null
		List<User> users=userMapper.selectBatchIds(null);
		assertNull(users);
	}
	
	@Test(expected=BadSqlGrammarException.class)
	@Transactional
	public void testSelectBatchIds3(){
		//Id列表不允许长度为0
		List<User> users=userMapper.selectBatchIds(new ArrayList<String>());
		assertNull(users);
	}

	@Test
	@Transactional
	public void testSelectById2(){
		User u=userMapper.selectById(null);
		assertNull(u);
	}
	
	@Test()
	@Transactional
	public void testSelectOne(){
		User user=createUser();
		assertTrue(userMapper.insert(user)==1);
		user.setUserNm(null);
		user.setUserCd(null);
		
		User u=userMapper.selectOne(user);
		assertNotNull(u);
	}
	@Test()
	@Transactional
	public void testSelectOne1(){
		//没有找到数据返回null
		User user=new User();
		user.setUserCd("aaa");
		User u=userMapper.selectOne(user);
		assertNull(u);
	}
	@Test(expected=MyBatisSystemException.class)
	@Transactional
	public void testSelectOne2(){
		//返回超过1条数据报异常
		userMapper.selectOne(new User());
	}
	@Test(expected=MyBatisSystemException.class)
	@Transactional
	public void testSelectOne3(){
		User u=userMapper.selectOne(null);
		assertNull(u);
	}
	
	@Test()
	@Transactional
	public void testSelectCount(){
		User user=createUser();
		assertTrue(userMapper.insert(user)==1);
		user.setUserNm(null);
		user.setUserCd(null);
		
		assertTrue(userMapper.selectCount(user)==1);
		
	}
	@Test()
	@Transactional
	public void testSelectCount1(){
		assertTrue(userMapper.selectCount(null)>=0);
	}
	@Test()
	@Transactional
	public void testSelectCount2(){
		assertTrue(userMapper.selectCount(new User())>=0);
	}
	
	@Test
	@Transactional
	public void testselectList(){
		User u=new User();
		u.setUserCd("cqyhm");
		EntityWrapper<User> entityWrapper=new EntityWrapper<User>(u,"createtime desc");
		List<User> users=userMapper.selectList(entityWrapper);
		assertNotNull(users);
	}
	
	@Test
	@Transactional
	public void testselectList1(){
		List<User> users=userMapper.selectList(null);
		assertNotNull(users);
	}
	
	@Test
	@Transactional
	public void testselectList2(){
		User u=new User();
		u.setUserCd("zangsan");//找不到数据
		EntityWrapper<User> entityWrapper=new EntityWrapper<User>(u,"createtime desc");
		List<User> users=userMapper.selectList(entityWrapper);
		assertNotNull(users);
	}
	@Test
	@Transactional
	public void testselectPage(){
		User u=new User();
		u.setUserCd("cqyhm");//找到数据
		EntityWrapper<User> entityWrapper=new EntityWrapper<User>(u,"createtime desc");
		List<User> users=userMapper.selectPage(RowBounds.DEFAULT,entityWrapper);
		assertNotNull(users);
	}
	
	@Test
	@Transactional
	public void testselectPage1(){
		User u=new User();
		u.setUserCd("zhansan");//找不到数据
		EntityWrapper<User> entityWrapper=new EntityWrapper<User>(u,"createtime desc");
		List<User> users=userMapper.selectPage(RowBounds.DEFAULT,entityWrapper);
		assertNotNull(users);
	}
	@Test(expected=MyBatisSystemException.class)
	public void testselectPage2(){
		User u=new User();
		u.setUserCd("cqyhm");//找到数据
		EntityWrapper<User> entityWrapper=new EntityWrapper<User>(u,"createtime desc");
		userMapper.selectPage(null,entityWrapper);
	}
	@Test
	@Transactional
	public void testselectPage3(){
		User u=new User();
		u.setUserCd("cqyhm");//找到数据
		EntityWrapper<User> entityWrapper=new EntityWrapper<User>(u,"createtime desc");
		List<User> users=userMapper.selectPage(new RowBounds(1,3),entityWrapper);
		assertNotNull(users);
	}
	@Test
	@Transactional
	public void testselectPage4(){
		User u=new User();
		u.setUserCd("cqyhm");//找到数据
		EntityWrapper<User> entityWrapper=new EntityWrapper<User>(u,"createtime desc");
		List<User> users=userMapper.selectPage(new Pagination(1, 3),entityWrapper);
		assertNotNull(users);
	}
	
	@Test
	@Transactional
	public void testselectPage5(){
		User u=new User();
		u.setUserCd("cqyhm");//找到数据
		EntityWrapper<User> entityWrapper=new EntityWrapper<User>(u,"createtime desc");
		Page<User> page=new Page<User>(1, 3);
		List<User> users=userMapper.selectPage(page,entityWrapper);
		page.setRecords(users);
		logger.debug("{}",page);
		assertNotNull(users);
	}
	@Test
	@Transactional
	public void testselectPage6(){
		Page<User> page=new Page<User>(1, 3);
		List<User> users=userMapper.selectPage(page,null);
		page.setRecords(users);
		logger.debug("{}",page);
		assertNotNull(users);
	}
	
	@Test
	public void testFinaPager1(){
		Page<User> page = new Page<User>(1, 2);
		List<User> users=userMapper.findPager(page,null);
		page.setRecords(users);
		
		assertNotNull(users);
		assertTrue(users.size()==2);
	}
	@Test
	@Transactional
	public void testFindPager2(){
		List<User> users=userMapper.findPager(new Pagination(2,3),null);
		assertNotNull(users);
		assertTrue(users.size()==3);
	}
}
