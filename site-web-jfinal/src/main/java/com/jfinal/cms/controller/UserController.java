package com.jfinal.cms.controller;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jfinal.aop.Duang;
import com.jfinal.cms.service.UserService;
import com.jfinal.core.Controller;


public class UserController extends Controller{
	
	private Logger logger=LoggerFactory.getLogger(getClass());
	
	private UserService service=Duang.duang(UserService.class);
	//返回视图
	public void index(){
		logger.debug("请求index");
		//renderText("这是一个测试");
		setAttr("user", service.findById("1"));
		//render("index.jsp");
		renderJsp("index.jsp");
	}
	//返回json参数
	public void save(){
		logger.debug("请求index");
		Map<String, String> param=new HashMap<String, String>();
		param.put("userCd", "cqyhm");
		param.put("userNm", "杨华明");
		renderJson(param);
	}
}
