package com.zdsoft.site.mina.model.customEnum;

import javax.xml.bind.annotation.XmlEnumValue;

/**
 * 文件业务功能
 * 
 * @author cqyhm
 *
 */
public enum FileBusinessCode {
	/** 文件业务功能 "0000":其他 */
	@XmlEnumValue("0000") A0000("0000", "其他"),

	/** 文件业务功能 "0001":出入金交易明细对账 btranYYYYMMDD.dat */
	@XmlEnumValue("0001") A0001("0001", "出入金交易明细对账"),

	/** 文件业务功能 "0016":还款兑付结果文件 srepayclechkYYYYMMDD.dat */
	@XmlEnumValue("0016") A0016("0016", "还款兑付结果文件"),

	/** 文件业务功能 "0017":核销不符文件 sverifychkYYYYMMDD.dat */
	@XmlEnumValue("0017") A0017("0017", "核销不符文件"),

	/** 文件业务功能 "0018":标的投标记录明细文件 sinvestchkYYYYMMDD.dat */
	@XmlEnumValue("0018") A0018("0018", "标的投标记录明细文件"),

	/** 文件业务功能 "0019":标的还款记录明细文件 srepaychkYYYYMMDD.dat */
	@XmlEnumValue("0019") A0019("0019", "标的还款记录明细文件"),

	/** 文件业务功能 "0020":债权转让明细文件 screditchkYYYYMMDD.dat */
	@XmlEnumValue("0020") A0020("0020", "债权转让明细文件"),

	/** 文件业务功能 "0021":历史资金流水明细 P2PZJLIST_YYYYMMDD_ZZZZZZZZ_1.dat */
	@XmlEnumValue("0021") A0021("0021", "历史资金流水明细"),

	/** 文件业务功能 "0022":平台备付金派送明细文件 sdepitsndYYYYMMDD.dat */
	@XmlEnumValue("0022") A0022("0022", "平台备付金派送明细文件"),

	/** 文件业务功能 "0023":余额文件 bbalaYYYYMMDD.dat */
	@XmlEnumValue("0023") A0023("0023", "余额文件");

	private String value;
	private String name;

	private FileBusinessCode(String value, String name) {
		this.value = value;
		this.name = name;
	}

	@Override
	public String toString() {
		return value;
	}

	public String getValue() {
		return value;
	}

	public String getName() {
		return name;
	}
}
