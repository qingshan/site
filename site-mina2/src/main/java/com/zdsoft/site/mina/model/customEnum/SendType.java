package com.zdsoft.site.mina.model.customEnum;

public enum SendType {
	
	A4("4","同步余额"),
	A5("5","还本付息同步"),
	A6("6","老标赎回同步");
	
	private String value;
	private String name;

	private SendType(String value, String name) {
		this.value = value;
		this.name = name;
	}

	@Override
	public String toString() {
		return value;
	}

	public String getValue() {
		return value;
	}

	public String getName() {
		return name;
	}	
}
