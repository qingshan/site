package com.zdsoft.site.mina.model.customEnum;
/**
 * 证件类型（CertificationType） 
 * @author cqyhm
 *
 */
public enum CertificationType {
	A1,//  身份证 
	A2,//  军官证 
	A3,//  国内护照 
	A4,//  户口本 
	A5,//  学员证 
	A6,//  退休证 
	A7,//  临时身份证 
	A8,//  组织机构代码 
	A9,//  营业执照 
	AA,//  武警证 
	AB,//  士兵证 
	AC,//  回乡证 
	AD,//  外国护照 
	AE//  港澳台居民身份证 
}
