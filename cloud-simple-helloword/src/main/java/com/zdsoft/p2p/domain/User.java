package com.zdsoft.p2p.domain;

import java.io.Serializable;

public class User implements Serializable {

	private static final long serialVersionUID = -6105888826819202437L;

	private String userCd;
	private String userNm;

	public User(String userCd, String userNm) {
		this.userCd = userCd;
		this.userNm = userNm;
	}

	public String getUserCd() {
		return userCd;
	}

	public void setUserCd(String userCd) {
		this.userCd = userCd;
	}

	public String getUserNm() {
		return userNm;
	}

	public void setUserNm(String userNm) {
		this.userNm = userNm;
	}
}
