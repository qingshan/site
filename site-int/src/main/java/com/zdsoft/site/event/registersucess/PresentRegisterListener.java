package com.zdsoft.site.event.registersucess;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import com.zdsoft.site.domain.User;

/**
 * 注册成功,赠送游戏大礼包给
 */
@Component
public class PresentRegisterListener implements ApplicationListener<RegisterSucessEvent> {
	
	private Logger logger=LoggerFactory.getLogger(getClass());
    @Async
    @Override
    public void onApplicationEvent(final RegisterSucessEvent event) {
    	logger.debug("注册成功,赠送游戏大礼包给:[{}]",((User)event.getSource()).getUsername());
    }
}
