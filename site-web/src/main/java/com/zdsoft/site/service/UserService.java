package com.zdsoft.site.service;

import java.util.List;

import com.zdsoft.p2p.user.domain.User;

public interface UserService {
	/**创建用户*/
	public User create(User user);

	/**更新用户*/
	public User update(User user);

	/**删除用户*/
	public void delete(Long userId);

	/**修改密码*/
	public void changePassword(Long userId, String newPassword);

	/**根据id查找用户*/
	public User findOne(Long userId);

	/**得到所有用户*/
	public List<User> findAll();

	/**根据用户名查找用户*/
	public User findByUserName(String username);
}
