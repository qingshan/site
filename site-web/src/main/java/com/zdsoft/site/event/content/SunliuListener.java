package com.zdsoft.site.event.content;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.event.SmartApplicationListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import com.zdsoft.p2p.user.domain.User;
import com.zdsoft.p2p.user.service.UserService;
import com.zdsoft.rock.common.AppException;

/**
 * 实现SmartApplicationListener定义【有序】监听器
 * 实现ApplicationListener 	     定义【无序】监听器
 * 缺省是同步,在事件方法【onApplicationEvent】上增加@Async注解即可变成异步
 * 孙刘就收到消息
 */
@Component
public class SunliuListener implements SmartApplicationListener {

	private Logger logger = LoggerFactory.getLogger(getClass());
	
	@Autowired
	private UserService userService;
	@Async
	@Override
	public void onApplicationEvent(final ApplicationEvent event) {
		try {
			Thread.sleep(1000L);	
		} catch (Exception e) {
			logger.debug("延时异常",e);
		}
		userService.save(new User("code111","name111"));
		logger.debug("【1010】【SunliuListener】[{}]", event.getSource());
    	throw new AppException("1010","【1010】【SunliuListener】异常");
	}
	/***
	 * 事件处理顺序,越大越靠后
	 **/
	@Override
	public int getOrder() {
		return 10;
	}
	/**
	 * 定义接收的事件类型
	 */
	@Override
	public boolean supportsEventType(final Class<? extends ApplicationEvent> eventType) {
		return eventType == ContentEvent.class;
	}
	/**
	 * 定义接收事件的来源类型
	 */
	@Override
	public boolean supportsSourceType(final Class<?> sourceType) {
		return sourceType == User.class;
	}
}
