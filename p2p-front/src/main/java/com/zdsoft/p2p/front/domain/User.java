package com.zdsoft.p2p.front.domain;

import java.io.Serializable;
import java.util.Date;

import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.Length;

import com.baomidou.mybatisplus.annotations.IdType;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;

/**
 *
 * 
 *
 */
@TableName(value = "user")
public class User implements Serializable {

	public User(){}
	
	public User(String usercd, String usernm) {
		this(null, usercd, usernm);
	}
	public User(String userid, String usercd, String usernm) {
		this.userId = userid;
		this.userCd = usercd;
		this.userNm = usernm;
	}
	
	@TableField(exist = false)
	private static final long serialVersionUID = 1L;

	/**  */
	@TableId(type = IdType.UUID)
	private String userId;

	/**  */
	private String createBy;

	/**  */
	private Date createTime;

	/**  */
	private Integer version;

	/**  */
	private Date effTime;

	/**  */
	private Date expTime;

	/**  */
	@Size(min=10)
	private Integer listSort;

	/**  */
	private String state;

	/**  */
	private Date stateTime;

	/**  */
	@Length(min=6,max=32)
	private String userCd;

	/**  */
	@Length(min=6,max=32)
	private String userNm;


	public Integer getVersion() {
		return this.version;
	}

	public void setVersion(Integer version) {
		this.version = version;
	}

	public String getState() {
		return this.state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getCreateBy() {
		return createBy;
	}

	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Date getEffTime() {
		return effTime;
	}

	public void setEffTime(Date effTime) {
		this.effTime = effTime;
	}

	public Date getExpTime() {
		return expTime;
	}

	public void setExpTime(Date expTime) {
		this.expTime = expTime;
	}

	public Integer getListSort() {
		return listSort;
	}

	public void setListSort(Integer listSort) {
		this.listSort = listSort;
	}

	public Date getStateTime() {
		return stateTime;
	}

	public void setStateTime(Date stateTime) {
		this.stateTime = stateTime;
	}

	public String getUserCd() {
		return userCd;
	}

	public void setUserCd(String userCd) {
		this.userCd = userCd;
	}

	public String getUserNm() {
		return userNm;
	}

	public void setUserNm(String userNm) {
		this.userNm = userNm;
	}

	@Override
	public String toString() {
		return "User [userId=" + userId + ", userCd=" + userCd + ", userNm=" + userNm + "]";
	}


}
