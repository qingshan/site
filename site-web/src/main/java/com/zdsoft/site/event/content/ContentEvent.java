package com.zdsoft.site.event.content;

import org.springframework.context.ApplicationEvent;

import com.zdsoft.p2p.user.domain.User;

/**
 * 内容事件
 */
public class ContentEvent extends ApplicationEvent {
	private static final long serialVersionUID = -2422772707993195740L;

//	public ContentEvent(final String content) {
//        super(content);
//    }
	public ContentEvent(final User user) {
        super(user);
    }
}
