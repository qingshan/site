package com.zdsoft.site.mina.model.request;

import java.math.BigDecimal;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.zdsoft.site.mina.model.BidInfo;
import com.zdsoft.site.mina.model.MessageHeader;
import com.zdsoft.site.mina.model.TransMangAcc;
import com.zdsoft.site.mina.model.customEnum.BusinessType;
import com.zdsoft.site.mina.model.customEnum.CurrencyCode;
import com.zdsoft.site.mina.model.customEnum.PackageFlag;

/**
 * 6.1.17.服务商发起客户开标确认(42705)—放款的过程 说明： 
 * 1.本功能是服务商方发起标的开标确认，并由本系统进行监管账户余额处理
 * 2.支持重发操作 请求：
 * 
 * @author cqyhm
 *
 */
@XmlRootElement(name = "MsgText")
public class RequestBidOpen extends AbstractRequest {

	public RequestBidOpen(){}
	
	public RequestBidOpen(MessageHeader msgHdr){
		setMsgHdr(msgHdr);
	}
	
	/** 02 报文标志 PackageFlag <PkgFlag> [0..1] PackageFlag 枚举 */
	@XmlElement(name = "PkgFlag")
	private PackageFlag PkgFlag;

	/** 03.标的信息 BidInfo <BidInfo> [1..1] BidInfo 组件 */
	@XmlElement(name = "BidInfo")
	private BidInfo BidInfo;

	/** 04.交易管理账户 ManageAccount <MgeAcct> [0..1] TransMangAcc 组件 */
	@XmlElement(name = "MgeAcct")
	private TransMangAcc MgeAcct;

	/** 05 币种 Currency <Ccy> [1..1] CurrencyCode 枚举 */
	@XmlElement(name = "Ccy")
	private CurrencyCode Ccy;

	/** 06.业务类别 BusinessType <BusType> [0..1] BusinessType 枚举 */
	@XmlElement(name = "BusType")
	private BusinessType BusType;

	/** 07.放款金额 TransferAmount <TrfAmt> [1..1] Amount */
	@XmlElement(name = "TrfAmt")
	private BigDecimal TrfAmt;

	/** 08.放款手续费 FeeAmount <FeeAmt> [0..1] Amount */
	@XmlElement(name = "FeeAmt")
	private BigDecimal FeeAmt;

	/** 放款次数 FulfillTimes <FulTimes> [0..1] Max20Text 说明这次是第几次放款 */
	@XmlElement(name = "FulTimes")
	private String FulTimes;

	/** 放款标志 FulfillFlag <FulFlag> [0..1] Max20Text 说明这次是否为最后一次放款 0-否1-是 */
	@XmlElement(name = "FulFlag")
	private String FulFlag;

	/** 09.短信验证码 MessageBox <MsgBox> [0..1] Max20Text */
	@XmlElement(name = "MsgBox")
	private String MsgBox;

	/** 10.摘要 Digest [Dgst] [0..1] Max128Text */
	@XmlElement(name = "Dgst")
	private String Dgst;

	/** 02 报文标志 PackageFlag <PkgFlag> [0..1] PackageFlag 枚举 */
	public PackageFlag getPkgFlag() {
		return PkgFlag;
	}

	/** 02 报文标志 PackageFlag <PkgFlag> [0..1] PackageFlag 枚举 */
	public void setPkgFlag(PackageFlag pkgFlag) {
		PkgFlag = pkgFlag;
	}

	/** 03.标的信息 BidInfo <BidInfo> [1..1] BidInfo 组件 */
	public BidInfo getBidInfo() {
		return BidInfo;
	}

	/** 03.标的信息 BidInfo <BidInfo> [1..1] BidInfo 组件 */
	public void setBidInfo(BidInfo bidInfo) {
		BidInfo = bidInfo;
	}

	/** 04.交易管理账户 ManageAccount <MgeAcct> [0..1] TransMangAcc 组件 */
	public TransMangAcc getMgeAcct() {
		return MgeAcct;
	}

	/** 04.交易管理账户 ManageAccount <MgeAcct> [0..1] TransMangAcc 组件 */
	public void setMgeAcct(TransMangAcc mgeAcct) {
		MgeAcct = mgeAcct;
	}

	/** 05 币种 Currency <Ccy> [1..1] CurrencyCode 枚举 */
	public CurrencyCode getCcy() {
		return Ccy;
	}

	/** 05 币种 Currency <Ccy> [1..1] CurrencyCode 枚举 */
	public void setCcy(CurrencyCode ccy) {
		Ccy = ccy;
	}

	/** 06.业务类别 BusinessType <BusType> [0..1] BusinessType 枚举 */
	public BusinessType getBusType() {
		return BusType;
	}

	/** 06.业务类别 BusinessType <BusType> [0..1] BusinessType 枚举 */
	public void setBusType(BusinessType busType) {
		BusType = busType;
	}

	/** 07.放款金额 TransferAmount <TrfAmt> [1..1] Amount */
	public BigDecimal getTrfAmt() {
		return TrfAmt;
	}

	/** 07.放款金额 TransferAmount <TrfAmt> [1..1] Amount */
	public void setTrfAmt(BigDecimal trfAmt) {
		TrfAmt = trfAmt;
	}

	/** 08.放款手续费 FeeAmount <FeeAmt> [0..1] Amount */
	public BigDecimal getFeeAmt() {
		return FeeAmt;
	}

	/** 08.放款手续费 FeeAmount <FeeAmt> [0..1] Amount */
	public void setFeeAmt(BigDecimal feeAmt) {
		FeeAmt = feeAmt;
	}

	/** 放款次数 FulfillTimes <FulTimes> [0..1] Max20Text 说明这次是第几次放款 */
	public String getFulTimes() {
		return FulTimes;
	}

	/** 放款次数 FulfillTimes <FulTimes> [0..1] Max20Text 说明这次是第几次放款 */
	public void setFulTimes(String fulTimes) {
		FulTimes = fulTimes;
	}

	/** 放款标志 FulfillFlag <FulFlag> [0..1] Max20Text 说明这次是否为最后一次放款 0-否1-是 */
	public String getFulFlag() {
		return FulFlag;
	}

	/** 放款标志 FulfillFlag <FulFlag> [0..1] Max20Text 说明这次是否为最后一次放款 0-否1-是 */
	public void setFulFlag(String fulFlag) {
		FulFlag = fulFlag;
	}

	/** 09.短信验证码 MessageBox <MsgBox> [0..1] Max20Text */
	public String getMsgBox() {
		return MsgBox;
	}

	/** 09.短信验证码 MessageBox <MsgBox> [0..1] Max20Text */
	public void setMsgBox(String msgBox) {
		MsgBox = msgBox;
	}

	/** 10.摘要 Digest [Dgst] [0..1] Max128Text */
	public String getDgst() {
		return Dgst;
	}

	/** 10摘要 Digest [Dgst] [0..1] Max128Text */
	public void setDgst(String digest) {
		Dgst = digest;
	}

}
