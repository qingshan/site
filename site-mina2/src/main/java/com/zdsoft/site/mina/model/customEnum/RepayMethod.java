package com.zdsoft.site.mina.model.customEnum;

import javax.xml.bind.annotation.XmlEnumValue;

/**
 * 付款方式(RepayMethod) 定义 付款方式
 * 
 * @author cqyhm
 *
 */
public enum RepayMethod {
	/**付款方式  "0", "客户还款"*/
	@XmlEnumValue("0") A0("0", "客户还款"),

	/**付款方式 "1", "风险准备金赔付"*/
	@XmlEnumValue("1") A1("1", "风险准备金赔付"),

	/**付款方式 "2", "担保金赔付"*/
	@XmlEnumValue("2") A2("2", "担保金赔付");

	private String value;
	private String name;

	private RepayMethod(String value, String name) {
		this.value = value;
		this.name = name;
	}

	@Override
	public String toString() {
		return value;
	}

	public String getValue() {
		return value;
	}

	public String getName() {
		return name;
	}
}
