package com.zdsoft.site.mina.p2p;

import org.apache.mina.core.session.IoSession;

import com.zdsoft.site.mina.model.request.AbstractRequest;
import com.zdsoft.site.mina.model.response.ResponseBindCard2;
/**
 * 6.2.2 客户银行账号绑定 (48702)（建立绑定关系、发送短信验证） 
 * 说明：
 * 1.P2P平台发起银行卡绑定关系建立发送验证短信请求到托管系统，托管系统接收到交易请求后，返回短信发送的状态。
 * 2.绑定的是借记卡，请求报文包括：绑定流水号、银行ID、账户名称、账户号码、开户证件类型、证件号码、手机号 3.只允许借记卡绑定
 *
 */
public class I48702Handler extends P2PIoHandler {
	
	public I48702Handler(AbstractRequest request) {
		super(request);
	}
	
	@Override
	public void messageSent(IoSession session, Object message) throws Exception {
		//发送的消息已经通过编码器编码为字符串
		logger.debug("1.3.发送数据到服务器：{}",message);
	}
	/**接收到的消息体为xml字符串*/
	@Override
	public void messageReceived(IoSession session, Object message) throws Exception {
		String text=message.toString();
		setResponse(new ResponseBindCard2(text));//作为同步的参数获取
		logger.debug("2.4.接收服务端的数据:[{}]",text);
		ResponseBindCard2 response=(ResponseBindCard2)getResponse();
		logger.debug("{}",response);
		logger.debug("{}",response.getRst());
		logger.debug("{}",response.getMgeAcct());
	}
	@Override
	public String getInstrCd() {
		return "48702";
	}
}
