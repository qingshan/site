package com.zdsoft.site.mina.model.customEnum;

import javax.xml.bind.annotation.XmlEnumValue;

/**
 * 标的类型（BidType）
 * 
 * @author cqyhm
 *
 */
public enum BidType {
	/**标的类型 "0", "借款标的" */
	@XmlEnumValue("0") A0("0", "借款标的"),

	/**标的类型 "1", "理财标的"*/
	@XmlEnumValue("1") A1("1", "理财标的");

	private String value;
	private String name;

	private BidType(String value, String name) {
		this.value = value;
		this.name = name;
	}

	@Override
	public String toString() {
		return value;
	}

	public String getValue() {
		return value;
	}

	public String getName() {
		return name;
	}
}
