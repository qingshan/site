package com.zdsoft.site.mina.model.customEnum;

/**
 * 7.31  文件传输类型(FileTransFlag) 
 * @author cqyhm
 *
 */
public enum FileTransFlag {
	A0("0"),//下载客户端从服务器获取文件 
	A1("1");//上传客户端发文件到服务器
	private String value;
	// 构造函数，枚举类型只能为私有
	private FileTransFlag(String value) {
		this.value=value;
	}
	@Override
    public String toString() {
        return this.value;
    }
}
