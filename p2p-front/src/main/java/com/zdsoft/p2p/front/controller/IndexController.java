package com.zdsoft.p2p.front.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping
@Controller
public class IndexController {
	/**
	 * 进入主页面
	 * @return
	 */
	@RequestMapping("/index.html")
	public String index(){
		return "index";
	}
}
