package com.zdsoft.common;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestController {

	private Logger logger=LoggerFactory.getLogger(getClass());
	
	@RequestMapping("/test1")
	public User test1(){
		logger.info("微服务提供服务");
		return new User("", "cqyhm", "本拉登");
	}
}
