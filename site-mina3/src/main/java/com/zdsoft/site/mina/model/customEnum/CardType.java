package com.zdsoft.site.mina.model.customEnum;

public enum CardType {
	A10(10),//个人借记 
	A20(20);// 个人贷记
	
	private Integer value;
	
	private CardType(Integer value){
		this.value=value;
	}
	@Override
	public String toString(){
		return String.valueOf(value);
	}	
}
