package com.zdsoft.site.event;

import org.springframework.context.ApplicationEvent;

import com.zdsoft.site.domain.User;

public class LoginSuccessEvent extends ApplicationEvent {

	private static final long serialVersionUID = -957312471314720418L;
	
	public LoginSuccessEvent(User user) {
		super(user);
	}
}
