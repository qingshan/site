package com.zdsoft.site.mina.model;

import com.zdsoft.site.mina.model.customEnum.FileBusinessCode;

/**
 * 文件信息(FileInfo) 
 * @author cqyhm
 *
 */
public class FileInfo {
//	文件业务功能  FileBusCode  <BusCode>  [0..1]  FileBusinessCode 可选  
	private FileBusinessCode FileBusCode;
//	文件业务日期  BusinessDate  <BusDate>  [0..1]  Date  可选 YYYYMMDD 
	private String BusinessDate;
//	文件存放主机  Host  <Host>  [0..1]  Max35Text   
	private String Host;
//	文件名称  FileName  <FileName>  [1..1]  Max128Text
	private String FileName;
//	文件长度  FileLength  <FileLen>  [0..1]  Number
	private String FileLength;
//	文件时间  FileTime  <FileTime>  [0..1]  DateTime   YYYYMMDDHHMMSS  
	private String FileTime;
//	文件校验码  FileMac  <FileMac>  [0..1]  Max128Text
	private String FileMac;
	public FileBusinessCode getFileBusCode() {
		return FileBusCode;
	}
	public void setFileBusCode(FileBusinessCode fileBusCode) {
		FileBusCode = fileBusCode;
	}
	public String getBusinessDate() {
		return BusinessDate;
	}
	public void setBusinessDate(String businessDate) {
		BusinessDate = businessDate;
	}
	public String getHost() {
		return Host;
	}
	public void setHost(String host) {
		Host = host;
	}
	public String getFileName() {
		return FileName;
	}
	public void setFileName(String fileName) {
		FileName = fileName;
	}
	public String getFileLength() {
		return FileLength;
	}
	public void setFileLength(String fileLength) {
		FileLength = fileLength;
	}
	public String getFileTime() {
		return FileTime;
	}
	public void setFileTime(String fileTime) {
		FileTime = fileTime;
	}
	public String getFileMac() {
		return FileMac;
	}
	public void setFileMac(String fileMac) {
		FileMac = fileMac;
	}
}
