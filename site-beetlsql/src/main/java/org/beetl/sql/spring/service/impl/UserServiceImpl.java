package org.beetl.sql.spring.service.impl;

import org.beetl.sql.spring.dao.UserDao;
import org.beetl.sql.spring.domain.User;
import org.beetl.sql.spring.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class UserServiceImpl implements UserService {
	
	@Autowired
	private UserDao userDao;
	
	@Override
	@Transactional
	public void save(User user) {
		userDao.save(user);
	}

	@Override
	@Transactional(readOnly=true)
	public int total(User user) {
		return userDao.total(user);
	}

}
