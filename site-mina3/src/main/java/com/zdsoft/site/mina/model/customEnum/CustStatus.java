package com.zdsoft.site.mina.model.customEnum;
/**
 * 客户状态（CustStatus） 
 * @author cqyhm
 *
 */
public enum CustStatus {
	A0("0"),//  正常   
	A1("1"),//  冻结   
	A2("2"),//  挂失   
	A3("3");//  销户   
	
	private String value;
	private CustStatus(String value){
		this.value=value;
	}
	@Override
	public String toString(){
		return value;
	}	
}
