package com.zdsoft.site.event.content;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.event.SmartApplicationListener;
import org.springframework.stereotype.Component;

/**
 * 张三接受消息
 */
@Component
public class ZansanListener implements SmartApplicationListener {

	private Logger logger=LoggerFactory.getLogger(getClass());

    @Override
    public void onApplicationEvent(ApplicationEvent event) {
		try {
			Thread.sleep(1000L);	
		} catch (Exception e) {
			logger.debug("延时异常",e);
		}
    	logger.debug("【1040】【ZansanListener】[{}]",event.getSource());
    	//throw new AppException("1040","【1040】【ZansanListener】异常");
    }

	@Override
	public int getOrder() {
		return 40;
	}

	@Override
	public boolean supportsEventType(Class<? extends ApplicationEvent> eventType) {
		return eventType == ContentEvent.class;
	}

	@Override
	public boolean supportsSourceType(Class<?> sourceType) {
		return true;
	}
}
