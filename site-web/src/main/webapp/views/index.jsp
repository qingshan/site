<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<html>
<head>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/jquery-1.8.0.js"></script>
</head>
<body>
<h2>Hello World!</h2>
<div style="color: red;"><a href="${pageContext.request.contextPath}/user/update.do">1.更新上下文</a></div>
<div><b>各个模块的功能列表</b></div>
<hr/>
<div><a href="${pageContext.request.contextPath}/finance/index.do">1-finance.finance_spring</a></div>
<hr/>
<div><a href="${pageContext.request.contextPath}/user/index.do">2-user.用户显示</a></div>
<hr/>
<div><a href="${pageContext.request.contextPath}/msg/index.do">3-webSocket测试</a></div>
<hr/>
<div><a href="${pageContext.request.contextPath}/jms/index.do">4-jms.消息队列</a></div>

<div><a id="ok">确定</a></div>
<script type="text/javascript">
	$("#ok").click(function(){
		$.ajax({
			url:"${pageContext.request.contextPath}/template/timeout.html",
			data:"",
			dataType:"",
			timeout:"121000",
			success:function(data){
				$("#ok").text(data);
			}
		});
	});
</script>

</body>
</html>
