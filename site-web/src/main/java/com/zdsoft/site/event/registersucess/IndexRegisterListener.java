package com.zdsoft.site.event.registersucess;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import com.zdsoft.p2p.user.domain.User;

/**
 * 注册成功,索引用户信息
 */
@Component
public class IndexRegisterListener implements ApplicationListener<RegisterSucessEvent> {
	
	private Logger logger=LoggerFactory.getLogger(getClass());
    @Async
    @Override
    public void onApplicationEvent(final RegisterSucessEvent event) {
    	try {
    		Thread.sleep(3000L);
    		logger.debug("注册成功,索引用户信息:[{}][{}]",Thread.currentThread().getId(),((User)event.getSource()).getUserNm());	
		} catch (Exception e) {
			logger.error("",e);
		}
    	
    }
}
