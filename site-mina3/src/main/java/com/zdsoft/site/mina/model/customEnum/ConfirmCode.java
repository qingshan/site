package com.zdsoft.site.mina.model.customEnum;
/**
 * 25  确认码（ConfirmCode） 
	定义 
	确认码 
	已生效0000 
	已作废  0001 
	其他错误代码 0002-9999 
 * @author cqyhm
 *
 */
public enum ConfirmCode {
	A0000,//  已生效 
	A0001,//  已作废 
	A0002_9999,//  其他错误 
}
