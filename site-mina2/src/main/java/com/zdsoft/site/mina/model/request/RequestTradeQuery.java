package com.zdsoft.site.mina.model.request;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.zdsoft.site.mina.model.MessageHeader;
import com.zdsoft.site.mina.model.Reference;
import com.zdsoft.site.mina.model.customEnum.CurrencyCode;

/**
 *6.1.10　交易结果查询(44711) 
 * 该功能用于服务商发起查询交易的结果
 * 目前支持查询的交易：
 * 1、服务商发起快捷支付入金(48704)
 * 2、服务商发起客户出金(44702)
 * 3、服务商收益划出(44705)
 * 4、服务商客户标的投标(42703)
 * 5、服务商发起客户开标确认(42705)
 * 6、服务商发起借款人还款(42707)
 * 7、服务商发起风险赔付(42708)
 * 8、服务商发起客户佣金派送(48709) 
 * @author cqyhm
 *
 */
@XmlRootElement(name = "MsgText")
public class RequestTradeQuery extends AbstractRequest {
	
	public RequestTradeQuery(){}
	
	public RequestTradeQuery(MessageHeader msgHdr){
		setMsgHdr(msgHdr);
	}
	
	/**02	原流水号	RequestReference	<RqRef >	[1..1]	Reference	组件*/
	@XmlElement(name="RqRef")
	private Reference RqRef;
	
	/**03	原业务功能码	InstructionCode	<InstrCd>	[1..1]	InstructionCode	枚举*/
	@XmlElement(name="InstrCd")
	private String InstrCd;
	
	/**04	原发生日期	CreateDate	<Date>	[1..1]	Date	*/
	@XmlElement(name="Date")
	private String Date;
	
	/**05	币种	Currency	<Ccy>	[1..1]	CurrencyCode	枚举*/
	@XmlElement(name="Ccy")
	private CurrencyCode Ccy;
	
	/** 06.摘要 Digest [Dgst] [0..1] Max128Text */
	@XmlElement(name = "Dgst")
	private String Dgst;

	/**02	原流水号	RequestReference	<RqRef >	[1..1]	Reference	组件*/
	public Reference getRqRef() {
		return RqRef;
	}

	/**02	原流水号	RequestReference	<RqRef >	[1..1]	Reference	组件*/
	public void setRqRef(Reference rqRef) {
		RqRef = rqRef;
	}

	/**03	原业务功能码	InstructionCode	<InstrCd>	[1..1]	InstructionCode	枚举*/
	public String getInstrCd() {
		return InstrCd;
	}

	/**03	原业务功能码	InstructionCode	<InstrCd>	[1..1]	InstructionCode	枚举*/
	public void setInstrCd(String instrCd) {
		InstrCd = instrCd;
	}

	/**04	原发生日期	CreateDate	<Date>	[1..1]	Date	*/
	public String getDate() {
		return Date;
	}

	/**04	原发生日期	CreateDate	<Date>	[1..1]	Date	*/
	public void setDate(String date) {
		Date = date;
	}

	/**05	币种	Currency	<Ccy>	[1..1]	CurrencyCode	枚举*/
	public CurrencyCode getCcy() {
		return Ccy;
	}

	/**05	币种	Currency	<Ccy>	[1..1]	CurrencyCode	枚举*/
	public void setCcy(CurrencyCode ccy) {
		Ccy = ccy;
	}
	
	/** 06.摘要 Digest [Dgst] [0..1] Max128Text */
	public String getDgst() {
		return Dgst;
	}

	/** 06.摘要 Digest [Dgst] [0..1] Max128Text */
	public void setDgst(String digest) {
		Dgst = digest;
	}
	
}
