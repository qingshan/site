package com.zdsoft.site.mina.model.customEnum;

import javax.xml.bind.annotation.XmlEnumValue;

/**
 * 8.1.6 机构类型(InstitutionType) 0--服务商 1--银行
 * 
 * @author cqyhm
 *
 */
public enum InstitutionType {
	/** 0--服务商 */
	@XmlEnumValue("0") A0("0", "服务商"), 
	
	/** 1--银行 */
	@XmlEnumValue("1") A1("1", "银行");

	private String value;
	private String name;

	private InstitutionType(String value, String name) {
		this.value = value;
		this.name = name;
	}

	public String getValue() {
		return value;
	}

	public String getName() {
		return name;
	}

	@Override
	public String toString() {
		return value;
	}
}
