package com.zdsoft.site.mina.model.response;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.springframework.beans.BeanUtils;

import com.zdsoft.site.mina.model.BidInfo;
import com.zdsoft.site.mina.model.TransMangAcc;
import com.zdsoft.site.mina.utils.JaxbUtil;

/**
 * 6.1.15 服务商客户标的投标(42703) 
 * 说明： 
 * 1.本功能是服务商方发起客户对指定标的投标，并由本系统进行监管账户余额处理
 * 2.投标金额为客户投资标的的总额，实际划扣金额 = 投标金额 – 红包抵扣金额
 * 3.支持重发操作 
 * 应答：
 * @author cqyhm
 *
 */
@XmlRootElement(name = "MsgText")
public class ResponseBidCust extends AbstractResponse {

	/** 03.标的信息 BidInfo <BidInfo> [0..1] BidInfo 组件 */
	@XmlElement(name = "BidInfo")
	private BidInfo BidInfo;

	/** 04.交易管理账户 ManageAccount <MgeAcct> [1..1] TransMangAcc 组件 */
	@XmlElement(name = "MgeAcct")
	private TransMangAcc MgeAcct;

	public ResponseBidCust() {
	}

	public ResponseBidCust(String xml) {
		ResponseBidCust response = JaxbUtil.converyToJavaBean(xml, this.getClass());
		BeanUtils.copyProperties(response, this);
	}

	/** 03.标的信息 BidInfo <BidInfo> [0..1] BidInfo 组件 */
	public BidInfo getBidInfo() {
		return BidInfo;
	}

	/** 03.标的信息 BidInfo <BidInfo> [0..1] BidInfo 组件 */
	public void setBidInfo(BidInfo bidInfo) {
		BidInfo = bidInfo;
	}

	/** 04.交易管理账户 ManageAccount <MgeAcct> [1..1] TransMangAcc 组件 */
	public TransMangAcc getMgeAcct() {
		return MgeAcct;
	}

	/** 04.交易管理账户 ManageAccount <MgeAcct> [1..1] TransMangAcc 组件 */
	public void setMgeAcct(TransMangAcc mgeAcct) {
		MgeAcct = mgeAcct;
	}

}
