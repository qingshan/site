package com.zdsoft.site.mina.model.customEnum;

import javax.xml.bind.annotation.XmlEnumValue;

/**
 * 客户状态（CustStatus）
 * 
 * @author cqyhm
 *
 */
public enum CustStatus {
	/*** 客户状态 "0", "正常" */
	@XmlEnumValue("0") A0("0", "正常"),

	/*** 客户状态 "1", "冻结" */
	@XmlEnumValue("1") A1("1", "冻结"),

	/*** 客户状态 "2", "挂失" */
	@XmlEnumValue("2") A2("2", "挂失"),

	/*** 客户状态 "3", "销户" */
	@XmlEnumValue("3") A3("3", "销户");

	private String value;
	private String name;

	private CustStatus(String value, String name) {
		this.value = value;
		this.name = name;
	}

	public String getValue() {
		return value;
	}

	public String getName() {
		return name;
	}

	@Override
	public String toString() {
		return value;
	}
}
