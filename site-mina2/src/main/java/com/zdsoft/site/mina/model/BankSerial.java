package com.zdsoft.site.mina.model;

import java.math.BigDecimal;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * 7.25  银行流水信息（BankSerial） 
 * @author cqyhm
 *
 */

@XmlAccessorType(XmlAccessType.FIELD)
public class BankSerial {
	
	/**银行流水号  BankSerial  [BkRef]  [1..1]  Reference  组件 */
	@XmlElement(name="BkRef")
	private Reference BkRef;
	
	/**银行账户  BankAccount  [BkAcct]  [1..1]  Account  组件*/
	@XmlElement(name="BkAcct")
	private Account BkAcct;
	
	/**成功转帐金额  SuccessTransferAmount  [SucTrfAmt]  [1..1]  Amount*/
	@XmlElement(name="SucTrfAmt")
	private BigDecimal SucTrfAmt;
	
	/**转账日期  TransferDate  [TrfDate]  [1..1]  Date*/
	@XmlElement(name="TrfDate")
	private String TrfDate;
	
	/**转账时间  TransferTime  [TrfTime]  [1..1]  Time*/
	@XmlElement(name="TrfTime")
	private String TrfTime;
	
	/**摘要  Digest  [Dgst]  [0..1]  Max128Text*/
	@XmlElement(name="Dgst")
	private String Dgst;
	
	/**银行流水号  BankSerial  [BkRef]  [1..1]  Reference  组件 */
	public Reference getBkRef() {
		return BkRef;
	}
	/**银行流水号  BankSerial  [BkRef]  [1..1]  Reference  组件 */
	public void setBkRef(Reference BkRef) {
		this.BkRef = BkRef;
	}
	/**银行账户  BankAccount  [BkAcct]  [1..1]  Account  组件*/
	public Account getBkAcct() {
		return BkAcct;
	}
	/**银行账户  BankAccount  [BkAcct]  [1..1]  Account  组件*/
	public void setBkAcct(Account BkAcct) {
		this.BkAcct = BkAcct;
	}
	/**成功转帐金额  SuccessTransferAmount  [SucTrfAmt]  [1..1]  Amount*/
	public BigDecimal getSucTrfAmt() {
		return SucTrfAmt;
	}
	/**成功转帐金额  SuccessTransferAmount  [SucTrfAmt]  [1..1]  Amount*/
	public void setSucTrfAmt(BigDecimal successTransferAmount) {
		SucTrfAmt = successTransferAmount;
	}
	/**转账日期  TransferDate  [TrfDate]  [1..1]  Date*/
	public String getTrfDate() {
		return TrfDate;
	}
	/**转账日期  TransferDate  [TrfDate]  [1..1]  Date*/
	public void setTrfDate(String transferDate) {
		TrfDate = transferDate;
	}
	/**转账时间  TransferTime  [TrfTime]  [1..1]  Time*/
	public String getTrfTime() {
		return TrfTime;
	}
	/**转账时间  TransferTime  [TrfTime]  [1..1]  Time*/
	public void setTrfTime(String transferTime) {
		TrfTime = transferTime;
	}
	/**摘要  Digest  [Dgst]  [0..1]  Max128Text*/
	public String getDgst() {
		return Dgst;
	}
	/**摘要  Digest  [Dgst]  [0..1]  Max128Text*/
	public void setDgst(String digest) {
		Dgst = digest;
	}
	
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("BankSerial [BkRef=").append(BkRef).append(", BkAcct=").append(BkAcct).append(", SucTrfAmt=").append(SucTrfAmt).append(", TrfDate=").append(TrfDate).append(", TrfTime=").append(TrfTime).append(", Dgst=").append(Dgst).append("]");
		return builder.toString();
	}
}
