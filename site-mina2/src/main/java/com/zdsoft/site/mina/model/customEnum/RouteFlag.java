package com.zdsoft.site.mina.model.customEnum;

import javax.xml.bind.annotation.XmlEnumValue;

/**
 * 汇款标识  0普通 1实时 2加急
 * @author cqyhm
 *
 */
public enum RouteFlag {
	
	/** 汇路 "0", "普通" */
	@XmlEnumValue("0") A0("0", "普通"),
	
	/** "1", "实时" */
	@XmlEnumValue("1") A1("1", "实时"),
	
	/** 汇路 "2", "加急" */
	@XmlEnumValue("2") A2("2", "加急");
	
	private String value;
	private String name;

	private RouteFlag(String value, String name) {
		this.value = value;
		this.name = name;
	}

	@Override
	public String toString() {
		return value;
	}

	public String getValue() {
		return value;
	}

	public String getName() {
		return name;
	}	
}
