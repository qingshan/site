package com.zdsoft.http;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.integration.support.MessageBuilder;
import org.springframework.integration.transformer.Transformer;
import org.springframework.messaging.Message;
/**
 * 消息转换器
 * @author 杨华明
 */
public class UserTransformer implements Transformer {
	
	private static Logger logger = LoggerFactory.getLogger(UserTransformer.class);
	@Override
	public Message<?> transform(Message<?> message) {
		logger.debug("消息转换:{}",message);
		return MessageBuilder.fromMessage(message).build();
	}

}
