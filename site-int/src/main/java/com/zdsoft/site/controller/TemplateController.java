package com.zdsoft.site.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.zdsoft.rock.common.controller.AbstractController;
import com.zdsoft.rock.common.model.domain.ResponseMsg;
import com.zdsoft.site.service.MemberService;

@Controller
@RequestMapping("/template")
public class TemplateController extends AbstractController {
	
	@Autowired
	private MemberService memberService;
	
	@RequestMapping("/member1.pdf")
	public ModelAndView  createpdf(){
		IText5PdfView view=new IText5PdfView();
		Map<String, Object> attributes=new HashMap<String, Object>();
		view.setAttributesMap(attributes);
		return new ModelAndView(view); 
	}
	
	@RequestMapping("/member2.pdf")
	public ModelAndView  createstamperpdf(){
		IText5PdfStamperView view=new IText5PdfStamperView();
		Map<String, Object> attributes=new HashMap<String, Object>();
		view.setAttributesMap(attributes);
		view.setUrl("/WEB-INF/template/template.pdf");//设置pdf模板文件路径
		return new ModelAndView(view); 
	}
	
	@RequestMapping("/test.html")
	public String index(){
		logger.debug("获取btl模板:{}",memberService.findById("1000"));
		return "/template/test";
	}
	
	@RequestMapping("/timeout.html")
	@ResponseBody
	public ResponseMsg<String> timeout(){
		logger.debug("ajax超时请求开始");
		try {
			Thread.sleep(120000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		logger.debug("ajax超时请求结束");
		return new ResponseMsg<String>("发送数据");
	}
	@RequestMapping("/testpost.json")
	@ResponseBody
	public ResponseMsg<String> test(String a1,String a2,String a3){
		logger.debug("testpost");
		logger.debug("a1={},a2={},a3={}",a1,a2,a3);
		return new ResponseMsg<String>("发送数据");
	}
}
