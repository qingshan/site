package com.zdsoft.site.event.observer;

import java.util.Observable;
import java.util.Observer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
/**
 * 观察者类 ExampleObserver
 * @author cqyhm
 *
 */
public class ExampleObserver implements Observer{
	
	private Logger logger=LoggerFactory.getLogger(getClass());
	/**
	 * 有被观察者发生变化，自动调用对应观察者的update方法
	 */
	@Override
	public void update(Observable o, Object arg) {
        ExampleObservable example = (ExampleObservable)o;//通过强制类型转换获取被观察者对象
        logger.debug("{}",arg);
        logger.debug("被观察者改变:{}",example.data);
	}

}
