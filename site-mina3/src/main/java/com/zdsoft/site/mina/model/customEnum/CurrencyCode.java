package com.zdsoft.site.mina.model.customEnum;
/**
 * 货币代码（CurrencyCode） 
 * @author cqyhm
 *
 */
public enum CurrencyCode {
	A0,//  人民币 
	A1,//  美元 
	A2 //  港元 
}
