package com.zdsoft.site.mina.model.customEnum;

import javax.xml.bind.annotation.XmlEnumValue;

/**
 * 7.18 赔付标志（RepayFlag）
 * 
 * @author cqyhm
 *
 */
public enum RepayFlag {

	/**赔付标志 "0", "风险准备金赔付"*/
	@XmlEnumValue("0") A0("0", "风险准备金赔付"),

	/**赔付标志 "1", "担保金赔付"*/
	@XmlEnumValue("1") A1("1", "担保金赔付");

	private String value;
	private String name;

	private RepayFlag(String value, String name) {
		this.value = value;
		this.name = name;
	}

	@Override
	public String toString() {
		return value;
	}

	public String getValue() {
		return value;
	}

	public String getName() {
		return name;
	}
}
