package com.zdsoft.site.mina.model;

import com.zdsoft.site.mina.model.customEnum.BalanceType;
/**
 * 7.9  余额(Balance) 
 * @author cqyhm
 *
 */
public class Balance {
//	余额类型  BalanceType  <Type>  [0..1]  BalanceType  枚举  可选 
	private BalanceType BalanceType;
//	余额  Balance  <Bal>  [1..1]  Amount  组件 必选
	private Balance Balance;
	
	public BalanceType getBalanceType() {
		return BalanceType;
	}
	public void setBalanceType(BalanceType balanceType) {
		BalanceType = balanceType;
	}
	public Balance getBalance() {
		return Balance;
	}
	public void setBalance(Balance balance) {
		Balance = balance;
	}
}
