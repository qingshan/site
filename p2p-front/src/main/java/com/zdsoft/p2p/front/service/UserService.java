package com.zdsoft.p2p.front.service;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.plugins.pagination.Pagination;
import com.zdsoft.p2p.front.domain.User;

/**
 *
 * User 表数据服务层接口
 *
 */
public interface UserService {
	/**
	 * <p>
	 * 插入一条记录
	 * </p>
	 * @param user
	 * 				实体对象
	 * @return boolean
	 */
	void save(User user );
	
	Page<User> findPager(Pagination page,User user);
}