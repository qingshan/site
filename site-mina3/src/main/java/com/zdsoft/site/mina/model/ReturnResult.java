package com.zdsoft.site.mina.model;
/**
 * 返回结果（ReturnResult） 
 * @author cqyhm
 */
public class ReturnResult {
	//返回码  Code  <Code>  [1..1]  ReturnCode  枚举  必选 
	private String code;
	//	返回信息  Info  <Info>  [0..1]  Max100Text  可选
	private String info;
	
	//返回码：指示本次交易是否成功。
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getInfo() {
		return info;
	}
	public void setInfo(String info) {
		this.info = info;
	} 
}
