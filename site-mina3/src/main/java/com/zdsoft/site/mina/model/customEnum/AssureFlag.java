package com.zdsoft.site.mina.model.customEnum;
/**
 * 担保标志（AssureFlag） 
 * @author cqyhm
 *
 */
public enum AssureFlag {
	A0(0),//  无担保 
	A1(1);// 有担保
	
	private Integer value;
	
	private AssureFlag(Integer value){
		this.value=value;
	}
	@Override
	public String toString(){
		return String.valueOf(value);
	}
	
}
