<%@ page language="java" contentType="text/html; charset=UTF-8"   pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>欢迎光临农产品发票综合系统</title>
	<link  href="resources/theme/default/resources/css/ext-all.css" type="text/css" rel="stylesheet"/>
	<script src="resources/extjs/adapter/ext/ext-base.js"  type="text/javascript" ></script>
	<script src="resources/extjs/ext-all.js" type="text/javascript" ></script>
	<script src="resources/extjs/ext-lang-zh_CN.js" type="text/javascript"></script>	
    
    <script type="text/javascript" src="resources/work/south.js"></script>
    <script type="text/javascript" src="resources/work/north.js"></script>
    <script type="text/javascript" src="resources/work/index.js"></script>
    <script type="text/javascript" src="resources/work/west.js"></script>
 
	<style type="text/css">
	html, body {
        font-family: Verdana, Arial, Helvetica, sans-serif;
        font-size: 11pt;
        margin:0;
        padding:0;
        border:0 none;
        overflow:hidden;
        height:100%;
    }
	p {
	    margin:5px;
	}
    .settings {
        background-image:url(resources/work/images/folder_wrench.png);
    }
    .nav {
        background-image:url(resources/work/images/folder_go.png);
    }
    .tabs {
    	background-image:url(resources/work/images/gears.gif ) !important;
    }
    .file-upload {
		border-color : #4CA0FF;
		background-color : #EAE8FF;
		border-style : solid;
		border-width : 1px;
		height:18px;
	}
	.div-search-company{
		padding: 5px 5px 5px 5px;
		/*height:100px;*/
		/*border: solid red 1px;*/
	}
	.folder .x-tree-node-icon {
		background:transparent url(resources/extjs/resources/images/default/tree/folder.gif);
	}
	.x-tree-node-expanded .x-tree-node-icon {
		background:transparent url(resources/extjs/resources/images/default/tree/folder-open.gif);
	}
	.search {
		background-image:url(images/rss_go.png) !important;
	}
    </style>
	<script type="text/javascript">
	</script>
</head>
<body>
  <div id="north"></div>
  <div>
	  <br><br>
	  <div id="center1" style="padding-top:20px"></div>
  </div>
  <div id="props-panel" style="width:200px;height:200px;overflow:hidden;"></div>
  <div id="south"></div>
 </body>
</html>
