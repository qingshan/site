/**
  tab上的上下文菜单
 */
Ext.define('Ext.ux.TabCloseMenu', {
    alias: 'plugin.tabclosemenu',

    mixins: {
        observable: 'Ext.util.Observable'
    },
    showRefreshTab:true,//是否显示刷新
    refreshTabText:'刷新标签',
	
	showCloseTab:true,//是否显示关闭
    closeTabText: '关闭当前页签',//Close Tab
	
    showCloseOthers: true,
    closeOthersTabsText: '关闭其它页签',//Close Other Tabs
    
    showCloseAll: true,
    closeAllTabsText: '关闭所有页签',//Close All Tabs
    
    extraItemsHead: null,
    extraItemsTail: null,
    constructor: function (config) {
        this.addEvents('aftermenu','beforemenu');
        this.mixins.observable.constructor.call(this, config);
    },

    init : function(tabpanel){
        this.tabPanel = tabpanel;
        this.tabBar = tabpanel.down("tabbar");
        
        this.mon(this.tabPanel, {
            scope: this,
            afterlayout: this.onAfterLayout,
            single: true
        });
    },

    onAfterLayout: function() {
        this.mon(this.tabBar.el, {
            scope: this,
            contextmenu: this.onContextMenu,
            delegate: '.x-tab'
        });
    },

    onBeforeDestroy : function(){
        Ext.destroy(this.menu);
        this.callParent(arguments);
    },

    // private
    onContextMenu : function(event, target){
        var me = this,
            menu = me.createMenu(),
            disableAll = true,
            disableOthers = true,
            tab = me.tabBar.getChildByElement(target),
            index = me.tabBar.items.indexOf(tab);

        me.item = me.tabPanel.getComponent(index);
        me.ctab=me.item;//当前选中的tab
        menu.child('*[text="' + me.closeTabText + '"]').setDisabled(!me.item.closable);

        if (me.showCloseAll || me.showCloseOthers) {
            me.tabPanel.items.each(function(item) {
                if (item.closable) {
                    disableAll = false;
                    if (item != me.item) {
                        disableOthers = false;
                        return false;
                    }
                }
                return true;
            });

            if (me.showCloseAll) {
                menu.child('*[text="' + me.closeAllTabsText + '"]').setDisabled(disableAll);
            }

            if (me.showCloseOthers) {
                menu.child('*[text="' + me.closeOthersTabsText + '"]').setDisabled(disableOthers);
            }
        }
        event.preventDefault();
        me.fireEvent('beforemenu', menu, me.item, me);
        menu.showAt(event.getXY());
    },

    createMenu : function() {
        var me = this;
        if (!me.menu) {
        	//刷新页面
            var items = [{
            	text: me.refreshTabText,
                scope: me,
                handler: me.onRefresh
            }];
            //菜单分隔符
            if(me.showCloseTab||me.showCloseAll || me.showCloseOthers){
            	items.push('-');
            }
            //关闭
            if(me.showCloseTab){
				items.push({
	                text: me.closeTabText,
	                scope: me,
	                handler: me.onClose                   
	            });
            }
			//关闭别的
            if (me.showCloseOthers) {
                items.push({
                    text: me.closeOthersTabsText,
                    scope: me,
                    handler: me.onCloseOthers
                });
            }

            if (me.showCloseAll) {
                items.push({
                    text: me.closeAllTabsText,
                    scope: me,
                    handler: me.onCloseAll
                });
            }

            if (me.extraItemsHead) {
                items = me.extraItemsHead.concat(items);
            }

            if (me.extraItemsTail) {
                items = items.concat(me.extraItemsTail);
            }
			
            me.menu = Ext.create('Ext.menu.Menu', {
                items: items,
                listeners: {
                    hide: me.onHideMenu,
                    scope: me
                }
            });
        }

        return me.menu;
    },

    onHideMenu: function () {
        var me = this;

        me.item = null;
        me.fireEvent('aftermenu', me.menu, me);
    },

    onClose : function(){
        this.tabPanel.remove(this.ctab);
    },
    onCloseOthers : function(){
        this.doClose(true);
    },
    onCloseAll : function(){
        this.doClose(false);
    },
	onRefresh:function(){
		if(this.ctab){ 
			this.ctab.update();
			//this.ctab.getUpdater().refresh();
		}
		console.error('点击刷新页面实现由问题:无法刷新');
	},
    doClose : function(excludeActive){
        var items = [];

        this.tabPanel.items.each(function(item){
            if(item.closable){
                if(!excludeActive || item != this.item){
                    items.push(item);
                }
            }
        }, this);

        Ext.each(items, function(item){
            this.tabPanel.remove(item);
        }, this);
    }
});
