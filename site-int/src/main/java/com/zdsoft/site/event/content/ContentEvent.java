package com.zdsoft.site.event.content;

import org.springframework.context.ApplicationEvent;

/**
 * 内容事件
 */
public class ContentEvent extends ApplicationEvent {
	private static final long serialVersionUID = -2422772707993195740L;

	public ContentEvent(final String content) {
        super(content);
    }
}
