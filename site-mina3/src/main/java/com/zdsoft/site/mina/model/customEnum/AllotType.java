package com.zdsoft.site.mina.model.customEnum;

/**
 * 调拨类型（AllotType） 定义 是否标志
 * 
 * @author cqyhm
 *
 */
public enum AllotType {
	A00, // 普通入金
	A01, // 普通出金
	AZ0, // 追加保证
	A10, // 收益入金
	A11, // 收益出金
	A20, // 利息入金
	A21, // 利息出金
	A30, // 头寸入金
	A31, // 头寸出金
	A41, // 分账户调拨
	A61, // 佣金出金
	A51, // 垫付划回
	A70, // 费用收取
	A71, // 费用支付
	A81, // 风险准备金调拨
	AD0, // 其他出金
	A90, // 其他入金
	A91, // 其他出金
	A60, // 20120113-01-佣金入金
	A13, // 备付金客户退款
	A14, // 20120113-01-手续费
	AA0, /// 20120113-01-融资入金
	AA1, // 20120113-01-融资出金
	AB0, // 20120113-01-批准划转款入金
	AB1, // 20130412-01-批准划转款出金
	AC0, // 头寸退回
	AF0, // 佣金划出退回
	AG0, // 利息划出退回
	AH0 // 收益划出退回 默认
}
