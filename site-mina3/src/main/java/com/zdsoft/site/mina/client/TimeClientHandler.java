package com.zdsoft.site.mina.client;

import org.apache.mina.core.service.IoHandlerAdapter;
import org.apache.mina.core.session.IdleStatus;
import org.apache.mina.core.session.IoSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TimeClientHandler extends IoHandlerAdapter {
	private Logger logger=LoggerFactory.getLogger(getClass());
	
    public void messageReceived(IoSession session, Object message) throws Exception {
        String content = message.toString();
        logger.debug("client receive a message is : {}",content);
    }

    public void messageSent(IoSession session, Object message) throws Exception {
    	logger.debug("messageSent -> ：{}",message);
    }
    @Override
	public void sessionCreated(IoSession session) throws Exception {
    	logger.debug("sessionCreated client");
	}

	@Override
	public void sessionOpened(IoSession session) throws Exception {
		logger.debug("sessionOpened client");
	}

	@Override
	public void sessionClosed(IoSession session) throws Exception {
		logger.debug("sessionClosed client");
	}

	@Override
	public void sessionIdle(IoSession session, IdleStatus status) throws Exception {
		logger.debug("sessionIdle client");
	}

	@Override
	public void exceptionCaught(IoSession session, Throwable cause) throws Exception {
		logger.debug("exceptionCaught client");
	}

	@Override
	public void inputClosed(IoSession session) throws Exception {
		super.inputClosed(session);
		logger.debug("inputClosed client");
	}
}