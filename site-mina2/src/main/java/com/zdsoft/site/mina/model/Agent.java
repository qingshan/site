package com.zdsoft.site.mina.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

import com.zdsoft.site.mina.model.customEnum.CertificationType;

/**
 * 代理人信息（Agent） 
 * @author cqyhm
 *
 */

@XmlAccessorType(XmlAccessType.FIELD)
public class Agent {
	/**代理人姓名  Name  [Name]  [0..1]  Max60Text   可选  */
	@XmlElement(name="Name")
	private String Name;
	
	/**证件类型  CertificationType  [CertType]  [0..1]  CertificationType  枚举  可选 */
	@XmlElement(name="CertType")
	private CertificationType CertType;
	
	/**证件号码  CertificationIdentifier  [CertId]  [0..1]  Max30Text   可选  */
	@XmlElement(name="CertId")
	private String CertId;

	/**代理人姓名  Name  [Name]  [0..1]  Max60Text   可选  */
	public String getName() {
		return Name;
	}
	/**代理人姓名  Name  [Name]  [0..1]  Max60Text   可选  */
	public void setName(String name) {
		Name = name;
	}
	/**证件类型  CertificationType  [CertType]  [0..1]  CertificationType  枚举  可选 */
	public CertificationType getCertType() {
		return CertType;
	}
	/**证件类型  CertificationType  [CertType]  [0..1]  CertificationType  枚举  可选 */
	public void setCertType(CertificationType certificationType) {
		CertType = certificationType;
	}
	/**证件号码  CertificationIdentifier  [CertId]  [0..1]  Max30Text   可选  */
	public String getCertId() {
		return CertId;
	}
	/**证件号码  CertificationIdentifier  [CertId]  [0..1]  Max30Text   可选  */
	public void setCertId(String certificationIdentifier) {
		CertId = certificationIdentifier;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Agent [Name=").append(Name).append(", CertType=").append(CertType).append(", CertId=").append(CertId).append("]");
		return builder.toString();
	}
	
}
