package com.zdsoft.site.thread.gReadWriteLock.A4;

import com.zdsoft.site.thread.gReadWriteLock.A4.readwritelock.Data;

public class Main {
    public static void main(String[] args) {
        Data data = new Data();
        new ReaderThread(data).start();
        new ReaderThread(data).start();
        new ReaderThread(data).start();
        new ReaderThread(data).start();
        new ReaderThread(data).start();
        new ReaderThread(data).start();
        new WriterThread(data, "ABCDEFGHIJKLMNOPQRSTUVWXYZ").start();
        new WriterThread(data, "abcdefghijklmnopqrstuvwxyz").start();
    }
}
