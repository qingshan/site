package com.nutz.cms.service;

import org.nutz.ioc.loader.annotation.IocBean;
import org.nutz.service.IdEntityService;

import com.nutz.cms.model.User;


@IocBean(args ={"refer:dao"})
public class UserService extends IdEntityService<User>{
	
    public User findById(Long id){
    	return fetch(id);
    }
    
    public int count(){
    	return super.count();
    }
}
