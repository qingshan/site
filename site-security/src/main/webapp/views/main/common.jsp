<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
	<h1>Common Page</h1>
	<p>每个人都能访问的页面.</p>
	<a href="${pageContext.request.contextPath}/main/admin.html">进入管理</a>
	<br />
	<a href="${pageContext.request.contextPath}/auth/login.html">退出登录</a>
</body>
</html>
