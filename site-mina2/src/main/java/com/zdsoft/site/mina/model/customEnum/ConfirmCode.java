package com.zdsoft.site.mina.model.customEnum;

import javax.xml.bind.annotation.XmlEnumValue;

/**
 * 25 确认码（ConfirmCode） 定义 确认码 已生效0000 已作废 0001 其他错误代码 0002-9999
 * 
 * @author cqyhm
 *
 */
public enum ConfirmCode {
	/** 确认码:"0000","已生效" */
	@XmlEnumValue("0000") A0000("0000", "已生效"),

	/** 确认码:"0001","已作废" */
	@XmlEnumValue("0001") A0001("0001", "已作废"),

	/** 确认码:"0002","其他错误" */
	@XmlEnumValue("0002") A0002("0002", "其他错误");

	private String value;
	private String name;

	private ConfirmCode(String value, String name) {
		this.value = value;
		this.name = name;
	}

	@Override
	public String toString() {
		return value;
	}

	public String getValue() {
		return value;
	}

	public String getName() {
		return name;
	}
}
