<%@ page language="java" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="zh">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/jquery.js"></script>
<script type="text/javascript">
	function broadcast(){
		$.ajax({
			url:'${pageContext.request.contextPath}/msg/broadcast.do',
			type:"post",
			data:{text:$("#msg").val()},
			dataType:"json",
			success:function(data){
				alert("发送成功");
			}
		});
	}
</script>
</head>
<body>
	发送广播
	<textarea style="width:100%;height:300px;" id="msg" ></textarea>
	<input type="button" value="发送" onclick="broadcast()">
</body>
</html>
