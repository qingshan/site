package com.zdsoft.site.thread.aIntroduction.A4;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Bank {
	private Logger logger=LoggerFactory.getLogger(getClass());
	private int money; // 存款数量
	private String name;// 存款账户名称

	public Bank(String name, int money) {
		this.name = name;
		this.money = money;
	}

	/** 往账户上存款 */
	public void deposit(int m) {
		money += m;
	}

	/** 账户扣款,并检查账户余额是否小于0 */
	public boolean withdraw(int m) {
		if (money >= m) {
			money -= m;
			check();
			return true;
		} else {
			return false;
		}
	}

	public String getName() {
		return name;
	}

	/** 检查账户余额小于0的情况 */
	private void check() {
		if (money < 0) {
			logger.debug("可能余额为负数 ! money = {}",money);
		}
	}
}
