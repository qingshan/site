package com.zdsoft.site.mina.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

import com.zdsoft.site.mina.model.customEnum.AccountType;

/**
 * 7.11 交易管理余额（ TransMangAccBal ）
 * 
 * @author cqyhm
 *
 */

@XmlAccessorType(XmlAccessType.FIELD)
public class TransMangAccBal {

	/** 账户名称 AccountName [Name] [0..1] Max60Text */
	@XmlElement(name = "Name")
	private String Name;

	/** 账户类别 AccountType [Type] [1..1] AccountType 枚举 */
	@XmlElement(name = "Type")
	private AccountType Type;

	/** 余额 Balance [Amt] [1..1] Balance */
	@XmlElement(name = "Amt")
	private Balance Amt;

	/** 账户名称 AccountName [Name] [0..1] Max60Text */
	public String getName() {
		return Name;
	}

	/** 账户名称 AccountName [Name] [0..1] Max60Text */
	public void setName(String accountName) {
		Name = accountName;
	}

	/** 账户类别 AccountType [Type] [1..1] AccountType 枚举 */
	public AccountType getType() {
		return Type;
	}

	/** 账户类别 AccountType [Type] [1..1] AccountType 枚举 */
	public void setType(AccountType accountType) {
		Type = accountType;
	}

	/** 余额 Balance [Amt] [1..1] Balance */
	public Balance getAmt() {
		return Amt;
	}

	/** 余额 Balance [Amt] [1..1] Balance */
	public void setAmt(Balance balance) {
		Amt = balance;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("TransMangAccBal [Name=").append(Name).append(", Type=").append(Type).append(", Amt=").append(Amt).append("]");
		return builder.toString();
	}
}
