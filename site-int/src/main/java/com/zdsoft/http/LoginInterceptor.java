package com.zdsoft.http;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.integration.support.MessageBuilder;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.support.ChannelInterceptor;
/**
 * 通道拦截器
 * @author 杨华明
 *
 */
public class LoginInterceptor implements ChannelInterceptor {
	
	private Logger logger=LoggerFactory.getLogger(getClass());
	
	@Override
	public boolean preReceive(MessageChannel arg0) {
		logger.debug("preReceive");
		return true;
	}
	
	@Override
	public Message<?> postReceive(Message<?> arg0, MessageChannel arg1) {
		logger.debug("postReceive");
		return arg0;
	}
	
	@Override
	public void afterReceiveCompletion(Message<?> arg0, MessageChannel arg1, Exception arg2) {
		logger.debug("afterReceiveCompletion");
	}

	@Override
	public Message<?> preSend(Message<?> arg0, MessageChannel arg1) {
		logger.debug("preSend:{}",arg0);
		return MessageBuilder.fromMessage(arg0).setHeaderIfAbsent("token", "8a2a8c2d4c360cf8014c360d154d0000").build();
	}
	
	@Override
	public void postSend(Message<?> arg0, MessageChannel arg1, boolean arg2) {
		logger.debug("postSend:{}",arg0);
	}
	
	@Override
	public void afterSendCompletion(Message<?> arg0, MessageChannel arg1, boolean arg2, Exception arg3) {
		logger.debug("afterSendCompletion:{},{}",arg0,arg1);
	}
}
