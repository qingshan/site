package com.zdsoft.site.event.observer;

import java.util.Observable;

/**
 * 简单的【被观察者类】 ExampleObservable
 * 相当于红绿灯
 * @author cqyhm
 *
 */
public class ExampleObservable extends Observable {
	int data=0;
	//数据改变==>相当于红绿灯发生改变
	public void setData(int data){
        this.data = data;
        this.setChanged();	     //标记此 Observable对象为已改变的对象 ,this.hasChanged() 返回true
        //this.notifyObservers();//通知所有的观察者
        this.notifyObservers("我改变了哟");
    }
}
