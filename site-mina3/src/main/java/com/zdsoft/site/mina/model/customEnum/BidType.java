package com.zdsoft.site.mina.model.customEnum;
/**
 * 标的类型（BidType） 
 * @author cqyhm
 *
 */
public enum BidType {
	A0(0),// 借款标的 
	A1(1);// 理财标的
	private Integer value;
	
	private BidType(Integer value){
		this.value=value;
	}
	@Override
	public String toString(){
		return String.valueOf(value);
	}
}
