package com.zdsoft.site.event.registersucess;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import com.zdsoft.p2p.user.domain.User;
import com.zdsoft.rock.common.AppException;

/**
 * 注册成功,赠送积分
 */
@Component
public class PointRegisterListener implements ApplicationListener<RegisterSucessEvent> {
	
	private Logger logger=LoggerFactory.getLogger(getClass());
    @Async
    @Override
    public void onApplicationEvent(final RegisterSucessEvent event) {
    	logger.debug("注册成功,赠送积分给:[{}]",((User)event.getSource()).getUserNm());
    	throw new AppException();
    }
}
