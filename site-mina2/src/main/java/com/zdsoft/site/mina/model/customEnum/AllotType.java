package com.zdsoft.site.mina.model.customEnum;

import javax.xml.bind.annotation.XmlEnumValue;

/**
 * 调拨类型（AllotType） 定义 是否标志
 * 
 * @author cqyhm
 *
 */
public enum AllotType {
	/** 调拨类型 "00","普通入金" */
	@XmlEnumValue("00") A00("00", "普通入金"),

	/** 调拨类型 "01","普通出金" */
	@XmlEnumValue("01") A01("01", "普通出金"),

	/** 调拨类型 "Z0","追加保证" */
	@XmlEnumValue("Z0") AZ0("Z0", "追加保证"),

	/** 调拨类型 "10","收益入金" */
	@XmlEnumValue("10") A10("10", "收益入金"),

	/** 调拨类型 "11","收益出金" */
	@XmlEnumValue("11") A11("11", "收益出金"),

	/** 调拨类型 "20","利息入金" */
	@XmlEnumValue("20") A20("20", "利息入金"),

	/** 调拨类型 "21","利息出金" */
	@XmlEnumValue("21") A21("21", "利息出金"),

	/** 调拨类型 "30","头寸入金" */
	@XmlEnumValue("30") A30("30", "头寸入金"),

	/** 调拨类型 "31","头寸出金" */
	@XmlEnumValue("31") A31("31", "头寸出金"),

	/** 调拨类型 "41","分账户调拨" */
	@XmlEnumValue("41") A41("41", "分账户调拨"),

	/** 调拨类型 "61","佣金出金" */
	@XmlEnumValue("61") A61("61", "佣金出金"),

	/** 调拨类型 "51","垫付划回" */
	@XmlEnumValue("51") A51("51", "垫付划回"),

	/** 调拨类型 "70","费用收取" */
	@XmlEnumValue("70") A70("70", "费用收取"),

	/** 调拨类型 "71","费用支付" */
	@XmlEnumValue("71") A71("71", "费用支付"),

	/** 调拨类型 "81","风险准备金调拨" */
	@XmlEnumValue("81") A81("81", "风险准备金调拨"),

	/** 调拨类型 "D0","其他出金" */
	@XmlEnumValue("D0") AD0("D0", "其他出金"),

	/** 调拨类型 "90","其他入金" */
	@XmlEnumValue("90") A90("90", "其他入金"),

	/** 调拨类型 "91","其他出金" */
	@XmlEnumValue("91") A91("91", "其他出金"),

	/** 调拨类型 "60","20120113-01-佣金入金" */
	@XmlEnumValue("60") A60("60", "佣金入金"),

	/** 调拨类型 "13","备付金客户退款" */
	@XmlEnumValue("13") A13("13", "备付金客户退款"),

	/** 调拨类型 "14","20120113-01-手续费" */
	@XmlEnumValue("14") A14("14", "手续费"),

	/** 调拨类型 "A0"," 20120113-01-融资入金" */
	@XmlEnumValue("A0") AA0("A0", "融资入金"),

	/** 调拨类型 "A1","20120113-01-融资出金" */
	@XmlEnumValue("A1") AA1("A1", "融资出金"),

	/** 调拨类型 "B0","20120113-01-批准划转款入金" */
	@XmlEnumValue("B0") AB0("B0", "批准划转款入金"),

	/** 调拨类型 "B1","20130412-01-批准划转款出金" */
	@XmlEnumValue("B1") AB1("B1", "批准划转款出金"),

	/** 调拨类型 "C0","头寸退回" */
	@XmlEnumValue("C0") AC0("C0", "头寸退回"),

	/** 调拨类型 "F0","佣金划出退回" */
	@XmlEnumValue("F0") AF0("F0", "佣金划出退回"),

	/** 调拨类型 "G0","利息划出退回" */
	@XmlEnumValue("G0") AG0("G0", "利息划出退回"),

	/** 调拨类型 "H0","收益划出退回 默认" */
	@XmlEnumValue("H0") AH0("H0", "收益划出退回默认");

	private String value;
	private String name;

	private AllotType(String value, String name) {
		this.value = value;
		this.name = name;
	}

	@Override
	public String toString() {
		return value;
	}

	public String getValue() {
		return value;
	}

	public String getName() {
		return name;
	}
}
