<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>
<html>
  <head>
  	<c:set var="ctx" value="${pageContext.request.contextPath}"/>
    <base href="${ctx}">
	<link  href="${ctx}/resources/theme/default/resources/css/ext-all.css" type="text/css" rel="stylesheet"/>
	<script src="${ctx}/resources/extjs/adapter/ext/ext-base.js"  type="text/javascript" ></script>
	<script src="${ctx}/resources/extjs/ext-all.js" type="text/javascript" ></script>
	<script src="${ctx}/resources/extjs/ext-lang-zh_CN.js" type="text/javascript"></script>
	
  </head>
  
  <body>
    <script type="text/javascript"> 
    	var WebApp = {};
    	WebApp.Desktop = function(){
    		this.Banner = new Ext.Panel({
    			region : "north",
    			margins: '0 0 2 0',    
    			contentEl : "header",			
    			height : 62,
    			bbar : this.createTopMenu()
    		});
    		
    		this.WestMenu = new Ext.Panel({
    			region : "west",
    			margins: '0 5 0 0',
    			layout:'accordion',
    			width : 200,
    			items : [	
    				{
    					title : "客户管理"
    				},{
    					title : "报表管理"
    				},{
    					title : "退货管理"
    				}
    			]
    		});
    		
    		this.MainPanel = new Ext.TabPanel({
    			region : "center",
			    activeTab: 0,
			    items: [{
			        title : '信息区',
			        frame : true
			    }]
			});			
			
			WebApp.Desktop.superclass.constructor.call(this, {
				layout : "border",
				items : [this.Banner, this.WestMenu, this.MainPanel]
			});    		
    	}
    	Ext.extend(WebApp.Desktop, Ext.Viewport, {
    		createTopMenu : function(){
    			return ['->',{
    				xtype : "tbitem",
    				autoEl : {
    					tag: 'img',
        				src: 'common/css/images/user_green.gif'    					
    				}
    			},{
    				xtype : "tbtext",
    				text : "用户名：czp"
    			},'-',{
    				xtype : "tbitem",
    				autoEl : {
    					tag: 'img',
        				src: 'common/css/images/user_green.gif'    					
    				}
    			},{
    				xtype : "tbtext",
    				text : "部门：研发部"
    			}]
    		}
    	});
		Ext.onReady(function(){
			new WebApp.Desktop();
		});
	</script>
	
	 <div id="header">
	 <div style="float:right;margin-right:10px;color:white;font:normal 16px tahoma, arial, sans-serif;">http://czpae86.javaeye.com</div>
	<div class="api-title">WebApp</div>
  </div>
  </body>
</html>
