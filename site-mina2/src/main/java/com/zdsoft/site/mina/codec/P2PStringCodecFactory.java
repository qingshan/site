package com.zdsoft.site.mina.codec;

import java.nio.charset.Charset;

import org.apache.mina.core.session.IoSession;
import org.apache.mina.filter.codec.ProtocolCodecFactory;
import org.apache.mina.filter.codec.ProtocolDecoder;
import org.apache.mina.filter.codec.ProtocolEncoder;
/**
 * 恒生电子包编码和解码器
 * @author cqyhm
 *
 */
public class P2PStringCodecFactory implements ProtocolCodecFactory{
	/**发送时编码器*/
    private final P2PStringEncoder encoder;
    /**接收时解码器*/
    private final P2PStringDecoder decoder;

    public P2PStringCodecFactory(Charset charset) {
        encoder = new P2PStringEncoder(charset);
        decoder = new P2PStringDecoder(charset);
    }
    
    public P2PStringCodecFactory() {
        this(Charset.defaultCharset());
    }
    /**
     * 获取编码器
     */
    public ProtocolEncoder getEncoder(IoSession session) throws Exception {
        return encoder;
    }
    /**
     * 获取解码器
     */
    public ProtocolDecoder getDecoder(IoSession session) throws Exception {
        return decoder;
    }
}
