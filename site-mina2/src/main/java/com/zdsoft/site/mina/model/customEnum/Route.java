package com.zdsoft.site.mina.model.customEnum;

import javax.xml.bind.annotation.XmlEnumValue;

/**
 * 汇路 0本行 1跨行同城 2跨行二代支付 3 跨行大小额
 * 
 * @author cqyhm
 *
 */
public enum Route {
	/** 汇路 "0","本行" */
	@XmlEnumValue("0") A0("0", "本行"),
	
	/** 汇路 "1","跨行同城" */
	@XmlEnumValue("1") A1("1", "跨行同城"),
	
	/** 汇路 "2","跨行二代支付" */
	@XmlEnumValue("2") A2("2", "跨行二代支付"),
	
	/** 汇路 "3"," 跨行大小额" */
	@XmlEnumValue("3") A3("3", " 跨行大小额");
	
	private String value;
	private String name;

	private Route(String value, String name) {
		this.value = value;
		this.name = name;
	}

	@Override
	public String toString() {
		return value;
	}

	public String getValue() {
		return value;
	}

	public String getName() {
		return name;
	}

}
