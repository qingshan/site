package com.zdsoft.site.event.registersucess;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.zdsoft.site.service.MemberService;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations={"classpath:applicationContext.xml"})
public class RegisterSucessEventTest {
	private Logger logger=LoggerFactory.getLogger(getClass());
    @Autowired
    private MemberService memberService;

    @Test
    public void testRegister() throws InterruptedException {
    	memberService.register("cqyhm", "123456");
        logger.debug("注册成功");
        Thread.sleep(10000L);
    }

    @Test
    public void test2(){
    	Integer a1=150;
    	Integer b1=150;
    	Assert.assertTrue(a1==b1);
    }
    
    @Test
    public void test3(){
    	Integer a1=100;
    	Integer b1=100;
    	Assert.assertTrue(a1==b1);
    }

}
