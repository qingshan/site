package com.zdsoft.site.mina.model.customEnum;

/**
 * 8.1.6  机构类型(InstitutionType) 
 * 0--服务商
 * 1--银行 
 * @author cqyhm
 *
 */
public enum InstitutionType {
	/**0--服务商*/
	A0("0","服务商"),
	/**1--银行*/
	A1("1","银行");
	
	private String value;
	private String name;
	private InstitutionType(String value,String name){
		this.value=value;
		this.name=name;
	}
	
	public String getValue() {
		return value;
	}
	public String getName() {
		return name;
	}
	@Override
	public String toString(){
		return value;
	}
}
