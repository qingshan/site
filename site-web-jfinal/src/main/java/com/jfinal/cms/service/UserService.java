package com.jfinal.cms.service;

import com.jfinal.cms.model.User;

public class UserService{
	
	public User findById(String id){
		return User.me.findById(id);
	}
}
