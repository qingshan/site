package com.zdsoft.site.mina.model.customEnum;
/**
 * 报文标志，默认为正常
 * @author cqyhm
 *
 */
public enum PackageFlag {
	N,//正常 
	Q //查询 
}
