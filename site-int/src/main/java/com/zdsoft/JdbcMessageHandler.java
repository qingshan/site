package com.zdsoft;

import java.util.List;

import org.apache.commons.lang.builder.ReflectionToStringBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageHandler;
import org.springframework.messaging.MessagingException;

public class JdbcMessageHandler implements MessageHandler {

	private static Logger logger = LoggerFactory.getLogger(JdbcMessageHandler.class);

	public JdbcMessageHandler() {
	}

	@Override
	public void handleMessage(Message<?> message) throws MessagingException {
		Object obj = message.getPayload();
		// 分别按照各种样式输出obj
		logger.debug("messageHeader:{}",message.getHeaders());
		if (obj == null) {
			logger.info("输出消息:null");
		} else if (obj instanceof String) {
			logger.info("输出消息:{}",obj);
		} else if (obj instanceof List) {
			@SuppressWarnings("rawtypes")
			List bean = (List) obj;
			logger.info("输出消息:{}",bean);
		} else {
			logger.info("输出消息:{}",ReflectionToStringBuilder.reflectionToString(message));
		}
	}

}
