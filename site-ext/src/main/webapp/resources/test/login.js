Ext.onReady(function() {
	//Ext.BLANK_IMAGE_URL = '../Scripts/ext/resources/images/default/s.gif';
	Ext.QuickTips.init();
	Ext.form.Field.prototype.msgTarget = 'side';
	
	var logoPanel = new Ext.Panel({
		id : 'login-logo',
		baseCls : 'x-plain',
		region : 'center'
	});
	var loginForm = new Ext.form.FormPanel({
		region : 'south',
		border : false,
		bodyStyle :'padding: 20px',
		baseCls : 'x-plain',
		waitMsgTarget : true,
		labelWidth : 30,
		defaults : {
			width : 280
		},
		height : 90,
		items : [{
			xtype : 'textfield',
			fieldLabel : '账号',
			name:'userCd',
			blankText : '登录名不能为空',
			validateOnBlur : false,
			allowBlank : false
		}, {
			xtype : 'textfield',
			fieldLabel : '密码',
			name:'passwd',
			blankText : '密码不能为空',
			inputType : 'password',
			cls : 'mima',
			validateOnBlur : false,
			allowBlank : false
		}]
	});
	//var sb = new Ext.ux.StatusBar({});
	var win = new Ext.Window({
		title : '登陆窗口',
		iconCls : 'locked',
		width : 429,
		height : 280,
		resizable : false,
		draggable : true,
		modal : false,
		closable : false,
		layout : 'border',
		bodyStyle : 'padding:5px;',
		plain : false,
		items : [logoPanel, loginForm],
		buttonAlign : 'center',
		buttons : [{
			text : '登录',
			cls : "x-btn-text-icon",
			icon : "../resources/extjs421/resources/lock_open.png",
			height : 30,
			handler : function() {
				window.location.href = '/index.jsp';
			}
		}, {
			text : '重置',
			cls : "x-btn-text-icon",
			icon : "../resources/extjs421/resources/arrow_redo.png",
			height : 30,
			handler : function() {
				loginForm.form.reset();
			}

		}]
		//,bbar : sb
	});
//	if (Ext.isChrome) {
//		sb.addButton({
//			text : 'ActiveX相关用户注意切换IE模式',
//			cls : "x-btn-text-icon",
//			icon : "/Content/ie.png",
//			handler : function() {
//				var googleWin = new Ext.Window({
//					iconCls : 'ie',
//					title : 'Google浏览器IE Tab插件安装',
//					width : 300,
//					height : 100,
//					closable : true,
//					html : "按照提示在Google浏览器中安装IETab<br>并在IE模式中运行与ActiveX操作相关的程序<iframe src='http://www.chromeextensions.org/wp-content/uploads/2009/12/ietab1.0.11208.1.crx' style='width:0%; height:0%;'></iframe>"
//				});
//				googleWin.show();
//			}
//		});
//	} else {
//		sb.addButton({
//			text : '建议使用Google浏览器运行本系统',
//			cls : "x-btn-text-icon",
//			icon : "/Content/google-chrome.png",
//			handler : function() {
//				var googleWin = new Ext.Window({
//					iconCls : 'google',
//					title : 'Google浏览器安装',
//					width : 850,
//					height : 450,
//					closable : true,
//					html : "<iframe src='http://www.google.com/chrome/eula.html?extra=devchannel' style='width:100%; height:100%;'></iframe>"
//				});
//				googleWin.show();
//			}
//		})
//	}
	win.show();
});