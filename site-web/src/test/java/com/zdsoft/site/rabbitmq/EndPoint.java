package com.zdsoft.site.rabbitmq;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

public abstract class EndPoint {
	
	protected Channel channel;
    protected Connection connection;
    protected String endPointName;
    protected Logger logger=LoggerFactory.getLogger(getClass());
    
	public EndPoint(String endPointName) throws IOException, TimeoutException {
		this.endPointName = endPointName;
		 //Create a connection factory
        ConnectionFactory factory = new ConnectionFactory();
        //hostname of your rabbitmq server
        factory.setHost("localhost");
        factory.setUsername("cqyhm");
        factory.setPassword("123456");
        //getting a connection
        connection = factory.newConnection();
        //creating a channel
        channel = connection.createChannel();
        //declaring a queue for this channel. If queue does not exist,
        //it will be created on the server.
        channel.queueDeclare(endPointName, false, false, false, null);
        channel.queueBind(endPointName, "amq.direct", endPointName);
	}
	/**
     * 关闭channel和connection。并非必须，因为隐含是自动调用的。
     * @throws IOException
	 * @throws TimeoutException 
     */
     public void close() throws IOException, TimeoutException{
         if(channel!=null) this.channel.close();
         if(connection!=null) this.connection.close();
     }
}
