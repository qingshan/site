create table attr_message ( 
  att_code varchar(20) not null, 
  parent_code varchar(20), 
  att_text  varchar(100), 
  seq 		numeric(8, 0), 
  opt_date date, 
  mark varchar(1) default 'N', 
  primary key(att_code) 
);