package com.zdsoft.site.mina.model.customEnum;

import javax.xml.bind.annotation.XmlEnumValue;

/**
 * 密码类型(PasswordType)
 * 
 * @author cqyhm
 *
 */
public enum PasswordType {
	/** "0","查询" */
	@XmlEnumValue("0") A0("0", "查询"),

	/** "1","取款" */
	@XmlEnumValue("1") A1("1", "取款"),

	/** "2","转账" */
	@XmlEnumValue("2") A2("2", "转账"),

	/** "3","交易" */
	@XmlEnumValue("3") A3("3", "交易");

	private String value;
	private String name;

	private PasswordType(String value, String name) {
		this.value = value;
		this.name = name;
	}

	public String getValue() {
		return value;
	}

	public String getName() {
		return name;
	}

	@Override
	public String toString() {
		return value;
	}
}
