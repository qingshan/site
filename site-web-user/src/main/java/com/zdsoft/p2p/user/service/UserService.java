package com.zdsoft.p2p.user.service;

import com.zdsoft.p2p.user.domain.User;
import com.zdsoft.rock.common.pagination.PageRequest;
import com.zdsoft.rock.common.pagination.PageResponse;

public interface UserService {
	
	public void sayHello(String words);
	/**
	 * 保存用户信息
	 * @param user
	 */
	public void save(User user);
	/**
	 * 根据Id查询用户信息
	 * @param id
	 * @return
	 */
	public User findById(String id);
	/**
	 * 分页查询用户信息
	 * @param user
	 * @param pageRequest
	 * @return
	 */
	public PageResponse<User> findPager(User user, PageRequest pageRequest);
}
