package com.zdsoft.site.mina.model;

import com.zdsoft.site.mina.model.customEnum.AccountStatus;
import com.zdsoft.site.mina.model.customEnum.AccountType;
import com.zdsoft.site.mina.model.customEnum.BankFlag;
import com.zdsoft.site.mina.model.customEnum.BankId;
import com.zdsoft.site.mina.model.customEnum.CardType;

/**
 *  7.7   账户（Account）  
 * @author cqyhm
 * amount 金额总的最大长度 18位，小数位长度最大为 2 
 */
public class Account {
//	银行管理账号 账号  AccountIdentification  <Id>  [1..1]  Max32Text 必填 
	private String AccountIdentification;
//	户主名称  AccountName  <Name>  [1..1]  Max60Text 可选   
	private String AccountName;
//	账户类别  AccountType  <Type>  [0..1]  AccountType  枚举  可选。 
	private AccountType AccountType;
//	账户状态  AccountStatus  <Status>  [0..1]  AccountStatus  枚举 
	private AccountStatus AccountStatus;
//	银行标志  BankFlag  <BkFlag>  [0..1]  BankFlag  枚举  可选，默认为本行 
	private BankFlag BankFlag;
//	银行编号  BankId  < BkId>  [1..1]  BankId  枚举 
	private BankId BankId;
//	卡类型  CardType  <CardType>  [1..1]  CardType  枚举 
	private CardType CardType;
//	归属联行号  BankCode  <BkCode>  [1..1]  Max16Text
	private String BankCode;
//	归属行行名  BankName  <BkName>  [1..1]  Max246Tex
	private String BankName;
//	密码  Password  <Pwd>  [0..1]  Password  组件  可选 
	private Password password;
//	开户日期  RegisterDate  <RegDt>  [0..1]  Date   YYYYMMDD  可选
	private String RegisterDate;
//	有效日期  ValidDate  <VldDt>  [0..1]  Date YYYYMMDD  可选
	private String ValidDate;
//	信用卡背后末3位数 CVN2  <CVN2>  [0..1]  Max3Tex信用卡支付必	填
	private String CVN2;
//	预留手机号  TelNo  < TelNo>  [1..1]  Max16Text  
	private String TelNo;
	public String getAccountIdentification() {
		return AccountIdentification;
	}
	public void setAccountIdentification(String accountIdentification) {
		AccountIdentification = accountIdentification;
	}
	public String getAccountName() {
		return AccountName;
	}
	public void setAccountName(String accountName) {
		AccountName = accountName;
	}
	public AccountType getAccountType() {
		return AccountType;
	}
	public void setAccountType(AccountType accountType) {
		AccountType = accountType;
	}
	public AccountStatus getAccountStatus() {
		return AccountStatus;
	}
	public void setAccountStatus(AccountStatus accountStatus) {
		AccountStatus = accountStatus;
	}
	public BankFlag getBankFlag() {
		return BankFlag;
	}
	public void setBankFlag(BankFlag bankFlag) {
		BankFlag = bankFlag;
	}
	public BankId getBankId() {
		return BankId;
	}
	public void setBankId(BankId bankId) {
		BankId = bankId;
	}
	public CardType getCardType() {
		return CardType;
	}
	public void setCardType(CardType cardType) {
		CardType = cardType;
	}
	public String getBankCode() {
		return BankCode;
	}
	public void setBankCode(String bankCode) {
		BankCode = bankCode;
	}
	public String getBankName() {
		return BankName;
	}
	public void setBankName(String bankName) {
		BankName = bankName;
	}
	public Password getPassword() {
		return password;
	}
	public void setPassword(Password password) {
		this.password = password;
	}
	public String getRegisterDate() {
		return RegisterDate;
	}
	public void setRegisterDate(String registerDate) {
		RegisterDate = registerDate;
	}
	public String getValidDate() {
		return ValidDate;
	}
	public void setValidDate(String validDate) {
		ValidDate = validDate;
	}
	public String getCVN2() {
		return CVN2;
	}
	public void setCVN2(String cVN2) {
		CVN2 = cVN2;
	}
	public String getTelNo() {
		return TelNo;
	}
	public void setTelNo(String telNo) {
		TelNo = telNo;
	}
}
