package com.zdsoft.p2p.user.validator;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
/**
 * 密码校验器
 * @author yhm
 *
 */
public class PasswordValidator implements ConstraintValidator<Password, String> {

	private String field;
	
	public String getField() {
		return field;
	}
	public void setField(String field) {
		this.field = field;
	}
	/**5~10位的数字与字母组合*/
	private static Pattern pattern = Pattern.compile("(?![0-9]+$)(?![a-zA-Z]+$)[0-9A-Za-z]{5,10}");

	@Override
	public void initialize(Password constraintAnnotation) {
		//获取注解上设置的属性
	}
	/**	
	 * 校验方法
	 */
	@Override
	public boolean isValid(String value, ConstraintValidatorContext context) {
		if (value == null) {
			return false;
		}
		Matcher m = pattern.matcher(value);
		return m.matches();
	}
}
