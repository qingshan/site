package com.zdsoft.site.mina.model.response;

import javax.xml.bind.annotation.XmlRootElement;
/**
 * 6.1.11.服务商文件生成完毕通知(44908)
 * 说明：
 * 1.本功能是服务商方生成清算文件后，通知银行方监管系统获取清算文件。
 * 2.本功能适合银行方主动下载服务商清算文件的传输模式。
 * 3.监管系统下载文件后才返回应答，建议服务商的通讯超时时间能达到5分钟以上。
 * 托管系统响应
 * 
 * 6.2.2　银行文件生成通知(43904)
 * 说明：
 * 1.在直联模式下，可以使用本功能通知服务商可以获取银行生成的某个文件。
 * P2P响应内容
 */
@XmlRootElement(name = "MsgText")
public class ResponseFileGen extends AbstractResponse {
}
