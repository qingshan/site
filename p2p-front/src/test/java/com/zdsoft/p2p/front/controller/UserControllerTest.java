package com.zdsoft.p2p.front.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import org.junit.Test;

import com.zdsoft.p2p.front.TestController;

public class UserControllerTest extends TestController {

	@Test
	public void testSave() throws Exception {
		mockMvc.perform(post("/user/save.html")
							.param("userCd", "qq")
							.param("userNm", "杨华明")
							.param("version","0")
							.param("language", "zh_CN")
							.param("listSort", "0")
						)
		   .andExpect(status().isOk())
		   //.andExpect(jsonPath("$.userCd","cqyhm").exists())
		   .andDo(print())
		   .andDo(log());
	}

}
