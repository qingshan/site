package com.zdsoft.site.mina.model.customEnum;
/**
 * 处理状态（DealStatus）  兑付状态 
 * @author cqyhm
 *
 */
public enum DealStatus {
	A0,//失败 
	A1// 成功
}
