package com.zdsoft.site.mina.model.response;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.springframework.beans.BeanUtils;

import com.zdsoft.site.mina.model.Account;
import com.zdsoft.site.mina.model.Customer;
import com.zdsoft.site.mina.model.Reference;
import com.zdsoft.site.mina.utils.JaxbUtil;
/**
* 6.1.1  客户注册(46701)说明： 
* 	1.  适用于服务商客户带卡指定监管银行签约和服务商客户预指定监管银行的签约。 
	2.  如果请求报中没有银行卡号，则银行监管系统只创建银行帐号和交易管理账号的对应关系，客
		户还需要进行增加银行卡绑定交易。 
	3.  实现客户单个币种的签约，不同币种需要进行多次本功能操作。 
	4.  服务商方根据应答包中的银行帐号确定和交易管理账号建立对应关系。 
	5.  银行方支持重复签约。
*/	 
@XmlRootElement(name="MsgText")
public class ResponseRegist extends AbstractResponse {
	
	/**银行流水号  BankReference  <BkRef>  [1..1]  Reference  组件 */
	@XmlElement(name="BkRef")
	private Reference BkRef;
	
	/**客户信息  Customer  <Cust>  [0..1]  Customer  组件 */
	@XmlElement(name="Cust")
	private Customer Cust;
	
	/**银行账户  BankAccount  <BkAcct>  [0..1]  Account  组件*/ 
	@XmlElement(name="BkAcct")
	private Account BkAcct;
	
	/**交易管理账户  ManageAccount  < MgeAcct>  [0..1]  Account  组件*/
	@XmlElement(name="MgeAcct")
	private Account MgeAcct;

	public ResponseRegist(){}
	
	public ResponseRegist(String xml){
		ResponseRegist response=JaxbUtil.converyToJavaBean(xml, this.getClass());
		BeanUtils.copyProperties(response, this);
	}
	/**银行流水号  BankReference  <BkRef>  [1..1]  Reference  组件 */
	public Reference getBkRef() {
		return BkRef;
	}
	/**银行流水号  BankReference  <BkRef>  [1..1]  Reference  组件 */
	public void setBkRef(Reference reference) {
		BkRef = reference;
	}

	public Customer getCust() {
		return Cust;
	}

	public void setCust(Customer customer) {
		Cust = customer;
	}

	public Account getBkAcct() {
		return BkAcct;
	}

	public void setBkAcct(Account bankAccount) {
		BkAcct = bankAccount;
	}

	public Account getMgeAcct() {
		return MgeAcct;
	}

	public void setMgeAcct(Account manageAccount) {
		MgeAcct = manageAccount;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ResponseRegist[")		
				.append("").append(getMsgHdr())
				.append("").append(getRst())
				.append(", BkRef=").append(BkRef)
				.append(", Cust=").append(Cust)
				.append(", BkAcct=").append(BkAcct)
				.append(", MgeAcct=").append(MgeAcct)
			    .append("]");
		return builder.toString();
	}
}
