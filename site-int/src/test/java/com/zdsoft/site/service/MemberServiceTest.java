package com.zdsoft.site.service;

import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.zdsoft.site.domain.Member;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations={"classpath:applicationContext.xml"})
public class MemberServiceTest {
	
	private final Logger logger=LoggerFactory.getLogger(getClass());
	@Autowired
	private MemberService memberService;
	@Test
	public void testFindById() {
		Member m=memberService.findById("1");
		assertNotNull(m);
		logger.debug("{}",m);
		
		m=memberService.findById("1");
		assertNotNull(m);
		logger.debug("{}",m);
	}

}
