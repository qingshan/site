package com.zdsoft.p2p.user.dao;

import com.zdsoft.p2p.user.domain.User;
import com.zdsoft.rock.common.model.dao.BaseDAO;
import com.zdsoft.rock.common.pagination.PageRequest;
import com.zdsoft.rock.common.pagination.PageResponse;

public interface UserDao extends BaseDAO<User> {

	public PageResponse<User> findPager(User user, PageRequest pageRequest);
}
