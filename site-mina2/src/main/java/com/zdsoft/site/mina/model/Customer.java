package com.zdsoft.site.mina.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

import com.zdsoft.site.mina.model.customEnum.CertificationType;
import com.zdsoft.site.mina.model.customEnum.CountryCode;
import com.zdsoft.site.mina.model.customEnum.CustomerType;
import com.zdsoft.site.mina.model.customEnum.GenderCode;

/**
 * 客户信息（Customer）
 * 
 * @author cqyhm
 *
 */

@XmlAccessorType(XmlAccessType.FIELD)
public class Customer {
	/** 客户姓名 Name [Name] [1..1] Max60Text 必选 */
	@XmlElement(name = "Name")
	private String Name;

	/** 证件类型 CertificationType [CertType] [1..1] CertificationType 枚举 必选 */
	@XmlElement(name = "CertType")
	private CertificationType CertType;

	/** 证件号码 CertificationIdentifier [CertId] [1..1] Max30Text 必选 */
	@XmlElement(name = "CertId")
	private String CertId;

	/** 客户类型 Type [Type] [1..1] CustomerType 枚举 可选 */
	@XmlElement(name = "Type")
	private CustomerType Type;

	/** 客户性别 Gender [Gender] [0..1] GenderCode 枚举 可选 */
	@XmlElement(name = "Gender")
	private GenderCode  Gender;

	/** 客户国籍 Nationality [Ntnl] [0..1] CountryCode 枚举 可选 */
	@XmlElement(name = "Ntnl")
	private CountryCode Ntnl;

	/** 通信地址 Address [Addr] [0..1] Max60Text 可选 */
	@XmlElement(name = "Addr")
	private String Addr;

	/** 邮政编码 Postcode [PstCd] [0..1] Max6Text 可选 */
	@XmlElement(name = "PstCd")
	private String PstCd;

	/** 电子邮件 Email [Email] [0..1] Max60Text 可选 */
	@XmlElement(name = "Email")
	private String Email;

	/** 传真 Fax [Fax] [0..1] Max20Text 可选 */
	@XmlElement(name = "Fax")
	private String Fax;

	/** 手机 Mobile [Mobile] [0..1] Max20Text 可选 */
	@XmlElement(name = "Mobile")
	private String Mobile;

	/** 电话 Telephone [Tel] [0..1] Max20Text 可选 */
	@XmlElement(name = "Tel")
	private String Tel;

	/** 客户姓名 Name [Name] [1..1] Max60Text 必选 */
	public String getName() {
		return Name;
	}

	/** 客户姓名 Name [Name] [1..1] Max60Text 必选 */
	public void setName(String name) {
		Name = name;
	}

	/** 证件类型 CertificationType [CertType] [1..1] CertificationType 枚举 必选 */
	public CertificationType getCertType() {
		return CertType;
	}

	/** 证件类型 CertificationType [CertType] [1..1] CertificationType 枚举 必选 */
	public void setCertType(CertificationType certificationType) {
		CertType = certificationType;
	}

	/** 证件号码 CertificationIdentifier [CertId] [1..1] Max30Text 必选 */
	public String getCertId() {
		return CertId;
	}

	/** 证件号码 CertificationIdentifier [CertId] [1..1] Max30Text 必选 */
	public void setCertId(String certificationIdentifier) {
		CertId = certificationIdentifier;
	}

	/** 客户类型 Type [Type] [1..1] CustomerType 枚举 可选 */
	public CustomerType getType() {
		return Type;
	}

	/** 客户类型 Type [Type] [1..1] CustomerType 枚举 可选 */
	public void setType(CustomerType customerType) {
		Type = customerType;
	}

	/** 客户性别 Gender [Gender] [0..1] GenderCode 枚举 可选 */
	public GenderCode getGender() {
		return Gender;
	}

	/** 客户性别 Gender [Gender] [0..1] GenderCode 枚举 可选 */
	public void setGender(GenderCode gender) {
		this.Gender = gender;
	}

	/** 客户国籍 Nationality [Ntnl] [0..1] CountryCode 枚举 可选 */
	public CountryCode getNtnl() {
		return Ntnl;
	}

	/** 客户国籍 Nationality [Ntnl] [0..1] CountryCode 枚举 可选 */
	public void setNtnl(CountryCode nationality) {
		Ntnl = nationality;
	}

	/** 通信地址 Address [Addr] [0..1] Max60Text 可选 */
	public String getAddr() {
		return Addr;
	}

	/** 通信地址 Address [Addr] [0..1] Max60Text 可选 */
	public void setAddr(String address) {
		Addr = address;
	}

	/** 邮政编码 Postcode [PstCd] [0..1] Max6Text 可选 */
	public String getPstCd() {
		return PstCd;
	}

	/** 邮政编码 Postcode [PstCd] [0..1] Max6Text 可选 */
	public void setPstCd(String postcode) {
		PstCd = postcode;
	}

	/** 电子邮件 Email [Email] [0..1] Max60Text 可选 */
	public String getEmail() {
		return Email;
	}

	/** 电子邮件 Email [Email] [0..1] Max60Text 可选 */
	public void setEmail(String email) {
		Email = email;
	}

	/** 传真 Fax [Fax] [0..1] Max20Text 可选 */
	public String getFax() {
		return Fax;
	}

	/** 传真 Fax [Fax] [0..1] Max20Text 可选 */
	public void setFax(String fax) {
		Fax = fax;
	}

	/** 手机 Mobile [Mobile] [0..1] Max20Text 可选 */
	public String getMobile() {
		return Mobile;
	}

	/** 手机 Mobile [Mobile] [0..1] Max20Text 可选 */
	public void setMobile(String mobile) {
		Mobile = mobile;
	}

	/** 电话 Telephone [Tel] [0..1] Max20Text 可选 */
	public String getTel() {
		return Tel;
	}

	/** 电话 Telephone [Tel] [0..1] Max20Text 可选 */
	public void setTel(String telephone) {
		Tel = telephone;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Customer [Name=").append(Name).append(", CertType=").append(CertType).append(", CertId=").append(CertId).append(", Type=").append(Type).append(", gender=").append(Gender).append(", Ntnl=")
				.append(Ntnl).append(", Addr=").append(Addr).append(", PstCd=").append(PstCd).append(", Email=").append(Email).append(", Fax=").append(Fax).append(", Mobile=").append(Mobile).append(", Tel=").append(Tel).append("]");
		return builder.toString();
	}

}
