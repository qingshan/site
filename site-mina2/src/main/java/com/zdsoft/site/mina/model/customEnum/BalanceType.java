package com.zdsoft.site.mina.model.customEnum;

import javax.xml.bind.annotation.XmlEnumValue;

/**
 *   余额类型(BalanceType) 
 * @author cqyhm
 *
 */
public enum BalanceType {
	/**余额类型 0:可用余额*/ 
	@XmlEnumValue("0")	A0("0","可用余额"),

	/**余额类型 1:支付待收金额*/ 
	@XmlEnumValue("1")	A1("1","支付待收金额"),

	/**余额类型 2:标的冻结金额*/ 
	@XmlEnumValue("2")	A2("2","标的冻结金额"),

	/**余额类型 3:资产余额*/ 
	@XmlEnumValue("3")	A3("3","资产余额");
	
	private String value;
	private String name;
	
	private BalanceType(String value,String name){
		this.value=value;
		this.name=name;
	}
	
	public String getValue() {
		return value;
	}
	public String getName() {
		return name;
	}
	@Override
	public String toString(){
		return value;
	}	
}
