package com.zdsoft.site.mina.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

import com.zdsoft.site.mina.model.customEnum.EncryMode;
import com.zdsoft.site.mina.model.customEnum.PasswordType;

/**
 * 密码（Password） 
 * @author cqyhm
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class Password {
	/**密码类型  Type  [Type]  [0..1]  PasswordType  枚举**/
	@XmlElement(name="Type")
	private PasswordType type;
	
	/**加密方式  EncryMode  [Enc]  [0..1]  EncryMode  枚举*/
	@XmlElement(name="Enc")
	private EncryMode Enc;
	
	/**密码  Password  [Pwd]  [0..1]  Max16Text*/
	@XmlElement(name="Pwd")
	private String Pwd;
	
	/**密码类型  Type  [Type]  [0..1]  PasswordType  枚举**/
	public PasswordType getType() {
		return type;
	}
	
	/**密码类型  Type  [Type]  [0..1]  PasswordType  枚举**/
	public void setType(PasswordType type) {
		this.type = type;
	}
	
	/**加密方式  EncryMode  [Enc]  [0..1]  EncryMode  枚举*/
	public EncryMode getEnc() {
		return Enc;
	}
	
	/**加密方式  EncryMode  [Enc]  [0..1]  EncryMode  枚举*/
	public void setEnc(EncryMode encryMode) {
		Enc = encryMode;
	}
	
	/**密码  Password  [Pwd]  [0..1]  Max16Text*/
	public String getPwd() {
		return Pwd;
	}
	
	/**密码  Password  [Pwd]  [0..1]  Max16Text*/
	public void setPwd(String password) {
		Pwd = password;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Password [type=").append(type).append(", Enc=").append(Enc).append(", Pwd=").append(Pwd).append("]");
		return builder.toString();
	}
}
