package com.zdsoft.site.mina.model.customEnum;
/**
 *   余额类型(BalanceType) 
 * @author cqyhm
 *
 */
public enum BalanceType {
	A0,//  可用余额 
	A1,//  支付待收金额 
	A2,//  标的冻结金额 
	A3//  资产余额 
}
