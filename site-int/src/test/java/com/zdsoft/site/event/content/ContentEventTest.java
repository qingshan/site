package com.zdsoft.site.event.content;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:applicationContext.xml" })
public class ContentEventTest {
	
	private Logger logger=LoggerFactory.getLogger(getClass());
	
	@Autowired
	private ApplicationContext applicationContext;

	@Test
	public void testPublishEvent() {
		logger.debug("登录成功");
		applicationContext.publishEvent(new ContentEvent("今年是龙年的博客更新了"));//触发事件
		logger.debug("记录登录地址");
		// Thread.sleep(3000L); 所有事件都是同步事件则不需要延时
	}

}
