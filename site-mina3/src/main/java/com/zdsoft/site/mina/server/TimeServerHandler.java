package com.zdsoft.site.mina.server;
import org.apache.mina.api.IdleStatus;
import org.apache.mina.api.IoSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 服务器端业务逻辑
 */
public class TimeServerHandler extends IoHandlerAdapter {

	private Logger logger=LoggerFactory.getLogger(getClass());
    /**
     * 连接创建事件
     */
    @Override
    public void sessionCreated(IoSession session){
        // 显示客户端的ip和端口
    	logger.debug("sessionCreated Server : remoteAddr={}",session.getRemoteAddress().toString());
    }
    
    @Override
    public void exceptionCaught(IoSession session, Throwable cause) throws Exception {
        cause.printStackTrace();
        logger.debug("exceptionCaught server");
    }
    
    /**
     * 消息接收事件
     */
    @Override
    public void messageReceived(IoSession session, Object message) throws Exception {
        String strMsg = message.toString();
        if (strMsg.trim().equalsIgnoreCase("quit")) {
            session.close(true);
            return;
        }
        // 返回消息字符串
        session.write("Hi Client!");
        // 打印客户端传来的消息内容
        logger.debug("Message written : " + strMsg);
    }

    @Override
    public void sessionIdle(IoSession session, IdleStatus status) throws Exception {
    	logger.debug("IDLE",session.getIdleCount(status));
    }

	@Override
	public void sessionOpened(IoSession session) throws Exception {
		logger.debug("sessionOpened server");
	}

	@Override
	public void sessionClosed(IoSession session) throws Exception {
		logger.debug("sessionClosed server");
	}

	@Override
	public void messageSent(IoSession session, Object message) throws Exception {
		logger.debug("messageSent server");
	}

	@Override
	public void inputClosed(IoSession session) throws Exception {
		super.inputClosed(session);
		logger.debug("inputClosed server");
	}
    
}
