package com.zdsoft.site.mina.client;

import java.net.InetSocketAddress;
import java.nio.charset.Charset;

import org.apache.mina.core.future.ConnectFuture;
import org.apache.mina.core.service.IoHandlerAdapter;
import org.apache.mina.core.session.IdleStatus;
import org.apache.mina.core.session.IoSession;
import org.apache.mina.filter.codec.ProtocolCodecFilter;
import org.apache.mina.filter.codec.textline.TextLineCodecFactory;
import org.apache.mina.filter.logging.LoggingFilter;
import org.apache.mina.transport.socket.SocketConnector;
import org.apache.mina.transport.socket.nio.NioSocketConnector;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class I4670Handler extends IoHandlerAdapter {
	
	private Logger logger=LoggerFactory.getLogger(getClass());
	/** The connector */
    private SocketConnector connector;
    /** The session */
    private static IoSession session;
    //private boolean received = false;
    
	public I4670Handler(){
		connector=new NioSocketConnector();
		connector.getFilterChain().addLast("logger", new LoggingFilter());
		connector.getFilterChain().addLast("codec", 
                new ProtocolCodecFilter(new TextLineCodecFactory(Charset.forName("GB2312"))));
//		connector.getFilterChain().addLast("codec", new ProtocolCodecFilter(
//                new ObjectSerializationCodecFactory()));
		connector.setHandler(this);		
		//ConnectFuture cf = connector.connect(new InetSocketAddress("120.55.174.14", 8015));
		ConnectFuture cf = connector.connect(new InetSocketAddress("127.0.0.1", 8015));
		cf.awaitUninterruptibly();
		session=cf.getSession();
	}
	
	@Override
	public void sessionCreated(IoSession session) throws Exception {
		super.sessionCreated(session);
		logger.debug("sessionCreated");
	}

	@Override
	public void sessionOpened(IoSession session) throws Exception {
		super.sessionOpened(session);
		logger.debug("sessionOpened");
	}

	@Override
	public void sessionClosed(IoSession session) throws Exception {
		super.sessionClosed(session);
		logger.debug("sessionClosed");
	}

	@Override
	public void sessionIdle(IoSession session, IdleStatus status) throws Exception {
		logger.debug("sessionIdle");
	}

	@Override
	public void exceptionCaught(IoSession session, Throwable cause) throws Exception {
		//super.exceptionCaught(session, cause);
		logger.debug("session.isConnected={},exceptionCaught={}",session.isConnected(),cause.getMessage());
	}

	@Override
	public void messageReceived(IoSession session, Object message) throws Exception {
		logger.debug("messageReceived-接收数据时:{},{}",session.isConnected(),message);
		//received=true;
	}

	@Override
	public void messageSent(IoSession session, Object message) throws Exception {
		logger.debug("messageSent-发送数据时：session.isConnected={},{}",session.isConnected(),message);
	}

	@Override
	public void inputClosed(IoSession session) throws Exception {
		//super.inputClosed(session);
		logger.debug("inputClosed");
	}
	
	public void write(Object message){
//		IoBuffer buffer = IoBuffer.allocate(1024);
//        buffer.putObject(message);
//        buffer.flip();
		if(session!=null){
			logger.debug("session.isConnected()={}",session.isConnected());
	        session.write(message);
	        logger.debug("session.isConnected()={}",session.isConnected());
		}
//        while (received == false) {
//            try {
//				Thread.sleep(1);
//			} catch (InterruptedException e) {
//				e.printStackTrace();
//			}
//        }
//        received = false;
	}

}
