<%@ page language="java" contentType="text/html; charset=UTF-8"   pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<link  href="${ctx}/resources/theme/default/resources/css/ext-all.css" type="text/css" rel="stylesheet"/>
<script src="${ctx}/resources/extjs/adapter/ext/ext-base.js"  type="text/javascript" ></script>
<script src="${ctx}/resources/extjs/ext-all.js" type="text/javascript" ></script>
<script src="${ctx}/resources/extjs/ext-lang-zh_CN.js" type="text/javascript"></script>
</head>
<body>
<a id="walk" href="#">单击事件</a>
<script type="text/javascript">
Ext.onReady(function(){  
    var accordion = new Ext.Panel({  
        title: "左边菜单",  
        layout: "accordion",  
        layoutConfig: {  
            animate: true  
        },  
        width: 250,  
        minWidth: 100,  
        region: "west",  //左边
        split: false,  
        collapsible: true,  
        items: [  
            {title:"嵌套面板一", html:"嵌套面板一", iconCls:"save"},  
            {title:"嵌套面板二", html:"嵌套面板二", iconCls:"search"},  
            {title:"嵌套面板三", html:"嵌套面板三", iconCls:"back"}  
        ]  
    });  
    new Ext.Viewport({  
        title: "视窗标题",  
        layout: "border",  
        defaults: {  
            bodyStyle: "background-color: #FFFFFF;",  
            frame: true  
        },  
        items: [  
            accordion,  
            {region:"north", height:100},  
            {region:"center"}  
        ]  
    });  
});
</script>
</body>
</html>
