package com.zdsoft.site.mina.model.customEnum;

import javax.xml.bind.annotation.XmlEnumValue;

/**
 * 国家代码(CountryCode)
 * 
 * @author cqyhm
 *
 */
public enum CountryCode {
	/** 国家代码 "CHN","中国" */
	@XmlEnumValue("CHN") CHN("CHN", "中国"),

	/** 国家代码 "USA","美国" */
	@XmlEnumValue("USA") USA("USA", "美国");

	private String value;
	private String name;

	private CountryCode(String value, String name) {
		this.value = value;
		this.name = name;
	}

	@Override
	public String toString() {
		return value;
	}

	public String getValue() {
		return value;
	}

	public String getName() {
		return name;
	}
}
