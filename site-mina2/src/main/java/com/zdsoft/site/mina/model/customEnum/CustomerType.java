package com.zdsoft.site.mina.model.customEnum;

import javax.xml.bind.annotation.XmlEnumValue;

/**
 * 客户类型（CustomerType）
 * 
 * @author cqyhm
 *
 */
public enum CustomerType {
	/** 客户类型 "0","个人" */
	@XmlEnumValue("0") A0("0", "个人"),

	/** 客户类型 "1","机构" */
	@XmlEnumValue("1") A1("1", "机构");

	private String value;
	private String name;

	private CustomerType(String value, String name) {
		this.value = value;
		this.name = name;
	}

	@Override
	public String toString() {
		return value;
	}

	public String getValue() {
		return value;
	}

	public String getName() {
		return name;
	}
}
