package com.zdsoft.p2p;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.coobird.thumbnailator.Thumbnails;

/**
 * 根据指定的目录及其子目录下的图像文件，生成同名的缩率图放到指定的子文件夹下
 * @author cqyhm
 *
 */
public class CreateImage {
	
	private static Logger logger=LoggerFactory.getLogger(CreateImage.class);
	/**
	 * 在指定的路径下需找所有的图形文件，并在指定的分辨率作为子路径下建立同级的缩略图
	 * 参数：CreateImage /Data/  180X90,400X300
	 * 必须带两个参数args[0] 起始路径 args[1],逗号隔开的图像分辨率
	 * @param args
	 * @throws IOException 
	 */
	public static void main(String[] args){
		if(args.length!=2){
			logger.debug("生成缩略图");
			logger.debug("命令格式:CreateImage 起始路径   图像分片率");
			logger.debug("示例:CreateImage /Data  180X120,120X90");
			return;
		}
		//检查目录是否存在
		File file=new File(args[0]);
		if(!file.exists()||!file.isDirectory()){
			logger.debug("指定的路径[{}]不存在或者不是有效的目录.无法处理",args[0]);
			return;
		}
		String str=args[1].toUpperCase();
		String[] resolution=str.split(",");
		
		//获取目录下的所有文件和文件夹
		List<File> allFiles=new ArrayList<File>();
		showDir(file, allFiles);
		//对所有文件进行处理
		for (File f : allFiles) {
			String oldFile=f.getAbsolutePath();
			String oldPath=oldFile.substring(0,oldFile.lastIndexOf(File.separatorChar));//只包含路径部分
			for (String r : resolution) {
				try {
					int width=Integer.parseInt(r.substring(0,r.indexOf("X")));
					int height=Integer.parseInt(r.substring(r.indexOf("X")+1));
					
					String newPath=String.format("%s%s%s",oldPath,File.separatorChar,r);
					new File(newPath).mkdirs();
					String newFile=String.format("%s%s%s", newPath,File.separatorChar,f.getName());
					if(!new File(newFile).exists()){
						logger.debug("源文件={}",oldFile);
						Thumbnails.of(f).size(width, height).keepAspectRatio(true).toFile(newFile);
						logger.debug("缩略图={}",newFile);
					}else{
						logger.debug("已有缩略图,忽略生成={}",newFile);
					}
				} catch (Exception e) {
					logger.error("生成缩略图异常",e);
				}
			}
		}
	}
	/**
	 * 循环扫描指定的路径，并获取所有文件
	 * @param dir
	 * @param allFiles
	 */
	public static void showDir(File dir,List<File> allFiles) {
		File[] files = dir.listFiles(new FileFilter() {
			@Override
			public boolean accept(File f) {
				//文件夹如果包含了分辨率,则已经是缩略图这个忽略不再处理
				if(f.isDirectory()){
					if(f.getName().matches("^[1-9]\\d*X[1-9]\\d*$")){
						return false;
					}
				}
				//是图片或者目录
				return f.isDirectory()||f.getName().endsWith(".png")||f.getName().endsWith(".jpg")||f.getName().endsWith(".gif");
			}
		});
		for (File file : files) {
			if (file.isDirectory()) {
				showDir(file,allFiles);
			} else {
				allFiles.add(file);
			}
		}
	}
}
