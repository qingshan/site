package com.zdsoft.site.mina.p2p;

import static org.junit.Assert.assertNotNull;

import java.io.UnsupportedEncodingException;

import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hundsun.encrypt.util.StringUtil;
import com.zdsoft.site.mina.model.Account;
import com.zdsoft.site.mina.model.Customer;
import com.zdsoft.site.mina.model.Institution;
import com.zdsoft.site.mina.model.MessageHeader;
import com.zdsoft.site.mina.model.Reference;
import com.zdsoft.site.mina.model.TransMangAcc;
import com.zdsoft.site.mina.model.customEnum.AccountStatus;
import com.zdsoft.site.mina.model.customEnum.AccountType;
import com.zdsoft.site.mina.model.customEnum.BankId;
import com.zdsoft.site.mina.model.customEnum.CertificationType;
import com.zdsoft.site.mina.model.customEnum.CustomerType;
import com.zdsoft.site.mina.model.customEnum.InstitutionType;
import com.zdsoft.site.mina.model.request.RequestAcctQuery;
import com.zdsoft.site.mina.model.request.RequestCancelAcct;
import com.zdsoft.site.mina.model.request.RequestChangeKey;
import com.zdsoft.site.mina.model.request.RequestRegist;

public class IHandlerTest {
	
	private Logger logger=LoggerFactory.getLogger(getClass());
	
	/**生成请求头信息*/
	public MessageHeader getMsgHdr(){
		MessageHeader msghdr = new MessageHeader();
		msghdr.setVer("3.0.0");
		msghdr.setDate();
		// 服务商The number of lines found so far
		Institution serviceInstitution = new Institution();
		serviceInstitution.setInstType(InstitutionType.A0);
		msghdr.setSvInst(serviceInstitution);
		// 银行
		Institution bkInst = new Institution();
		bkInst.setInstType(InstitutionType.A1);
		bkInst.setInstId(BankId.Ah.getValue());//北京银行
		bkInst.setInstNm(BankId.Ah.getName()); //北京银行
		msghdr.setBkInst(bkInst);

		Reference requestReference = new Reference();
		requestReference.setRef(String.valueOf(System.currentTimeMillis()));
		msghdr.setRqRef(requestReference);
		return msghdr;
	}
	//交换秘钥
	@Test
	public void testChangeKey() throws UnsupportedEncodingException{
		//请求消息
		RequestChangeKey request=new RequestChangeKey(getMsgHdr());
		ChangeKeyHandler hander = new ChangeKeyHandler(request);
		Assert.assertNotNull(hander);
	}
	//客户注册
	@Test
	public void testCustRegist() {
		RequestRegist request=new RequestRegist(getMsgHdr());
		///////////////交易成功--还未绑定银行卡////////////////////////////
		//客户信息
		Customer cust=new Customer();
		cust.setName("admin");
		cust.setCertId("5555555555");
		cust.setCertType(CertificationType.A2);
		cust.setType(CustomerType.A0);
		
		request.setCust(cust);
		//交易账户
		TransMangAcc mgeAcct=new TransMangAcc();
		mgeAcct.setId("admin");
		mgeAcct.setName("admin");
		mgeAcct.setStatus(AccountStatus.A0);
		mgeAcct.setType(AccountType.A0);
		mgeAcct.setRegDt("20151223");
		mgeAcct.setVldDt("20251231");
		request.setMgeAcct(mgeAcct);
		
		//银行账户
		Account BkAcct=new Account();
		BkAcct.setId("1");
		BkAcct.setName("杨华明");
		BkAcct.setType(AccountType.A0);
		//request.setBkAcct(BkAcct);
		
		request.setOPFlag("0");
		request.setDgst("");
		CustRegistHandler handler=new CustRegistHandler(request);
		assertNotNull(handler);
	}
	//客户撤销银行账号
	@Test
	public void testCancelAcct() {
		RequestCancelAcct request=new RequestCancelAcct(getMsgHdr());
		/////////////////////////////////////////////
		//客户信息
		Customer cust=new Customer();
		cust.setName("杨华明");
		cust.setCertId("5555555555");
		cust.setCertType(CertificationType.A2);
		cust.setType(CustomerType.A0);
		
		request.setCust(cust);
		//交易账户
		TransMangAcc mgeAcct=new TransMangAcc();
		mgeAcct.setId("1");
		mgeAcct.setName("杨华明");
		mgeAcct.setStatus(AccountStatus.A0);
		mgeAcct.setType(AccountType.A0);
		mgeAcct.setRegDt("20151223");
		mgeAcct.setVldDt("20251231");
		request.setMgeAcct(mgeAcct);
		
		//银行账户
		Account BkAcct=new Account();
		BkAcct.setId("1");
		BkAcct.setName("");
		BkAcct.setType(AccountType.A0);
		//request.setBkAcct(BkAcct);
		request.setCnlFlag("0");
		request.setDgst("");
		
		CancelAcctHandler handler=new CancelAcctHandler(request);
		assertNotNull(handler);
	}
	//管理账户信息查询
	@Test
	public void testAcctQuery(){
		RequestAcctQuery request=new RequestAcctQuery(getMsgHdr());
		//客户信息
		Customer cust=new Customer();
		cust.setName("admin");
		cust.setCertId("5555555555");
		cust.setCertType(CertificationType.A2);
		cust.setType(CustomerType.A0);
		
		request.setCust(cust);
		
		//交易账户
		TransMangAcc mgeAcct=new TransMangAcc();
		mgeAcct.setId("admin");
		mgeAcct.setName("admin");
		mgeAcct.setStatus(AccountStatus.A0);
		mgeAcct.setType(AccountType.A0);
		mgeAcct.setRegDt("20151223");
		mgeAcct.setVldDt("20251231");
		
		request.setMgeAcct(mgeAcct);
		request.setDgst("");
		
		AcctQueryHandler handler=new AcctQueryHandler(request);
		assertNotNull(handler);
		logger.debug("{}",handler.getResponse());
	}
	@Test
	public void test1(){
		logger.debug("{}",StringUtil.alignRight("aaa", 32));
		logger.debug("{}",StringUtil.rightPad("aa", 32));
		logger.debug("{}",StringUtil.rightPad("aa", 32,"0"));
	}
}
