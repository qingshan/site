package org.beetl.sql.spring.dao.impl;

import java.util.List;

import org.beetl.sql.core.SQLManager;
import org.beetl.sql.ext.spring.SpringBeetlSql;
import org.beetl.sql.spring.dao.UserDao;
import org.beetl.sql.spring.domain.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class UserDaoImpl implements UserDao {
	
	@Autowired
	private SpringBeetlSql beetlsql;
	
	public SQLManager getSQLManager(){
		return beetlsql.getSQLMananger();
	}
	@Override
	public void save(User user) {
		SQLManager sql = getSQLManager();
		sql.insert(User.class, user);
	}
	
	@Override
	public int total(User user) {
		SQLManager sql = getSQLManager();
		sql.deleteById(User.class, 3);
		List<User> list = sql.all(User.class);
		int total = list .size();
		return total;
	}

}
