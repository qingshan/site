package com.zdsoft.site.mina.model.response;

import java.math.BigDecimal;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.springframework.beans.BeanUtils;

import com.zdsoft.site.mina.model.TransMangAcc;
import com.zdsoft.site.mina.utils.JaxbUtil;

/**
 * 6.1.18.服务商发起客户债权受让(42706) 
 * 说明： 
 * 1.本功能是服务商方发起客户债权转让，并由本系统进行监管账户余额处理
 * 2.实际扣减债权转入客户资金 = 受让金额 – 红包抵扣金额 
 * 3.对资金处理时还需扣减虚拟红包子账户金额 
 * 4.支持重发操作
 * 5.资金变动说明：【转入方】A用户，用x元，购买【转出方】B用户，y元债权，其中收取A手续费a，收取B手续费b;
 * 那么A用户可用资金减少（x+a）元、资产余额增加y元；B用户可用资金增加(x-b)元、资产余额减少y元;
 * 应答：
 * 
 * @author cqyhm
 *
 */
@XmlRootElement(name = "MsgText")
public class ResponseCreditRight extends AbstractResponse {

	/** 03.债权买入者 ManageAccountIn <MgeAcctIn> [1..1] TransMangAcc 组件 */
	@XmlElement(name = "MgeAcctIn")
	private TransMangAcc MgeAcctIn;

	/** 04.债权卖出者 ManageAccountOut <MgeAcctOut> [1..1] TransMangAcc 组件 */
	@XmlElement(name = "MgeAcctOut")
	private TransMangAcc MgeAcctOut;

	/** 05.成功转让份数 TransCount <TransCount> [1..1] Max20Text */
	@XmlElement(name = "TransCount")
	private String TransCount;

	/** 06.成功转让金额 TransBala <TransBala> [1..1] Amount */
	@XmlElement(name = "TransBala")
	private BigDecimal TransBala;

	/** 07.手续费 FeeAmount <FeeAmt> [0..1] Amount */
	@XmlElement(name = "FeeAmt")
	private BigDecimal FeeAmt;

	public ResponseCreditRight() {
	}

	public ResponseCreditRight(String xml) {
		ResponseCreditRight response = JaxbUtil.converyToJavaBean(xml, this.getClass());
		BeanUtils.copyProperties(response, this);
	}

	/** 03.债权买入者 ManageAccountIn <MgeAcctIn> [1..1] TransMangAcc 组件 */
	public TransMangAcc getMgeAcctIn() {
		return MgeAcctIn;
	}

	/** 03.债权买入者 ManageAccountIn <MgeAcctIn> [1..1] TransMangAcc 组件 */
	public void setMgeAcctIn(TransMangAcc mgeAcctIn) {
		MgeAcctIn = mgeAcctIn;
	}

	/** 04.债权卖出者 ManageAccountOut <MgeAcctOut> [1..1] TransMangAcc 组件 */
	public TransMangAcc getMgeAcctOut() {
		return MgeAcctOut;
	}

	/** 04.债权卖出者 ManageAccountOut <MgeAcctOut> [1..1] TransMangAcc 组件 */
	public void setMgeAcctOut(TransMangAcc mgeAcctOut) {
		MgeAcctOut = mgeAcctOut;
	}

	/** 05.成功转让份数 TransCount <TransCount> [1..1] Max20Text */
	public String getTransCount() {
		return TransCount;
	}

	/** 05.成功转让份数 TransCount <TransCount> [1..1] Max20Text */
	public void setTransCount(String transCount) {
		TransCount = transCount;
	}

	/** 06.成功转让金额 TransBala <TransBala> [1..1] Amount */
	public BigDecimal getTransBala() {
		return TransBala;
	}

	/** 06.成功转让金额 TransBala <TransBala> [1..1] Amount */
	public void setTransBala(BigDecimal transBala) {
		TransBala = transBala;
	}

	/** 07.手续费 FeeAmount <FeeAmt> [0..1] Amount */
	public BigDecimal getFeeAmt() {
		return FeeAmt;
	}

	/** 07.手续费 FeeAmount <FeeAmt> [0..1] Amount */
	public void setFeeAmt(BigDecimal feeAmt) {
		FeeAmt = feeAmt;
	}
}
