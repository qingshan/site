package com.zdsoft.site.mina.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.zdsoft.site.mina.model.customEnum.InstitutionType;

/**
 * 7.4 机构信息（Institution）
 * 
 * @author cqyhm
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "SvInst")
public class Institution {

	/** 机构类型 [InstType] [0..1] InstitutionType 枚举 可选 */
	@XmlElement(name = "InstType")
	private InstitutionType InstType;

	/** 机构标识 [InstId] [0..1] Max16Text 可选 */
	@XmlElement(name = "InstId")
	private String InstId;

	/** 机构名称 [InstNm] [0..1] Max60Text 可选 */
	@XmlElement(name = "InstNm")
	private String InstNm;

	/** 分支机构编码 [BrchId] [0..1] Max16Text 可选 */
	@XmlElement(name = "BrchId")
	private String BrchId;

	/** 分支机构名称 [BrchNm] [0..1] Max60Text 可选 */
	@XmlElement(name = "BrchNm")
	private String BrchNm;

	/** 网点号 [SubBrchId] [0..1] Max16Text 可选 */
	@XmlElement(name = "SubBrchId")
	private String SubBrchId;

	/** 网点名称 [SubBrchNm] [0..1] Max60Text 可选 */
	@XmlElement(name = "SubBrchNm")
	private String SubBrchNm;

	/** 机构类型 [InstType] [0..1] InstitutionType 枚举 可选 */
	public InstitutionType getInstType() {
		return InstType;
	}

	/** 机构类型 [InstType] [0..1] InstitutionType 枚举 可选 */
	public void setInstType(InstitutionType institutionType) {
		this.InstType = institutionType;
	}

	/** 机构标识 [InstId] [0..1] Max16Text 可选 */
	public String getInstId() {
		return InstId;
	}

	/** 机构标识 [InstId] [0..1] Max16Text 可选 */
	public void setInstId(String InstId) {
		this.InstId = InstId;
	}

	/** 机构名称 [InstNm] [0..1] Max60Text 可选 */
	public String getInstNm() {
		return InstNm;
	}

	/** 机构名称 [InstNm] [0..1] Max60Text 可选 */
	public void setInstNm(String InstNm) {
		this.InstNm = InstNm;
	}

	/** 分支机构编码 [BrchId] [0..1] Max16Text 可选 */
	public String getBrchId() {
		return BrchId;
	}

	/** 分支机构编码 [BrchId] [0..1] Max16Text 可选 */
	public void setBrchId(String BrchId) {
		this.BrchId = BrchId;
	}

	/** 分支机构名称 [BrchNm] [0..1] Max60Text 可选 */
	public String getBrchNm() {
		return BrchNm;
	}

	/** 分支机构名称 [BrchNm] [0..1] Max60Text 可选 */
	public void setBrchNm(String BrchNm) {
		this.BrchNm = BrchNm;
	}

	/** 网点号 [SubBrchId] [0..1] Max16Text 可选 */
	public String getSubBrchId() {
		return SubBrchId;
	}

	/** 网点号 [SubBrchId] [0..1] Max16Text 可选 */
	public void setSubBrchId(String SubBrchId) {
		this.SubBrchId = SubBrchId;
	}

	/** 网点名称 [SubBrchNm] [0..1] Max60Text 可选 */
	public String getSubBrchNm() {
		return SubBrchNm;
	}

	/** 网点名称 [SubBrchNm] [0..1] Max60Text 可选 */
	public void setSubBrchNm(String subBranchName) {
		this.SubBrchNm = subBranchName;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Institution [InstType=").append(InstType).append(", InstId=").append(InstId).append(", InstNm=").append(InstNm).append(", BrchId=").append(BrchId).append(", BrchNm=").append(BrchNm)
				.append(", SubBrchId=").append(SubBrchId).append(", SubBrchNm=").append(SubBrchNm).append("]");
		return builder.toString();
	}
}
