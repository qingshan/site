package com.zdsoft.site.mina.model;

import java.math.BigDecimal;

import com.zdsoft.site.mina.model.customEnum.BidType;
import com.zdsoft.site.mina.model.customEnum.RepayMethod;

/**
 * 7.27  还款兑付信息（RepayBillInfo） 
 * @author cqyhm
 *
 */
public class RepayBillInfo {
//	兑付流水号  RepayBillSerial  < RepayBillSeril>  [0..1]  Max20Text  
	private String RepayBillSerial;
//	标的编号  BidNo  <BidNo>  [1..1]  Max60Text   
	private String BidNo;
//	标的类型  BidType  <BidType>  [1..1]  BidType  枚举 
	private BidType BidType;
//	付款方式  RepayMethod  < RepayMethod >  [1..1]  RepayMethod  枚举
	private RepayMethod RepayMethod;
//	兑付总额  RepaySumBalance  < RepaySumBala>  [0..1]  Amount   
	private BigDecimal RepaySumBalance;
//	资金账号  AccountIdentification 	<AcctId>  [1..1]  Max32Text  
	private String AccountIdentification;
//	应收总额 RewardBalance  <RewardBala>  [0..1]  Amount 兑付客户应收总额（包含收益） 
	private BigDecimal RewardBalance;
//	本次还款本金  CurrentRepayPrincipal<CurRepayPrcpl>  [1..1]  Amount  
	private BigDecimal CurrentRepayPrincipal;
//	本次还款利息  CurrentRepayProfit  <CurRepayPro>  [1..1]  Amount 
	private BigDecimal CurrentRepayProfit;
//	剩余未还金额  RestBalance  <RestBala>  [0..1]  Amount  
	private BigDecimal RestBalance;
//	手续费  FeeAmount  <FeeAmt>  [0..1]  Amount   
	private BigDecimal FeeAmount;
//	还款期次  RepayPeriod  < RepayPeriod >  [1..1]  Max32Text   
	private String RepayPeriod;
//	备注  Remark  < Remark >  [0..1]  Max128Text   
	private String Remark;
	
	public String getRepayBillSerial() {
		return RepayBillSerial;
	}
	public void setRepayBillSerial(String repayBillSerial) {
		RepayBillSerial = repayBillSerial;
	}
	public String getBidNo() {
		return BidNo;
	}
	public void setBidNo(String bidNo) {
		BidNo = bidNo;
	}
	public BidType getBidType() {
		return BidType;
	}
	public void setBidType(BidType bidType) {
		BidType = bidType;
	}
	public RepayMethod getRepayMethod() {
		return RepayMethod;
	}
	public void setRepayMethod(RepayMethod repayMethod) {
		RepayMethod = repayMethod;
	}
	public BigDecimal getRepaySumBalance() {
		return RepaySumBalance;
	}
	public void setRepaySumBalance(BigDecimal repaySumBalance) {
		RepaySumBalance = repaySumBalance;
	}
	public String getAccountIdentification() {
		return AccountIdentification;
	}
	public void setAccountIdentification(String accountIdentification) {
		AccountIdentification = accountIdentification;
	}
	public BigDecimal getRewardBalance() {
		return RewardBalance;
	}
	public void setRewardBalance(BigDecimal rewardBalance) {
		RewardBalance = rewardBalance;
	}
	public BigDecimal getCurrentRepayPrincipal() {
		return CurrentRepayPrincipal;
	}
	public void setCurrentRepayPrincipal(BigDecimal currentRepayPrincipal) {
		CurrentRepayPrincipal = currentRepayPrincipal;
	}
	public BigDecimal getCurrentRepayProfit() {
		return CurrentRepayProfit;
	}
	public void setCurrentRepayProfit(BigDecimal currentRepayProfit) {
		CurrentRepayProfit = currentRepayProfit;
	}
	public BigDecimal getRestBalance() {
		return RestBalance;
	}
	public void setRestBalance(BigDecimal restBalance) {
		RestBalance = restBalance;
	}
	public BigDecimal getFeeAmount() {
		return FeeAmount;
	}
	public void setFeeAmount(BigDecimal feeAmount) {
		FeeAmount = feeAmount;
	}
	public String getRepayPeriod() {
		return RepayPeriod;
	}
	public void setRepayPeriod(String repayPeriod) {
		RepayPeriod = repayPeriod;
	}
	public String getRemark() {
		return Remark;
	}
	public void setRemark(String remark) {
		Remark = remark;
	}
}
