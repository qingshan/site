package com.zdsoft.site.thread.aIntroduction.A4;

public class ClientThread extends Thread {
	  
    private Bank bank;
    /*用一个银行账户,构造线程对象*/
    public ClientThread(Bank bank) {
        this.bank = bank;
    }
    /*不停的扣款1000，如果扣款成功则不停的存款1000*/
    public void run() {
        while (true) {
            boolean ok = bank.withdraw(1000);
            if (ok) {
                bank.deposit(1000);
            }
        }
    }
}
