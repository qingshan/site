package com.zdsoft.site.event.observer;

import java.util.Observable;
import java.util.Observer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 对于壹個类而言，可以既是观察者又是被观察者，只要既继承 Observable 类，又实现 Observer 接口就可以了
 * 实现该接口的类在观察者模式中可以充当观察者，实现该类的时候需要实现这个接口中的update()方法。
 * @author cqyhm
 *
 */
public class ObserverA extends Observable implements Observer {

	private Logger logger=LoggerFactory.getLogger(getClass());
	
	@Override
	public void update(Observable o, Object arg) {
		ObserverB observerB = (ObserverB)o;
		logger.debug("observerB改变,新值 observerB.data是",observerB.data);
        this.setChanged();
        this.notifyObservers();
		
	}

}
