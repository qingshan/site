package com.zdsoft.tag;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class HtmlTag extends TagSupport {

	private static final long serialVersionUID = -4458165940027461418L;
	//head中显示的标题
	private String title="";
	//上下文路径
	private String contextPath="";
	
	private Boolean enabled=false; 
	
	private Logger logger=LoggerFactory.getLogger(getClass());
	@Override
	public int doStartTag() throws JspException {
		try {
			StringBuffer sb=new StringBuffer();
			sb.append("<!DOCTYPE html>");
			sb.append("<html>");
			sb.append("<head><title>");
			sb.append(title);
			sb.append("</title></head>");
			logger.debug("标签内容:[{}]{}",enabled,sb.toString());
			pageContext.getOut().write(sb.toString());	
//			Enumeration<String> keys=getValues();
//			while (keys.hasMoreElements()) {
//				String key = keys.nextElement();
//				logger.debug("{}={}",key,getValue(key));
//			}
		} catch (Exception e) {
			logger.error("生成html标签错误");
			e.printStackTrace();
		}
		
		return EVAL_BODY_INCLUDE;
	}
	@Override
	public int doEndTag() throws JspException {
		try {
			pageContext.getOut().write("</html>");	
		} catch (Exception e) {
			logger.error("生成html标签错误");
			e.printStackTrace();
		}
		return EVAL_PAGE;
	}
	@Override
	public void release() {
		super.release();
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getContextPath() {
		return contextPath;
	}
	public void setContextPath(String contextPath) {
		this.contextPath = contextPath;
	}
	public Boolean getEnabled() {
		return enabled;
	}
	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}
	
	
}
