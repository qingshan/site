package com.zdsoft.http;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageHandler;
import org.springframework.messaging.MessagingException;

public class UserMessageHandler implements MessageHandler {

	private static Logger logger = LoggerFactory.getLogger(UserMessageHandler.class);
	
	@Override
	public void handleMessage(Message<?> message) throws MessagingException {
		logger.debug("获取负载:{}",message.getPayload());
		logger.debug("获取头部:{}",message.getHeaders());
	}

}
