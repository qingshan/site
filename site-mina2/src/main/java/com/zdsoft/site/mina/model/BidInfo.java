package com.zdsoft.site.mina.model;

import java.math.BigDecimal;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

import com.zdsoft.site.mina.model.customEnum.AssureFlag;
import com.zdsoft.site.mina.model.customEnum.BidType;
import com.zdsoft.site.mina.model.customEnum.PayMethod;

/**
 * 标的信息（BidInfo）
 * 
 * @author cqyhm
 *
 */

@XmlAccessorType(XmlAccessType.FIELD)
public class BidInfo {
	/** 标的编号 BidNo [BidNo] [1..1] Max60Text */
	@XmlElement(name = "BidNo")
	private String BidNo;

	/** 标的类型 BidType [BidType] [1..1] BidType 枚举 */
	@XmlElement(name = "BidType")
	private BidType BidType;

	/** 标的描述 BidDescribe [BidDescri] [0..1] Max250Text */
	@XmlElement(name = "BidDescri")
	private String BidDescri;

	/** 资金账号 ManageAccount [MgeAcct] [1..1] Max20Text */
	@XmlElement(name = "MgeAcct")
	private String MgeAcct;

	/** 标的总额 BidTotBala [ BidTotBala ] [1..1] Amount */
	@XmlElement(name = "BidTotBala")
	private BigDecimal  BidTotBala;

	/** 预期年化收益 AnnualRate [AnnuRate] [1..1] Amount */
	@XmlElement(name = "AnnuRate")
	private BigDecimal AnnuRate;

	/** 产品期限 BidPeriod [BidPeriod] [0..1] Max20Text */
	@XmlElement(name = "BidPeriod")
	private String      BidPeriod;

	/** 标的份数 TotalCount [ TotalCount ] [0..1] Max20Text */
	@XmlElement(name = "TotalCount")
	private String      TotalCount;

	/** 每份金额 PerBalance [ PerBalance] [0..1] Amount */
	@XmlElement(name = "PerBalance")
	private BigDecimal  PerBalance;

	/** 担保标志 AssureFlag [AssureFlag] [0..1] AssureFlag 枚举 */
	@XmlElement(name = "AssureFlag")
	private AssureFlag  AssureFlag;

	/** 担保账号 AssureAccount [AssureAcc] [0..1] Account */
	@XmlElement(name = "AssureAcc")
	private Account 	AssureAcc;

	/** 担保公司编号 AssureComId [AssureComId] [0..1] Max20Text */
	@XmlElement(name = "AssureComId")
	private String 		AssureComId;

	/** 担保公司名称 AssureComName [AssComName] [0..1] Max60Text */
	@XmlElement(name = "AssComName")
	private String 		AssComName;

	/** 申请日期 ApplyDate [ ApplyDate ] [0..1] Date */
	@XmlElement(name = "ApplyDate")
	private String 		ApplyDate;

	/** 建立日期 BuildDate [ BuildDate ] [0..1] Date */
	@XmlElement(name = "BuildDate")
	private String 		BuildDate;

	/** 募集起始日 RaiseBeginDate [RaiseBegDate] [1..1] Date */
	@XmlElement(name = "RaiseBegDate")
	private String 		RaiseBegDate;

	/** 募集结束日 RaiseEndDate [RaiseEndDate] [1..1] Date */
	@XmlElement(name = "RaiseEndDate")
	private String 		RaiseEndDate;

	/** 约定开标日期 AgreeBuildDay [AgreeBuildDate] [0..1] Date */
	@XmlElement(name = "AgreeBuildDate")
	private String 		AgreeBuildDate;

	/** 标的截止日期 BidBeginDate [ BidEndDate] [0..1] Date */
	@XmlElement(name = "BidEndDate")
	private String 		BidEndDate;

	/** 还款方式 PayMethod [PayMeth] [1..1] PayMethod 枚举 */
	@XmlElement(name = "PayMeth")
	private PayMethod 	PayMeth;

	/** 还款期限 RepayPeriod [RepayPeri] [0..1] Max20Text */
	@XmlElement(name = "RepayPeri")
	private String 		RepayPeri;

	/** 应还资金总额 TotalPayBala [TotPayTax] [0..1] Amount */
	@XmlElement(name = "TotPayTax")
	private BigDecimal  TotPayTax;

	/** 应付利息总额 TotalTax [TotTax] [0..1] Amount */
	@XmlElement(name = "TotTax")
	private BigDecimal  TotTax;

	/** 月还金额 MonthlyBala [MonthBala] [0..1] Amount */
	@XmlElement(name = "MonthBala")
	private BigDecimal  MonthBala;

	/** 首次还款日期 FirstPayDay [FirstPayDay] [0..1] Date */
	@XmlElement(name = "FirstPayDay")
	private String 		FirstPayDay;

	/** 提前还款费率 FeeRate [FeeRate] [0..1] Amount */
	@XmlElement(name = "FeeRate")
	private BigDecimal  FeeRate;

	/** 摘要 Dgst [Dgst] [0..1] Max128Text */
	@XmlElement(name = "Dgst")
	private String Dgst;

	/** 标的编号 BidNo [BidNo] [1..1] Max60Text */
	public String getBidNo() {
		return BidNo;
	}

	/** 标的编号 BidNo [BidNo] [1..1] Max60Text */
	public void setBidNo(String bidNo) {
		BidNo = bidNo;
	}

	/** 标的类型 BidType [BidType] [1..1] BidType 枚举 */
	public BidType getBidType() {
		return BidType;
	}

	/** 标的类型 BidType [BidType] [1..1] BidType 枚举 */
	public void setBidType(BidType bidType) {
		BidType = bidType;
	}

	/** 标的描述 BidDescribe [BidDescri] [0..1] Max250Text */
	public String getBidDescri() {
		return BidDescri;
	}

	/** 标的描述 BidDescribe [BidDescri] [0..1] Max250Text */
	public void setBidDescri(String bidDescribe) {
		BidDescri = bidDescribe;
	}

	/** 资金账号 ManageAccount [MgeAcct] [1..1] Max20Text */
	public String getMgeAcct() {
		return MgeAcct;
	}

	/** 资金账号 ManageAccount [MgeAcct] [1..1] Max20Text */
	public void setMgeAcct(String manageAccount) {
		MgeAcct = manageAccount;
	}

	/** 标的总额 BidTotBala [ BidTotBala ] [1..1] Amount */
	public BigDecimal getBidTotBala() {
		return BidTotBala;
	}

	/** 标的总额 BidTotBala [ BidTotBala ] [1..1] Amount */
	public void setBidTotBala(BigDecimal bidTotBala) {
		BidTotBala = bidTotBala;
	}

	/** 预期年化收益 AnnuRate [AnnuRate] [1..1] Amount */
	public BigDecimal getAnnuRate() {
		return AnnuRate;
	}

	/** 预期年化收益 AnnualRate [AnnuRate] [1..1] Amount */
	public void setAnnuRate(BigDecimal annualRate) {
		AnnuRate = annualRate;
	}

	/** 产品期限 BidPeriod [BidPeriod] [0..1] Max20Text */
	public String getBidPeriod() {
		return BidPeriod;
	}

	/** 产品期限 BidPeriod [BidPeriod] [0..1] Max20Text */
	public void setBidPeriod(String bidPeriod) {
		BidPeriod = bidPeriod;
	}

	/** 标的份数 TotalCount [ TotalCount ] [0..1] Max20Text */
	public String getTotalCount() {
		return TotalCount;
	}

	/** 标的份数 TotalCount [ TotalCount ] [0..1] Max20Text */
	public void setTotalCount(String totalCount) {
		TotalCount = totalCount;
	}

	/** 每份金额 PerBalance [ PerBalance] [0..1] Amount */
	public BigDecimal getPerBalance() {
		return PerBalance;
	}

	/** 每份金额 PerBalance [ PerBalance] [0..1] Amount */
	public void setPerBalance(BigDecimal perBalance) {
		PerBalance = perBalance;
	}

	/** 担保标志 AssureFlag [AssureFlag] [0..1] AssureFlag 枚举 */
	public AssureFlag getAssureFlag() {
		return AssureFlag;
	}

	/** 担保标志 AssureFlag [AssureFlag] [0..1] AssureFlag 枚举 */
	public void setAssureFlag(AssureFlag assureFlag) {
		AssureFlag = assureFlag;
	}

	/** 担保账号 AssureAccount [AssureAcc] [0..1] Account */
	public Account getAssureAcc() {
		return AssureAcc;
	}

	/** 担保账号 AssureAccount [AssureAcc] [0..1] Account */
	public void setAssureAcc(Account assureAccount) {
		AssureAcc = assureAccount;
	}

	/** 担保公司编号 AssureComId [AssureComId] [0..1] Max20Text */
	public String getAssureComId() {
		return AssureComId;
	}

	/** 担保公司编号 AssureComId [AssureComId] [0..1] Max20Text */
	public void setAssureComId(String assureComId) {
		AssureComId = assureComId;
	}

	/** 担保公司名称 AssureComName [AssComName] [0..1] Max60Text */
	public String getAssComName() {
		return AssComName;
	}

	/** 担保公司名称 AssureComName [AssComName] [0..1] Max60Text */
	public void setAssComName(String assureComName) {
		AssComName = assureComName;
	}

	/** 申请日期 ApplyDate [ ApplyDate ] [0..1] Date */
	public String getApplyDate() {
		return ApplyDate;
	}

	/** 申请日期 ApplyDate [ ApplyDate ] [0..1] Date */
	public void setApplyDate(String applyDate) {
		ApplyDate = applyDate;
	}

	/** 建立日期 BuildDate [ BuildDate ] [0..1] Date */
	public String getBuildDate() {
		return BuildDate;
	}

	/** 建立日期 BuildDate [ BuildDate ] [0..1] Date */
	public void setBuildDate(String buildDate) {
		BuildDate = buildDate;
	}

	/** 募集起始日 RaiseBeginDate [RaiseBegDate] [1..1] Date */
	public String getRaiseBegDate() {
		return RaiseBegDate;
	}

	/** 募集起始日 RaiseBeginDate [RaiseBegDate] [1..1] Date */
	public void setRaiseBegDate(String raiseBeginDate) {
		RaiseBegDate = raiseBeginDate;
	}

	/** 募集结束日 RaiseEndDate [RaiseEndDate] [1..1] Date */
	public String getRaiseEndDate() {
		return RaiseEndDate;
	}

	/** 募集结束日 RaiseEndDate [RaiseEndDate] [1..1] Date */
	public void setRaiseEndDate(String raiseEndDate) {
		RaiseEndDate = raiseEndDate;
	}

	/** 约定开标日期 AgreeBuildDay [AgreeBuildDate] [0..1] Date */
	public String getAgreeBuildDate() {
		return AgreeBuildDate;
	}

	/** 约定开标日期 AgreeBuildDay [AgreeBuildDate] [0..1] Date */
	public void setAgreeBuildDate(String agreeBuildDay) {
		AgreeBuildDate = agreeBuildDay;
	}

	/** 标的截止日期 BidBeginDate [ BidEndDate] [0..1] Date */
	public String getBidEndDate() {
		return BidEndDate;
	}

	/** 标的截止日期 BidBeginDate [ BidEndDate] [0..1] Date */
	public void setBidEndDate(String bidBeginDate) {
		BidEndDate = bidBeginDate;
	}

	/** 还款方式 PayMethod [PayMeth] [1..1] PayMethod 枚举 */
	public PayMethod getPayMeth() {
		return PayMeth;
	}

	/** 还款方式 PayMethod [PayMeth] [1..1] PayMethod 枚举 */
	public void setPayMeth(PayMethod payMethod) {
		PayMeth = payMethod;
	}

	/** 还款期限 RepayPeriod [RepayPeri] [0..1] Max20Text */
	public String getRepayPeri() {
		return RepayPeri;
	}

	/** 还款期限 RepayPeriod [RepayPeri] [0..1] Max20Text */
	public void setRepayPeri(String repayPeriod) {
		RepayPeri = repayPeriod;
	}

	/** 应还资金总额 TotalPayBala [TotPayTax] [0..1] Amount */
	public BigDecimal getTotPayTax() {
		return TotPayTax;
	}

	/** 应还资金总额 TotalPayBala [TotPayTax] [0..1] Amount */
	public void setTotPayTax(BigDecimal totalPayBala) {
		TotPayTax = totalPayBala;
	}

	/** 应付利息总额 TotalTax [TotTax] [0..1] Amount */
	public BigDecimal getTotTax() {
		return TotTax;
	}

	/** 应付利息总额 TotalTax [TotTax] [0..1] Amount */
	public void setTotTax(BigDecimal totalTax) {
		TotTax = totalTax;
	}

	/** 月还金额 MonthlyBala [MonthBala] [0..1] Amount */
	public BigDecimal getMonthBala() {
		return MonthBala;
	}

	/** 月还金额 MonthlyBala [MonthBala] [0..1] Amount */
	public void setMonthBala(BigDecimal monthlyBala) {
		MonthBala = monthlyBala;
	}

	/** 首次还款日期 FirstPayDay [FirstPayDay] [0..1] Date */
	public String getFirstPayDay() {
		return FirstPayDay;
	}

	/** 首次还款日期 FirstPayDay [FirstPayDay] [0..1] Date */
	public void setFirstPayDay(String firstPayDay) {
		FirstPayDay = firstPayDay;
	}

	/** 提前还款费率 FeeRate [FeeRate] [0..1] Amount */
	public BigDecimal getFeeRate() {
		return FeeRate;
	}

	/** 提前还款费率 FeeRate [FeeRate] [0..1] Amount */
	public void setFeeRate(BigDecimal feeRate) {
		FeeRate = feeRate;
	}

	/** 摘要 Digest [Dgst] [0..1] Max128Text */
	public String getDgst() {
		return Dgst;
	}

	/** 摘要 Digest [Dgst] [0..1] Max128Text */
	public void setDgst(String digest) {
		Dgst = digest;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("BidInfo [BidNo=").append(BidNo).append(", BidType=").append(BidType).append(", BidDescri=").append(BidDescri).append(", MgeAcct=").append(MgeAcct).append(", BidTotBala=").append(BidTotBala).append(", AnnuRate=").append(AnnuRate).append(", BidPeriod=")
				.append(BidPeriod).append(", TotalCount=").append(TotalCount).append(", PerBalance=").append(PerBalance).append(", AssureFlag=").append(AssureFlag).append(", AssureAcc=").append(AssureAcc).append(", AssureComId=").append(AssureComId).append(", AssComName=")
				.append(AssComName).append(", ApplyDate=").append(ApplyDate).append(", BuildDate=").append(BuildDate).append(", RaiseBegDate=").append(RaiseBegDate).append(", RaiseEndDate=").append(RaiseEndDate).append(", AgreeBuildDate=").append(AgreeBuildDate).append(", BidEndDate=")
				.append(BidEndDate).append(", PayMeth=").append(PayMeth).append(", RepayPeri=").append(RepayPeri).append(", TotPayTax=").append(TotPayTax).append(", TotTax=").append(TotTax).append(", MonthBala=").append(MonthBala).append(", FirstPayDay=").append(FirstPayDay)
				.append(", FeeRate=").append(FeeRate).append(", Digest=").append(Dgst).append("]");
		return builder.toString();
	}
}
