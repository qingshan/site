package com.zdsoft.site.mina.model.customEnum;
/**
 * 金额类型(AmountType) 
 * @author cqyhm
 *
 */
public enum AmountType {
	A00,//  追保 
	A01 //  批划 
}
