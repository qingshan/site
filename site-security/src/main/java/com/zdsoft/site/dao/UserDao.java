package com.zdsoft.site.dao;

import com.zdsoft.site.domain.User;

public interface UserDao {

	public User findByCode(String username);

}
