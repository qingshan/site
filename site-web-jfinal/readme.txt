需要在conf/context.xml文件中配置以下作为JNDI数据源

<Resource 
     name="jdbc/p2pdb"
     factory="com.alibaba.druid.pool.DruidDataSourceFactory"
     auth="Container"
     type="javax.sql.DataSource"
     driverClassName="com.mysql.jdbc.Driver"
     url="jdbc:mysql://localhost:2222/p2p20_cqyhm?useUnicode=true&amp;characterEncoding=UTF-8&amp;zeroDateTimeBehavior=convertToNull" 
     username="root"
     password="123root456"
     maxActive="50"
     maxWait="10000"
     removeabandoned="true"
     removeabandonedtimeout="60"
     logabandoned="false"
     filters="stat"/>