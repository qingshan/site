package com.zdsoft.site.mina.model;

import java.math.BigDecimal;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

import com.zdsoft.site.mina.model.customEnum.BidType;
import com.zdsoft.site.mina.model.customEnum.RepayMethod;

/**
 * 7.27 还款兑付信息（RepayBillInfo）
 * 
 * @author cqyhm
 *
 */

@XmlAccessorType(XmlAccessType.FIELD)
public class RepayBillInfo {
	/** 兑付流水号 RepayBillSerial [ RepayBillSeril] [0..1] Max20Text */
	@XmlElement(name = "RepayBillSeril")
	private String RepayBillSerial;

	/** 标的编号 BidNo [BidNo] [1..1] Max60Text */
	@XmlElement(name = "BidNo")
	private String BidNo;

	/** 标的类型 BidType [BidType] [1..1] BidType 枚举 */
	@XmlElement(name = "BidType")
	private BidType BidType;

	/** 付款方式 RepayMethod [ RepayMethod ] [1..1] RepayMethod 枚举 */
	@XmlElement(name = "RepayMethod")
	private RepayMethod RepayMethod;

	/** 兑付总额 RepaySumBalance [ RepaySumBala] [0..1] Amount */
	@XmlElement(name = "RepaySumBala")
	private BigDecimal  RepaySumBala;

	/** 资金账号 AccountIdentification [AcctId] [1..1] Max32Text */
	@XmlElement(name = "AcctId")
	private String AcctId;

	/** 应收总额 RewardBalance [RewardBala] [0..1] Amount 兑付客户应收总额（包含收益） */
	@XmlElement(name = "RewardBala")
	private BigDecimal  RewardBala;

	/** 本次还款本金 CurrentRepayPrincipal[CurRepayPrcpl] [1..1] Amount */
	@XmlElement(name = "CurRepayPrcpl")
	private BigDecimal  CurRepayPrcpl;

	/** 本次还款利息 CurrentRepayProfit [CurRepayPro] [1..1] Amount */
	@XmlElement(name = "CurRepayPro")
	private BigDecimal  CurRepayPro;

	/** 剩余未还金额 RestBalance [RestBala] [0..1] Amount */
	@XmlElement(name = "RestBala")
	private BigDecimal  RestBala;

	/** 手续费 FeeAmount [FeeAmt] [0..1] Amount */
	@XmlElement(name = "FeeAmt")
	private BigDecimal 	FeeAmt;

	/** 还款期次 RepayPeriod [ RepayPeriod ] [1..1] Max32Text */
	@XmlElement(name = "RepayPeriod")
	private String 		RepayPeriod;

	/** 备注 Remark [ Remark ] [0..1] Max128Text */
	@XmlElement(name = "Remark")
	private String Remark;

	/** 兑付流水号 RepayBillSerial [ RepayBillSeril] [0..1] Max20Text */
	public String getRepayBillSerial() {
		return RepayBillSerial;
	}

	/** 兑付流水号 RepayBillSerial [ RepayBillSeril] [0..1] Max20Text */
	public void setRepayBillSerial(String repayBillSerial) {
		RepayBillSerial = repayBillSerial;
	}

	/** 标的编号 BidNo [BidNo] [1..1] Max60Text */
	public String getBidNo() {
		return BidNo;
	}

	/** 标的编号 BidNo [BidNo] [1..1] Max60Text */
	public void setBidNo(String bidNo) {
		BidNo = bidNo;
	}
	/** 标的类型 BidType [BidType] [1..1] BidType 枚举 */
	public BidType getBidType() {
		return BidType;
	}
	/** 标的类型 BidType [BidType] [1..1] BidType 枚举 */
	public void setBidType(BidType bidType) {
		BidType = bidType;
	}
	/** 付款方式 RepayMethod [ RepayMethod ] [1..1] RepayMethod 枚举 */
	public RepayMethod getRepayMethod() {
		return RepayMethod;
	}
	/** 付款方式 RepayMethod [ RepayMethod ] [1..1] RepayMethod 枚举 */
	public void setRepayMethod(RepayMethod repayMethod) {
		RepayMethod = repayMethod;
	}
	/** 兑付总额 RepaySumBalance [ RepaySumBala] [0..1] Amount */
	public BigDecimal getRepaySumBala() {
		return RepaySumBala;
	}
	/** 兑付总额 RepaySumBalance [ RepaySumBala] [0..1] Amount */
	public void setRepaySumBala(BigDecimal repaySumBalance) {
		RepaySumBala = repaySumBalance;
	}
	/** 资金账号 AccountIdentification [AcctId] [1..1] Max32Text */
	public String getAcctId() {
		return AcctId;
	}
	/** 资金账号 AccountIdentification [AcctId] [1..1] Max32Text */
	public void setAcctId(String accountIdentification) {
		AcctId = accountIdentification;
	}
	/** 应收总额 RewardBalance [RewardBala] [0..1] Amount 兑付客户应收总额（包含收益） */
	public BigDecimal getRewardBala() {
		return RewardBala;
	}
	/** 应收总额 RewardBalance [RewardBala] [0..1] Amount 兑付客户应收总额（包含收益） */
	public void setRewardBala(BigDecimal rewardBalance) {
		RewardBala = rewardBalance;
	}
	/** 本次还款本金 CurrentRepayPrincipal[CurRepayPrcpl] [1..1] Amount */
	public BigDecimal getCurRepayPrcpl() {
		return CurRepayPrcpl;
	}
	/** 本次还款本金 CurrentRepayPrincipal[CurRepayPrcpl] [1..1] Amount */
	public void setCurRepayPrcpl(BigDecimal currentRepayPrincipal) {
		CurRepayPrcpl = currentRepayPrincipal;
	}
	/** 本次还款利息 CurrentRepayProfit [CurRepayPro] [1..1] Amount */
	public BigDecimal getCurRepayPro() {
		return CurRepayPro;
	}
	/** 本次还款利息 CurrentRepayProfit [CurRepayPro] [1..1] Amount */
	public void setCurRepayPro(BigDecimal currentRepayProfit) {
		CurRepayPro = currentRepayProfit;
	}
	/** 剩余未还金额 RestBalance [RestBala] [0..1] Amount */
	public BigDecimal getRestBala() {
		return RestBala;
	}
	/** 剩余未还金额 RestBalance [RestBala] [0..1] Amount */
	public void setRestBala(BigDecimal restBalance) {
		RestBala = restBalance;
	}
	/** 手续费 FeeAmount [FeeAmt] [0..1] Amount */
	public BigDecimal getFeeAmt() {
		return FeeAmt;
	}
	/** 手续费 FeeAmount [FeeAmt] [0..1] Amount */
	public void setFeeAmt(BigDecimal feeAmount) {
		FeeAmt = feeAmount;
	}
	/** 还款期次 RepayPeriod [ RepayPeriod ] [1..1] Max32Text */
	public String getRepayPeriod() {
		return RepayPeriod;
	}
	/** 还款期次 RepayPeriod [ RepayPeriod ] [1..1] Max32Text */
	public void setRepayPeriod(String repayPeriod) {
		RepayPeriod = repayPeriod;
	}
	/** 备注 Remark [ Remark ] [0..1] Max128Text */
	public String getRemark() {
		return Remark;
	}
	/** 备注 Remark [ Remark ] [0..1] Max128Text */
	public void setRemark(String remark) {
		Remark = remark;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("RepayBillInfo [RepayBillSerial=").append(RepayBillSerial).append(", BidNo=").append(BidNo).append(", BidType=").append(BidType).append(", RepayMethod=").append(RepayMethod).append(", RepaySumBala=").append(RepaySumBala).append(", AcctId=")
				.append(AcctId).append(", RewardBala=").append(RewardBala).append(", CurRepayPrcpl=").append(CurRepayPrcpl).append(", CurRepayPro=").append(CurRepayPro).append(", RestBala=").append(RestBala).append(", FeeAmt=")
				.append(FeeAmt).append(", RepayPeriod=").append(RepayPeriod).append(", Remark=").append(Remark).append("]");
		return builder.toString();
	}
}
