package com.zdsoft.site.mina.model.response;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.springframework.beans.BeanUtils;

import com.zdsoft.site.mina.model.Account;
import com.zdsoft.site.mina.model.Balance;
import com.zdsoft.site.mina.model.Customer;
import com.zdsoft.site.mina.model.Reference;
import com.zdsoft.site.mina.model.customEnum.BankBindStatus;
import com.zdsoft.site.mina.model.customEnum.CustStatus;
import com.zdsoft.site.mina.utils.JaxbUtil;

/**
 *6.1.21　管理账户信息查询(46720)
 *说明：
 *	1.适用于服务商发起的对客户管理账户信息查询功能。
 *	2.可以查询客户托管账户状态，绑卡信息，余额信息等信息
 * 应答：
 * 
 * @author cqyhm
 *
 */
@XmlRootElement(name = "MsgText")
public class ResponseAcctQuery extends AbstractResponse {
	
	/** 03.银行流水号 BankReference <BkRef> [1..1] Reference 组件 */
	@XmlElement(name = "BkRef")
	private Reference BkRef;

	/** 04.客户信息 Customer <Cust> [0..1] Customer 组件 */
	@XmlElement(name = "Cust")
	private Customer Cust;

	/** 05.托管状态 CustStatus <CustStatus> [1..1] CustStatus 枚举 */
	@XmlElement(name = "CustStatus")
	private CustStatus CustStatus;

	/** 06.银行账户 BankAccount <BkAcct> [1..1] Account 组件 */
	@XmlElement(name = "BkAcct")
	private Account BkAcct;

	/** 07.银行卡绑定状态 BankBindStatus <BindStatus> [1..1] BankBindStatus 枚举 */
	@XmlElement(name = "BindStatus")
	private BankBindStatus BindStatus;

	/** 08.交易管理账户 ManageAccount < MgeAcct> [0..1] Account 组件 */
	@XmlElement(name = "MgeAcct")
	private Account MgeAcct;

	/** 09.可用余额 AvailBalance <AvaiBal> [1..1] Balance 组件 */
	@XmlElement(name = "AvaiBal")
	private Balance AvaiBal;

	/** 10.支付待收余额 DhxBalance <DhxBal> [1..1] Balance 组件 */
	@XmlElement(name = "DhxBal")
	private Balance DhxBal;

	/** 11.投标冻结金额 FrozenBalance <FroBal> [1..1] Balance 组件 */
	@XmlElement(name = "FroBal")
	private Balance FroBal;

	/** 12.资产余额 AssetBalance <AssetBal> [1..1] Balance 组件 */
	@XmlElement(name = "AssetBal")
	private Balance AssetBal;

	/** 13.摘要 Digest <Dgst> [0..1] Max128Text */
	@XmlElement(name = "Dgst")
	private String Dgst;

	public ResponseAcctQuery() {
	}

	public ResponseAcctQuery(String xml) {
		ResponseAcctQuery response = JaxbUtil.converyToJavaBean(xml, this.getClass());
		BeanUtils.copyProperties(response, this);
	}

	/** 03.银行流水号 BankReference <BkRef> [1..1] Reference 组件 */
	public Reference getBkRef() {
		return BkRef;
	}

	/** 03.银行流水号 BankReference <BkRef> [1..1] Reference 组件 */
	public void setBkRef(Reference bkRef) {
		BkRef = bkRef;
	}

	/** 04.客户信息 Customer <Cust> [0..1] Customer 组件 */
	public Customer getCust() {
		return Cust;
	}

	/** 04.客户信息 Customer <Cust> [0..1] Customer 组件 */
	public void setCust(Customer cust) {
		Cust = cust;
	}

	/** 05.托管状态 CustStatus <CustStatus> [1..1] CustStatus 枚举 */
	public CustStatus getCustStatus() {
		return CustStatus;
	}

	/** 05.托管状态 CustStatus <CustStatus> [1..1] CustStatus 枚举 */
	public void setCustStatus(CustStatus custStatus) {
		CustStatus = custStatus;
	}

	/** 06.银行账户 BankAccount <BkAcct> [1..1] Account 组件 */
	public Account getBkAcct() {
		return BkAcct;
	}

	/** 06.银行账户 BankAccount <BkAcct> [1..1] Account 组件 */
	public void setBkAcct(Account bkAcct) {
		BkAcct = bkAcct;
	}

	/** 07.银行卡绑定状态 BankBindStatus <BindStatus> [1..1] BankBindStatus 枚举 */
	public BankBindStatus getBindStatus() {
		return BindStatus;
	}

	/** 07.银行卡绑定状态 BankBindStatus <BindStatus> [1..1] BankBindStatus 枚举 */
	public void setBindStatus(BankBindStatus bindStatus) {
		BindStatus = bindStatus;
	}

	/** 08.交易管理账户 ManageAccount < MgeAcct> [0..1] Account 组件 */
	public Account getMgeAcct() {
		return MgeAcct;
	}

	/** 08.交易管理账户 ManageAccount < MgeAcct> [0..1] Account 组件 */
	public void setMgeAcct(Account mgeAcct) {
		MgeAcct = mgeAcct;
	}

	/** 09.可用余额 AvailBalance <AvaiBal> [1..1] Balance 组件 */
	public Balance getAvaiBal() {
		return AvaiBal;
	}

	/** 09.可用余额 AvailBalance <AvaiBal> [1..1] Balance 组件 */
	public void setAvaiBal(Balance avaiBal) {
		AvaiBal = avaiBal;
	}

	/** 10.支付待收余额 DhxBalance <DhxBal> [1..1] Balance 组件 */
	public Balance getDhxBal() {
		return DhxBal;
	}

	/** 10.支付待收余额 DhxBalance <DhxBal> [1..1] Balance 组件 */
	public void setDhxBal(Balance dhxBal) {
		DhxBal = dhxBal;
	}

	/** 11.投标冻结金额 FrozenBalance <FroBal> [1..1] Balance 组件 */
	public Balance getFroBal() {
		return FroBal;
	}

	/** 11.投标冻结金额 FrozenBalance <FroBal> [1..1] Balance 组件 */
	public void setFroBal(Balance froBal) {
		FroBal = froBal;
	}

	/** 12.资产余额 AssetBalance <AssetBal> [1..1] Balance 组件 */
	public Balance getAssetBal() {
		return AssetBal;
	}

	/** 12.资产余额 AssetBalance <AssetBal> [1..1] Balance 组件 */
	public void setAssetBal(Balance assetBal) {
		AssetBal = assetBal;
	}

	/** 13.摘要 Digest <Dgst> [0..1] Max128Text */
	public String getDgst() {
		return Dgst;
	}

	/** 13.摘要 Digest <Dgst> [0..1] Max128Text */
	public void setDgst(String dgst) {
		Dgst = dgst;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ResponseAcctQuery [BkRef=").append(BkRef).append(", Cust=").append(Cust).append(", CustStatus=").append(CustStatus).append(", BkAcct=").append(BkAcct).append(", BindStatus=").append(BindStatus).append(", MgeAcct=").append(MgeAcct).append(", AvaiBal=").append(AvaiBal)
				.append(", DhxBal=").append(DhxBal).append(", FroBal=").append(FroBal).append(", AssetBal=").append(AssetBal).append(", Dgst=").append(Dgst).append("]");
		return builder.toString();
	}
}
