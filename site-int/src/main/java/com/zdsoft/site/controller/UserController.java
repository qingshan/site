package com.zdsoft.site.controller;

import java.util.Enumeration;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.zdsoft.http.User;
import com.zdsoft.rock.common.model.domain.ResponseMsg;

@RequestMapping("/user")
@Controller
public class UserController {
	
	private Logger logger=LoggerFactory.getLogger(getClass());
	
	@RequestMapping("/save.html")
	@ResponseBody
	public ResponseMsg<User> save(@RequestBody User user,HttpServletRequest request,@RequestHeader("x-token")String token ){
		logger.debug("获取的token={}",token);
		Enumeration<String> names=request.getHeaderNames();
		while (names.hasMoreElements()) {
			String key = names.nextElement();
			logger.debug("服务端接收参数:{}={}",key,request.getHeader(key));
		}
		ResponseMsg<User> msg=new ResponseMsg<User>(user);
		//logger.debug("接收到头信息:{}",headers);
		logger.debug("接收JSON参数:{}",user);
		msg.setData(user);
		msg.setMsg("保存成功");
		return msg;
	}
	//无法接收到参数
//	@RequestMapping("/save.html")
//	@ResponseBody
//	public ResponseMsg<User> save(User user){
//		ResponseMsg<User> msg=new ResponseMsg<User>(user);
//		logger.debug("接收普通参数:{}",user);
//		msg.setData(user);
//		msg.setMsg("保存成功");
//		return msg;
//	}	
}
