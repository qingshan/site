package com.zdsoft.site.event.content;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

/**
 * 李四接收到消息
 */
@Component
public class LisiListener implements ApplicationListener<ApplicationEvent> {

	private Logger logger=LoggerFactory.getLogger(getClass());
    @Override
    public void onApplicationEvent(final ApplicationEvent event) {
        if(event instanceof ContentEvent) {
        	logger.debug("李四收到了新的内容:{}",event.getSource());
        }       
    }
}
