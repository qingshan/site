package com.zdsoft.site.mina.model.request;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

import com.zdsoft.site.mina.model.MessageHeader;
import com.zdsoft.site.mina.utils.JaxbUtil;
/**封装请求的方法公共部分*/
@XmlAccessorType(XmlAccessType.FIELD)
public class AbstractRequest {
	/**
	 * 请求消息头
	 */
	@XmlElement(name="MsgHdr")
	private MessageHeader MsgHdr;
	/**
	 * 请求消息头
	 */
	public MessageHeader getMsgHdr() {
		return MsgHdr;
	}
	/**
	 * 请求消息头
	 */
	public void setMsgHdr(MessageHeader msgHdr) {
		MsgHdr = msgHdr;
	}
	
	public String toXml(){
		return JaxbUtil.convertToXml(this,"GB2312",false);
	}
}
