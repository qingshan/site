package com.zdsoft.p2p.front.controller;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.zdsoft.p2p.front.domain.User;
import com.zdsoft.p2p.front.service.UserService;

@RequestMapping("/user")
@Controller
public class UserController {
	
	private Logger logger=LoggerFactory.getLogger(getClass());
	@Autowired
	private UserService userService;
	/**
	 * 进入登录页面
	 * @return
	 */
	@RequestMapping("/login.html")
	public String login(){
		return "login";
	}
	/**
	 * 进入推出登录页面
	 * @param session
	 * @return
	 */
	@RequestMapping("/logout.html")
	public String logout(HttpSession session){
		session.invalidate();
		return "login";
	}
	
	@RequestMapping("/save.html")
	@ResponseBody
	public User save(@Valid User user,BindingResult br){
		logger.debug("线程标识:{}",Thread.currentThread().getId());
		if(br.hasErrors()){
			List<FieldError> errors=br.getFieldErrors();
			for (FieldError err : errors) {
				logger.debug("字段[{}]{}",err.getField(),err.getDefaultMessage());
			}
		}
		user.setCreateTime(new Date());
		try {
			userService.save(user);	
		} catch (Exception e) {
			user.setUserNm("保存时出现异常");
		}
		return user;
	}
}
