package com.zdsoft.p2p.user.validator;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import static java.lang.annotation.ElementType.ANNOTATION_TYPE;
import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.METHOD;

import javax.validation.Constraint;
import javax.validation.Payload;

@Target({METHOD, FIELD, ANNOTATION_TYPE })
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = PasswordValidator.class)
@Documented
public @interface Password {
	
	String message() default "{org.hibernate.validator.constraints.Length.message}";
	
	Class<?>[] groups() default { };

	Class<? extends Payload>[] payload() default { };
	/**
	 * 需要比较的字段 判断是否相等
	 */
	String field();
}
