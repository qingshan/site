package com.zdsoft.p2p.finace.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.zdsoft.p2p.user.service.UserService;
import com.zdsoft.rock.common.controller.AbstractController;

@Controller
@RequestMapping("/finance")
public class FinanceController extends AbstractController{
	
	@RequestMapping("/index.do")
	public String index(){
		logger.debug("进入金融项目");
		return "finance/index";
	}
	
	@RequestMapping("/test.do")
	public ModelAndView test(){
		logger.debug("进入测试项目1");
		return new ModelAndView("finance/finance");
	}
	@Autowired
	private UserService userService;
	
	@RequestMapping("/userlist.do")
	public String userList(){
		logger.debug("通过金融模块计入用户模块");
		userService.sayHello("金融模块调用用户模块");
		return "redirect:/user/list.do";
	}
	
}
