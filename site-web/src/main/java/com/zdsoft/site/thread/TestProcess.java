package com.zdsoft.site.thread;

import java.io.IOException;
import java.util.Scanner;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TestProcess {
	private Logger logger = LoggerFactory.getLogger(getClass());
	/**
	 * 创建进程
	 * @throws IOException
	 */
	@Test
	public void test1() throws IOException {
		ProcessBuilder pb = new ProcessBuilder("cmd", "/c", "ipconfig/all");
		Process process = pb.start();
		Scanner scanner = new Scanner(process.getInputStream(),"GBK");

		while (scanner.hasNextLine()) {
			
			logger.debug(scanner.nextLine());
		}
		scanner.close();
	}
	/**
	 * 创建进程
	 * @throws IOException
	 */
	@Test
	public void test2() throws IOException {
		String cmd = "cmd " + "/c " + "ipconfig/all";
		//String cmd = "cmd " + "/c " + "java";
		Process process = Runtime.getRuntime().exec(cmd);
		Scanner scanner = new Scanner(process.getInputStream(),"GBK");

		while (scanner.hasNextLine()) {
			logger.debug(scanner.nextLine());
		}
		scanner.close();

	}

}
