package com.zdsoft.site.velocity;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.StringWriter;

import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class VeloCityTest {

	private Logger logger=LoggerFactory.getLogger(getClass());
	@Test
	public void test() throws FileNotFoundException {
		VelocityEngine ve = new VelocityEngine(); 
		//初始化环境，并将数据放入环境 
		ve.init();
		File file=new File("hello.vm");
		logger.debug("{}",file.getAbsolutePath());
		
		Template t = ve.getTemplate(file.getName()); 
		VelocityContext context = new VelocityContext(); 
		context.put("name", "Eiffel Qiu");
		context.put("site", "http://www.eiffelqiu.com"); 
		StringWriter writer = new StringWriter();
		t.merge(context, writer ); 
		logger.debug(writer.toString());
	}

}
