package com.zdsoft.site.mina.model.customEnum;

import javax.xml.bind.annotation.XmlEnumValue;

/**
 * 处理状态（DealStatus） 兑付状态
 * 
 * @author cqyhm
 *
 */
public enum DealStatus {
	/** 处理状态（DealStatus） 兑付状态 "0", "失败" */
	@XmlEnumValue("0") A0("0", "失败"),

	/** 处理状态（DealStatus） 兑付状态 "1", "成功" */
	@XmlEnumValue("1") A1("1", "成功");
	private String value;
	private String name;

	private DealStatus(String value, String name) {
		this.value = value;
		this.name = name;
	}

	@Override
	public String toString() {
		return value;
	}

	public String getValue() {
		return value;
	}

	public String getName() {
		return name;
	}
}
