package com.zdsoft.p2p.front.mapper;

import org.apache.ibatis.jdbc.SQL;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MyBatisUtils extends SQL {
	
	private Logger logger=LoggerFactory.getLogger(getClass());
	@Test
	public void testInsert() {
		//logger.debug("{}",getSelf());
		logger.debug("{}",INSERT_INTO("User"));
	}
	@Test
	public void testUpdate(){
		logger.debug("{}",UPDATE("User"));
	}
	@Test
	public void testSets(){
		logger.debug("{}",SET("User"));
	}
	@Test
	public void testSelect(){
		logger.debug("{}",SELECT("*").FROM("user"));
	}
}
