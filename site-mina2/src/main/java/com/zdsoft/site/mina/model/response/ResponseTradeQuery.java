package com.zdsoft.site.mina.model.response;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.springframework.beans.BeanUtils;

import com.zdsoft.site.mina.model.ReturnResult;
import com.zdsoft.site.mina.utils.JaxbUtil;

/**
 *6.1.10　交易结果查询(44711) 
 * 该功能用于服务商发起查询交易的结果
 * 目前支持查询的交易：
 * 1、服务商发起快捷支付入金(48704)
 * 2、服务商发起客户出金(44702)
 * 3、服务商收益划出(44705)
 * 4、服务商客户标的投标(42703)
 * 5、服务商发起客户开标确认(42705)
 * 6、服务商发起借款人还款(42707)
 * 7、服务商发起风险赔付(42708)
 * 8、服务商发起客户佣金派送(48709)
 * 应答：
 * 
 * @author cqyhm
 *
 */
@XmlRootElement(name = "MsgText")
public class ResponseTradeQuery extends AbstractResponse {

	/** 03 原交易结果 PreReturnResult <PreRst> [1..1] ReturnResult 组件 */
	@XmlElement(name = "PreRst")
	private ReturnResult PreRst;

	/** 04.摘要 Digest [Dgst] [0..1] Max128Text */
	@XmlElement(name = "Dgst")
	private String Dgst;

	public ResponseTradeQuery() {
	}

	public ResponseTradeQuery(String xml) {
		ResponseTradeQuery response = JaxbUtil.converyToJavaBean(xml, this.getClass());
		BeanUtils.copyProperties(response, this);
	}

	/** 03 原交易结果 PreReturnResult <PreRst> [1..1] ReturnResult 组件 */
	public ReturnResult getPreRst() {
		return PreRst;
	}

	/** 03 原交易结果 PreReturnResult <PreRst> [1..1] ReturnResult 组件 */
	public void setPreRst(ReturnResult preRst) {
		PreRst = preRst;
	}

	/** 04. 摘要 Digest [Dgst] [0..1] Max128Text */
	public String getDgst() {
		return Dgst;
	}

	/** 04.摘要 Digest [Dgst] [0..1] Max128Text */
	public void setDgst(String dgst) {
		Dgst = dgst;
	}

}
