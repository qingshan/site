package com.zdsoft.site.mina.model.response;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

import com.zdsoft.site.mina.model.MessageHeader;
import com.zdsoft.site.mina.model.ReturnResult;

/**封装返回的消息公共部分*/
@XmlAccessorType(XmlAccessType.FIELD)
public class AbstractResponse {
	
	public AbstractResponse() {
	}
	public AbstractResponse(String xml) {
	}
	/** 消息头 */
	@XmlElement(name = "MsgHdr")
	private MessageHeader MsgHdr;
	/** 返回结果 */
	@XmlElement(name = "Rst")
	private ReturnResult Rst;
	


	/** 消息头 */
	public MessageHeader getMsgHdr() {
		return MsgHdr;
	}

	/** 返回结果 */
	public void setMsgHdr(MessageHeader msgHdr) {
		MsgHdr = msgHdr;
	}

	/** 返回结果 */
	public ReturnResult getRst() {
		return Rst;
	}
	/** 返回结果 */
	public void setRst(ReturnResult rst) {
		Rst = rst;
	}	
}
