package com.zdsoft.site.mina.state;

public interface PlayGame {
	public void start();

	public void play();

	public void stop();
}
