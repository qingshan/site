package com.zdsoft.site.domain;

import java.io.Serializable;

public class Member implements Serializable{

	private static final long serialVersionUID = 2843338824707142206L;
	private String id;
	private String cd;
	private String nm;
	
	public Member(String id){
		this.id=id;
	}
	public Member(){}
	public Member(String id, String cd, String nm) {
		super();
		this.id = id;
		this.cd = cd;
		this.nm = nm;
	}
	
	
	public String getId() {
		return id;
	}
	
	public void setId(String id) {
		this.id = id;
	}
	public String getCd() {
		return cd;
	}
	public void setCd(String cd) {
		this.cd = cd;
	}
	public String getNm() {
		return nm;
	}
	public void setNm(String nm) {
		this.nm = nm;
	}
	@Override
	public String toString() {
		return "Member [id=" + id + ", cd=" + cd + ", nm=" + nm + "]";
	}
}
