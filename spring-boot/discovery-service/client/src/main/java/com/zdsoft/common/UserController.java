package com.zdsoft.common;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.cloud.client.loadbalancer.LoadBalancerClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserController {
	private Logger logger=LoggerFactory.getLogger(getClass());
	
	@Autowired  
    private LoadBalancerClient loadBalancer;  
	
	@Autowired  
    private DiscoveryClient discovery; 
	
	@RequestMapping("/discovery")  
    public String discovery() {  
		logger.info("{}",loadBalancer.choose("tomcat"));  
        return "discovery";  
    }  
	
	@RequestMapping("/all")  
    public String all() {  
		logger.info("{}",discovery.getServices());  
        return "all";  
    }
	@Autowired
	private UserClient userClient;
	
	@RequestMapping("/test1")
	public User test1(){
		logger.info("客户端调用微服务");
		User user=userClient.test();
		return user;
	}
}
