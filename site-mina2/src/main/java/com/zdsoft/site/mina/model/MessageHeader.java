package com.zdsoft.site.mina.model;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.zdsoft.site.mina.model.customEnum.InstitutionType;
import com.zdsoft.site.mina.model.customEnum.SystemType;
import com.zdsoft.site.mina.model.customEnum.YesNoIndicator;
import com.zdsoft.site.mina.utils.JaxbUtil;

/**
 *  消息头（MessgeHeader） 
 * @author cqyhm
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="MsgHdr")
public class MessageHeader implements Serializable {

	private static final long serialVersionUID = -6628244502508785879L;
	/**	版本 [Ver] [1..1] Max30Text 必选
	版本号：描述本消息体采用的版本号，版本号由标准委员会统一发布。*/
	@XmlElement(name="Ver")
	private String Ver="3.0.0";
	
	/**应用系统类型  [SysType] [1..1]  SystemType  可选，默认为监管 
	         应用系统类型：指示本消息所属的业务系统，比如监管、普通银证转账等。*/
	@XmlElement(name="SysType")
	private SystemType SysType=SystemType.A4;
	
	/**业务功能码    [InstrCd] [1..1]  InstructionCode  枚举  必选 
	         业务功能码：指示本消息完成的业务功能。*/ 
	@XmlElement(name="InstrCd")
	private String InstrCd; 
	
	/**交易发起方    [TradSrc] [1..1]  InstitutionType  枚举 必选 
	         交易发起方：指示本次交易的发起方机构类型，比如是银行发起还是服务商发起。*/ 
	@XmlElement(name="TradSrc")
	private InstitutionType TradSrc=InstitutionType.A0;
	
	/**服务商机构信息   [SvInst] [1..1]  Institution  组件 必选 
	         服务商机构信息：指示本消息的服务商机构信息，包括机构类别、机构代码、营业部机构代码等信息。*/
	@XmlElement(name="SvInst")
	private Institution SvInst; 
	
	/**银行机构信息   [BkInst] [1..1]  Institution  组件 必选
	         银行机构信息：指示本消息的银行机构信息，包括机构类别，机构代码，分支机构代码、网点号等信息。*/ 
	@XmlElement(name="BkInst")
	private Institution BkInst; 
	
	/**发生日期[Date] [0..1]  Date 交易日期  YYYYMMDD 可选*/ 
	@XmlElement(name="Date")
	private String Date;   
	
	/**发生时间  [Time] [0..1]  Time  交易时间  HHMMSS 可选*/ 
	@XmlElement(name="Time")
	private String Time; 
	
	/**请求流水号    [RqRef ] [1..1]  Reference  组件 必选 
		消息流水号：本消息体的唯一标识号,如流水号。 */
	@XmlElement(name="RqRef")
	private Reference RqRef;
	
	/**最后分片标志   [LstFrag] [0..1]  YesNoIndicator
	  	最后分片标志：是否为一揽子消息体中的最后一个，Y是，N否。   可选*/ 
	@XmlElement(name="LstFrag")
	private YesNoIndicator LstFrag=YesNoIndicator.Y;

	/**	版本 [Ver] [1..1] Max30Text 必选
	版本号：描述本消息体采用的版本号，版本号由标准委员会统一发布。*/
	public String getVer() {
		return Ver;
	}
	/**	版本 [Ver] [1..1] Max30Text 必选
	版本号：描述本消息体采用的版本号，版本号由标准委员会统一发布。*/
	public void setVer(String version) {
		Ver = version;
	}
	/**发生日期[Date] [0..1]  Date 交易日期  YYYYMMDD 可选*/ 
	public String getDate() {
		return Date;
	}
	/**发生日期[Date] [0..1]  Date 交易日期  YYYYMMDD 可选*/ 
	public void setDate(String Date) {
		this.Date = Date;
	}
	/**发生日期[Date] [0..1]  Date 交易日期  YYYYMMDD 可选,默认为当前日期*/ 
	public void setDate() {
		//日期为当前日期
		Date d=new Date();
		DateFormat df1=new SimpleDateFormat("yyyyMMdd");
		this.Date =df1.format(d) ;
		//时间为当前时间
		df1=new SimpleDateFormat("HHmmss");
		this.Time = df1.format(d);
	}
	/**发生时间  [Time] [0..1]  Time  交易时间  HHMMSS 可选*/ 
	public String getTime() {
		return Time;
	}
	/**发生时间  [Time] [0..1]  Time  交易时间  HHMMSS 可选*/ 
	public void setTime(String Time) {
		this.Time = Time;
	}	
	/**服务商机构信息   [SvInst] [1..1]  Institution  组件 必选 
    	服务商机构信息：指示本消息的服务商机构信息，包括机构类别、机构代码、营业部机构代码等信息。*/
	public void setSvInst(Institution svInst) {
		this.SvInst = svInst;
	}
	/**服务商机构信息   [SvInst] [1..1]  Institution  组件 必选 
	服务商机构信息：指示本消息的服务商机构信息，包括机构类别、机构代码、营业部机构代码等信息。*/
	public Institution getSvInst() {
		return SvInst;
	}	
	/**银行机构信息   [BkInst] [1..1]  Institution  组件 必选
    	银行机构信息：指示本消息的银行机构信息，包括机构类别，机构代码，分支机构代码、网点号等信息。*/
	public void setBkInst(Institution institution) {
		this.BkInst = institution;
	}
	/**银行机构信息   [BkInst] [1..1]  Institution  组件 必选
    	银行机构信息：指示本消息的银行机构信息，包括机构类别，机构代码，分支机构代码、网点号等信息。*/
	public Institution getBkInst() {
		return BkInst;
	}
	
	/**请求流水号    [RqRef ] [1..1]  Reference  组件 必选 
	消息流水号：本消息体的唯一标识号,如流水号。 */
	public Reference getRqRef() {
		return RqRef;
	}
	/**
	 * 请求流水号    RqRef [1..1]  Reference  组件 必选 
	 * 消息流水号：本消息体的唯一标识号,如流水号。 
	 */
	public void setRqRef(Reference reference) {
		RqRef = reference;
	}
	/**业务功能码    [InstrCd] [1..1]  InstructionCode  枚举  必选 
    	业务功能码：指示本消息完成的业务功能。*/ 
	public String getInstrCd() {
		return InstrCd;
	}
	/**业务功能码    [InstrCd] [1..1]  InstructionCode  枚举  必选 
    	业务功能码：指示本消息完成的业务功能。*/ 
	public void setInstrCd(String InstrCd) {
		this.InstrCd = InstrCd;
	}
	/**交易发起方：指示本次交易的发起方机构类型，比如是银行发起还是服务商发起*/
	public InstitutionType getTradSrc() {
		return TradSrc;
	}
	/**交易发起方：指示本次交易的发起方机构类型，比如是银行发起还是服务商发起*/
	public void setTradSrc(InstitutionType tradeSource) {
		TradSrc = tradeSource;
	}
	/**应用系统类型  [SysType] [1..1]  SystemType  可选，默认为监管 
    	应用系统类型：指示本消息所属的业务系统，比如监管、普通银证转账等。*/
	public SystemType getSysType() {
		return SysType;
	}
	/**应用系统类型  [SysType] [1..1]  SystemType  可选，默认为监管 
    	应用系统类型：指示本消息所属的业务系统，比如监管、普通银证转账等。*/
	public void setSysType(SystemType systemType) {
		SysType = systemType;
	}
	/**最后分片标志   [LstFrag] [0..1]  YesNoIndicator
  	最后分片标志：是否为一揽子消息体中的最后一个，Y是，N否。   可选*/
	public YesNoIndicator getLstFrag() {
		return LstFrag;
	}
	/**最后分片标志   [LstFrag] [0..1]  YesNoIndicator
  	最后分片标志：是否为一揽子消息体中的最后一个，Y是，N否。   可选*/
	public void setLstFrag(YesNoIndicator lastFragment) {
		LstFrag = lastFragment;
	} 
	/**
	 * 把对象转换为xml字符串
	 * 默认不格式化所有内容都放在一行中
	 * */
	public String toXml(){
		return toXml(false);
	}
	/**把对象转换为xml字符串**/
	public String toXml(boolean format){
		return JaxbUtil.convertToXml(this,format);
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("MessgeHeader [Ver=").append(Ver).append(", SysType=").append(SysType).append(", InstrCd=").append(InstrCd).append(", TradSrc=").append(TradSrc).append(SvInst)
				.append(BkInst).append(", Date=").append(Date).append(", Time=").append(Time).append(RqRef).append(", LstFrag=").append(LstFrag).append("]");
		return builder.toString();
	}
}
