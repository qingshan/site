package com.zdsoft.site.mina.client;

import org.apache.mina.core.filterchain.IoFilterAdapter;
import org.apache.mina.core.filterchain.IoFilterChain;
import org.apache.mina.core.session.IdleStatus;
import org.apache.mina.core.session.IoSession;
import org.apache.mina.core.write.WriteRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MinaTimeFilter extends IoFilterAdapter {
	
	private static Logger logger=LoggerFactory.getLogger(MinaTimeFilter.class);
	@Override
	public void init() throws Exception {
		super.init();
		logger.debug("自定义过滤器: init");
	}

	@Override
	public void destroy() throws Exception {
		super.destroy();
		logger.debug("自定义过滤器: destroy");
	}

	@Override
	public void onPreAdd(IoFilterChain parent, String name, NextFilter nextFilter) throws Exception {
		super.onPreAdd(parent, name, nextFilter);
		logger.debug("自定义过滤器: onPreAdd");
	}

	@Override
	public void onPostAdd(IoFilterChain parent, String name, NextFilter nextFilter) throws Exception {
		super.onPostAdd(parent, name, nextFilter);
		logger.debug("自定义过滤器: onPostAdd");
	}

	@Override
	public void onPreRemove(IoFilterChain parent, String name, NextFilter nextFilter) throws Exception {
		super.onPreRemove(parent, name, nextFilter);
		logger.debug("自定义过滤器: onPreRemove");
	}

	@Override
	public void onPostRemove(IoFilterChain parent, String name, NextFilter nextFilter) throws Exception {
		super.onPostRemove(parent, name, nextFilter);
		logger.debug("自定义过滤器: onPostRemove");
	}

	@Override
	public void sessionCreated(NextFilter nextFilter, IoSession session) throws Exception {
		super.sessionCreated(nextFilter, session);
		logger.debug("自定义过滤器: sessionCreated");
	}

	@Override
	public void sessionOpened(NextFilter nextFilter, IoSession session) throws Exception {
		super.sessionOpened(nextFilter, session);
		logger.debug("自定义过滤器: sessionOpened");
	}

	@Override
	public void sessionClosed(NextFilter nextFilter, IoSession session) throws Exception {
		super.sessionClosed(nextFilter, session);
		logger.debug("自定义过滤器: sessionClosed");
	}

	@Override
	public void sessionIdle(NextFilter nextFilter, IoSession session, IdleStatus status) throws Exception {
		super.sessionIdle(nextFilter, session, status);
		logger.debug("自定义过滤器: sessionIdle");
	}

	@Override
	public void exceptionCaught(NextFilter nextFilter, IoSession session, Throwable cause) throws Exception {
		super.exceptionCaught(nextFilter, session, cause);
		logger.debug("自定义过滤器: exceptionCaught");
	}

	@Override
	public void messageReceived(NextFilter nextFilter, IoSession session, Object message) throws Exception {
		super.messageReceived(nextFilter, session, message);
		logger.debug("自定义过滤器: messageReceived");
	}

	@Override
	public void messageSent(NextFilter nextFilter, IoSession session, WriteRequest writeRequest) throws Exception {
		super.messageSent(nextFilter, session, writeRequest);
		logger.debug("自定义过滤器: messageSent");
	}

	@Override
	public void filterWrite(NextFilter nextFilter, IoSession session, WriteRequest writeRequest) throws Exception {
		super.filterWrite(nextFilter, session, writeRequest);
		logger.debug("自定义过滤器: filterWrite");
	}

	@Override
	public void filterClose(NextFilter nextFilter, IoSession session) throws Exception {
		super.filterClose(nextFilter, session);
		logger.debug("自定义过滤器: filterClose");
	}

	@Override
	public void inputClosed(NextFilter nextFilter, IoSession session) throws Exception {
		super.inputClosed(nextFilter, session);
		logger.debug("自定义过滤器: inputClosed");
	}

	@Override
	public String toString() {
		return super.toString();
	}

}
