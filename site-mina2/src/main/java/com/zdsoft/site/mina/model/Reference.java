package com.zdsoft.site.mina.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.zdsoft.site.mina.model.customEnum.InstitutionType;
/**
 * 7.3　流水号(Reference)
 * @author cqyhm
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="RqRef")
public class Reference {
	/**
	 * 流水号    [Ref]  [1..1]  Max20Text 
	 *流水号：本次交易的唯一标识符(必选)*/ 
	@XmlElement(name="Ref")
	private String Ref;
	
	/**
	 * 流水号发布者类型    [IssrType]  [0..1]  InstitutionType  可选
	 * 流水号发布者类型：本流水号发布者的机构类型,机构类别，取值参考 InstitutionType 
	 */
	@XmlElement(name="IssrType")
	private InstitutionType IssrType=InstitutionType.A0;
	
	/**发布者  ReferenceIssure  [RefIssr]  [0..1]  Max30Text
	 * 本流水号发布者的机构代码 
	 */
	@XmlElement(name="RefIssr")
	private String RefIssr;

	
	/**
	 * 流水号    [Ref]  [1..1]  Max20Text 
	 *流水号：本次交易的唯一标识符(必选)*/ 
	public String getRef() {
		return Ref;
	}

	/**
	 * 流水号    [Ref]  [1..1]  Max20Text 
	 *流水号：本次交易的唯一标识符(必选)*/ 
	public void setRef(String reference) {
		this.Ref = reference;
	}

	/**发布者  ReferenceIssure  [RefIssr]  [0..1]  Max30Text
	 * 本流水号发布者的机构代码 
	 */
	public String getRefIssr() {
		return RefIssr;
	}

	/**发布者  ReferenceIssure  [RefIssr]  [0..1]  Max30Text
	 * 本流水号发布者的机构代码 
	 */
	public void setRefIssr(String referenceIssure) {
		this.RefIssr = referenceIssure;
	}
	/**
	 * 流水号发布者类型    [IssrType]  [0..1]  InstitutionType  可选
	 * 流水号发布者类型：本流水号发布者的机构类型,机构类别，取值参考 InstitutionType 
	 */
	public InstitutionType getIssrType() {
		return IssrType;
	}
	/**
	 * 流水号发布者类型    [IssrType]  [0..1]  InstitutionType  可选
	 * 流水号发布者类型：本流水号发布者的机构类型,机构类别，取值参考 InstitutionType 
	 */
	public void setIssrType(InstitutionType institutionType) {
		this.IssrType = institutionType;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Reference [Ref=").append(Ref).append(", IssrType=").append(IssrType).append(", RefIssr=").append(RefIssr).append("]");
		return builder.toString();
	}

}
