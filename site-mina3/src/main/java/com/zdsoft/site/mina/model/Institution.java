package com.zdsoft.site.mina.model;

import com.zdsoft.site.mina.model.customEnum.InstitutionType;
/**
 * 7.4   机构信息（Institution）
 * @author cqyhm
 */
public class Institution {
//	机构类型   <InstType>  [0..1]  InstitutionType  枚举   可选 
	private InstitutionType institutionType;
	
//	机构标识    <InstId>  [0..1]  Max16Text    可选 
	private String institutionIdentifier;
	
//	机构名称    <InstNm>  [0..1]  Max60Text   可选
	private String institutionName;
	
//	分支机构编码    <BrchId>  [0..1]  Max16Text    可选
	private String branchIdentifier;
	
//	分支机构名称    <BrchNm>  [0..1]  Max60Text   可选
	private String branchName;
	
//	网点号    <SubBrchId>  [0..1]  Max16Text   可选
	private String subBranchIdentifier;
	
//	网点名称    <SubBrchNm>  [0..1]  Max60Text   可选
	private String subBranchName;

	public InstitutionType getInstitutionType() {
		return institutionType;
	}

	public void setInstitutionType(InstitutionType institutionType) {
		this.institutionType = institutionType;
	}

	public String getInstitutionIdentifier() {
		return institutionIdentifier;
	}

	public void setInstitutionIdentifier(String institutionIdentifier) {
		this.institutionIdentifier = institutionIdentifier;
	}

	public String getInstitutionName() {
		return institutionName;
	}

	public void setInstitutionName(String institutionName) {
		this.institutionName = institutionName;
	}

	public String getBranchIdentifier() {
		return branchIdentifier;
	}

	public void setBranchIdentifier(String branchIdentifier) {
		this.branchIdentifier = branchIdentifier;
	}

	public String getBranchName() {
		return branchName;
	}

	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}

	public String getSubBranchIdentifier() {
		return subBranchIdentifier;
	}

	public void setSubBranchIdentifier(String subBranchIdentifier) {
		this.subBranchIdentifier = subBranchIdentifier;
	}

	public String getSubBranchName() {
		return subBranchName;
	}

	public void setSubBranchName(String subBranchName) {
		this.subBranchName = subBranchName;
	}
}
