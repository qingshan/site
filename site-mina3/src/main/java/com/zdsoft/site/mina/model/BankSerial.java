package com.zdsoft.site.mina.model;

import java.math.BigDecimal;

/**
 * 7.25  银行流水信息（BankSerial） 
 * @author cqyhm
 *
 */
public class BankSerial {
//	银行流水号  BankSerial  <BkRef>  [1..1]  Reference  组件
	private Reference BankSerial;
//	银行账户  BankAccount  <BkAcct>  [1..1]  Account  组件 
	private Account BankAccount;
//	成功转帐金额  SuccessTransferAmount  <SucTrfAmt>  [1..1]  Amount   
	private BigDecimal SuccessTransferAmount;
//	转账日期  TransferDate  <TrfDate>  [1..1]  Date   
	private String TransferDate;
//	转账时间  TransferTime  <TrfTime>  [1..1]  Time
	private String TransferTime;
//	摘要  Digest  <Dgst>  [0..1]  Max128Text   
	private String Digest;
	public Reference getBankSerial() {
		return BankSerial;
	}
	public void setBankSerial(Reference bankSerial) {
		BankSerial = bankSerial;
	}
	public Account getBankAccount() {
		return BankAccount;
	}
	public void setBankAccount(Account bankAccount) {
		BankAccount = bankAccount;
	}
	public BigDecimal getSuccessTransferAmount() {
		return SuccessTransferAmount;
	}
	public void setSuccessTransferAmount(BigDecimal successTransferAmount) {
		SuccessTransferAmount = successTransferAmount;
	}
	public String getTransferDate() {
		return TransferDate;
	}
	public void setTransferDate(String transferDate) {
		TransferDate = transferDate;
	}
	public String getTransferTime() {
		return TransferTime;
	}
	public void setTransferTime(String transferTime) {
		TransferTime = transferTime;
	}
	public String getDigest() {
		return Digest;
	}
	public void setDigest(String digest) {
		Digest = digest;
	}
}
