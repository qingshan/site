package com.zdsoft.p2p.user.service;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.zdsoft.p2p.TestAbstract;
import com.zdsoft.p2p.user.domain.User;
public class UserServiceTest extends TestAbstract {

	@Autowired
	private UserService userService;
	@Test
	public void testSave() {
		User user=new User();
		user.setUserCd("zhangsan");
		user.setUserNm("张三");
		userService.save(user);
	}

}
