package com.zdsoft.site.service;
/**
 * 此处通过OAuthService实现进行auth code和access token的维护。
 * @author yhm
 *
 */
public interface OAuthService {
	/**添加 auth code*/
	public void addAuthCode(String authCode, String username);

	/**添加accesstoken*/
	public void addAccessToken(String accessToken, String username);

	/**验证auth code是否有效*/
	public boolean checkAuthCode(String authCode);

	/**验证access token是否有效*/
	public boolean checkAccessToken(String accessToken);

	/**根据auth code获取用户名*/
	public String getUsernameByAuthCode(String authCode);

	/**根据access token获取用户名*/
	public String getUsernameByAccessToken(String accessToken);

	/**auth code / access token 过期时间*/
	public long getExpireIn();

	/**检查客户端id是否存在*/
	public boolean checkClientId(String clientId);

	/**坚持客户端安全KEY是否存在*/
	public boolean checkClientSecret(String clientSecret);
}
