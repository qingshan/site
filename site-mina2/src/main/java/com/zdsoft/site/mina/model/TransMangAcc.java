package com.zdsoft.site.mina.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

import com.zdsoft.site.mina.model.customEnum.AccountStatus;
import com.zdsoft.site.mina.model.customEnum.AccountType;

/**
 * 7.10  交易管理账号账户（TransMangAcc ） 
 * @author cqyhm
 *
 */

@XmlAccessorType(XmlAccessType.FIELD)
public class TransMangAcc {
	/**账号  AccountIdentification  [Id]  [1..1]  Max32Text*/
	@XmlElement(name="Id")
	private String Id;
	
	/**户主名称  AccountName  [Name]  [0..1]  Max60Text*/
	@XmlElement(name="Name")
	private String Name;
	
	/**账户类别  AccountType  [Type]  [0..1]  AccountType  枚举*/
	@XmlElement(name="Type")
	private AccountType Type;
	
	/**账户状态  AccountStatus  [Status]  [0..1]  AccountStatus  枚举*/
	@XmlElement(name="Status")
	private AccountStatus Status;
	
	/**密码  Password  [Pwd]  [0..1]  Password  组件*/
	@XmlElement(name="Pwd")
	private Password Pwd;
	
	/**开户日期  RegisterDate  [RegDt]  [0..1]  Date*/
	@XmlElement(name="RegDt")
	private String RegDt;
	
	/**有效日期  ValidDate  [VldDt]  [0..1]  Date*/
	@XmlElement(name="VldDt")
	private String VldDt;

	/**账号  AccountIdentification  [Id]  [1..1]  Max32Text*/
	public String getId() {
		return Id;
	}
	/**账号  AccountIdentification  [Id]  [1..1]  Max32Text*/
	public void setId(String Id) {
		this.Id = Id;
	}
	/**户主名称  AccountName  [Name]  [0..1]  Max60Text*/
	public String getName() {
		return Name;
	}
	/**户主名称  AccountName  [Name]  [0..1]  Max60Text*/
	public void setName(String accountName) {
		Name = accountName;
	}
	/**账户类别  AccountType  [Type]  [0..1]  AccountType  枚举*/
	public AccountType getType() {
		return Type;
	}
	/**账户类别  AccountType  [Type]  [0..1]  AccountType  枚举*/
	public void setType(AccountType accountType) {
		Type = accountType;
	}
	/**账户状态  AccountStatus  [Status]  [0..1]  AccountStatus  枚举*/
	public AccountStatus getStatus() {
		return Status;
	}
	/**账户状态  AccountStatus  [Status]  [0..1]  AccountStatus  枚举*/
	public void setStatus(AccountStatus accountStatus) {
		Status = accountStatus;
	}
	/**密码  Password  [Pwd]  [0..1]  Password  组件*/
	public Password getPwd() {
		return Pwd;
	}
	/**密码  Password  [Pwd]  [0..1]  Password  组件*/
	public void setPwd(Password password) {
		Pwd = password;
	}
	/**开户日期  RegisterDate  [RegDt]  [0..1]  Date*/
	public String getRegDt() {
		return RegDt;
	}
	/**开户日期  RegisterDate  [RegDt]  [0..1]  Date*/
	public void setRegDt(String registerDate) {
		RegDt = registerDate;
	}
	/**有效日期  ValidDate  [VldDt]  [0..1]  Date*/
	public String getVldDt() {
		return VldDt;
	}
	/**有效日期  ValidDate  [VldDt]  [0..1]  Date*/
	public void setVldDt(String validDate) {
		VldDt = validDate;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("TransMangAcc [Id=").append(Id).append(", Name=").append(Name).append(", Type=").append(Type).append(", Status=").append(Status).append(", Pwd=").append(Pwd).append(", RegDt=")
				.append(RegDt).append(", VldDt=").append(VldDt).append("]");
		return builder.toString();
	}
	
}
