package com.zdsoft.site.mina.model.customEnum;
/**
 * 支付公司编号（PaymentId） 
 * @author cqyhm
 *
 */
public enum PaymentId {
	A80080000,//中金支付有限公司 
	A80250000,//连连银通电子支付有限公司 
	A80360000,//通联支付网络服务股份有限公司 
	A80830000//银联商务有限公司 
}
