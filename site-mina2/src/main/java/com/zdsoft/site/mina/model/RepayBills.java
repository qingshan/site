package com.zdsoft.site.mina.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

/**
 * 7.26 还款兑付信息集合（RepayBills）
 * 
 * @author cqyhm
 *
 */

@XmlAccessorType(XmlAccessType.FIELD)
public class RepayBills {
	/** 还款兑付信息 RepayBillInfo [RepayBillInfo] [0..1] RepayBillInfo 组件 */
	private RepayBillInfo RepayBillInfo1;

	/** 还款兑付信息 RepayBillInfo [RepayBillInfo] [0..1] RepayBillInfo 组件 */
	private RepayBillInfo RepayBillInfo2;

	/** 还款兑付信息 RepayBillInfo [RepayBillInfo] [0..1] RepayBillInfo 组件 */
	public RepayBillInfo getRepayBillInfo1() {
		return RepayBillInfo1;
	}

	/** 还款兑付信息 RepayBillInfo [RepayBillInfo] [0..1] RepayBillInfo 组件 */
	public void setRepayBillInfo1(RepayBillInfo repayBillInfo1) {
		RepayBillInfo1 = repayBillInfo1;
	}

	/** 还款兑付信息 RepayBillInfo [RepayBillInfo] [0..1] RepayBillInfo 组件 */
	public RepayBillInfo getRepayBillInfo2() {
		return RepayBillInfo2;
	}

	/** 还款兑付信息 RepayBillInfo [RepayBillInfo] [0..1] RepayBillInfo 组件 */
	public void setRepayBillInfo2(RepayBillInfo repayBillInfo2) {
		RepayBillInfo2 = repayBillInfo2;
	}
}
