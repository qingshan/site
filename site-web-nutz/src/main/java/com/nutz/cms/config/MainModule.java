package com.nutz.cms.config;

import org.nutz.mvc.annotation.IocBy;
import org.nutz.mvc.annotation.Modules;
import org.nutz.mvc.annotation.SetupBy;
import org.nutz.mvc.ioc.provider.ComboIocProvider;

/**
 * 
 *  ComboIocProvider的args参数, 星号开头的是类名或内置缩写,剩余的是各加载器的参数
 *	js   是JsonIocLoader,负责加载js/json结尾的ioc配置文件
 *	anno 是AnnotationIocLoader,负责处理注解式Ioc, 例如@IocBean
 *	tx 是TransIocLoader,负责加载内置的事务拦截器定义, 1.b.52开始自带
 * 	@author cqyhm
 *
 */
@IocBy(type=ComboIocProvider.class, args={
		"*org.nutz.ioc.loader.json.JsonLoader", "ioc/dao.js",
		"*org.nutz.ioc.loader.annotation.AnnotationIocLoader", "com.nutz.cms"})
@SetupBy(value=MainSetup.class)


@Modules(scanPackage=true)
public class MainModule {
}
