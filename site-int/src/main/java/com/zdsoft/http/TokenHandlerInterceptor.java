package com.zdsoft.http;

import java.util.Collection;
import java.util.Enumeration;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

public class TokenHandlerInterceptor extends HandlerInterceptorAdapter {

	private Logger logger=LoggerFactory.getLogger(getClass());
	
	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
		Enumeration<String> names1=request.getHeaderNames();
		while (names1.hasMoreElements()) {
			String key = names1.nextElement();
			logger.debug("请求头参数:{}={}",key,request.getHeader(key));
		}
		
		Collection<String> names2=response.getHeaderNames();
		for (String key : names2) {
			logger.debug("响应头参数:{}={}",key,request.getHeader(key));
		}
		return super.preHandle(request, response, handler);
	}
}
