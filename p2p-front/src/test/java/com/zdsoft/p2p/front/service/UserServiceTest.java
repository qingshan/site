package com.zdsoft.p2p.front.service;

import static org.junit.Assert.fail;

import java.util.Date;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.zdsoft.p2p.front.TestAbstract;
import com.zdsoft.p2p.front.domain.User;

public class UserServiceTest extends TestAbstract {
	
	@Autowired
	private UserService userService;
	@Test
	public void testSave() {
		User user=new User("zhangsan", "张三");
		user.setCreateBy("system");
		user.setCreateTime(new Date());
		user.setEffTime(new Date());
		user.setExpTime(new Date());
		user.setVersion(0);
		user.setListSort(0);
		user.setState("F0A");
		user.setStateTime(new Date());
		userService.save(user);
	}

	@Test
	public void testInsertSelective() {
		fail("Not yet implemented");
	}

	@Test
	public void testInsertBatch() {
		fail("Not yet implemented");
	}

	@Test
	public void testDeleteById() {
		fail("Not yet implemented");
	}

	@Test
	public void testDeleteSelective() {
		fail("Not yet implemented");
	}

	@Test
	public void testDeleteBatchIds() {
		fail("Not yet implemented");
	}

	@Test
	public void testUpdateById() {
		fail("Not yet implemented");
	}

	@Test
	public void testUpdateSelectiveById() {
		fail("Not yet implemented");
	}

	@Test
	public void testUpdate() {
		fail("Not yet implemented");
	}

	@Test
	public void testUpdateSelective() {
		fail("Not yet implemented");
	}

	@Test
	public void testUpdateBatchById() {
		fail("Not yet implemented");
	}

	@Test
	public void testSelectById() {
		fail("Not yet implemented");
	}

	@Test
	public void testSelectBatchIds() {
		fail("Not yet implemented");
	}

	@Test
	public void testSelectOne() {
		fail("Not yet implemented");
	}

	@Test
	public void testSelectCount() {
		fail("Not yet implemented");
	}

	@Test
	public void testSelectListTString() {
		fail("Not yet implemented");
	}

	@Test
	public void testSelectListT() {
		fail("Not yet implemented");
	}

	@Test
	public void testSelectPagePageOfTTString() {
		fail("Not yet implemented");
	}

	@Test
	public void testSelectPagePageOfTT() {
		fail("Not yet implemented");
	}

}
