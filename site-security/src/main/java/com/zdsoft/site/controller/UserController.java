package com.zdsoft.site.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.zdsoft.rock.common.model.domain.ResponseMsg;
import com.zdsoft.site.domain.User;
import com.zdsoft.site.service.UserService;

@RequestMapping()
@Controller
public class UserController {
	
	private Logger logger=LoggerFactory.getLogger(getClass());
	
	@Autowired
	private UserService userService;
	
	@RequestMapping("/save.html")
	@ResponseBody
	public ResponseMsg<User> save(){
		ResponseMsg<User> msg=new ResponseMsg<User>();
		logger.debug("输出数据");
		return msg;
	}
	@RequestMapping("/auth/login.html")
	public String login(){
		logger.debug("进入登录页面.");
		return "auth/login";
	}
	@RequestMapping("/main/common.html")
	public String common(){
		logger.debug("进入登录公共页面.");
		return "main/common";
	}	
	
	@RequestMapping("/auth/logout.html")
	public String logout(){
		logger.debug("退出系统请求");
		return "main/common";
	}
	
	@RequestMapping("/auth/denied.html")
	public String denied(){
		logger.debug("没有访问权限");
		return "main/common";
	}
}
