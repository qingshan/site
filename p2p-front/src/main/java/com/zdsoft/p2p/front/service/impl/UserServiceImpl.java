package com.zdsoft.p2p.front.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CachePut;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.plugins.pagination.Pagination;
import com.zdsoft.p2p.front.domain.User;
import com.zdsoft.p2p.front.mapper.UserMapper;
import com.zdsoft.p2p.front.service.UserService;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	private UserMapper userMapper;
	
	@Override
	@Transactional
	@CachePut(cacheNames={"user"},key="#user.userId")
	public void save(User user) {
		if(user==null) throw new RuntimeException();
		if(userMapper.insertSelective(user)!=1) throw new RuntimeException();
	}

	@Override
	@Transactional(readOnly=true)
	public Page<User> findPager(Pagination page, User user) {
		Page<User> pager=new Page<User>(page.getCurrent(),page.getSize());
		pager.setRecords(userMapper.findPager(pager,new EntityWrapper<User>(user)));
		return pager;
	}
}
