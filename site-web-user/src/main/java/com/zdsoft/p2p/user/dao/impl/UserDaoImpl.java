package com.zdsoft.p2p.user.dao.impl;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.zdsoft.p2p.user.dao.UserDao;
import com.zdsoft.p2p.user.domain.User;
import com.zdsoft.rock.common.model.dao.impl.BaseDAOImpl;
import com.zdsoft.rock.common.pagination.PageRequest;
import com.zdsoft.rock.common.pagination.PageResponse;
import com.zdsoft.rock.common.util.StringUtil;

@Repository
public class UserDaoImpl extends BaseDAOImpl<User> implements UserDao {

	@Override
	public PageResponse<User> findPager(User user, PageRequest pageRequest) {
		String hql="from User where 1=1";
		Map<String, Object> condition=new HashMap<String, Object>();
		if(user!=null){
			if(StringUtil.isNotEmpty(user.getUserCd())){
				hql+=" and userCd=:userCd";
				condition.put("userCd", user.getUserCd());
			}
			if(StringUtil.isNotEmpty(user.getUserNm())){
				hql+=" and userNm like :userNm";
				condition.put("userNm", "%"+user.getUserNm()+"%");
			}
		}
		return findPager(hql, condition, pageRequest);
	}
}
