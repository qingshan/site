package com.zdsoft.site.mina.model.customEnum;

import javax.xml.bind.annotation.XmlEnumValue;

/**
 * 账户状态
 * ** 0 :正常 缺省值 *
 * ** 3 销户 *
 * ** 5:预指定 *
 * ** 6 :待销户 *
 * ** 7 :待换卡 *
 * ** 8 :空户 *
 * @author cqyhm
 */
public enum AccountStatus {

	/** 0 :正常 缺省值 */
	@XmlEnumValue("0") A0("0", "正常"),

	/** 3 销户 */
	@XmlEnumValue("3") A3("3", "销户"),

	/** 5:预指定 */
	@XmlEnumValue("5") A5("5", "预指定"),

	/** 6 :待销户 */
	@XmlEnumValue("6") A6("6", "待销户"),

	/** 7 :待换卡 */
	@XmlEnumValue("7") A7("7", "待换卡"),

	/** 8 :空户 */
	@XmlEnumValue("8") A8("8", "空户");

	private String value;
	private String name;

	private AccountStatus(String value, String name) {
		this.value = value;
		this.name = name;
	}

	public String getValue() {
		return value;
	}

	public String getName() {
		return name;
	}

	@Override
	public String toString() {
		return value;
	}
}
