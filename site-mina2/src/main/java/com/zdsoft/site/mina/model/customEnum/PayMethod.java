package com.zdsoft.site.mina.model.customEnum;

import javax.xml.bind.annotation.XmlEnumValue;

/**
 * 还款方式（PayMethod）
 * @author cqyhm
 *
 */
public enum PayMethod {
	/**还款方式"0":一次性还本付息*/  
	@XmlEnumValue("0") A0("0","一次性还本付息"),   

	/**还款方式"1":先息后本*/	
	@XmlEnumValue("1") A1("1","先息后本"),

	/**还款方式"2":等额本息*/	
	@XmlEnumValue("2") A2("2","等额本息"),

	/**还款方式"3":等额本金*/	
	@XmlEnumValue("3") A3("3","等额本金"),

	/**还款方式"4":其他*/	
	@XmlEnumValue("4") A4("4","其他");
	
	private String value;
	private String name;

	private PayMethod(String value, String name) {
		this.value = value;
		this.name = name;
	}

	@Override
	public String toString() {
		return value;
	}

	public String getValue() {
		return value;
	}

	public String getName() {
		return name;
	}
}
