package com.zdsoft.site.mina.model.customEnum;
/**
 * 账户状态 
 * @author cqyhm
 *
 */
public enum AccountStatus {
	A0,//  正常  缺省值 
	A3,//  销户   
	A5,//  预指定   
	A6,//  待销户   
	A7,//  待换卡   
	A8//  空户   
}
