package com.zdsoft.site.mina.model.customEnum;

import javax.xml.bind.annotation.XmlEnumValue;

/**
 * 加密方式(EncryMode)
 * 
 * @author cqyhm
 *
 */
public enum EncryMode {
	/** 加密方式 "00","不加密" */
	@XmlEnumValue("00") A00("00", "不加密"),

	/** 加密方式 "01","DES" */
	@XmlEnumValue("01") A01("01", "DES"),

	/** 加密方式 "02","3DES" */
	@XmlEnumValue("02") A02("02", "3DES");

	private String value;
	private String name;

	private EncryMode(String value, String name) {
		this.value = value;
		this.name = name;
	}

	public String getValue() {
		return value;
	}

	public String getName() {
		return name;
	}

	@Override
	public String toString() {
		return value;
	}
}
