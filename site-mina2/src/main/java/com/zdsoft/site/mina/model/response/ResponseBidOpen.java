package com.zdsoft.site.mina.model.response;

import java.math.BigDecimal;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.springframework.beans.BeanUtils;

import com.zdsoft.site.mina.model.BidInfo;
import com.zdsoft.site.mina.model.TransMangAcc;
import com.zdsoft.site.mina.utils.JaxbUtil;

/**
 * 6.1.17.服务商发起客户开标确认(42705)—放款的过程 说明： 
 * 1.本功能是服务商方发起标的开标确认，并由本系统进行监管账户余额处理
 * 2.支持重发操作 应答：
 * @author cqyhm
 *
 */
@XmlRootElement(name = "MsgText")
public class ResponseBidOpen extends AbstractResponse {

	/** 03.标的信息 BidInfo <BidInfo> [0..1] BidInfo 组件 */
	@XmlElement(name = "BidInfo")
	private BidInfo BidInfo;

	/** 04.交易管理账户 ManageAccount <MgeAcct> [1..1] TransMangAcc 组件 */
	@XmlElement(name = "MgeAcct")
	private TransMangAcc MgeAcct;

	/** 05.成功放款金额 SuccessTransferAmount <SucTrfAmt> [0..1] Amount */
	@XmlElement(name = "SucTrfAmt")
	private BigDecimal SucTrfAmt;

	/** 06.放款手续费 FeeAmount <FeeAmt> [0..1] Amount */
	@XmlElement(name = "FeeAmt")
	private BigDecimal FeeAmt;

	public ResponseBidOpen() {
	}

	public ResponseBidOpen(String xml) {
		ResponseBidOpen response = JaxbUtil.converyToJavaBean(xml, this.getClass());
		BeanUtils.copyProperties(response, this);
	}

	/** 03.标的信息 BidInfo <BidInfo> [0..1] BidInfo 组件 */
	public BidInfo getBidInfo() {
		return BidInfo;
	}

	/** 03.标的信息 BidInfo <BidInfo> [0..1] BidInfo 组件 */
	public void setBidInfo(BidInfo bidInfo) {
		BidInfo = bidInfo;
	}

	/** 04.交易管理账户 ManageAccount <MgeAcct> [1..1] TransMangAcc 组件 */
	public TransMangAcc getMgeAcct() {
		return MgeAcct;
	}

	/** 04.交易管理账户 ManageAccount <MgeAcct> [1..1] TransMangAcc 组件 */
	public void setMgeAcct(TransMangAcc mgeAcct) {
		MgeAcct = mgeAcct;
	}

	/** 05.成功放款金额 SuccessTransferAmount <SucTrfAmt> [0..1] Amount */
	public BigDecimal getSucTrfAmt() {
		return SucTrfAmt;
	}

	/** 05.成功放款金额 SuccessTransferAmount <SucTrfAmt> [0..1] Amount */
	public void setSucTrfAmt(BigDecimal sucTrfAmt) {
		SucTrfAmt = sucTrfAmt;
	}

	/** 06.放款手续费 FeeAmount <FeeAmt> [0..1] Amount */
	public BigDecimal getFeeAmt() {
		return FeeAmt;
	}

	/** 06.放款手续费 FeeAmount <FeeAmt> [0..1] Amount */
	public void setFeeAmt(BigDecimal feeAmt) {
		FeeAmt = feeAmt;
	}

}
