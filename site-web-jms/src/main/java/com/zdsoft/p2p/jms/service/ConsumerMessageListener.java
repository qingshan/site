package com.zdsoft.p2p.jms.service;

import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ConsumerMessageListener implements MessageListener {
	
	private Logger logger=LoggerFactory.getLogger(getClass());
	@Override
	public void onMessage(Message message) {
		//这里我们知道生产者发送的就是一个纯文本消息，所以这里可以直接进行强制转换  
		TextMessage textMsg=(TextMessage)message;
		logger.debug("收到一个纯文本的消息:{}",Thread.currentThread().getId());
		try {
			logger.debug("消息内容是:{}",textMsg.getText());
		} catch (Exception e) {
			logger.error("获取消息异常:{}",e.getMessage());
		}
	}

}
	