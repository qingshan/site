package com.zdsoft.site;

import static org.junit.Assert.*;

import java.util.HashMap;
import java.util.Map;

import org.joda.time.DateTime;
import org.joda.time.Days;
import org.joda.time.Hours;
import org.joda.time.Minutes;
import org.joda.time.Months;
import org.joda.time.Seconds;
import org.joda.time.Years;
import org.joda.time.format.DateTimeFormat;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.zdsoft.rock.common.util.HttpClientUtil;

public class DemoTest {

	private Logger logger=LoggerFactory.getLogger(getClass());
	@Test
	public void test() {
		DateTime dt1=new DateTime(2016,8,20,11,28,39);
		DateTime dt2=new DateTime(2015,7,18,11,28,39);
		assertTrue(dt1.isAfterNow());
		logger.debug("{}",dt1.toDate());
		logger.debug("{}",dt1.toString("yyyy-MM-dd HH:mm:ss"));
		logger.debug("{}",dt1.plusDays(20).toString("yyyy-MM-dd HH:mm:ss"));
		logger.debug("{}",dt1.withDate(2015, 12, 31).toString("yyyy-MM-dd HH:mm:ss"));
		//获取某个日期在当前的最后时刻和开始时刻
		logger.debug("{}",dt1.withHourOfDay(0).withMinuteOfHour(0).withSecondOfMinute(0).toString("yyyy-MM-dd HH:mm:ss"));
		logger.debug("{}",dt1.withHourOfDay(0).withMinuteOfHour(0).withSecondOfMinute(0).plusDays(1).minusMillis(1).toString("yyyy-MM-dd HH:mm:ss"));
		logger.debug("{}",dt1.withHourOfDay(23).withMinuteOfHour(59).withSecondOfMinute(59).toString("yyyy-MM-dd HH:mm:ss"));
		//获取某个日期在当前月份的第一天和最后一天
		logger.debug("{}",dt1.withDayOfMonth(1).withHourOfDay(0).withMinuteOfHour(0).withSecondOfMinute(0).toString("yyyy-MM-dd HH:mm:ss"));
		logger.debug("{}",dt1.plusMonths(1).withDayOfMonth(1).minusDays(1).toString("yyyy-MM-dd HH:mm:ss"));
		//计算两个时间之间相差的天数
		logger.debug("years:{}"  ,Years.yearsBetween(dt2, dt1).getYears());
		logger.debug("Months:{}" ,Months.monthsBetween(dt2, dt1).getMonths());
		logger.debug("Days:{}"   ,Days.daysBetween(dt2, dt1).getDays());
		logger.debug("Hours:{}"  ,Hours.hoursBetween(dt2, dt1).getHours());
		logger.debug("Minutes:{}",Minutes.minutesBetween(dt2, dt1).getMinutes());
		logger.debug("Seconds:{}",Seconds.secondsBetween(dt2, dt1).getSeconds());
		logger.debug("Seconds:{}",Seconds.secondsBetween(dt2, dt1).getSeconds());
		//日期计算增减
		DateTime dt3=DateTime.parse("20160318",DateTimeFormat.forPattern("yyyyMMdd"));
		logger.debug("{}",dt3.toString("yyyy-MM-dd HH:mm:ss"));
		logger.debug("{}",dt3.plusWeeks(1).toString("yyyy-MM-dd HH:mm:ss"));
		logger.debug("{}",dt3.plusYears(-1).toString("yyyy-MM-dd HH:mm:ss"));
		logger.debug("{}",dt3.plusMonths(1).toString("yyyy-MM-dd HH:mm:ss"));
		logger.debug("{}",dt3.plusDays(1).toString("yyyy-MM-dd HH:mm:ss"));
		logger.debug("{}",dt3.plusHours(1).toString("yyyy-MM-dd HH:mm:ss"));
		logger.debug("{}",dt3.plusMinutes(1).toString("yyyy-MM-dd HH:mm:ss"));
		logger.debug("{}",dt3.plusSeconds(1).toString("yyyy-MM-dd HH:mm:ss"));
		logger.debug("{}",dt3.plusMillis(1).toString("yyyy-MM-dd HH:mm:ss"));
		
		DateTime dt4=DateTime.parse("99991231",DateTimeFormat.forPattern("yyyyMMdd"));
		logger.debug("{}",dt4.toString("yyyy-MM-dd HH:mm:ss"));
		logger.debug("{}",dt4.toDate());
		logger.debug("{}",dt1.dayOfWeek().get());
	}
    @Test
    public void test4(){
    	//http://datainterface.eastmoney.com/EM_DataCenter/JS.aspx?type=NS&sty=NSST&st=12&sr=-1&p=1&ps=50&js=var%20SRjMzxnV={pages:(pc),data:[(x)]}&stat=4&rt=48862936
    	//http://datainterface.eastmoney.com/EM_DataCenter/JS.aspx?type=NS&sty=NSST&st=12&sr=-1&p=1&ps=50&js=var rbKNqrxh={pages:(pc),data:[(x)]}&stat=4&rt=48862930
    	//http://datainterface.eastmoney.com/EM_DataCenter/JS.aspx?type=NS&sty=NSST&st=12&sr=-1&p=1&ps=50&js=var MbBFybdy={pages:(pc),data:[(x)]}&stat=3&rt=48862932
    	//http://datainterface.eastmoney.com/EM_DataCenter/JS.aspx?type=NS&sty=NSST&st=12&sr=-1&p=1&ps=50&js=var ELoEeshw={pages:(pc),data:[(x)]}&stat=2&rt=48862932	
    	Map<String, String> postParam=new HashMap<>();
    	postParam.put("type", "NS");
    	postParam.put("sty", "NSST");
    	postParam.put("st", "12");
    	postParam.put("sr", "-1");
    	postParam.put("p", "1");
    	postParam.put("ps", "50");
    	postParam.put("js", "var cievmwmk={pages:(pc),data:[(x)]}");
    	postParam.put("stat", "4");
    	postParam.put("rt", "48862902");
    	String url="http://datainterface.eastmoney.com/EM_DataCenter/JS.aspx";
    	String s1=HttpClientUtil.doHttpPost(url, postParam);
    	logger.debug("{}",s1);
    }
    @Test
    public void test1(){
    	Demo d=new Demo();
    	assertNotNull(d);
    }
}
