package com.zdsoft.site.mina.model.customEnum;

import javax.xml.bind.annotation.XmlEnumValue;

/**
 * 结息类型
 * 
 * @author cqyhm
 *
 */
public enum ClearAccuralType {
	/** 结息类型"1", "销户结息" */
	@XmlEnumValue("1") A1("1", "销户结息"),

	/** 结息类型"2", "季度结息" */
	@XmlEnumValue("2") A2("2", "季度结息");

	private String value;
	private String name;

	private ClearAccuralType(String value, String name) {
		this.value = value;
		this.name = name;
	}

	public String getValue() {
		return value;
	}

	public String getName() {
		return name;
	}

	@Override
	public String toString() {
		return value;
	}
}
