package com.zdsoft.site.mina.model.response;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.springframework.beans.BeanUtils;

import com.zdsoft.site.mina.model.BidInfo;
import com.zdsoft.site.mina.model.TransMangAcc;
import com.zdsoft.site.mina.utils.JaxbUtil;

/**
 *6.1.12服务商发起标的建立(42701)
 *	说明：
 *	1.本功能是服务商方对标的进行审核通过后，发送本系统进行登记处理。
 * 应答：
 * 
 * @author cqyhm
 *
 */
@XmlRootElement(name = "MsgText")
public class ResponseBidCreate extends AbstractResponse {

	/**03.交易管理账户	ManageAccount	<MgeAcct>	[1..1]	TransMangAcc	组件*/
	@XmlElement(name="MgeAcct")
	private TransMangAcc MgeAcct;
	
	/**04.标的信息	BidInfo	<BidInfo>	[0..1]	BidInfo	组件*/
	@XmlElement(name="BidInfo")
	private BidInfo BidInfo;
	
	public ResponseBidCreate() {
	}

	public ResponseBidCreate(String xml) {
		ResponseBidCreate response = JaxbUtil.converyToJavaBean(xml, this.getClass());
		BeanUtils.copyProperties(response, this);
	}
	/**03.交易管理账户	ManageAccount	<MgeAcct>	[1..1]	TransMangAcc	组件*/
	public TransMangAcc getMgeAcct() {
		return MgeAcct;
	}
	/**03.交易管理账户	ManageAccount	<MgeAcct>	[1..1]	TransMangAcc	组件*/
	public void setMgeAcct(TransMangAcc mgeAcct) {
		MgeAcct = mgeAcct;
	}
	/**04.标的信息	BidInfo	<BidInfo>	[0..1]	BidInfo	组件*/
	public BidInfo getBidInfo() {
		return BidInfo;
	}
	/**04.标的信息	BidInfo	<BidInfo>	[0..1]	BidInfo	组件*/
	public void setBidInfo(BidInfo bidInfo) {
		BidInfo = bidInfo;
	}

}
