package com.zdsoft.site.mina.model.customEnum;

import javax.xml.bind.annotation.XmlEnumValue;

/**
 * 账户类型（AccountType）
 * 
 * @author cqyhm
 *
 */
public enum AccountType {
	/** 0 :监管汇总帐户 市场子账户类型 */
	@XmlEnumValue("0") A0("0", "监管汇总帐户 "),

	/** Y :利息子账户账户 市场子账户类型 **/
	@XmlEnumValue("Y") AY("Y", "利息子账户账户"),

	/** K :收益子账户账户 市场子账户类型 */
	@XmlEnumValue("K") AK("K", "收益子账户账户"),

	/** Z :虚拟头寸账户 市场子账户类型 */
	@XmlEnumValue("Z") AZ("Z", "虚拟头寸账户"),

	/** b :虚拟融资账户 市场子账户类型 */
	@XmlEnumValue("b") Ab("b", "虚拟融资账户"),

	/** B :银行结算账户 市场子账户类型 */
	@XmlEnumValue("B") AB("B", "银行结算账户"),

	/** R :服务商手续费支付账户 市场子账户类型 */
	@XmlEnumValue("R") AR("R", "服务商手续费支付账户"),

	/** P :服务商佣金子账户 市场子账户类型 */
	@XmlEnumValue("P") AP("P", "服务商佣金子账户"),

	/** O :服务商划出退回子账户 市场子账户类型 */
	@XmlEnumValue("O") AO("O", "服务商划出退回子账户"),

	/** N :货款（垫付）子账户 市场子账户类型 */
	@XmlEnumValue("N") AN("N", "货款（垫付）子账户 "),

	/** 待明确子账户 市场子账户类型 */
	@XmlEnumValue("M") AM("M", "待明确子账户"),

	/** d :非累计式账户 市场子账户类型 */
	@XmlEnumValue("d") Ad("d", "非累计式账户"),

	/** c :累计式预留子账户 市场子账户类型 */
	@XmlEnumValue("c") Ac("c", "累计式预留子账户"),

	/** r :红包子账户 市场子账户类型 */
	@XmlEnumValue("r") Ar("r", "红包子账户"),

	/** 03 :可用金额 会员子账户 */
	@XmlEnumValue("03") A03("03", "可用金额"),

	/** 04 :支付待收金额 会员子账户 */
	@XmlEnumValue("04") A04("04", "支付待收金额"),

	/** 05 :标的冻结金额 会员子账户 */
	@XmlEnumValue("05") A05("05", "标的冻结金额"),

	/** 06 :资产余额 会员子账户 */
	@XmlEnumValue("06") A06("06", "资产余额");

	private String value;
	private String name;

	private AccountType(String value, String name) {
		this.value = value;
		this.name = name;
	}

	public String getValue() {
		return value;
	}

	public String getName() {
		return name;
	}

	@Override
	public String toString() {
		return value;
	}

}
