package com.zdsoft.site;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class FlagBitUtilTest {

	private Logger logger = LoggerFactory.getLogger(getClass());

	@Test
	public void testSignIntInt() {
		int flag = 97;
		flag=FlagBitUtil.sign(flag, 1);
		logger.debug("{}",flag);
		flag=FlagBitUtil.unSign(flag, 2);
		logger.debug("{}",flag);
	}

	@Test
	public void testSignIntIntArray() {
		int flag = 97;
		flag=FlagBitUtil.sign(flag, 1, 2, 3);
		logger.debug("{}",flag);
		logger.debug("{}",FlagBitUtil.checkSign(flag, 1,2,3));
		
		
		flag=FlagBitUtil.unSign(flag, 1, 2, 3);
		logger.debug("{}",flag);
		logger.debug("{}",FlagBitUtil.checkSign(flag, 1,2,3));
		
	}

	@Test
	public void testMain() {

		int flag = 0; // 原始标

		flag = FlagBitUtil.sign(flag, 3); // 将标的第3位标记
		logger.debug("{}-{}", flag, Integer.toBinaryString(flag));

		flag = FlagBitUtil.sign(flag, 4); // 将标的第4位标记
		logger.debug("{}-{}", flag, Integer.toBinaryString(flag));

		flag = FlagBitUtil.unSign(flag, 3); // 将标的第3位标记移除
		logger.debug("{}-{}", flag, Integer.toBinaryString(flag));

		// 检查第1、2、3、4、5位是否被打标
		logger.debug("{}", Integer.toBinaryString(flag));
		logger.debug("01={}", FlagBitUtil.checkSign(flag, 1));
		logger.debug("02={}", FlagBitUtil.checkSign(flag, 2));
		logger.debug("03={}", FlagBitUtil.checkSign(flag, 3));
		logger.debug("04={}", FlagBitUtil.checkSign(flag, 4));
		logger.debug("05={}", FlagBitUtil.checkSign(flag, 5));
		logger.debug("32={}", FlagBitUtil.checkSign(flag, 32));
	}
}
