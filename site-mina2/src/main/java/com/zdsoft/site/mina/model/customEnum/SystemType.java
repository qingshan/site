package com.zdsoft.site.mina.model.customEnum;

import javax.xml.bind.annotation.XmlEnumValue;

/**
 * 应用系统类型（SystemType） 
 * @author cqyhm
 *
 */
public enum SystemType {
	/**
	 * 4:  P2P托管系统*/
	@XmlEnumValue("4")
	A4("4","P2P托管系统");
	
	private String value;
	private String name;
	
	private SystemType(String value,String name){
		this.value=value;
		this.name=name;
	}
	
	public String getValue() {
		return value;
	}
	
	public String getName() {
		return name;
	}
	
	@Override
	public String toString(){
		return value;
	}
	
}
