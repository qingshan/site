<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="zh">
<head>
	<meta charset="UTF-8">
	<meta name="_csrf" content="${_csrf.token}"/>
	<meta name="_csrf_header" content="${_csrf.headerName}"/>
	<title>登录表单</title>
</head>
<body>
	<form action="${pageContext.request.contextPath}/login" method="post">
		<fieldset>
			<legend>登陆系统</legend>
			<p>
				账号账号<input name="username"/>
			</p>
			<p>
				账号密码<input name="password" type="password"/>
			</p>
			<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
			<p>
				记住我<input type="checkbox" name="remember-me">
			</p>
			<input type="submit" value="提交"/> 
		</fieldset>
	</form>
</body>
</html>