package com.zdsoft.site.mina.model.customEnum;

import javax.xml.bind.annotation.XmlEnumValue;

/**
 * 银行编号
 * 
 * @author cqyhm
 *
 */
public enum BankId {
	/** "0","中行" */
	@XmlEnumValue("0") A0("0", "中行"),

	/** "1","工行" */
	@XmlEnumValue("1") A1("1", "工行"),

	/** "2","建行" */
	@XmlEnumValue("2") A2("2", "建行"),

	/** "3","农行" */
	@XmlEnumValue("3") A3("3", "农行"),

	/** "4","交行" */
	@XmlEnumValue("4") A4("4", "交行"), 
	
	/** "5","招行" */
	@XmlEnumValue("5") A5("5", "招行"),

	/** "6","浦发" */
	@XmlEnumValue("6") A6("6", "浦发"),

	/** "7","上海" */
	@XmlEnumValue("7") A7("7", "上海"),

	/** "8","光大" */
	@XmlEnumValue("8") A8("8", "光大"),

	/** "9","华夏" */
	@XmlEnumValue("9") A9("9", "华夏"),

	/** "a","兴业" */
	@XmlEnumValue("a") Aa("a", "兴业"),

	/** "b","民生" */
	@XmlEnumValue("b") Ab("b", "民生"),

	/** "c","深圳发展" */
	@XmlEnumValue("c") Ac("c", "深圳发展"),

	/** "d","中信" */
	@XmlEnumValue("d") Ad("d", "中信"),

	/** "h","北京" */
	@XmlEnumValue("h") Ah("h", "北京"),

	/** "i","徽商" */
	@XmlEnumValue("i") Ai("i", "徽商"),

	/** "j","宁波" */
	@XmlEnumValue("j") Aj("j", "宁波"),

	/** "k","绍兴" */
	@XmlEnumValue("k") Ak("k", "绍兴"),

	/** "l","青岛" */
	@XmlEnumValue("l") Al("l", "青岛"),

	/** "m","齐鲁" */
	@XmlEnumValue("m") Am("m", "齐鲁"),

	/** "n","南京" */
	@XmlEnumValue("n") An("n", "南京"),

	/** "o","汉口" */
	@XmlEnumValue("o") Ao("o", "汉口"),

	/** "p","江苏" */
	@XmlEnumValue("p") Ap("p", "江苏"),

	/** "q","杭州" */
	@XmlEnumValue("q") Aq("q", "杭州"),

	/** "r","渤海" */
	@XmlEnumValue("r") Ar("r", "渤海"),

	/** "s","渣打" */
	@XmlEnumValue("s") As("s", "渣打"),

	/** "t","广发" */
	@XmlEnumValue("t") At("t", "广发"),

	/** "u","深圳平安" */
	@XmlEnumValue("u") Au("u", "深圳平安"),

	/** "v","上海农商" */
	@XmlEnumValue("v") Av("v", "上海农商"),

	/** "w","北京农村商业" */
	@XmlEnumValue("w") Aw("w", "北京农村商业"),

	/** "x","东亚" */
	@XmlEnumValue("x") Ax("x", "东亚"),

	/** "y","邮政储蓄" */
	@XmlEnumValue("y") Ay("y", "邮政储蓄"),

	/** "z","浙商" */
	@XmlEnumValue("z") Az("z", "浙商"),

	/** "A","东莞农商" */
	@XmlEnumValue("A") AA("A", "东莞农商"),

	/** "B","恒丰" */
	@XmlEnumValue("B") AB("B", "恒丰"),

	/** "C","大连" */
	@XmlEnumValue("C") AC("C", "大连"),

	/** "D","中国进出口银行" */
	@XmlEnumValue("D") AD("D", "中国进出口银行"),

	/** "E","安阳银行" */
	@XmlEnumValue("E") AE("E", "安阳银行"),

	/** "F","鞍山银行" */
	@XmlEnumValue("F") AF("F", "鞍山银行"),

	/** "G","包商银行" */
	@XmlEnumValue("G") AG("G", "包商银行"),

	/** "H","保定银行" */
	@XmlEnumValue("H") AH("H", "保定银行"), 
	
	/** "I","本溪市商业银行" */
	@XmlEnumValue("I") AI("I", "本溪市商业银行"), 
	
	/** "J","沧州银行" */
	@XmlEnumValue("J") AJ("J", "沧州银行"), 
	
	/** "K","朝阳银行" */
	@XmlEnumValue("K") AK("K", "朝阳银行"), 
	
	/** "L","成都银行" */
	@XmlEnumValue("L") AL("L", "成都银行"), 
	
	/** "M","承德银行" */
	@XmlEnumValue("M") AM("M", "承德银行"), 
	
	/** "N","达州市商业银行" */
	@XmlEnumValue("N") AN("N", "达州市商业银行"), 
	
	/** "O","大同银行" */
	@XmlEnumValue("O") AO("O", "大同银行"), 
	
	/** "P","丹东银行" */
	@XmlEnumValue("P") AP("P", "丹东银行"), 
	
	/** "Q","德阳银行" */
	@XmlEnumValue("Q") AQ("Q", "德阳银行"), 
	
	/** "R","德州银行" */
	@XmlEnumValue("R") AR("R", "德州银行"), 
	
	/** "S","东营银行" */
	@XmlEnumValue("S") AS("S", "东营银行"), 
	
	/** "T","鄂尔多斯银行" */
	@XmlEnumValue("T") AT("T", "鄂尔多斯银行"), 
	
	/** "U","福建海峡银行" */
	@XmlEnumValue("U") AU("U", "福建海峡银行"), 
	
	/** "V","抚顺银行" */
	@XmlEnumValue("V") AV("V", "抚顺银行"), 
	
	/** "W","阜新银行" */
	@XmlEnumValue("W") AW("W", "阜新银行"), 
	
	/** "X","富滇银行" */
	@XmlEnumValue("X") AX("X", "富滇银行"), 
	
	/** "Y","甘肃银行" */
	@XmlEnumValue("Y") AY("Y", "甘肃银行"), 
	
	/** "Z","赣州银行" */
	@XmlEnumValue("Z") AZ("Z", "赣州银行"), 
	
	/** "10","广东华兴银行" */
	@XmlEnumValue("10") A10("10", "广东华兴银行"), 
	
	/** "11","广东南粤银行" */
	@XmlEnumValue("11") A11("11", "广东南粤银行"), 
	
	/** "12","广西北部湾银行" */
	@XmlEnumValue("12") A12("12", "广西北部湾银行"), 
	
	/** "13","广州银行" */
	@XmlEnumValue("13") A13("13", "广州银行"), 
	
	/** "14","贵阳银行" */
	@XmlEnumValue("14") A14("14", "贵阳银行"), 
	
	/** "15","贵州银行" */
	@XmlEnumValue("15") A15("15", "贵州银行"), 
	
	/** "16","桂林市商业银行" */
	@XmlEnumValue("16") A16("16", "桂林市商业银行"), 
	
	/** "17","桂林银行" */
	@XmlEnumValue("17") A17("17", "桂林银行"), 
	
	/** "18","哈尔滨银行" */
	@XmlEnumValue("18") A18("18", "哈尔滨银行"), 
	
	/** "19","哈密市商业银行" */
	@XmlEnumValue("19") A19("19", "哈密市商业银行"), 
	
	/** "20","邯郸银行" */
	@XmlEnumValue("20") A20("20", "邯郸银行"), 
	
	/** "21","河北银行" */
	@XmlEnumValue("21") A21("21", "河北银行"), 
	
	/** "22","鹤壁银行" */
	@XmlEnumValue("22") A22("22", "鹤壁银行"), 
	
	/** "23","衡水银行" */
	@XmlEnumValue("23") A23("23", "衡水银行"), 
	
	/** "24","葫芦岛银行" */
	@XmlEnumValue("24") A24("24", "葫芦岛银行"), 
	
	/** "25","湖北银行" */
	@XmlEnumValue("25") A25("25", "湖北银行"), 
	
	/** "26","湖州银行" */
	@XmlEnumValue("26") A26("26", "湖州银行"), 
	
	/** "27","华融湘江银行" */
	@XmlEnumValue("27") A27("27", "华融湘江银行"), 
	
	/** "28","吉林银行" */
	@XmlEnumValue("28") A28("28", "吉林银行"), 
	
	/** "29","济宁银行" */
	@XmlEnumValue("29") A29("29", "济宁银行"), 
	
	/** "30","嘉兴银行" */
	@XmlEnumValue("30") A30("30", "嘉兴银行"), 
	
	/** "31","江苏长江商业银行" */
	@XmlEnumValue("31") A31("31", "江苏长江商业银行"), 
	
	/** "32","焦作市商业银行" */
	@XmlEnumValue("32") A32("32", "焦作市商业银行"), 
	
	/** "33","金华银行" */
	@XmlEnumValue("33") A33("33", "金华银行"), 
	
	/** "34","锦州银行" */
	@XmlEnumValue("34") A34("34", "锦州银行"), 
	
	/** "35","晋城银行" */
	@XmlEnumValue("35") A35("35", "晋城银行"), 
	
	/** "36","晋商银行" */
	@XmlEnumValue("36") A36("36", "晋商银行"), 
	
	/** "37","晋中银行" */
	@XmlEnumValue("37") A37("37", "晋中银行"), 
	
	/** "38","景德镇市商业银行" */
	@XmlEnumValue("38") A38("38", "景德镇市商业银行"), 
	
	/** "39","九江银行" */
	@XmlEnumValue("39") A39("39", "九江银行"), 
	
	/** "40","开封市商业银行" */
	@XmlEnumValue("40") A40("40", "开封市商业银行"), 
	
	/** "41","库尔勒市商业银行" */
	@XmlEnumValue("41") A41("41", "库尔勒市商业银行"), 
	
	/** "42","昆仑银行" */
	@XmlEnumValue("42") A42("42", "昆仑银行"), 
	
	/** "43","莱商银行" */
	@XmlEnumValue("43") A43("43", "莱商银行"), 
	
	/** "44","兰州银行" */
	@XmlEnumValue("44") A44("44", "兰州银行"), 
	
	/** "45","廊坊银行" */
	@XmlEnumValue("45") A45("45", "廊坊银行"), 
	
	/** "46","乐山市商业银行" */
	@XmlEnumValue("46") A46("46", "乐山市商业银行"), 
	
	/** "47","凉山州商业银行" */
	@XmlEnumValue("47") A47("47", "凉山州商业银行"), 
	
	/** "48","辽阳银行" */
	@XmlEnumValue("48") A48("48", "辽阳银行"), 
	
	/** "49","林州德丰村镇银行" */
	@XmlEnumValue("49") A49("49", "林州德丰村镇银行"), 
	
	/** "50","临商银行" */
	@XmlEnumValue("50") A50("50", "临商银行"), 
	
	/** "51","柳州银行" */
	@XmlEnumValue("51") A51("51", "柳州银行"), 
	
	/** "52","龙江银行" */
	@XmlEnumValue("52") A52("52", "龙江银行"), 
	
	/** "53","泸州市商业银行" */
	@XmlEnumValue("53") A53("53", "泸州市商业银行"), 
	
	/** "54","洛阳银行" */
	@XmlEnumValue("54") A54("54", "洛阳银行"), 
	
	/** "55","漯河银行" */
	@XmlEnumValue("55") A55("55", "漯河银行"), 
	
	/** "56","绵阳市商业银行" */
	@XmlEnumValue("56") A56("56", "绵阳市商业银行"), 
	
	/** "57","南昌银行" */
	@XmlEnumValue("57") A57("57", "南昌银行"), 
	
	/** "58","南充市商业银行" */
	@XmlEnumValue("58") A58("58", "南充市商业银行"), 
	
	/** "59","南阳银行" */
	@XmlEnumValue("59") A59("59", "南阳银行"), 
	
	/** "60","内蒙古银行" */
	@XmlEnumValue("60") A60("60", "内蒙古银行"), 
	
	/** "61","宁波东海银行" */
	@XmlEnumValue("61") A61("61", "宁波东海银行"), 
	
	/** "62","宁波通商银行" */
	@XmlEnumValue("62") A62("62", "宁波通商银行"), 
	
	/** "63","宁夏银行" */
	@XmlEnumValue("63") A63("63", "宁夏银行"), 
	
	/** "64","攀枝花市商业银行" */
	@XmlEnumValue("64") A64("64", "攀枝花市商业银行"), 
	
	/** "65","盘锦市商业银行" */
	@XmlEnumValue("65") A65("65", "盘锦市商业银行"), 
	
	/** "66","平顶山银行" */
	@XmlEnumValue("66") A66("66", "平顶山银行"), 
	
	/** "67","濮阳银行" */
	@XmlEnumValue("67") A67("67", "濮阳银行"), 
	
	/** "68","齐商银行" */
	@XmlEnumValue("68") A68("68", "齐商银行"), 
	
	/** "69","祁阳村镇银行" */
	@XmlEnumValue("69") A69("69", "祁阳村镇银行"),

	/** "70","秦皇岛银行" */
	@XmlEnumValue("70") A70("70", "秦皇岛银行"), 
	
	/** "71","青海银行" */
	@XmlEnumValue("71") A71("71", "青海银行"), 
	
	/** "72","曲靖市商业银行" */
	@XmlEnumValue("72") A72("72", "曲靖市商业银行"), 
	
	/** "73","泉州银行" */
	@XmlEnumValue("73") A73("73", "泉州银行"), 
	
	/** "74","日照银行" */
	@XmlEnumValue("74") A74("74", "日照银行"), 
	
	/** "75","三门峡银行" */
	@XmlEnumValue("75") A75("75", "三门峡银行"), 
	
	/** "76","厦门银行" */
	@XmlEnumValue("76") A76("76", "厦门银行"), 
	
	/** "77","商丘银行" */
	@XmlEnumValue("77") A77("77", "商丘银行"), 
	
	/** "78","上饶银行" */
	@XmlEnumValue("78") A78("78", "上饶银行"), 
	
	/** "79","盛京银行" */
	@XmlEnumValue("79") A79("79", "盛京银行"), 
	
	/** "80","石嘴山银行" */
	@XmlEnumValue("80") A80("80", "石嘴山银行"), 
	
	/** "81","苏州银行" */
	@XmlEnumValue("81") A81("81", "苏州银行"), 
	
	/** "82","遂宁市商业银行" */
	@XmlEnumValue("82") A82("82", "遂宁市商业银行"), 
	
	/** "83","台州银行" */
	@XmlEnumValue("83") A83("83", "台州银行"), 
	
	/** "84","泰安市商业银行" */
	@XmlEnumValue("84") A84("84", "泰安市商业银行"), 
	
	/** "85","唐山市商业银行" */
	@XmlEnumValue("85") A85("85", "唐山市商业银行"), 
	
	/** "86","天津银行" */
	@XmlEnumValue("86") A86("86", "天津银行"), 
	
	/** "87","铁岭银行" */
	@XmlEnumValue("87") A87("87", "铁岭银行"), 
	
	/** "88","威海市商业银行" */
	@XmlEnumValue("88") A88("88", "威海市商业银行"), 
	
	/** "89","潍坊银行" */
	@XmlEnumValue("89") A89("89", "潍坊银行"), 
	
	/** "90","温州银行" */
	@XmlEnumValue("90") A90("90", "温州银行"), 
	
	/** "91","乌海银行" */
	@XmlEnumValue("91") A91("91", "乌海银行"), 
	
	/** "92","乌鲁木齐市商业银行" */
	@XmlEnumValue("92") A92("92", "乌鲁木齐市商业银行"), 
	
	/** "93","西安银行" */
	@XmlEnumValue("93") A93("93", "西安银行"), 
	
	/** "94","西藏银行" */
	@XmlEnumValue("94") A94("94", "西藏银行"), 
	
	/** "95","新疆汇和银行" */
	@XmlEnumValue("95") A95("95", "新疆汇和银行"), 
	
	/** "96","新乡银行" */
	@XmlEnumValue("96") A96("96", "新乡银行"), 
	
	/** "97","信阳银行" */
	@XmlEnumValue("97") A97("97", "信阳银行"), 
	
	/** "98","邢台银行" */
	@XmlEnumValue("98") A98("98", "邢台银行"), 
	
	/** "99","许昌银行" */
	@XmlEnumValue("99") A99("99", "许昌银行"), 
	
	/** "aa","雅安市商业银行" */
	@XmlEnumValue("aa") Aaa("aa", "雅安市商业银行"), 
	
	/** "bb","烟台银行" */
	@XmlEnumValue("bb") Abb("bb", "烟台银行"), 
	
	/** "cc","阳泉市商业银行" */
	@XmlEnumValue("cc") Acc("cc", "阳泉市商业银行"), 
	
	/** "dd","宜宾市商业银行" */
	@XmlEnumValue("dd") Add("dd", "宜宾市商业银行"), 
	
	/** "ee","营口沿海银行" */
	@XmlEnumValue("ee") Aee("ee", "营口沿海银行"), 
	
	/** "ff","营口银行" */
	@XmlEnumValue("ff") Aff("ff", "营口银行"), 
	
	/** "gg","玉溪市商业银行" */
	@XmlEnumValue("gg") Agg("gg", "玉溪市商业银行"), 
	
	/** "hh","枣庄银行" */
	@XmlEnumValue("hh") Ahh("hh", "枣庄银行"), 
	
	/** "ii","张家口市商业银行" */
	@XmlEnumValue("ii") Aii("ii", "张家口市商业银行"), 
	
	/** "jj","长安银行" */
	@XmlEnumValue("jj") Ajj("jj", "长安银行"), 
	
	/** "kk","长沙银行" */
	@XmlEnumValue("kk") Akk("kk", "长沙银行"), 
	
	/** "ll","长治银行" */
	@XmlEnumValue("ll") All("ll", "长治银行"), 
	
	/** "mm","浙江稠州商业银行" */
	@XmlEnumValue("mm") Amm("mm", "浙江稠州商业银行"), 
	
	/** "nn","浙江民泰商业银行" */
	@XmlEnumValue("nn") Ann("nn", "浙江民泰商业银行"), 
	
	/** "oo","浙江泰隆商业银行" */
	@XmlEnumValue("oo") Aoo("oo", "浙江泰隆商业银行"), 
	
	/** "pp","郑州银行" */
	@XmlEnumValue("pp") App("pp", "郑州银行"), 
	
	/** "qq","重庆银行" */
	@XmlEnumValue("qq") Aqq("qq", "重庆银行"), 
	
	/** "rr","周口银行" */
	@XmlEnumValue("rr") Arr("rr", "周口银行"), 
	
	/** "ss","珠海华润银行" */
	@XmlEnumValue("ss") Ass("ss", "珠海华润银行"), 
	
	/** "tt","驻马店银行" */
	@XmlEnumValue("tt") Att("tt", "驻马店银行"), 
	
	/** "uu","自贡市商业银行" */
	@XmlEnumValue("uu") Auu("uu", "自贡市商业银行"), 
	
	/** "vv","遵义市商业银行"); */
	@XmlEnumValue("vv") Avv("vv", "遵义市商业银行");

	private String value;
	private String name;

	private BankId(String value, String name) {
		this.value = value;
		this.name = name;
	}

	public String getValue() {
		return value;
	}

	public String getName() {
		return name;
	}

	@Override
	public String toString() {
		return value;
	}
}
