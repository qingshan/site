package com.zdsoft.site.mina.model.customEnum;
/**
 * 加密方式(EncryMode) 
 * @author cqyhm
 *
 */
public enum EncryMode {
	A00,//  不加密 
	A01,//  DES 
	A02//  3DES 
}
