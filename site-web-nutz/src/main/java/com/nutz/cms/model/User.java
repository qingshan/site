package com.nutz.cms.model;

import java.io.Serializable;
import java.util.Date;

import org.nutz.dao.entity.annotation.Column;
import org.nutz.dao.entity.annotation.Id;
import org.nutz.dao.entity.annotation.Name;
import org.nutz.dao.entity.annotation.Table;

@Table("t_user")
public class User implements Serializable{

	private static final long serialVersionUID = -3314574263601513238L;
	
	@Id
	private int id;
	
	@Name(casesensitive=false)
    @Column
    private String name;
	
	@Column("passwd")
    private String password;
	
	@Column
    private String salt;
	
	@Column("ct")
    private Date createTime;
	
	@Column("ut")
    private Date updateTime;
    
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getSalt() {
		return salt;
	}
	public void setSalt(String salt) {
		this.salt = salt;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public Date getUpdateTime() {
		return updateTime;
	}
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("User [id=").append(id).append(", name=").append(name).append(", password=").append(password).append(", salt=").append(salt).append(", createTime=").append(createTime).append(", updateTime=").append(updateTime).append("]");
		return builder.toString();
	}
}
