package com.zdsoft.site.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.itextpdf.text.pdf.AcroFields;
import com.itextpdf.text.pdf.PdfStamper;

public class IText5PdfStamperView extends AbstractIText5PdfStamperView{

	public static final String FILENAME = "mergePdfFileName";
	@Override
	protected void mergePdfDocument(Map<String, Object> model, PdfStamper stamper, HttpServletRequest request, HttpServletResponse response) throws Exception {
		
		response.setHeader("Content-Disposition", "attachment;filename=" + new String(model.get(FILENAME).toString().getBytes(), "ISO8859-1"));
		AcroFields fields = stamper.getAcroFields();
		for (String key : model.keySet()) {
			String value = (String)model.get(key);
			fields.setField(key, value);
		}
		stamper.setFormFlattening(true);
	}
}
