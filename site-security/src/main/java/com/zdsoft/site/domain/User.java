package com.zdsoft.site.domain;

import java.io.Serializable;

/**
 * 用户对象
 */
public class User implements Serializable {

	private static final long serialVersionUID = -7596657488209268365L;
	private String username;
    private String password;
    
    public User(){}
    //为了测试方便 简化了实现
    public User(final String username, final String password) {
        this.username = username;
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(final String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(final String password) {
        this.password = password;
    }
}
