package com.zdsoft.site.mina.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

import com.zdsoft.site.mina.model.customEnum.ConfirmCode;

/**
 * 确认结果（confrimResult）
 * 
 * @author cqyhm
 *
 */

@XmlAccessorType(XmlAccessType.FIELD)
public class ConfrimResult {
	/** 返回码 Code [Code] [1..1] 确认码 枚举 */
	@XmlElement(name = "Code")
	private ConfirmCode code;

	/** 返回信息 Info [Info] [0..1] Max128Text */
	@XmlElement(name = "Info")
	private String Info;

	/** 返回码 Code [Code] [1..1] 确认码 枚举 */
	public ConfirmCode getCode() {
		return code;
	}

	/** 返回码 Code [Code] [1..1] 确认码 枚举 */
	public void setCode(ConfirmCode code) {
		this.code = code;
	}

	/** 返回信息 Info [Info] [0..1] Max128Text */
	public String getInfo() {
		return Info;
	}

	/** 返回信息 Info [Info] [0..1] Max128Text */
	public void setInfo(String info) {
		Info = info;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ConfrimResult [code=").append(code).append(", Info=").append(Info).append("]");
		return builder.toString();
	}

}
