package com.zdsoft.tag;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class BodyTag extends TagSupport {

	private static final long serialVersionUID = -4458165940027461418L;
	private Logger logger=LoggerFactory.getLogger(getClass());
	@Override
	public int doStartTag() throws JspException {
		try {
			StringBuffer sb=new StringBuffer();
			sb.append("<body>");
			pageContext.getOut().write(sb.toString());	
		} catch (Exception e) {
			logger.error("生成body标签错误");
			e.printStackTrace();
		}
		
		return EVAL_BODY_INCLUDE;//输出标签体到当前的输出流中
	}
	@Override
	public int doEndTag() throws JspException {
		try {
			pageContext.getOut().write("</body>");	
		} catch (Exception e) {
			logger.error("生成body标签错误");
			e.printStackTrace();
		}
		return EVAL_PAGE;//执行页面余下的部分
	}
	@Override
	public void release() {
		super.release();
	}
	
	
}
