package com.zdsoft.p2p.jms.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.zdsoft.p2p.jms.service.ProductServiceImpl;
import com.zdsoft.rock.common.controller.AbstractController;

@Controller
@RequestMapping("/jms")
public class JmsController extends AbstractController {
	
	@Autowired
	private ProductServiceImpl productService;
	/**
	 * 进入消息队列
	 * @return
	 */
	@RequestMapping("/index.do")
	public String index(){
		return "jms/index";
	}
	@RequestMapping("/test1.do")
	public String test1(String userName,String content){
		int i=0;
		productService.sendMessage1(content+(++i));
		productService.sendMessage1(content+(++i));
		productService.sendMessage1(content+(++i));
		productService.sendMessage1(content+(++i));
		productService.sendMessage1(content+(++i));
		productService.sendMessage1(content+(++i));
		productService.sendMessage1(content+(++i));
		productService.sendMessage1(content+(++i));
		productService.sendMessage1(content+(++i));
		logger.debug("发送消息到队列");
		return "jms/1";
	}
	@RequestMapping("/test2.do")
	public String test2(String userName,String content){
		productService.sendMessage2(content);
		return "jms/1";
	}
}
