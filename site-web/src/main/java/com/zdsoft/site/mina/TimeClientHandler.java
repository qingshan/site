package com.zdsoft.site.mina;

import org.apache.mina.core.service.IoHandlerAdapter;
import org.apache.mina.core.session.IoSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TimeClientHandler extends IoHandlerAdapter {
	
	private Logger logger=LoggerFactory.getLogger(getClass());
	
    public void messageReceived(IoSession session, Object message) throws Exception {
        String content = message.toString();
        logger.debug("client receive a message is : {}",content);
    }

    public void messageSent(IoSession session, Object message) throws Exception {
    	logger.debug("messageSent -> ：{}",message);
    }
    
}