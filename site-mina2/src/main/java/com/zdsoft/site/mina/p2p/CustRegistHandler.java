package com.zdsoft.site.mina.p2p;

import org.apache.mina.core.session.IoSession;

import com.zdsoft.site.mina.model.request.AbstractRequest;
import com.zdsoft.site.mina.model.response.ResponseRegist;
/**
 * 	6.1.1  客户注册(46701)
 * 	说明：
 * 	1.  适用于服务商客户带卡指定监管银行签约和服务商客户预指定监管银行的签约。
 * 	2.  如果请求报中没有银行卡号，则银行监管系统只创建银行帐号和交易管理账号的对应关系，
 * 		客户还需要进行增加银行卡绑定交易。
 * 	3.  实现客户单个币种的签约，不同币种需要进行多次本功能操作。
 * 	4.  服务商方根据应答包中的银行帐号确定和交易管理账号建立对应关系。
 * 	5.  银行方支持重复签约。 
 * @author cqyhm
 *
 */
public class CustRegistHandler extends P2PIoHandler {
	
	public CustRegistHandler(AbstractRequest request) {
		super(request);
	}
	
	@Override
	public void messageSent(IoSession session, Object message) throws Exception {
		//发送的消息已经通过编码器编码为字符串
		logger.debug("1.3.发送数据到服务器：{}",message);
	}
	/**接收到的消息体为xml字符串*/
	@Override
	public void messageReceived(IoSession session, Object message) throws Exception {
		String text=message.toString();
		setResponse(new ResponseRegist(text));
		logger.debug("2.4.接收服务端的数据:[{}]",text);
		ResponseRegist response=(ResponseRegist)getResponse();
		logger.debug("{}",response);
		logger.debug("{}",response.getRst());
		logger.debug("{}",response.getMgeAcct());
	}
	@Override
	public String getInstrCd() {
		return "46701";
	}
}
