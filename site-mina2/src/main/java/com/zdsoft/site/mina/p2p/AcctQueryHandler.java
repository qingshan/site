package com.zdsoft.site.mina.p2p;

import org.apache.mina.core.session.IoSession;

import com.zdsoft.site.mina.model.request.AbstractRequest;
import com.zdsoft.site.mina.model.response.ResponseAcctQuery;

public class AcctQueryHandler extends P2PIoHandler{
	
	public AcctQueryHandler(AbstractRequest request) {
		super(request);
	}

	@Override
	public String getInstrCd() {
		return "46720";
	}

	@Override
	public void messageReceived(IoSession session, Object message) throws Exception {
		String text=message.toString();
		setResponse(new ResponseAcctQuery(text));
		logger.debug("2.6.接收到消息:{}",getResponse().getRst());
	}

	@Override
	public void messageSent(IoSession session, Object message) throws Exception {
		logger.debug("1.3.发送数据.");
		super.messageSent(session, message);
	}
}
