package com.zdsoft.site.mina.model.customEnum;
/**
 * 密码类型(PasswordType) 
 * @author cqyhm
 *
 */
public enum PasswordType {
	A0,//  查询 
	A1,//  取款 
	A2,//  转账 
	A3//  交易 
}
