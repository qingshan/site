package com.zdsoft.p2p.user.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.zdsoft.p2p.user.dao.UserDao;
import com.zdsoft.p2p.user.domain.User;
import com.zdsoft.p2p.user.service.UserService;
import com.zdsoft.rock.common.pagination.PageRequest;
import com.zdsoft.rock.common.pagination.PageResponse;
import com.zdsoft.rock.common.util.StringUtil;

@Service("userService")
@Transactional
public class UserServiceImpl implements UserService {

	private Logger logger=LoggerFactory.getLogger(getClass());
	@Autowired
	private UserDao userDao;
	
	@Override
	public void sayHello(String words) {
		logger.debug("执行helloWorld:{}",words);
	}
	@Override
	@Transactional
	public void save(User user){
		if(user==null) return;
		user.init();
		if(StringUtil.isEmpty(user.getId())){
			userDao.save(user);
		}else{
			User u=userDao.findById(user.getId());
			if(u==null) u=new User();
			u.init();
			BeanUtils.copyProperties(user, u,new String[]{"id"});
			userDao.update(u);
			user=u;
		}
	}
	
	@Override
	public User findById(String id){
		return userDao.findById(id);
	}
	
	@Override
	public PageResponse<User> findPager(User user,PageRequest pageRequest){
		return userDao.findPager(user,pageRequest);
	}

}
