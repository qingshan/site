package com.zdsoft.site.mina.server;

import java.io.IOException;
import java.net.InetSocketAddress;

import org.apache.mina.api.IdleStatus;
import org.apache.mina.codec.textline.TextLineDecoder;
import org.apache.mina.codec.textline.TextLineEncoder;
import org.apache.mina.filter.codec.ProtocolCodecFilter;
import org.apache.mina.filter.logging.LoggingFilter;
import org.apache.mina.transport.nio.NioTcpServer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MinaTimeServer {
    // 定义监听端口
    private static final int PORT = 8015;
    private static Logger logger=LoggerFactory.getLogger(MinaTimeServer.class);
    
    public static void main(String[] args) throws IOException {
        // 创建服务端监控线程
    	NioTcpServer server=new NioTcpServer();    	
    	server.getSessionConfig().setReadBufferSize(2048);
    	server.getSessionConfig().setIdleTimeInMillis(IdleStatus.WRITE_IDLE, 10);
        // 设置日志记录器
    	LoggingFilter loggingFilter=new LoggingFilter();
    	ProtocolCodecFilter codecFilter=new ProtocolCodecFilter(new TextLineEncoder(),new TextLineDecoder());
    	// 设置编码过滤器
    	server.setFilters(loggingFilter,codecFilter);
        // 指定业务逻辑处理器
    	server.setIoHandler();
        // 设置端口号
    	server.bind(new InetSocketAddress(PORT));
        // 启动监听线程    	
        logger.debug("服务器侦听启动");
    }
}