package com.zdsoft.site.mina.codec;

import org.apache.mina.core.filterchain.IoFilterAdapter;
import org.apache.mina.core.session.IoSession;
import org.apache.mina.core.write.WriteRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class P2PFilter extends IoFilterAdapter {
	
	private Logger logger=LoggerFactory.getLogger(getClass());
	
	/***已经被上一个过滤器去掉了发送的报文头部，剩下的只有消息头和消息体**/
	@Override
	public void messageReceived(NextFilter nextFilter, IoSession session, Object message) throws Exception {
		logger.debug("2.5.过滤器接收到的消息体:{}",message);
		super.messageReceived(nextFilter, session, message);
	}
	
	@Override
	public void messageSent(NextFilter nextFilter, IoSession session, WriteRequest writeRequest) throws Exception {
		logger.debug("1.2.过滤器发送过去的数据:{}",writeRequest.getMessage());
		super.messageSent(nextFilter, session, writeRequest);
	}
}
