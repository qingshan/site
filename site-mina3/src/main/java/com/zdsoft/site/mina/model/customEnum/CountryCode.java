package com.zdsoft.site.mina.model.customEnum;
/**
 * 国家代码(CountryCode) 
 * @author cqyhm
 *
 */
public enum CountryCode {
	CHN,//中国 
	USA //美国 
}
