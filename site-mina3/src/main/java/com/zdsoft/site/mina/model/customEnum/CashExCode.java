package com.zdsoft.site.mina.model.customEnum;
/**
 * 汇钞标志(CashExCode) 
 * @author cqyhm
 *
 */
public enum CashExCode {
	A1,//汇 
	A2 //钞 
}
