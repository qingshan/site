package com.zdsoft.p2p.front;

import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 不带spring容器的测试公共基类
 * @author yhm
 *
 */
public abstract class TestCommon {
	//日志处理：一般在子类当中使用
	protected static Logger logger=LoggerFactory.getLogger(TestCommon.class);
	
	
	public static void main(String[] args) {
		/*String string="select   mem.realCd,mem.realNm,IFNULL(invest_data.investSum,0) investSum,   IFNULL(except_data.expAmount,0) expAmount,IFNULL(except_data.expCapital,0) expCapital, IFNULL(except_data.expInterest,0) expInterest,   IFNULL(rec_data.recCapital,0) recCapital, IFNULL(rec_data.recInterest,0) recInterest,   IFNULL(repay_plan_data.repayPlanAmount,0) repayPlanAmount,   IFNULL(repay_data.repayAmount,0) repayAmount from Member mem LEFT JOIN (   select SUM(po.amount) investSum,po.memberId from product_order po where po.`status`=1 and po.investWay in ('0','1') GROUP BY po.memberId ) invest_data on invest_data.memberId = mem.memberId LEFT JOIN (   select SUM(mr.expectAmount) expAmount,SUM(mr.expectCapital) expCapital,SUM(mr.expectInterest) expInterest, mr.memberId from member_receivables mr where mr.`status`=0 GROUP BY mr.memberId ) except_data on except_data.memberId = mem.memberId LEFT JOIN (   select SUM(mr.actualCapital) recCapital,SUM(mr.actualInterest) recInterest,mr.memberId from member_receivables mr where mr.`status`=1 GROUP BY mr.memberId ) rec_data on rec_data.memberId = mem.memberId LEFT JOIN (   select SUM(rt.planRepayAmount) repayPlanAmount,mem.memberId from repaymenttrack rt LEFT JOIN product pro on rt.productId=pro.productId LEFT JOIN member mem on mem.memberId=pro.memberId where rt.planStatus in (0,3,9) GROUP BY mem.memberId ) repay_plan_data on repay_plan_data.memberId = mem.memberId LEFT JOIN (   select SUM(rt.repayAmount) repayAmount,mem.memberId from repaymenttrack rt LEFT JOIN product pro on rt.productId=pro.productId LEFT JOIN member mem on mem.memberId=pro.memberId where rt.planStatus in (1,4) GROUP BY mem.memberId ) repay_data on repay_data.memberId = mem.memberId where 1=1  GROUP BY mem.memberId";
		String shql = "select count(*) " + string.substring(string.indexOf("from"));
		System.out.println(shql);*/
		/*System.err.println(new BigDecimal("54798.64").longValue());
		String temp ="D:/data/p2p/upload/htmb/A73FA93B61D84D0983A0EA368265832C.docx";
		String aString = temp.replace("/", "\\\\");
		System.out.println(aString);*/
	/*	String template="D:\\data\\p2p\\upload\\htmb\\A73FA93B61D84D0983A0EA368265832C.docx";
//		String template="D:/data/p2p/upload/htmb/asd.txt";
		 try {
			OPCPackage pack = POIXMLDocument.openPackage(template);
			System.out.println(pack);
		} catch (IOException e) {
			e.printStackTrace();
		}  */
	}
	public static String getOfficeHome() {
		String osName = System.getProperty("os.name");
        if (Pattern.matches("Linux.*", osName)) {
          return "/opt/openoffice.org3";
        } else if (Pattern.matches("Windows.*", osName)) {
         return "C:/Program Files/OpenOfficePortable/App/openoffice";
        }
		return null;
	}
	
}
