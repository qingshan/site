package com.zdsoft.site.event.content;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

/**
 * 张三接受消息
 */
@Component
public class ZhangsanListener implements ApplicationListener<ContentEvent> {

	private Logger logger=LoggerFactory.getLogger(getClass());
    @Override
    public void onApplicationEvent(final ContentEvent event) {
    	logger.debug("张三收到了新的内容:[{}]",event.getSource());
    }
}
