package com.jfinal.cms.config;

import javax.naming.InitialContext;
import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jfinal.cms.controller.UserController;
import com.jfinal.cms.model.User;
import com.jfinal.config.Constants;
import com.jfinal.config.Handlers;
import com.jfinal.config.Interceptors;
import com.jfinal.config.JFinalConfig;
import com.jfinal.config.Plugins;
import com.jfinal.config.Routes;
import com.jfinal.plugin.activerecord.ActiveRecordPlugin;
import com.jfinal.render.ViewType;
/**
 * 应用程序配置
 * @author cqyhm
 *
 */
public class AppConfig extends JFinalConfig {
	
	private Logger logger=LoggerFactory.getLogger(getClass());
	@Override
	public void configConstant(Constants me) {
		me.setDevMode(true);		  //设置开发模式
		me.setViewType(ViewType.JSP); //渲染模板类型
		me.setBaseViewPath("/jsp");   //jsp所在的基本路径
		logger.debug("配置Constants完成");
	}

	@Override
	public void configHandler(Handlers me) {
		logger.debug("配置Handler完成");
	}

	@Override
	public void configInterceptor(Interceptors me) {
		logger.debug("配置Interceptor完成");
	}

	@Override
	public void configPlugin(Plugins me) {
		loadPropertyFile("db.properties");
		try {
			//1.配置Druid数据源插件
			//DruidPlugin dp=new DruidPlugin(getProperty("url"),getProperty("username"),getProperty("password"));
			//me.add(dp);
			//2.配置jndi数据源
			DataSource ds = (DataSource)new InitialContext().lookup(getProperty("jndiName"));
			logger.debug("jndi配置完成");
			//3.配置数据操作
			ActiveRecordPlugin arp=new ActiveRecordPlugin(ds);
			me.add(arp);
			logger.debug("ActiveRecordPlugin配置完成");
			//表和实体类对应关系
			arp.addMapping("user","userId",User.class);
			logger.debug("建立表和实体映射关系完成");
		} catch (Exception e) {
			logger.error("",e);
		}

	}

	@Override
	public void configRoute(Routes me) {
		me.add("/", UserController.class);
		logger.debug("配置路由完成");
	}

}
