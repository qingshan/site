package com.zdsoft.site.mina.model;

import com.zdsoft.site.mina.model.customEnum.CertificationType;
import com.zdsoft.site.mina.model.customEnum.CountryCode;
import com.zdsoft.site.mina.model.customEnum.CustomerType;
import com.zdsoft.site.mina.model.customEnum.GenderCode;
/**
 * 客户信息（Customer） 
 * @author cqyhm
 *
 */
public class Customer {
	// 客户姓名 Name <Name> [1..1] Max60Text 必选
	private String Name;
	// 证件类型 CertificationType <CertType> [1..1] CertificationType 枚举 必选
	private CertificationType CertificationType;
	// 证件号码 CertificationIdentifier <CertId> [1..1] Max30Text 必选
	private String CertificationIdentifier;
	// 客户类型 Type <Type> [1..1] CustomerType 枚举 可选
	private CustomerType CustomerType;
	// 客户性别 Gender <Gender> [0..1] GenderCode 枚举 可选
	private GenderCode gender;
	// 客户国籍 Nationality <Ntnl> [0..1] CountryCode 枚举 可选
	private CountryCode Nationality;
	// 通信地址 Address <Addr> [0..1] Max60Text 可选
	private String Address;
	// 邮政编码 Postcode <PstCd> [0..1] Max6Text 可选
	private String Postcode;

	// 电子邮件 Email <Email> [0..1] Max60Text 可选
	private String Email;
	// 传真 Fax <Fax> [0..1] Max20Text 可选
	private String Fax;
	// 手机 Mobile <Mobile> [0..1] Max20Text 可选
	private String Mobile;
	// 电话 Telephone <Tel> [0..1] Max20Text 可选
	private String Telephone;

	public String getName() {
		return Name;
	}

	public void setName(String name) {
		Name = name;
	}

	public CertificationType getCertificationType() {
		return CertificationType;
	}

	public void setCertificationType(CertificationType certificationType) {
		CertificationType = certificationType;
	}

	public String getCertificationIdentifier() {
		return CertificationIdentifier;
	}

	public void setCertificationIdentifier(String certificationIdentifier) {
		CertificationIdentifier = certificationIdentifier;
	}

	public CustomerType getCustomerType() {
		return CustomerType;
	}

	public void setCustomerType(CustomerType customerType) {
		CustomerType = customerType;
	}

	public GenderCode getGender() {
		return gender;
	}

	public void setGender(GenderCode gender) {
		this.gender = gender;
	}

	public CountryCode getNationality() {
		return Nationality;
	}

	public void setNationality(CountryCode nationality) {
		Nationality = nationality;
	}

	public String getAddress() {
		return Address;
	}

	public void setAddress(String address) {
		Address = address;
	}

	public String getPostcode() {
		return Postcode;
	}

	public void setPostcode(String postcode) {
		Postcode = postcode;
	}

	public String getEmail() {
		return Email;
	}

	public void setEmail(String email) {
		Email = email;
	}

	public String getFax() {
		return Fax;
	}

	public void setFax(String fax) {
		Fax = fax;
	}

	public String getMobile() {
		return Mobile;
	}

	public void setMobile(String mobile) {
		Mobile = mobile;
	}

	public String getTelephone() {
		return Telephone;
	}

	public void setTelephone(String telephone) {
		Telephone = telephone;
	}

}
