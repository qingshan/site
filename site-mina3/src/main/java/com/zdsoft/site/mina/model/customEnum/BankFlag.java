package com.zdsoft.site.mina.model.customEnum;
/**
 * 银行标志，默认为本行 
 * @author cqyhm
 *
 */
public enum BankFlag {
	A0,//  本行 
	A1 //  他行 
}
