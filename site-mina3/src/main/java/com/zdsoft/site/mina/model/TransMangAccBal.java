package com.zdsoft.site.mina.model;

import com.zdsoft.site.mina.model.customEnum.AccountType;

/**
 * 7.11  交易管理余额（  TransMangAccBal  ） 
 * @author cqyhm
 *
 */
public class TransMangAccBal {
//	账户名称  AccountName  <Name>  [0..1]  Max60Text   
	private String AccountName;
//	账户类别  AccountType  <Type>  [1..1]  AccountType  枚举 
	private AccountType AccountType;
//	余额  Balance  <Amt>  [1..1]  Balance
	private Balance Balance;
	
	public String getAccountName() {
		return AccountName;
	}
	public void setAccountName(String accountName) {
		AccountName = accountName;
	}
	public AccountType getAccountType() {
		return AccountType;
	}
	public void setAccountType(AccountType accountType) {
		AccountType = accountType;
	}
	public Balance getBalance() {
		return Balance;
	}
	public void setBalance(Balance balance) {
		Balance = balance;
	}
}
