package com.zdsoft.site.pdf;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;

import org.junit.Test;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;

public class PdfActionTest {

	@Test
	public void testCreate1() throws FileNotFoundException, DocumentException {
		//1.创建document
		Document doc=new Document();
		//2.获取一个pdfWriter
		PdfWriter.getInstance(doc,new FileOutputStream(new File("c:/1.pdf")));
		//3.打开文档
		doc.open();
		//4.写入内容
		doc.add(new Paragraph("HelloWorld"));
		//5.关闭文档
		doc.close();
	}

}
