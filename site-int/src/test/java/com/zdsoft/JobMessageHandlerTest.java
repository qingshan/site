package com.zdsoft;

import static org.junit.Assert.assertNotNull;

import java.nio.charset.Charset;
import java.util.HashMap;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.integration.support.MessageBuilder;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageHeaders;
import org.springframework.messaging.PollableChannel;
import org.springframework.messaging.support.GenericMessage;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

public class JobMessageHandlerTest {

	private Logger logger=LoggerFactory.getLogger(getClass());
	
	/***
	 * JDBC Integration
	 * @throws InterruptedException
	 */
	@Test
	public void testHandleMessage() throws InterruptedException {
		ClassPathXmlApplicationContext context=new ClassPathXmlApplicationContext("intjdbc.xml");
		assertNotNull(context);
		context.start();
		Thread.sleep(120000L);
		context.close();
	}
	
	/***
	 * 实现功能：每秒间隔轮询D:/temp/input目录，如果发现该目录有文件，就依次复制到D:/temp/output目录中。实际上就是文件copy的功能
	 * 该示例使用了Spring Integration提供的File适配器。
	 * 1.防止重复读取:prevent-duplicates="true"
	 * 2.文件过滤：filename-pattern="*.doc"或者filename-regex="[ABC]_positions.doc"
	 * 3.文件锁：<file:nio-locker/>
	 * 4.删除源文件：delete-source-files="true"
	 */
	@Test
	public void testIntFile() throws InterruptedException {
		ClassPathXmlApplicationContext context=new ClassPathXmlApplicationContext("intfile.xml");
		assertNotNull(context);
		context.start();
		
		Thread.sleep(120000L);
		context.close();
	}
	/**
	 * sftp integration
	 * 文件下载
	 * @throws InterruptedException
	 */
	@Test
	public void testIntSftp() throws InterruptedException{
		ClassPathXmlApplicationContext context=new ClassPathXmlApplicationContext("intsftp.xml");
		context.start();
		PollableChannel ftpChannel=context.getBean("receiveChannel",PollableChannel.class);
		Message<?> message=ftpChannel.receive();
		logger.debug("接收到的文件:{}",message);
		Thread.sleep(1200000L);
		context.close();
	}
	
	/**
	 * sftp integration
	 * 文件上传
	 * @throws InterruptedException
	 */
	@Test
	public void testUploadSftp() throws InterruptedException{
		ClassPathXmlApplicationContext context=new ClassPathXmlApplicationContext("intsftp.xml");
		context.start();
		PollableChannel ftpChannel=context.getBean("sendChannel",PollableChannel.class);
		Message<?> message=ftpChannel.receive();
		logger.debug("接收到的文件:{}",message);
		Thread.sleep(1200000L);
		context.close();
	}
	@Test
	public void test1(){
		//构造消息的方式1
		Message<String> m1=MessageBuilder.withPayload("hello World").build();
		assertNotNull(m1);
		logger.debug("{}",m1);
		
		Message<String> m2=MessageBuilder.withPayload("hello cqyhm").setHeaderIfAbsent("user", "cqyhm").build();
		assertNotNull(m2);
		logger.debug("{}",m2);
		//构造消息的方式2
		Message<String> m3=new GenericMessage<String>("测试消息");
		logger.debug("{}",m3);
		
		Message<String> m4=new GenericMessage<String>("测试消息",new MessageHeaders(new HashMap<String,Object>()));
		logger.debug("{}",m4);
	}
	//消息通道
	@Test
	public void test2(){
		//AppException
		//MessageChannel channel;
		//DirectChannel
		//QueueChannel
		//PriorityQueue
		//用于接收消息(点对点通道),只有1个接收者,包括直接通道、队列通道、优先级通道
		//PollableChannel channel1;
		//发布消息，订阅和取消(发布订阅通道)
		//SubscribableChannel channel2;
	}
	@Test
	public void test3(){
		//客户端上传文件
		RestTemplate template = new RestTemplate();
		String uri = "http://localhost:8780/site-int/inboundAdapter.htm";
		Resource s2logo =new ClassPathResource("test.png");
		MultiValueMap<String,Object> map = new LinkedMultiValueMap<String,Object>();
		map.add("company", new String("正大软件"));
		map.add("logo", s2logo);
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(new MediaType("multipart", "form-data",Charset.forName("UTF-8")));
		headers.set("charset", "UTF-8");
		headers.set("Accept-Charset", "utf-8");  
		HttpEntity<MultiValueMap<String,Object>> request = new HttpEntity<>(map, headers);
		
		template.getMessageConverters().add(new StringHttpMessageConverter(Charset.forName("UTF-8")));
		
		ResponseEntity<String> httpResponse = template.exchange(uri, HttpMethod.POST, request,String.class);
		assertNotNull(httpResponse);
	}
}
