/**
 * Copyright (c) 2011-2014, hubin (jobob@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.baomidou.mybatisplus.test.mysql;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.log4j.BasicConfigurator;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.baomidou.mybatisplus.MybatisSessionFactoryBuilder;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.plugins.pagination.Pagination;
import com.baomidou.mybatisplus.test.mysql.entity.User;
import com.baomidou.mybatisplus.toolkit.IdWorker;

/**
 * MybatisPlus 测试类
 * RUN 测试
 * 
 * <p>
 * MybatisPlus 加载 SQL 顺序：
 * </p>
 * 1、加载XML中的SQL<br>
 * 2、加载sqlProvider中的SQL<br>
 * 3、xmlSql 与 sqlProvider不能包含相同的SQL<br>
 * <br>
 * 调整后的SQL优先级：xmlSql > sqlProvider > crudSql
 * <br>
 * @author hubin
 * @Date 2016-01-23
 */
public class UserMapperTest {

	private static Logger logger=LoggerFactory.getLogger(UserMapperTest.class);
	private static SqlSession session;
	private static UserMapper userMapper;
	@BeforeClass
	public static void tesBeforeClass(){
		//使用缺省Log4j环境
		BasicConfigurator.configure();
		//加载配置文件
		InputStream in = UserMapperTest.class.getClassLoader().getResourceAsStream("mysql-config.xml");
		/*
		 * 此处采用 MybatisSessionFactoryBuilder 构建
		 * SqlSessionFactory，目的是引入AutoMapper功能
		 */
		SqlSessionFactory sessionFactory = new MybatisSessionFactoryBuilder().build(in);
		session = sessionFactory.openSession();
		userMapper = session.getMapper(UserMapper.class);
		logger.debug(" debug run 查询执行 user 表数据变化！ ");
		session.delete("deleteAll");
	}
	@AfterClass
	public static void testAfterClass(){
		/* 删除测试数据  */
		//rlt = session.delete("deleteAll");
		//logger.debug("清空测试数据！ rlt={}",rlt);
		/**
		 * 提交
		 */		
		session.commit();
	}
	/**插入*/
	@Test
	public void testInsert(){
		long id = IdWorker.getId();
		logger.debug("{}",id);
		int rlt = userMapper.insert(new User(id, "abc", 18, 1));
		logger.debug("insert {}",rlt);
	}
	@Test
	public void testinsertSelective(){
		int rlt = userMapper.insertSelective(new User("abc", 18));
		logger.debug("insertSelective {}",rlt);
	}
	@Test
	public void testinsertBatch(){
		List<User> ul = new ArrayList<User>();
		/* 手动输入 ID */
		ul.add(new User(11L, "1", 1, 0));
		ul.add(new User(12L, "2", 2, 1));
		ul.add(new User(13L, "3", 3, 1));
		ul.add(new User(14L, "delname", 4, 0));
		ul.add(new User(15L, "5", 5, 1));
		ul.add(new User(16L, "6", 6, 0));
		ul.add(new User(17L, "7", 7, 0));
		
		/* 使用 ID_WORKER 自动生成 ID */
		ul.add(new User("8", 8, 1));
		ul.add(new User("9", 9, 1));
		int rlt = userMapper.insertBatch(ul);
		logger.debug("insertBatch {}",rlt);
	}
	@Test
	public void testdeleteById(){
		long id = IdWorker.getId();
		logger.debug("{}",id);
		int rlt = userMapper.insert(new User(id, "abc", 18, 1));
		logger.debug("insert {}",rlt);
		
		rlt = userMapper.deleteById(id);
		logger.debug("deleteById delete id={},result={}",id ,rlt);
	}
	@Test
	public void testdeleteBatchIds(){
		List<Long> il = new ArrayList<Long>();
		il.add(16L);
		il.add(17L);
		int rlt = userMapper.deleteBatchIds(il);
		logger.debug("deleteBatchIds,delete id={},result={}" ,il,rlt);
	}
	@Test
	public void testdeleteSelective(){
		int rlt = userMapper.deleteSelective(new User(14L, "delname"));
		logger.debug("deleteSelective result={}",rlt);
	}
	////////////////////////////////////////////////////////////////////
	public void testupdateSelectiveById(){
		int rlt = userMapper.updateSelectiveById(new User(12L, "MybatisPlus"));
		logger.debug("updateSelectiveById,result={}",rlt);
	}
	@Test
	public void testupdateById(){
		//* updateById 是从 AutoMapper 中继承而来的，UserMapper.xml中并没有申明改sql
		int rlt = userMapper.updateById(new User(12L, "update all column", 12, 12));
		logger.debug("updateById result={}",rlt);
	}
	@Test
	public void testupdate(){
		int rlt = userMapper.update(new User("55", 55, 5), new User(15L, "5"));
		logger.debug("update result={}",rlt);
	}
	@Test
	public void testupdateSelective(){
		/* 无条件选择更新 */
		//userMapper.updateSelective(new User("11"), null);
		
		int rlt = userMapper.updateSelective(new User("00"), new User(15L, "55"));
		logger.debug("updateSelective result={}",rlt);
	}
	@Test
	public void testupdateBatchById(){
		List<User> userList = new ArrayList<User>();
		userList.add(new User(11L, "updateBatchById-1", 1, 1));
		userList.add(new User(12L, "updateBatchById-2", 2, 2));
		userList.add(new User(13L, "updateBatchById-3", 3, 3));
		int rlt = userMapper.updateBatchById(userList);
		logger.debug("updateBatchById result={}",rlt);
	}
	@Test
	public void testselectById(){
		long id = IdWorker.getId();
		logger.debug("{}",id);
		userMapper.insert(new User(id, "abc", 18, 1));
		
		logger.debug("selectById");
		User user = userMapper.selectById(id);
		Assert.assertNotNull(user);
		logger.debug("{}",user);
	}
	@Test
	public void selectBatchIds(){
		logger.debug("selectBatchIds");
		List<Long> idList = new ArrayList<Long>();
		idList.add(11L);
		idList.add(12L);
		List<User> ul1 = userMapper.selectBatchIds(idList);
		for ( int i = 0 ; i < ul1.size() ; i++ ) {
			logger.debug("{}",ul1.get(i));
		}
	}
	@Test
	public void testselectOne(){
		logger.debug("selectOne");
		User userOne = userMapper.selectOne(new User("abc"));
		logger.debug("{}",userOne);
	}
	@Test
	public void testselectCount(){
		logger.debug("selectCount");
		logger.debug("查询 type=1 总记录数：" + userMapper.selectCount(new User(1)));
		logger.debug("总记录数：" + userMapper.selectCount(null));
	}
	@Test
	public void testselectList(){
		logger.debug("selectList-----所有数据----id--DESC--排序----");
		List<User> ul2 = userMapper.selectList(new EntityWrapper<User>(null, "id DESC"));
		for ( int i = 0 ; i < ul2.size() ; i++ ) {
			logger.debug("{}",ul2.get(i));
		}
	}
	@Test
	public void testSelectPager(){
		logger.debug("list 分页查询 ----查询 testType = 1 的所有数据--id--DESC--排序--------");
		Page<User> page = new Page<User>(1, 2);
		EntityWrapper<User> ew = new EntityWrapper<User>(new User(1), "id DESC");
		List<User> paginList = userMapper.selectPage(page, ew);
		page.setRecords(paginList);
		for ( int i = 0 ; i < page.getRecords().size() ; i++ ) {
			logger.debug("{}",page.getRecords().get(i));
		}
		logger.debug(" 翻页：" + page.toString());
	}
	@Test
	public void testselectListRow() {
		logger.debug("xml---selectListRow 分页查询，不查询总数（此时可自定义 count 查询）----无查询条件");
		//TODO 查询总数传 Page 对象即可
		List<User> rowList = userMapper.selectListRow(new Pagination(0, 2));
		for ( int i = 0 ; i < rowList.size() ; i++ ) {
			logger.debug("{}",rowList.get(i));
		}
	}
}
