package com.zdsoft.common;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;

@FeignClient("tomcat")
public interface UserClient {
	
	@RequestMapping(path = "/test1")
	public User test();
}
