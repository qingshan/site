package com.zdsoft.site.mina.model.request;

import java.math.BigDecimal;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.zdsoft.site.mina.model.BidInfo;
import com.zdsoft.site.mina.model.MessageHeader;
import com.zdsoft.site.mina.model.TransMangAcc;
import com.zdsoft.site.mina.model.customEnum.BusinessType;
import com.zdsoft.site.mina.model.customEnum.CurrencyCode;
import com.zdsoft.site.mina.model.customEnum.PackageFlag;

/**
 * 6.1.18.服务商发起客户债权受让(42706) 
 * 说明： 
 * 1.本功能是服务商方发起客户债权转让，并由本系统进行监管账户余额处理
 * 2.实际扣减债权转入客户资金 = 受让金额 – 红包抵扣金额
 * 3.对资金处理时还需扣减虚拟红包子账户金额 
 * 4.支持重发操作
 * 5.资金变动说明：【转入方】A用户，用x元，购买【转出方】B用户，y元债权，其中收取A手续费a，收取B手续费b;
 * 那么A用户可用资金减少（x+a）元、资产余额增加y元；B用户可用资金增加(x-b)元、资产余额减少y元; 
 * 请求：
 * 
 * @author cqyhm
 *
 */
@XmlRootElement(name = "MsgText")
public class RequestCreditRight extends AbstractRequest {

	public RequestCreditRight(){}
	
	public RequestCreditRight(MessageHeader msgHdr){
		setMsgHdr(msgHdr);
	}
	
	/** 02 报文标志 PackageFlag <PkgFlag> [0..1] PackageFlag 枚举 */
	@XmlElement(name = "PkgFlag")
	private PackageFlag PkgFlag;

	/** 03.标的信息 BidInfo <BidInfo> [1..1] BidInfo 组件 */
	@XmlElement(name = "BidInfo")
	private BidInfo BidInfo;

	/** 04 债权买入者 ManageAccountIn <MgeAcctIn> [1..1] TransMangAcc 组件 */
	@XmlElement(name = "MgeAcctIn")
	private TransMangAcc MgeAcctIn;

	/** 05 债权卖出者 ManageAccountOut <MgeAcctOut> [1..1] TransMangAcc 组件 */
	@XmlElement(name = "MgeAcctOut")
	private TransMangAcc MgeAcctOut;

	/** 06 已收本金 CapitalAmount <CapitalAmt> [1..1] Amount */
	@XmlElement(name = "CapitalAmt")
	private BigDecimal CapitalAmt;

	/** 07 已收收益 ProfitAmount < ProfitAmt> [1..1] Amount */
	@XmlElement(name = "ProfitAmt")
	private BigDecimal ProfitAmt;

	/** 08 债权份数 TransCount <TransCount> [0..1] Max20Text 转出份数 */
	@XmlElement(name = "TransCount")
	private String TransCount;

	/** 09 债权金额 CreditAmount <CreAmt> [1..1] Amount 转出金额 */
	@XmlElement(name = "CreAmt")
	private BigDecimal CreAmt;

	/** 10 受让金额 TransferAmount <TrfAmt> [1..1] Amount 成交金额 */
	@XmlElement(name = "TrfAmt")
	private BigDecimal TrfAmt;

	/** 11 红包抵扣金额 RedPackBala <RedPackAmt> [0..1] Amount */
	@XmlElement(name = "RedPackAmt")
	private BigDecimal RedPackAmt;

	/** 12 手续费 FeeAmt <FeeAmt> [0..1] Amount */
	@XmlElement(name = "FeeAmt")
	private BigDecimal FeeAmt;

	/** 13 转入会员手续费 FeeAmountIN <FeeAmtIN> [0..1] Amount */
	@XmlElement(name = "FeeAmtIN")
	private BigDecimal FeeAmtIN;

	/** 14 币种 Currency <Ccy> [1..1] CurrencyCode 枚举 */
	@XmlElement(name = "Ccy")
	private CurrencyCode Ccy;

	/** 15.业务类别 BusinessType <BusType> [0..1] BusinessType 枚举 */
	@XmlElement(name = "BusType")
	private BusinessType BusType;

	/** 16.短信验证码 MessageBox <MsgBox> [0..1] Max20Text */
	@XmlElement(name = "MsgBox")
	private String MsgBox;

	/** 17.摘要 Digest [Dgst] [0..1] Max128Text */
	@XmlElement(name = "Dgst")
	private String Dgst;

	/** 02 报文标志 PackageFlag <PkgFlag> [0..1] PackageFlag 枚举 */
	public PackageFlag getPkgFlag() {
		return PkgFlag;
	}

	/** 02 报文标志 PackageFlag <PkgFlag> [0..1] PackageFlag 枚举 */
	public void setPkgFlag(PackageFlag pkgFlag) {
		PkgFlag = pkgFlag;
	}

	/** 03.标的信息 BidInfo <BidInfo> [1..1] BidInfo 组件 */
	public BidInfo getBidInfo() {
		return BidInfo;
	}

	/** 03.标的信息 BidInfo <BidInfo> [1..1] BidInfo 组件 */
	public void setBidInfo(BidInfo bidInfo) {
		BidInfo = bidInfo;
	}

	/** 04 债权买入者 ManageAccountIn <MgeAcctIn> [1..1] TransMangAcc 组件 */
	public TransMangAcc getMgeAcctIn() {
		return MgeAcctIn;
	}

	/** 04 债权买入者 ManageAccountIn <MgeAcctIn> [1..1] TransMangAcc 组件 */
	public void setMgeAcctIn(TransMangAcc mgeAcctIn) {
		MgeAcctIn = mgeAcctIn;
	}

	/** 05 债权卖出者 ManageAccountOut <MgeAcctOut> [1..1] TransMangAcc 组件 */
	public TransMangAcc getMgeAcctOut() {
		return MgeAcctOut;
	}

	/** 05 债权卖出者 ManageAccountOut <MgeAcctOut> [1..1] TransMangAcc 组件 */
	public void setMgeAcctOut(TransMangAcc mgeAcctOut) {
		MgeAcctOut = mgeAcctOut;
	}

	/** 06 已收本金 CapitalAmount <CapitalAmt> [1..1] Amount */
	public BigDecimal getCapitalAmt() {
		return CapitalAmt;
	}

	/** 06 已收本金 CapitalAmount <CapitalAmt> [1..1] Amount */
	public void setCapitalAmt(BigDecimal capitalAmt) {
		CapitalAmt = capitalAmt;
	}

	/** 07 已收收益 ProfitAmount < ProfitAmt> [1..1] Amount */
	public BigDecimal getProfitAmt() {
		return ProfitAmt;
	}

	/** 07 已收收益 ProfitAmount < ProfitAmt> [1..1] Amount */
	public void setProfitAmt(BigDecimal profitAmt) {
		ProfitAmt = profitAmt;
	}

	/** 08 债权份数 TransCount <TransCount> [0..1] Max20Text 转出份数 */
	public String getTransCount() {
		return TransCount;
	}

	/** 08 债权份数 TransCount <TransCount> [0..1] Max20Text 转出份数 */
	public void setTransCount(String transCount) {
		TransCount = transCount;
	}

	/** 09 债权金额 CreditAmount <CreAmt> [1..1] Amount 转出金额 */
	public BigDecimal getCreAmt() {
		return CreAmt;
	}

	/** 09 债权金额 CreditAmount <CreAmt> [1..1] Amount 转出金额 */
	public void setCreAmt(BigDecimal creAmt) {
		CreAmt = creAmt;
	}

	/** 10 受让金额 TransferAmount <TrfAmt> [1..1] Amount 成交金额 */
	public BigDecimal getTrfAmt() {
		return TrfAmt;
	}

	/** 10 受让金额 TransferAmount <TrfAmt> [1..1] Amount 成交金额 */
	public void setTrfAmt(BigDecimal trfAmt) {
		TrfAmt = trfAmt;
	}

	/** 11 红包抵扣金额 RedPackBala <RedPackAmt> [0..1] Amount */
	public BigDecimal getRedPackAmt() {
		return RedPackAmt;
	}

	/** 11 红包抵扣金额 RedPackBala <RedPackAmt> [0..1] Amount */
	public void setRedPackAmt(BigDecimal redPackAmt) {
		RedPackAmt = redPackAmt;
	}

	/** 12 手续费 FeeAmt <FeeAmt> [0..1] Amount */
	public BigDecimal getFeeAmt() {
		return FeeAmt;
	}

	/** 12 手续费 FeeAmt <FeeAmt> [0..1] Amount */
	public void setFeeAmt(BigDecimal feeAmt) {
		FeeAmt = feeAmt;
	}
	
	/** 13 转入会员手续费 FeeAmountIN <FeeAmtIN> [0..1] Amount */
	public BigDecimal getFeeAmtIN() {
		return FeeAmtIN;
	}

	/** 13 转入会员手续费 FeeAmountIN <FeeAmtIN> [0..1] Amount */
	public void setFeeAmtIN(BigDecimal feeAmtIN) {
		FeeAmtIN = feeAmtIN;
	}

	/** 14 币种 Currency <Ccy> [1..1] CurrencyCode 枚举 */
	public CurrencyCode getCcy() {
		return Ccy;
	}

	/** 14 币种 Currency <Ccy> [1..1] CurrencyCode 枚举 */
	public void setCcy(CurrencyCode ccy) {
		Ccy = ccy;
	}

	/** 15.业务类别 BusinessType <BusType> [0..1] BusinessType 枚举 */
	public BusinessType getBusType() {
		return BusType;
	}

	/** 15.业务类别 BusinessType <BusType> [0..1] BusinessType 枚举 */
	public void setBusType(BusinessType busType) {
		BusType = busType;
	}

	/** 16.短信验证码 MessageBox <MsgBox> [0..1] Max20Text */
	public String getMsgBox() {
		return MsgBox;
	}

	/** 16.短信验证码 MessageBox <MsgBox> [0..1] Max20Text */
	public void setMsgBox(String msgBox) {
		MsgBox = msgBox;
	}

	/** 17.摘要 Digest [Dgst] [0..1] Max128Text */
	public String getDgst() {
		return Dgst;
	}

	/** 17.摘要 Digest [Dgst] [0..1] Max128Text */
	public void setDgst(String dgst) {
		Dgst = dgst;
	}


}
