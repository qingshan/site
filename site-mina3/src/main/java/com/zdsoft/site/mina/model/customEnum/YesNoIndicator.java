package com.zdsoft.site.mina.model.customEnum;
/**
 * 是否标志(YesNoIndicator) 
 * @author cqyhm
 *
 */
public enum YesNoIndicator {
	/**Y=是*/
	Y("Y","是"),
	/**N=否*/
	N("N","否");
	
	private String value;
	private String name;
	
	private YesNoIndicator(String value,String name){
		this.value=value;
		this.name=name;
	}
	
	public String getValue() {
		return value;
	}
	public String getName() {
		return name;
	}
	@Override
	public String toString(){
		return value;
	}
}
