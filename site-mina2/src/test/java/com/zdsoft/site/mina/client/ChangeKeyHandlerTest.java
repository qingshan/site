package com.zdsoft.site.mina.client;

import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.Charset;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ChangeKeyHandlerTest {

	public static Logger logger = LoggerFactory.getLogger(ChangeKeyHandlerTest.class);
	public static String mackey="3BE65E59898AF597";
	public static String pingKey="21085368E5FF2243";
	public static String pkgKey="21085368E5FF224362CED3BC65F553F021085368E5FF2243";

	@Test
	public void test1() {
		ByteBuffer buffer = ByteBuffer.allocate(32);// 32个字节的容量
		logger.debug("capacity={},position={},remaining={},limit={}", buffer.capacity(), buffer.position(), buffer.remaining(), buffer.limit());
		buffer.putInt(0X02030405);
		logger.debug("capacity={},position={},remaining={},limit={}", buffer.capacity(), buffer.position(), buffer.remaining(), buffer.limit());
		buffer.flip();
		logger.debug("capacity={},position={},remaining={},limit={}", buffer.capacity(), buffer.position(), buffer.remaining(), buffer.limit());
		int x=buffer.getShort();
		buffer.mark();
		int y=buffer.getShort();
		logger.debug("{},{},{}",x,y,0X02030405);
		logger.debug("capacity={},position={},remaining={},limit={}", buffer.capacity(), buffer.position(), buffer.remaining(), buffer.limit());
		buffer.reset();
		logger.debug("capacity={},position={},remaining={},limit={}", buffer.capacity(), buffer.position(), buffer.remaining(), buffer.limit());
		
	}
	// 将字符转为字节(编码)
	public byte[] getBytes(char[] chars) {
		Charset cs = Charset.forName("UTF-8");
		CharBuffer cb = CharBuffer.allocate(chars.length);
		cb.put(chars);
		cb.flip();
		ByteBuffer bb = cs.encode(cb);
		return bb.array();
	}
	//将字节转为字符(解码)
	public char[] getChars (byte[] bytes) {
		Charset cs = Charset.forName ("UTF-8");
		ByteBuffer bb = ByteBuffer.allocate (bytes.length);
		bb.put (bytes);
		bb.flip ();
		CharBuffer cb = cs.decode (bb);
		return cb.array();
	}
}
