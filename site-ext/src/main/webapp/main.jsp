<%@ page language="java" contentType="text/html; charset=UTF-8"   pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html lang="zh">
<head>
	<c:set var="ctx" value="${pageContext.request.contextPath}"/>
	<meta charset="UTF-8">
	<title>P2P3.0业务系统管理后台</title>
	<link  href="${ctx}/resources/theme/default/resources/css/ext-all.css" type="text/css" rel="stylesheet"/>
	<script src="${ctx}/resources/extjs/adapter/ext/ext-base.js"  type="text/javascript" ></script>
	<script src="${ctx}/resources/extjs/ext-all.js" type="text/javascript" ></script>
	<script src="${ctx}/resources/extjs/ext-lang-zh_CN.js" type="text/javascript"></script>		
</head>
<body>
	<div id="north-top" class="x-hide-display">
		<p>P2P3.0业务系统管理后台</p>
	</div>
	<div id="west-left" class="x-hide-display">
        <p>可以是菜单内容</p>
    </div>
	<div id="center-work" class="x-hide-display">		
	</div>
	<div id="east-right" class="x-hide-display">
        <p>右边消息区域</p>
    </div>    
    <div id="south-bottom" class="x-hide-display">
        <p>底部状态栏用于显示状态</p>
    </div> 
	<!--显示loding区域-->
	<div id="loading-mask" style="display:none"></div>
	<!--载入进度显示-->
	<div id="loading" style="display:none">
		<div class=loading-indicator>
			<img style="margin-right:8px" height=32 src="./resource/image/ajax1.gif" width="36"/>正在初始化,请稍等...
		</div>
	</div>    	
</body>
</html>
<script type="text/javascript">
Ext.onReady(function(){
	//中部工作区对应的tab容器
	var centerTabs=new Ext.TabPanel({
        region: 'center', // a center region is ALWAYS required for border layout
        split:true,
        deferredRender: false,
        activeTab: 0,     // first tab initially active
        items: [{
             contentEl: 'center-work'
            ,title: '我的主页'
            ,border: false
            ,closable: false
            ,autoScroll: true
            //iconCls:'home'
        }]
    });
    //整个窗体作为区域
	var viewport = new Ext.Viewport({
		layout: 'border',
		items:[
			//顶部广告区域
			new Ext.BoxComponent({
                region: 'north',
                height: 52, // 
                border: false,
                contentEl:'north-top'
            }),{
            	//左边导航菜单
            	region: 'west',
                id: 'west-panel', // see Ext.getCmp() below
                title: '系统导航',
                split: true,
                width: 200,
                minSize: 175,
                maxSize: 400,
                collapsible: true,
                margins: '0 0 0 2',
                layout: {
                    type: 'accordion',
                    animate: true
                },
                items: [{
                	title: '系统管理',
                    contentEl: 'west-left',                    
                    border: false,
                    iconCls: 'nav' // see the HEAD section for style used
                }, {
                    title: '业务设置',
                    html: '<p>Some settings in here.</p>',
                    border: false,
                    iconCls: 'settings'
                }]
            },
			//中部工作区域
			centerTabs,
            //底部状态区域
			new Ext.BoxComponent({
				id:'statusbar',
                region: 'south',
                split:true,
                height: 32, // 
                contentEl:'south-bottom'
            })
		]
	});
});
</script>