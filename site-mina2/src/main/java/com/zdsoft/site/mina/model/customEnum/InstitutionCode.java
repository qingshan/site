package com.zdsoft.site.mina.model.customEnum;

import javax.xml.bind.annotation.XmlEnumValue;

/**
 * 机构代码(InstitutionCode) 银行编号
 * 
 * @author cqyhm
 *
 */
public enum InstitutionCode {
	/** 机构代码银行编号"0","中行" */
	@XmlEnumValue("0") A0("0", "中行"),

	/** 机构代码银行编号"1","工行" */
	@XmlEnumValue("1") A1("1", "工行"),

	/** 机构代码银行编号"2","建行" */
	@XmlEnumValue("2") A2("2", "建行"),

	/** 机构代码银行编号"3","农行" */
	@XmlEnumValue("3") A3("3", "农行"),

	/** 机构代码银行编号"4","交行" */
	@XmlEnumValue("4") A4("4", "交行"),

	/** 机构代码银行编号"5","招行" */
	@XmlEnumValue("5") A5("5", "招行"),

	/** 机构代码银行编号"6","浦发" */
	@XmlEnumValue("6") A6("6", "浦发"),

	/** 机构代码银行编号"7","合行" */
	@XmlEnumValue("7") A7("7", "合行"),

	/** 机构代码银行编号"8","光大" */
	@XmlEnumValue("8") A8("8", "光大"),

	/** 机构代码银行编号"9","华夏" */
	@XmlEnumValue("9") A9("9", "华夏"),

	/** 机构代码银行编号"a","兴业" */
	@XmlEnumValue("a") Aa("a", "兴业"),

	/** 机构代码银行编号"b","民生" */
	@XmlEnumValue("b") Ab("b", "民生"),

	/** 机构代码银行编号"c","深发" */
	@XmlEnumValue("c") Ac("c", "深发"),

	/** 机构代码银行编号"d","中信" */
	@XmlEnumValue("d") Ad("d", "中信");

	private String value;
	private String name;

	private InstitutionCode(String value, String name) {
		this.value = value;
		this.name = name;
	}

	public String getValue() {
		return value;
	}

	public String getName() {
		return name;
	}

	@Override
	public String toString() {
		return value;
	}
}
