package com.zdsoft.site.event.observer;

import java.util.Observable;
import java.util.Observer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ObserverB  extends Observable implements Observer{
	
	private Logger logger=LoggerFactory.getLogger(getClass());
	
	int data = 0;
    @Override
    public void update(Observable object, Object arg) {
    	logger.debug("ObserverB found that ObserverA changed...");
    }
    
    public void setData(int data){
        this.data = data;
        this.setChanged();
        this.notifyObservers();
    }

}
