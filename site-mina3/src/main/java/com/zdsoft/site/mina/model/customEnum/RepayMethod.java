package com.zdsoft.site.mina.model.customEnum;
/**
 * 付款方式(RepayMethod) 
 * 定义  付款方式 
 * @author cqyhm
 *
 */
public enum RepayMethod {
	A0,//客户还款 
	A1,//风险准备金赔付 
	A2,//担保金赔付 
}
