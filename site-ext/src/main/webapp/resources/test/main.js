Ext.QuickTips.init();
Ext.onReady(function(){
	Ext.QuickTips.init();
	console.info('进入函数');
	// 主显示区
	var tabMain = new Ext.TabPanel({
		id : "TabMain",
		region : "center",
		layout : 'fit',  
		autoScroll : true,
		enableTabScroll : true,
		activeTab : 0,
		plugins: [new Ext.ux.TabCloseMenu()],//标签上的右键菜单
		items : [new Ext.Panel({
			id : 'tab-0001',
			title : '首页',
			autoScroll : true,
			layout : 'fit',
			border : false,
			iconCls : 'house',
			autoLoad : {
				url : '../../site-ext/views/home.jsp',//自动载入的页面
				scope : this,
				scripts : true,
				loadMask:true,
				text : '页面加载中,请稍候....'
			}
		}),{
			id:'tab-002',
			title:'用户管理',
			closable:true
		}]
	});
	// 居顶工具栏
	var topBar = new Ext.Toolbar({
		region : 'north',
		border : false,
		split : true,
		height :  26,
		minSize : 26,
		maxSize : 26,
		items : [{
				text : "重庆正大华日软件",
				//cls : 'x-btn-text-icon',
				icon : '../../site-ext/resources/icons/house.png',
				disabled : true,
				disabledClass : ''
			}, "-", {
				text : "金融事业部",
				//cls : 'x-btn-text-icon',
				icon : '../../site-ext/resources/icons/layers.png',
				disabled : true,
				disabledClass : ''
			}, "-", {
				text : "杨华明",
				//cls : 'x-btn-text-icon',
				icon : '../../site-ext/resources/icons/user.png',
				disabled : true,
				disabledClass : ''
			}, "->", "-", {
				//xtype : "button",
				minWidth : 80,
				text : "刷新当前页",
				//cls : "x-btn-text-icon",
				icon : "../../site-ext/resources/icons/arrow_refresh.png",
				handler : function(btn, e) {
					var tab = tabMain.getActiveTab();
					tab.removeAll(true);
					console.info(tab);
					tab.getUpdater().refresh();
				}
			},"-", {
				//xtype : "button",
				minWidth : 80,
				text : "设置为常用页",
				//cls : "x-btn-text-icon",
				icon : "../../site-ext/resources/icons/asterisk_yellow.png",
				handler : function(btn, e) {
				}
			},  "-", {
				//xtype : "button",
				minWidth : 80,
				text : "修改密码",
				//cls : "x-btn-text-icon",
				icon : "../../site-ext/resources/icons/key.png",
				handler : function(btn, e) {
				}
			}, "-", {
				//xtype : "button",
				minWidth : 80,
				text : "帮助",
				//cls : "x-btn-text-icon",
				icon : "../../site-ext/resources/icons/lightbulb.png",
				handler : function(btn, e) {
				}
			},"-", {
				//xtype : "button",
				minWidth : 80,
				text : "注销",
				//cls : "x-btn-text-icon",
				icon : "../../site-ext/resources/icons/lock_go.png",
				handler : function(btn, e) {
				}
			}, "-", {
				//xtype : "button",
				minWidth : 80,
				text : "退出",
				//cls : "x-btn-text-icon",
				icon : "../../site-ext/resources/icons/door_out.png",
				handler : function(btn, e) {
				}
			}]
	});	
	// 左边的菜单
	var menu = new Ext.TreePanel({
		title : '功能菜单',
		region : "west",
		width : 220,
		minSize : 220,
		maxSize : 220,
		iconCls : 'plugin',
		autoScroll : true,
		enableTabScroll : true,
		collapsible : true,
		collapsed : false,//是否折叠
		split : true,
		rootVisible : true,
		lines : false,
		loadMask: true,
		store:new Ext.data.TreeStore({
			root : {
	        	id : '0',
	            text : '系统管理',
	            expanded : true
	        },
			proxy : {
	            type : 'ajax',
	            //actionMethods : 'get',
	            url : '../resources/test/tree.json',
	            reader : {
	                type : 'json'
	            }
	        },
	        autoLoad: false,
	        extraParams: {
                action: 'TreeLoad'
            }
//	        ,folderSort : true,
//	        sorters : [{
//	            property : 'text',
//	            direction : 'ASC'
//	        }]
		}),
		listeners:{
			'itemclick':function(view,record){
//			    var id      = record.get('id');//节点Id
//            	var text    = record.get('text');//节点文字
//            	var leaf    = record.get('leaf');//是否是叶子节点
//            	var tabUrl  = record.get('url');//链接地址
				var row=record.raw;				
				var id=row.id;
				var text=row.text;
				var leaf=row.leaf;
				var tabUrl=row.url;
				
            	var tabPanel = Ext.getCmp('TabMain');//对应的tabs
            	console.info(row);
	            var tab = tabPanel.getComponent(id);
	            if (leaf) {
	                if (!tab) {
	                   tabPanel.add(new Ext.Panel({
	                   		id:id,title:text,closable:true,layout: 'fit',
                    		loadMask: '页面加载中...',
                    		border: false,
	                   		autoLoad:{url:tabUrl}
	                   })).show();
	                }
	                tabPanel.setActiveTab(tab);
	            }else {
	                var expand = record.get('expanded')
	                if (expand) {
	                    view.collapse(record);
	                }else {
	                    view.expand(record);
	                }
				}
			}
		}
	});
	//底部工具栏
	var footBar = new Ext.Toolbar({
		region : "south",
	    margin  : '5 0 0 0',
	    items   : ["->", "老杨版权所有"]
	});
	//创建框架
	new Ext.Viewport({
		id : "Main_ViewPort",
		layout : 'border',
		items:[tabMain,topBar,footBar,menu]
	});
	
});