package com.zdsoft.site.mina.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

import com.zdsoft.site.mina.model.customEnum.FileBusinessCode;

/**
 * 文件信息(FileInfo)
 * 
 * @author cqyhm
 *
 */

@XmlAccessorType(XmlAccessType.FIELD)
public class FileInfo {
	/** 文件业务功能 FileBusCode [BusCode] [0..1] FileBusinessCode 可选 */
	@XmlElement(name = "BusCode")
	private FileBusinessCode BusCode;

	/** 文件业务日期 BusinessDate [BusDate] [0..1] Date 可选 YYYYMMDD */
	@XmlElement(name = "BusDate")
	private String BusDate;

	/** 文件存放主机 Host [Host] [0..1] Max35Text */
	@XmlElement(name = "Host")
	private String Host;

	/** 文件名称 FileName [FileName] [1..1] Max128Text */
	@XmlElement(name = "FileName")
	private String FileName;

	/** 文件长度 FileLength [FileLen] [0..1] Number */
	@XmlElement(name = "FileLen")
	private String FileLen;

	/** 文件时间 FileTime [FileTime] [0..1] DateTime YYYYMMDDHHMMSS */
	@XmlElement(name = "FileTime")
	private String FileTime;

	/** 文件校验码 FileMac [FileMac] [0..1] Max128Text */
	@XmlElement(name = "FileMac")
	private String FileMac;

	/** 文件业务功能 FileBusCode [BusCode] [0..1] FileBusinessCode 可选 */
	public FileBusinessCode getBusCode() {
		return BusCode;
	}

	/** 文件业务功能 FileBusCode [BusCode] [0..1] FileBusinessCode 可选 */
	public void setBusCode(FileBusinessCode fileBusCode) {
		BusCode = fileBusCode;
	}

	/** 文件业务日期 BusinessDate [BusDate] [0..1] Date 可选 YYYYMMDD */
	public String getBusDate() {
		return BusDate;
	}

	/** 文件业务日期 BusinessDate [BusDate] [0..1] Date 可选 YYYYMMDD */
	public void setBusDate(String businessDate) {
		BusDate = businessDate;
	}

	/** 文件存放主机 Host [Host] [0..1] Max35Text */
	public String getHost() {
		return Host;
	}

	/** 文件存放主机 Host [Host] [0..1] Max35Text */
	public void setHost(String host) {
		Host = host;
	}

	/** 文件名称 FileName [FileName] [1..1] Max128Text */
	public String getFileName() {
		return FileName;
	}

	/** 文件名称 FileName [FileName] [1..1] Max128Text */
	public void setFileName(String fileName) {
		FileName = fileName;
	}

	/** 文件长度 FileLength [FileLen] [0..1] Number */
	public String getFileLen() {
		return FileLen;
	}

	/** 文件长度 FileLength [FileLen] [0..1] Number */
	public void setFileLen(String fileLength) {
		FileLen = fileLength;
	}

	/** 文件时间 FileTime [FileTime] [0..1] DateTime YYYYMMDDHHMMSS */
	public String getFileTime() {
		return FileTime;
	}

	/** 文件时间 FileTime [FileTime] [0..1] DateTime YYYYMMDDHHMMSS */
	public void setFileTime(String fileTime) {
		FileTime = fileTime;
	}

	/** 文件校验码 FileMac [FileMac] [0..1] Max128Text */
	public String getFileMac() {
		return FileMac;
	}

	/** 文件校验码 FileMac [FileMac] [0..1] Max128Text */
	public void setFileMac(String fileMac) {
		FileMac = fileMac;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("FileInfo [BusCode=").append(BusCode).append(", BusDate=").append(BusDate).append(", Host=").append(Host).append(", FileName=").append(FileName).append(", FileLen=").append(FileLen).append(", FileTime=").append(FileTime).append(", FileMac=")
				.append(FileMac).append("]");
		return builder.toString();
	}
}
