package com.zdsoft.cms;

import org.junit.Test;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class GreeterServiceTest {

	@Test
	public void testGreet() {
		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("app.xml");
		GreeterService greeterService = context.getBean("greeterServiceImpl", GreeterService.class);
		greeterService.greet("Spring Integration!");
		context.close();
	}

}
