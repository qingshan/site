package com.zdsoft.site.mina.p2p;

import org.apache.mina.core.session.IoSession;

import com.zdsoft.site.mina.model.request.AbstractRequest;
import com.zdsoft.site.mina.model.response.ResponseCancelAcct;
/**
 * 6.1.4　客户撤销银行账号(46703)
 *
 */
public class CancelAcctHandler extends P2PIoHandler {
	
	public CancelAcctHandler(AbstractRequest request) {
		super(request);
	}
	
	@Override
	public void messageSent(IoSession session, Object message) throws Exception {
		//发送的消息已经通过编码器编码为字符串
		logger.debug("1.3.发送数据到服务器：{}",message);
	}
	/**接收到的消息体为xml字符串*/
	@Override
	public void messageReceived(IoSession session, Object message) throws Exception {
		String text=message.toString();
		setResponse(new ResponseCancelAcct(text));
		logger.debug("2.4.接收服务端的数据:[{}]",text);
		ResponseCancelAcct response=(ResponseCancelAcct)getResponse();
		logger.debug("{}",response);
		logger.debug("{}",response.getRst());
		logger.debug("{}",response.getMgeAcct());
	}
	@Override
	public String getInstrCd() {
		return "46703";
	}
}
