package com.zdsoft.site.mina.model;

import com.zdsoft.site.mina.model.customEnum.InstitutionType;

public class Reference {
	//	流水号    <Ref>  [1..1]  Max20Text 
	//流水号：本次交易的唯一标识符(必选) 
	private String reference;
	
	//流水号发布者类型    <IssrType>  [0..1]  InstitutionType
	//流水号发布者类型：本流水号发布者的机构类型,机构类别，取值参考 InstitutionType  
	//可选
	private InstitutionType refrenceIssureType;
	
	//发布者  ReferenceIssure  <RefIssr>  [0..1]  Max30Text
	//本流水号发布者的机构代码 
	private String referenceIssure;

	
	public String getReference() {
		return reference;
	}

	public void setReference(String reference) {
		this.reference = reference;
	}

	public String getReferenceIssure() {
		return referenceIssure;
	}

	public void setReferenceIssure(String referenceIssure) {
		this.referenceIssure = referenceIssure;
	}

	public InstitutionType getRefrenceIssureType() {
		return refrenceIssureType;
	}

	public void setRefrenceIssureType(InstitutionType refrenceIssureType) {
		this.refrenceIssureType = refrenceIssureType;
	}

}
