package com.zdsoft.site.mina.model.response;

import java.math.BigDecimal;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.springframework.beans.BeanUtils;

import com.zdsoft.site.mina.model.Account;
import com.zdsoft.site.mina.model.Reference;
import com.zdsoft.site.mina.model.TransMangAcc;
import com.zdsoft.site.mina.model.customEnum.CurrencyCode;
import com.zdsoft.site.mina.utils.JaxbUtil;

/**
 *  6.1.8.服务商发起客户出金(44702)
 *
 *	1.可以支持重发，重发逻辑为当服务商未收到应答时可以对该笔交易进行重新发起，重发的交易信息必须和原交易完全一致，
 *	      如果银行收到该该笔交易未处理过则处理，并且返回相应结果；如果银行对原交易已经处理过则返回原交易的处理结果
 *	2.使用重发时不使用冲正交易进行交易一致性保障。
 * 应答：
 * 
 * @author cqyhm
 *
 */
@XmlRootElement(name = "MsgText")
public class ResponseCustOutPayment extends AbstractResponse {
		
	/** 03. 银行流水号 BankReference [BkRef] [1..1] Reference 组件 */
	@XmlElement(name = "BkRef")
	private Reference BkRef;

	/** 04. 银行账户 BankAccount [BkAcct] [0..1] Account 组件 */
	@XmlElement(name = "BkAcct")
	private Account BkAcct;

	/** 05. 交易管理账户 ManageAccount [ MgeAcct] [0..1] TransMangAcc 组件 */
	@XmlElement(name = "MgeAcct")
	private TransMangAcc MgeAcct;

	/** 06. 币种 Currency [Ccy] [0..1] CurrencyCode 枚举 */
	@XmlElement(name = "Ccy")
	private CurrencyCode Ccy;

	/** 07. 转帐金额 TransferAmount [TrfAmt] [0..1] Amount */
	@XmlElement(name = "TrfAmt")
	private BigDecimal TrfAmt;

	/** 08. 成功转帐金额 SuccessTransferAmount [SucTrfAmt] [0..1] Amount */
	@XmlElement(name = "SucTrfAmt")
	private BigDecimal SucTrfAmt;

	/**09.手续费	FeeAmount	<FeeAmt>	[0..1]	Amount*/
	@XmlElement(name = "FeeAmt")
	private BigDecimal FeeAmt;

	/** 10. 摘要 Digest [Dgst] [0..1] Max128Text */
	@XmlElement(name = "Dgst")
	private String Dgst;

	public ResponseCustOutPayment() {
	}

	public ResponseCustOutPayment(String xml) {
		ResponseCustOutPayment response = JaxbUtil.converyToJavaBean(xml, this.getClass());
		BeanUtils.copyProperties(response, this);
	}

	/** 银行流水号 BankReference [BkRef] [1..1] Reference 组件 */
	public Reference getBkRef() {
		return BkRef;
	}

	/** 银行流水号 BankReference [BkRef] [1..1] Reference 组件 */
	public void setBkRef(Reference bkRef) {
		BkRef = bkRef;
	}

	/** 银行账户 BankAccount [BkAcct] [0..1] Account 组件 */
	public Account getBkAcct() {
		return BkAcct;
	}

	/** 银行账户 BankAccount [BkAcct] [0..1] Account 组件 */
	public void setBkAcct(Account bkAcct) {
		BkAcct = bkAcct;
	}

	/** 交易管理账户 ManageAccount [ MgeAcct] [0..1] TransMangAcc 组件 */
	public TransMangAcc getMgeAcct() {
		return MgeAcct;
	}

	/** 交易管理账户 ManageAccount [ MgeAcct] [0..1] TransMangAcc 组件 */
	public void setMgeAcct(TransMangAcc mgeAcct) {
		MgeAcct = mgeAcct;
	}

	/** 币种 Currency [Ccy] [0..1] CurrencyCode 枚举 */
	public CurrencyCode getCcy() {
		return Ccy;
	}

	/** 币种 Currency [Ccy] [0..1] CurrencyCode 枚举 */
	public void setCcy(CurrencyCode ccy) {
		Ccy = ccy;
	}

	/** 转帐金额 TransferAmount [TrfAmt] [0..1] Amount */
	public BigDecimal getTrfAmt() {
		return TrfAmt;
	}

	/** 转帐金额 TransferAmount [TrfAmt] [0..1] Amount */
	public void setTrfAmt(BigDecimal trfAmt) {
		TrfAmt = trfAmt;
	}

	/** 成功转帐金额 SuccessTransferAmount [SucTrfAmt] [0..1] Amount */
	public BigDecimal getSucTrfAmt() {
		return SucTrfAmt;
	}

	/** 成功转帐金额 SuccessTransferAmount [SucTrfAmt] [0..1] Amount */
	public void setSucTrfAmt(BigDecimal sucTrfAmt) {
		SucTrfAmt = sucTrfAmt;
	}

	/** 摘要 Digest [Dgst] [0..1] Max128Text */
	public String getDgst() {
		return Dgst;
	}

	/** 摘要 Digest [Dgst] [0..1] Max128Text */
	public void setDgst(String dgst) {
		Dgst = dgst;
	}
	/**09.手续费	FeeAmount	<FeeAmt>	[0..1]	Amount*/
	public BigDecimal getFeeAmt() {
		return FeeAmt;
	}
	/**09.手续费	FeeAmount	<FeeAmt>	[0..1]	Amount*/
	public void setFeeAmt(BigDecimal feeAmt) {
		FeeAmt = feeAmt;
	}
}
