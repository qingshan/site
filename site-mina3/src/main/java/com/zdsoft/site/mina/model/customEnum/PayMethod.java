package com.zdsoft.site.mina.model.customEnum;
/**
 * 还款方式（PayMethod）
 * @author cqyhm
 *
 */
public enum PayMethod {
	A0(0),//  一次性还本付息 
	A1(1),//  先息后本 
	A2(2),//  等额本息 
	A3(3),//  等额本金 
	A4(4);//  其他 
	private Integer value;
	
	private PayMethod(Integer value){
		this.value=value;
	}
	@Override
	public String toString(){
		return String.valueOf(value);
	}
}
