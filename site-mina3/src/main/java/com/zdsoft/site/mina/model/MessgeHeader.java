package com.zdsoft.site.mina.model;

import java.io.Serializable;

import com.zdsoft.site.mina.model.customEnum.InstitutionType;
import com.zdsoft.site.mina.model.customEnum.InstructionCode;
import com.zdsoft.site.mina.model.customEnum.SystemType;
import com.zdsoft.site.mina.model.customEnum.YesNoIndicator;

/**
 *  消息头（MessgeHeader） 
 * @author cqyhm
 *
 */
public class MessgeHeader implements Serializable {

	private static final long serialVersionUID = -6628244502508785879L;
	//	版本 <Ver> [1..1] Max30Text 必选
	//版本号：描述本消息体采用的版本号，版本号由标准委员会统一发布。
	private String Version;
	
	//	应用系统类型  <SysType> [1..1]  SystemType  可选，默认为监管 
	//应用系统类型：指示本消息所属的业务系统，比如监管、普通银证转账等。 
	private SystemType SystemType;
	
	//	业务功能码    <InstrCd> [1..1]  InstructionCode  枚举  必选 
	//业务功能码：指示本消息完成的业务功能。 
	private InstructionCode InstructionCode; 
	
	//	交易发起方    <TradSrc> [1..1]  InstitutionType  枚举 必选 
	//交易发起方：指示本次交易的发起方机构类型，比如是银行发起还是服务商发起。 
	private InstitutionType TradeSource;
	
	//	服务商机构信息   <SvInst> [1..1]  Institution  组件 必选 
	//服务商机构信息：指示本消息的服务商机构信息，包括机构类别、机构代码、营业部机构代码等信息。 
	private Institution ServiceInstitution; 
	
	//	银行机构信息   <BkInst> [1..1]  Institution  组件 必选
	//银行机构信息：指示本消息的银行机构信息，包括机构类别，机构代码，分支机构代码、网点号等信息。 
	private Institution BankInstitution; 
	
	//	发生日期<Date> [0..1]  Date 交易日期  YYYYMMDD 可选 
	private String CreateDate;   
	
	//	发生时间  <Time> [0..1]  Time  交易时间  HHMMSS 可选 
	private String CreateTime; 
	
	//	请求流水号    <RqRef > [1..1]  Reference  组件 必选 
	// 消息流水号：本消息体的唯一标识号,如流水号。 
	private Reference RequestReference;
	
	//	最后分片标志   <LstFrag> [0..1]  YesNoIndicator
	//  最后分片标志：是否为一揽子消息体中的最后一个，Y是，N否。   可选 
	private YesNoIndicator LastFragment;

	
	public String getVersion() {
		return Version;
	}

	public void setVersion(String version) {
		Version = version;
	}
	public String getCreateDate() {
		return CreateDate;
	}

	public void setCreateDate(String createDate) {
		CreateDate = createDate;
	}

	public String getCreateTime() {
		return CreateTime;
	}

	public void setCreateTime(String createTime) {
		CreateTime = createTime;
	}
	/**服务商机构信息*/
	public void setServiceInstitution(Institution serviceInstitution) {
		ServiceInstitution = serviceInstitution;
	}
	/**银行机构信息*/
	public void setBankInstitution(Institution bankInstitution) {
		BankInstitution = bankInstitution;
	}
	/**服务商机构信息*/
	public Institution getServiceInstitution() {
		return ServiceInstitution;
	}
	/**银行机构信息*/
	public Institution getBankInstitution() {
		return BankInstitution;
	}

	public Reference getRequestReference() {
		return RequestReference;
	}

	public void setRequestReference(Reference requestReference) {
		RequestReference = requestReference;
	}

	public InstructionCode getInstructionCode() {
		return InstructionCode;
	}

	public void setInstructionCode(InstructionCode instructionCode) {
		InstructionCode = instructionCode;
	}
	/**交易发起方：指示本次交易的发起方机构类型，比如是银行发起还是服务商发起*/
	public InstitutionType getTradeSource() {
		return TradeSource;
	}
	/**交易发起方：指示本次交易的发起方机构类型，比如是银行发起还是服务商发起*/
	public void setTradeSource(InstitutionType tradeSource) {
		TradeSource = tradeSource;
	}

	public SystemType getSystemType() {
		return SystemType;
	}

	public void setSystemType(SystemType systemType) {
		SystemType = systemType;
	}

	public YesNoIndicator getLastFragment() {
		return LastFragment;
	}
	/**最后分片标志*/
	public void setLastFragment(YesNoIndicator lastFragment) {
		LastFragment = lastFragment;
	} 
}
