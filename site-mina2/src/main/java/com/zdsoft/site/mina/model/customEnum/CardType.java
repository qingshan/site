package com.zdsoft.site.mina.model.customEnum;

import javax.xml.bind.annotation.XmlEnumValue;

public enum CardType {
	
	/**"10","个人借记 "*/
	@XmlEnumValue("10")	A10("10","个人借记 "),
	
	/**"20","个人贷记"*/
	@XmlEnumValue("20")	A20("20","个人贷记");
	
	private String value;
	private String name;
	
	private CardType(String value,String name){
		this.value=value;
		this.name=name;
	}
	
	@Override
	public String toString(){
		return value;
	}

	public String getValue() {
		return value;
	}

	public String getName() {
		return name;
	}	
}
