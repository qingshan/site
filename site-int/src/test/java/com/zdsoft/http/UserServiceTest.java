package com.zdsoft.http;

import static org.junit.Assert.assertNotNull;

import java.util.HashMap;
import java.util.Map;

import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.zdsoft.rock.common.model.domain.ResponseMsg;

public class UserServiceTest {

	private Logger logger=LoggerFactory.getLogger(getClass());
	
	private static ClassPathXmlApplicationContext context=null;
	@BeforeClass
	public static void setup(){
		context=new ClassPathXmlApplicationContext("inthttp.xml");
		assertNotNull(context);
	}
	@Test
	public void testSave1() {
		UserService userService=context.getBean("userService",UserService.class);
		User user=new User("cqyhm","杨华明");
		ResponseMsg<String> msg=userService.save(user);
		
		logger.debug("传递参数:{}",user);
		logger.debug("{}={}",msg.getMsg(),msg.getData());
	}
	@Test
	public void testSave2() {
		UserService userService=context.getBean("userService",UserService.class);
		User user=new User("cqyhm","杨华明");
		Map<String, Object> param=new HashMap<String, Object>();
		param.put("userCd", user.getUserCd());
		param.put("userNm", user.getUserNm());
		ResponseMsg<String> msg=userService.save(param);
		
		logger.debug("传递参数:{}",user);
		logger.debug("{}={}",msg.getMsg(),msg.getData());
	}
	@Test
	public void testSave3() {
		UserService userService=context.getBean("userService",UserService.class);
		
		User user=new User("cqyhm","杨华明");
		ResponseMsg<String> msg=userService.save("cqyhm","杨华明");
		logger.debug("传递参数:{}",user);
		logger.debug("{}={}",msg.getMsg(),msg.getData());
	}	
}
