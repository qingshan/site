package demo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HomeController {

	private static final String template = "Hello, %s!";
	private Logger logger = LoggerFactory.getLogger(getClass());

	@Value("${name:World}")
	private String name;

	@RequestMapping("/")
	public String home() {
		logger.info("注入的名字为:{}", name);
		return String.format(template, name);
	}

}
