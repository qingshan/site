package com.zdsoft.site.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import org.hibernate.annotations.GenericGenerator;

import com.zdsoft.rock.common.model.domain.AbstractEntity;

@Entity
public class Member extends AbstractEntity{

	private static final long serialVersionUID = 2843338824707142206L;
	@Id
	@GenericGenerator(name = "systemUUID", strategy = "uuid")
	@GeneratedValue(generator = "systemUUID")
	@Column(length=32)
	private String id;
	
	@Column(length=32)
	private String cd;
	
	@Column(length=128)
	private String nm;
	
	public Member(String id){
		this.id=id;
	}
	public Member(){}
	public Member(String id, String cd, String nm) {
		super();
		this.id = id;
		this.cd = cd;
		this.nm = nm;
	}
	public Member(String cd, String nm){
		this("",cd,nm);
	}
	@Override
	public String getId() {
		return id;
	}
	@Override
	public void setId(String id) {
		this.id = id;
	}
	public String getCd() {
		return cd;
	}
	public void setCd(String cd) {
		this.cd = cd;
	}
	public String getNm() {
		return nm;
	}
	public void setNm(String nm) {
		this.nm = nm;
	}
	@Override
	public String toString() {
		return "Member [id=" + id + ", cd=" + cd + ", nm=" + nm + "]";
	}
}
