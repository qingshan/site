package com.zdsoft.site.mina.model.request;

import java.math.BigDecimal;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.zdsoft.site.mina.model.Account;
import com.zdsoft.site.mina.model.Customer;
import com.zdsoft.site.mina.model.MessageHeader;
import com.zdsoft.site.mina.model.TransMangAcc;
import com.zdsoft.site.mina.model.customEnum.BusinessType;
import com.zdsoft.site.mina.model.customEnum.CurrencyCode;
import com.zdsoft.site.mina.model.customEnum.PackageFlag;

/**
  *6.1.7 服务商发起网银支付入金(48705) 
  *1，回调URL指支付完成时跳转的网址
  *2，服务商可以直接跳转至应答报文的“支付网关页面”进入网关支付
  *请求：
 * 
 * @author cqyhm
 *
 */
@XmlRootElement(name = "MsgText")
public class RequestOnlinePayment extends AbstractRequest {

	public RequestOnlinePayment(){}
	
	public RequestOnlinePayment(MessageHeader msgHdr){
		setMsgHdr(msgHdr);
	}
	
	/**02 报文标志	PackageFlag	[PkgFlag]	[0..1]	PackageFlag	枚举*/
	@XmlElement(name="PkgFlag")
	private PackageFlag PkgFlag;
	/**03 客户信息 Customer [Cust] [0..1] Customer 组件 */
	@XmlElement(name = "Cust")
	private Customer Cust;
	/**04  银行账户 BankAccount [BkAcct] [1..1] Account 组件 */
	@XmlElement(name = "BkAcct")
	private Account BkAcct;
	/**05  交易管理账户 ManageAccount [MgeAcct] [1..1] TransMangAcc 组件 */
	@XmlElement(name = "MgeAcct")
	private TransMangAcc MgeAcct;
	/**06  服务商汇总账户 ServiceAccount [SrvAcct] [0..1] Account 组件 */
	@XmlElement(name = "SrvAcct")
	private Account SrvAcct;
	/**07 币种 Currency [Ccy] [1..1] CurrencyCode 枚举 */
	@XmlElement(name = "Ccy")
	private CurrencyCode Ccy;
	/**08 业务类别 BusinessType [BusType] [0..1] BusinessType 枚举 */
	@XmlElement(name = "BusType")
	private BusinessType BusType;
	/**09  转帐金额 TransferAmount [TrfAmt] [1..1] Amount */
	@XmlElement(name = "TrfAmt")
	private BigDecimal TrfAmt;
	/**10 手续费	FeeAmount	<FeeAmt>	[0..1]	Amount	*/
	@XmlElement(name="FeeAmt")
	private BigDecimal FeeAmt;
	/**11 回调URL	NotifiURL	<NotiURL>	[1..1]	Max128Text*/
	@XmlElement(name="NotiURL")
	private String NotiURL;
	/**12  摘要 Digest [Dgst] [0..1] Max128Text */
	@XmlElement(name = "Dgst")
	private String Dgst;

	/** 客户信息 Customer [Cust] [0..1] Customer 组件 */
	public Customer getCust() {
		return Cust;
	}

	/** 客户信息 Customer [Cust] [0..1] Customer 组件 */
	public void setCust(Customer cust) {
		Cust = cust;
	}

	/** 银行账户 BankAccount [BkAcct] [1..1] Account 组件 */
	public Account getBkAcct() {
		return BkAcct;
	}

	/** 银行账户 BankAccount [BkAcct] [1..1] Account 组件 */
	public void setBkAcct(Account bkAcct) {
		BkAcct = bkAcct;
	}

	/** 交易管理账户 ManageAccount [MgeAcct] [1..1] TransMangAcc 组件 */
	public TransMangAcc getMgeAcct() {
		return MgeAcct;
	}

	/** 交易管理账户 ManageAccount [MgeAcct] [1..1] TransMangAcc 组件 */
	public void setMgeAcct(TransMangAcc mgeAcct) {
		MgeAcct = mgeAcct;
	}

	/** 服务商汇总账户 ServiceAccount [SrvAcct] [0..1] Account 组件 */
	public Account getSrvAcct() {
		return SrvAcct;
	}

	/** 服务商汇总账户 ServiceAccount [SrvAcct] [0..1] Account 组件 */
	public void setSrvAcct(Account srvAcct) {
		SrvAcct = srvAcct;
	}

	/** 币种 Currency [Ccy] [1..1] CurrencyCode 枚举 */
	public CurrencyCode getCcy() {
		return Ccy;
	}

	/** 币种 Currency [Ccy] [1..1] CurrencyCode 枚举 */
	public void setCcy(CurrencyCode ccy) {
		Ccy = ccy;
	}

	/** 业务类别 BusinessType [BusType] [0..1] BusinessType 枚举 */
	public BusinessType getBusType() {
		return BusType;
	}

	/** 业务类别 BusinessType [BusType] [0..1] BusinessType 枚举 */
	public void setBusType(BusinessType busType) {
		BusType = busType;
	}

	/** 转帐金额 TransferAmount [TrfAmt] [1..1] Amount */
	public BigDecimal getTrfAmt() {
		return TrfAmt;
	}

	/** 转帐金额 TransferAmount [TrfAmt] [1..1] Amount */
	public void setTrfAmt(BigDecimal trfAmt) {
		TrfAmt = trfAmt;
	}

	/** 摘要 Digest [Dgst] [0..1] Max128Text */
	public String getDgst() {
		return Dgst;
	}

	/** 摘要 Digest [Dgst] [0..1] Max128Text */
	public void setDgst(String dgst) {
		Dgst = dgst;
	}
	/**报文标志	PackageFlag	[PkgFlag]	[0..1]	PackageFlag	枚举*/
	public PackageFlag getPkgFlag() {
		return PkgFlag;
	}
	/**报文标志	PackageFlag	[PkgFlag]	[0..1]	PackageFlag	枚举*/
	public void setPkgFlag(PackageFlag pkgFlag) {
		PkgFlag = pkgFlag;
	}
	/**10 手续费	FeeAmount	<FeeAmt>	[0..1]	Amount	*/
	public BigDecimal getFeeAmt() {
		return FeeAmt;
	}
	/**10 手续费	FeeAmount	<FeeAmt>	[0..1]	Amount	*/
	public void setFeeAmt(BigDecimal feeAmt) {
		FeeAmt = feeAmt;
	}
	/**11 回调URL	NotifiURL	<NotiURL>	[1..1]	Max128Text*/
	public String getNotiURL() {
		return NotiURL;
	}
	/**11 回调URL	NotifiURL	<NotiURL>	[1..1]	Max128Text*/
	public void setNotiURL(String notiURL) {
		NotiURL = notiURL;
	}
}
