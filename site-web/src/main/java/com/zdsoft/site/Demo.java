package com.zdsoft.site;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
/**
 * 父类的构造函数应该会先执行
 * @author admin
 *
 */
public class Demo extends Parent{
	private Logger logger=LoggerFactory.getLogger(Demo.class);
	public Demo(){
		//super();
		logger.debug("子类构造");
	}
}
class Parent extends Ancestor{
	private Logger logger=LoggerFactory.getLogger(Parent.class);
	public Parent(){
		logger.debug("父类构造");
	}
}
class Ancestor{
	private Logger logger=LoggerFactory.getLogger(Parent.class);
	public Ancestor(){
		logger.debug("祖类构造");
	}
}