package com.zdsoft.site.thread.bSingleThreadedExecution.Q6;

public class Tool {
    private final String name;
    public Tool(String name) {
        this.name = name;
    }
    public String toString() {
        return "[ " + name + " ]";
    }
}
