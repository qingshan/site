package com.zdsoft.site.mina.model.request;

import java.math.BigDecimal;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.zdsoft.site.mina.model.BidInfo;
import com.zdsoft.site.mina.model.MessageHeader;
import com.zdsoft.site.mina.model.TransMangAcc;
import com.zdsoft.site.mina.model.customEnum.BusinessType;
import com.zdsoft.site.mina.model.customEnum.CurrencyCode;
import com.zdsoft.site.mina.model.customEnum.PackageFlag;

/**
 * 6.1.19.服务商发起借款人还款(42707)
 * 说明：
 * 1.本功能是服务商方发起借款人还款，并由本系统进行监管账户余额处理
 * 2.支持重发操作
 * 请求：
 * 
 * @author cqyhm
 *
 */
@XmlRootElement(name = "MsgText")
public class RequestRepayment extends AbstractRequest {
	
	public RequestRepayment(){}
	
	public RequestRepayment(MessageHeader msgHdr){
		setMsgHdr(msgHdr);
	}
	
	/** 02.报文标志 PackageFlag <PkgFlag> [0..1] PackageFlag 枚举 */
	@XmlElement(name="PkgFlag")
	private PackageFlag PkgFlag;
	
	/** 03.标的信息 BidInfo <BidInfo> [1..1] BidInfo 组件 */
	@XmlElement(name="BidInfo")
	private BidInfo BidInfo;
	
	/** 04.交易管理账户 ManageAccount <MgeAcct> [0..1] TransMangAcc 组件 */
	@XmlElement(name="MgeAcct")
	private TransMangAcc MgeAcct;
	
	/** 05.还款期次 PayPeriod < PayPeriod > [0..1] Max20Text */
	@XmlElement(name="PayPeriod")
	private String PayPeriod;
	
	/** 06.还款金额 TransferAmount <TrfAmt> [1..1] Amount */
	@XmlElement(name="TrfAmt")
	private BigDecimal TrfAmt;
	
	/** 07.手续费 FeeAmount <FeeAmt> [1..1] Amount 含逾期罚息 */
	@XmlElement(name="FeeAmt")
	private BigDecimal FeeAmt;
	
	/** 08.币种 Currency <Ccy> [1..1] CurrencyCode 枚举 */
	@XmlElement(name="Ccy")
	private CurrencyCode Ccy;
	
	/** 09.业务类别 BusinessType <BusType> [0..1] BusinessType 枚举 */
	@XmlElement(name="BusType")
	private BusinessType BusType;
	
	/** 10.短信验证码 MessageBox <MsgBox> [0..1] Max20Text */
	@XmlElement(name="MsgBox")
	private String MsgBox;
	
	/** 11.摘要 Digest <Dgst> [0..1] Max128Text */
	@XmlElement(name="Dgst")
	private String Dgst;

	/** 02.报文标志 PackageFlag <PkgFlag> [0..1] PackageFlag 枚举 */
	public PackageFlag getPkgFlag() {
		return PkgFlag;
	}
	
	/** 02.报文标志 PackageFlag <PkgFlag> [0..1] PackageFlag 枚举 */
	public void setPkgFlag(PackageFlag pkgFlag) {
		PkgFlag = pkgFlag;
	}

	/** 03.标的信息 BidInfo <BidInfo> [1..1] BidInfo 组件 */
	public BidInfo getBidInfo() {
		return BidInfo;
	}

	/** 03.标的信息 BidInfo <BidInfo> [1..1] BidInfo 组件 */
	public void setBidInfo(BidInfo bidInfo) {
		BidInfo = bidInfo;
	}

	/** 04.交易管理账户 ManageAccount <MgeAcct> [0..1] TransMangAcc 组件 */
	public TransMangAcc getMgeAcct() {
		return MgeAcct;
	}

	/** 04.交易管理账户 ManageAccount <MgeAcct> [0..1] TransMangAcc 组件 */
	public void setMgeAcct(TransMangAcc mgeAcct) {
		MgeAcct = mgeAcct;
	}

	/** 05.还款期次 PayPeriod < PayPeriod > [0..1] Max20Text */
	public String getPayPeriod() {
		return PayPeriod;
	}

	/** 05.还款期次 PayPeriod < PayPeriod > [0..1] Max20Text */
	public void setPayPeriod(String payPeriod) {
		PayPeriod = payPeriod;
	}

	/** 06.还款金额 TransferAmount <TrfAmt> [1..1] Amount */
	public BigDecimal getTrfAmt() {
		return TrfAmt;
	}

	/** 06.还款金额 TransferAmount <TrfAmt> [1..1] Amount */
	public void setTrfAmt(BigDecimal trfAmt) {
		TrfAmt = trfAmt;
	}

	/** 07.手续费 FeeAmount <FeeAmt> [1..1] Amount 含逾期罚息 */
	public BigDecimal getFeeAmt() {
		return FeeAmt;
	}

	/** 07.手续费 FeeAmount <FeeAmt> [1..1] Amount 含逾期罚息 */
	public void setFeeAmt(BigDecimal feeAmt) {
		FeeAmt = feeAmt;
	}

	/** 08.币种 Currency <Ccy> [1..1] CurrencyCode 枚举 */
	public CurrencyCode getCcy() {
		return Ccy;
	}

	/** 08.币种 Currency <Ccy> [1..1] CurrencyCode 枚举 */
	public void setCcy(CurrencyCode ccy) {
		Ccy = ccy;
	}

	/** 09.业务类别 BusinessType <BusType> [0..1] BusinessType 枚举 */
	public BusinessType getBusType() {
		return BusType;
	}

	/** 09.业务类别 BusinessType <BusType> [0..1] BusinessType 枚举 */
	public void setBusType(BusinessType busType) {
		BusType = busType;
	}
	
	/** 10.短信验证码 MessageBox <MsgBox> [0..1] Max20Text */
	public String getMsgBox() {
		return MsgBox;
	}

	/** 10.短信验证码 MessageBox <MsgBox> [0..1] Max20Text */
	public void setMsgBox(String msgBox) {
		MsgBox = msgBox;
	}

	/** 11.摘要 Digest <Dgst> [0..1] Max128Text */
	public String getDgst() {
		return Dgst;
	}

	/** 11.摘要 Digest <Dgst> [0..1] Max128Text */
	public void setDgst(String dgst) {
		Dgst = dgst;
	}
}
