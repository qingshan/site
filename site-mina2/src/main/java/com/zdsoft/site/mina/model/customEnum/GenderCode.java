package com.zdsoft.site.mina.model.customEnum;

import javax.xml.bind.annotation.XmlEnumValue;

/**
 * 性别代码
 * 
 * @author cqyhm
 *
 */
public enum GenderCode {
	/** 性别代码 "F","女" */
	@XmlEnumValue("F") F("F", "女"),

	/** 性别代码 "M","男" */
	@XmlEnumValue("M") M("M", "男"),

	/** 性别代码 "O","其他 如机构等" */
	@XmlEnumValue("O") O("O", "其他 如机构等");

	private String value;
	private String name;

	private GenderCode(String value, String name) {
		this.value = value;
		this.name = name;
	}

	@Override
	public String toString() {
		return value;
	}

	public String getValue() {
		return value;
	}

	public String getName() {
		return name;
	}
}
