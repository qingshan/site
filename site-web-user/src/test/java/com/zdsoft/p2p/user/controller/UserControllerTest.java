package com.zdsoft.p2p.user.controller;

import static org.junit.Assert.assertNotNull;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;

import org.junit.Test;
import org.springframework.test.web.servlet.ResultActions;

import com.zdsoft.p2p.TestWebAbstract;

public class UserControllerTest extends TestWebAbstract {

	@Test
	public void testIndex() throws Exception {
		logger.debug("查询会员列表");
		ResultActions resultAction=mockMvc.perform((post("/user/index.do")
			   .param("userCd", "cqyhm")))  
	 	   //.andExpect(status().isOk())
	 	   .andDo(print());
		assertNotNull(resultAction);
	}

	@Test
	public void testUpdate() {
	}

	@Test
	public void testUser() {
	}

	@Test
	public void testList() {
	}

	@Test
	public void testEdit() {
	}

	@Test
	public void testSave() throws Exception {
		logger.debug("查询会员列表");
		ResultActions resultAction=mockMvc.perform((post("/user/save.do")
			   .param("userId", "2000")
			   .param("userCd", "wuch")
			   .param("userNm", "吴冲")))  
	 	   //.andExpect(status().isOk())
			   .andDo(print());
		assertNotNull(resultAction);		
	}

}
