package com.zdsoft.site.mina.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

import com.zdsoft.site.mina.model.customEnum.DealStatus;

/**
 * 7.29  兑付结果信息（RepayResultInfo） 
 * @author cqyhm
 *
 */

@XmlAccessorType(XmlAccessType.FIELD)
public class RepayResultInfo {
	/**兑付流水号  RepayBillSerial  [ RepayBillSeril]  [0..1]  Max20Text */
	@XmlElement(name="RepayBillSeril")
	private String RepayBillSerial;
	
	/**兑付状态  RepayStatus  [ RepayStatus]  [1..1]  DealStatus  枚举*/
	@XmlElement(name="RepayStatus")
	private DealStatus RepayStatus;
	
	/**兑付信息  RepayInformation  [RepayInfo]  [1..1]  Max128Text*/
	@XmlElement(name="RepayInfo")
	private String RepayInfo;
	
	/**备注  Remark  [ Remark ]  [0..1]  Max128Text*/
	@XmlElement(name="Remark")
	private String Remark;
	
	/**兑付流水号  RepayBillSerial  [ RepayBillSeril]  [0..1]  Max20Text */
	public String getRepayBillSerial() {
		return RepayBillSerial;
	}
	/**兑付流水号  RepayBillSerial  [ RepayBillSeril]  [0..1]  Max20Text */
	public void setRepayBillSerial(String repayBillSerial) {
		RepayBillSerial = repayBillSerial;
	}
	/**兑付状态  RepayStatus  [ RepayStatus]  [1..1]  DealStatus  枚举*/
	public DealStatus getRepayStatus() {
		return RepayStatus;
	}
	/**兑付状态  RepayStatus  [ RepayStatus]  [1..1]  DealStatus  枚举*/
	public void setRepayStatus(DealStatus repayStatus) {
		RepayStatus = repayStatus;
	}
	/**兑付信息  RepayInformation  [RepayInfo]  [1..1]  Max128Text*/
	public String getRepayInfo() {
		return RepayInfo;
	}
	/**兑付信息  RepayInformation  [RepayInfo]  [1..1]  Max128Text*/
	public void setRepayInfo(String repayInformation) {
		RepayInfo = repayInformation;
	}
	/**备注  Remark  [ Remark ]  [0..1]  Max128Text*/
	public String getRemark() {
		return Remark;
	}
	/**备注  Remark  [ Remark ]  [0..1]  Max128Text*/
	public void setRemark(String remark) {
		Remark = remark;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("RepayResultInfo [RepayBillSerial=").append(RepayBillSerial).append(", RepayStatus=").append(RepayStatus).append(", RepayInfo=").append(RepayInfo).append(", Remark=").append(Remark).append("]");
		return builder.toString();
	}
}
