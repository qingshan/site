package com.zdsoft.site.mina.model.customEnum;
/**
 * 7.18  赔付标志（RepayFlag） 
 * @author cqyhm
 *
 */
public enum RepayFlag {
	A0(0),//  风险准备金赔付 
	A1(1);// 担保金赔付
	private Integer value;
	
	private RepayFlag(Integer value){
		this.value=value;
	}
	@Override
	public String toString(){
		return String.valueOf(value);
	}
}
