package com.zdsoft.site.mina.model.response;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.springframework.beans.BeanUtils;

import com.zdsoft.site.mina.model.Account;
import com.zdsoft.site.mina.model.Customer;
import com.zdsoft.site.mina.model.Reference;
import com.zdsoft.site.mina.utils.JaxbUtil;
/**
* 6.2.2    客户银行账号绑定 (48702)（建立绑定关系、发送短信验证）
	说明：
	1.P2P平台发起银行卡绑定关系建立发送验证短信请求到托管系统，托管系统接收到交易请求后，返回短信发送的状态。 
	2.绑定的是借记卡，请求报文包括：绑定流水号、银行ID、账户名称、账户号码、开户证件类型、证件号码、手机号
	3.只允许借记卡绑定
*/	 
@XmlRootElement(name="MsgText")
public class ResponseBindCard extends AbstractResponse {
	
	/**银行流水号  BankReference  [BkRef]  [1..1]  Reference  组件 */
	@XmlElement(name="BkRef")
	private Reference BkRef;
	
	/**客户信息  Customer  [Cust]  [0..1]  Customer  组件 */
	@XmlElement(name="Cust")
	private Customer Cust;
	
	/**银行账户  BankAccount  [BkAcct]  [0..1]  Account  组件*/ 
	@XmlElement(name="BkAcct")
	private Account BkAcct;
	
	/**交易管理账户  ManageAccount  [ MgeAcct]  [0..1]  Account  组件*/
	@XmlElement(name="MgeAcct")
	private Account MgeAcct;

	public ResponseBindCard(){}
	
	public ResponseBindCard(String xml){
		ResponseBindCard response=JaxbUtil.converyToJavaBean(xml, this.getClass());
		BeanUtils.copyProperties(response, this);
	}
	/**银行流水号  BankReference  [BkRef]  [1..1]  Reference  组件 */
	public Reference getBkRef() {
		return BkRef;
	}
	/**银行流水号  BankReference  [BkRef]  [1..1]  Reference  组件 */
	public void setBkRef(Reference reference) {
		BkRef = reference;
	}
	/**客户信息  Customer  [Cust]  [0..1]  Customer  组件 */
	public Customer getCust() {
		return Cust;
	}
	/**客户信息  Customer  [Cust]  [0..1]  Customer  组件 */
	public void setCust(Customer customer) {
		Cust = customer;
	}
	/**银行账户  BankAccount  [BkAcct]  [0..1]  Account  组件*/ 
	public Account getBkAcct() {
		return BkAcct;
	}
	/**银行账户  BankAccount  [BkAcct]  [0..1]  Account  组件*/ 
	public void setBkAcct(Account bankAccount) {
		BkAcct = bankAccount;
	}
	/**交易管理账户  ManageAccount  [ MgeAcct]  [0..1]  Account  组件*/
	public Account getMgeAcct() {
		return MgeAcct;
	}
	/**交易管理账户  ManageAccount  [ MgeAcct]  [0..1]  Account  组件*/
	public void setMgeAcct(Account manageAccount) {
		MgeAcct = manageAccount;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ResponseBindCard[")		
				.append("").append(getMsgHdr())
				.append("").append(getRst())
				.append(", BkRef=").append(BkRef)
				.append(", Cust=").append(Cust)
				.append(", BkAcct=").append(BkAcct)
				.append(", MgeAcct=").append(MgeAcct)
			    .append("]");
		return builder.toString();
	}
}
