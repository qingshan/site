package com.zdsoft.p2p.jms.service;

import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jms.listener.SessionAwareMessageListener;
import org.springframework.stereotype.Component;
/**
 * 接受消息并回复
 * @author yhm
 *
 */
@Component
public class ConsumerSessionAwareMessageListener implements SessionAwareMessageListener<TextMessage> {

	private Logger logger=LoggerFactory.getLogger(getClass());
	
	@Qualifier("queueDestination2")
	private Destination destination;
	@Override
	public void onMessage(TextMessage message, Session session) throws JMSException {
		logger.debug("收到一条消息");
		logger.debug("消息内容是：{}",message.getText());
		MessageProducer producer = session.createProducer(destination);
		Message textMessage = session.createTextMessage("ConsumerSessionAwareMessageListener。。。");
		producer.send(textMessage);
	}

}
