package com.zdsoft.site.mina.model.request;

import java.math.BigDecimal;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.zdsoft.site.mina.model.Account;
import com.zdsoft.site.mina.model.Customer;
import com.zdsoft.site.mina.model.MessageHeader;
import com.zdsoft.site.mina.model.TransMangAcc;
import com.zdsoft.site.mina.model.customEnum.BusinessType;
import com.zdsoft.site.mina.model.customEnum.CurrencyCode;
import com.zdsoft.site.mina.model.customEnum.PackageFlag;

/**
 *  6.1.6 服务商发起快捷支付入金(48704) –短信验证并支付
 *	1.可以支持重发，重发逻辑为当服务商未收到应答时可以对该笔交易进行重新发起，重发的交易信息必须和原交易完全一致，
 *		如果银行收到该该笔交易未处理过则处理，并且返回相应结果；如果银行对原交易已经处理过则返回原交易的处理结果
 *	2.使用重发时不使用冲正交易进行交易一致性保障。
 *	请求：
 * 
 * @author cqyhm
 *
 */
@XmlRootElement(name = "MsgText")
public class RequestQuickPayment2 extends AbstractRequest {
	
	public RequestQuickPayment2(){}
	
	public RequestQuickPayment2(MessageHeader msgHdr){
		setMsgHdr(msgHdr);
	}
	
	/**报文标志	PackageFlag	[PkgFlag]	[0..1]	PackageFlag	枚举*/
	@XmlElement(name="PkgFlag")
	private PackageFlag PkgFlag;
	/** 客户信息 Customer [Cust] [0..1] Customer 组件 */
	@XmlElement(name = "Cust")
	private Customer Cust;
	/** 银行账户 BankAccount [BkAcct] [1..1] Account 组件 */
	@XmlElement(name = "BkAcct")
	private Account BkAcct;
	/** 交易管理账户 ManageAccount [MgeAcct] [1..1] TransMangAcc 组件 */
	@XmlElement(name = "MgeAcct")
	private TransMangAcc MgeAcct;
	/** 服务商汇总账户 ServiceAccount [SrvAcct] [0..1] Account 组件 */
	@XmlElement(name = "SrvAcct")
	private Account SrvAcct;
	/** 币种 Currency [Ccy] [1..1] CurrencyCode 枚举 */
	@XmlElement(name = "Ccy")
	private CurrencyCode Ccy;
	/** 业务类别 BusinessType [BusType] [0..1] BusinessType 枚举 */
	@XmlElement(name = "BusType")
	private BusinessType BusType;
	/** 转帐金额 TransferAmount [TrfAmt] [1..1] Amount */
	@XmlElement(name = "TrfAmt")
	private BigDecimal TrfAmt;
	/**短信验证码	MessageBox	[MsgBox]	[1..1]	Max20Text	*/
	@XmlElement(name="MsgBox")
	private String MsgBox;	
	/** 摘要 Digest [Dgst] [0..1] Max128Text */
	@XmlElement(name = "Dgst")
	private String Dgst;

	/** 客户信息 Customer [Cust] [0..1] Customer 组件 */
	public Customer getCust() {
		return Cust;
	}

	/** 客户信息 Customer [Cust] [0..1] Customer 组件 */
	public void setCust(Customer cust) {
		Cust = cust;
	}

	/** 银行账户 BankAccount [BkAcct] [1..1] Account 组件 */
	public Account getBkAcct() {
		return BkAcct;
	}

	/** 银行账户 BankAccount [BkAcct] [1..1] Account 组件 */
	public void setBkAcct(Account bkAcct) {
		BkAcct = bkAcct;
	}

	/** 交易管理账户 ManageAccount [MgeAcct] [1..1] TransMangAcc 组件 */
	public TransMangAcc getMgeAcct() {
		return MgeAcct;
	}

	/** 交易管理账户 ManageAccount [MgeAcct] [1..1] TransMangAcc 组件 */
	public void setMgeAcct(TransMangAcc mgeAcct) {
		MgeAcct = mgeAcct;
	}

	/** 服务商汇总账户 ServiceAccount [SrvAcct] [0..1] Account 组件 */
	public Account getSrvAcct() {
		return SrvAcct;
	}

	/** 服务商汇总账户 ServiceAccount [SrvAcct] [0..1] Account 组件 */
	public void setSrvAcct(Account srvAcct) {
		SrvAcct = srvAcct;
	}

	/** 币种 Currency [Ccy] [1..1] CurrencyCode 枚举 */
	public CurrencyCode getCcy() {
		return Ccy;
	}

	/** 币种 Currency [Ccy] [1..1] CurrencyCode 枚举 */
	public void setCcy(CurrencyCode ccy) {
		Ccy = ccy;
	}

	/** 业务类别 BusinessType [BusType] [0..1] BusinessType 枚举 */
	public BusinessType getBusType() {
		return BusType;
	}

	/** 业务类别 BusinessType [BusType] [0..1] BusinessType 枚举 */
	public void setBusType(BusinessType busType) {
		BusType = busType;
	}

	/** 转帐金额 TransferAmount [TrfAmt] [1..1] Amount */
	public BigDecimal getTrfAmt() {
		return TrfAmt;
	}

	/** 转帐金额 TransferAmount [TrfAmt] [1..1] Amount */
	public void setTrfAmt(BigDecimal trfAmt) {
		TrfAmt = trfAmt;
	}

	/** 摘要 Digest [Dgst] [0..1] Max128Text */
	public String getDgst() {
		return Dgst;
	}

	/** 摘要 Digest [Dgst] [0..1] Max128Text */
	public void setDgst(String dgst) {
		Dgst = dgst;
	}
	/**报文标志	PackageFlag	[PkgFlag]	[0..1]	PackageFlag	枚举*/
	public PackageFlag getPkgFlag() {
		return PkgFlag;
	}
	/**报文标志	PackageFlag	[PkgFlag]	[0..1]	PackageFlag	枚举*/
	public void setPkgFlag(PackageFlag pkgFlag) {
		PkgFlag = pkgFlag;
	}
	/**短信验证码	MessageBox	[MsgBox]	[1..1]	Max20Text	*/
	public String getMsgBox() {
		return MsgBox;
	}
	/**短信验证码	MessageBox	[MsgBox]	[1..1]	Max20Text	*/
	public void setMsgBox(String msgBox) {
		MsgBox = msgBox;
	}
}
