package com.zdsoft.site.event.content;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.event.SmartApplicationListener;
import org.springframework.stereotype.Component;

import com.zdsoft.p2p.user.domain.User;

/**
 * 王五接受消息
 * 异步处理消息@Async
 */
@Component
public class WangwuListener implements SmartApplicationListener {

	private Logger logger = LoggerFactory.getLogger(getClass());
	@Override
	public void onApplicationEvent(final ApplicationEvent event) {
		try {
			Thread.sleep(1000L);	
		} catch (Exception e) {
			logger.debug("延时异常",e);
		}
		logger.debug("【1030】【WangwuListener】[{}]", event.getSource());
    	//throw new AppException("1030","【1030】【WangwuListener】异常");		
	}
	/***
	 * 接收事件处理顺序,越大越靠后
	 **/
	@Override
	public int getOrder() {
		return 30;
	}
	/**
	 * 定义接收的事件类型:可以增加配置判断,在配置的某些情况下不处理该事件
	 */
	@Override
	public boolean supportsEventType(final Class<? extends ApplicationEvent> eventType) {
		return eventType == ContentEvent.class;
	}
	/**
	 * 定义接收事件的来源类型
	 */
	@Override
	public boolean supportsSourceType(final Class<?> sourceType) {
		return sourceType == User.class;
	}

}
