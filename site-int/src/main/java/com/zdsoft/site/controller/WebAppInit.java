package com.zdsoft.site.controller;

import java.util.Enumeration;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ApplicationContextEvent;
import org.springframework.context.event.ContextClosedEvent;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.ContextStartedEvent;
import org.springframework.context.event.ContextStoppedEvent;
import org.springframework.stereotype.Component;
import org.springframework.web.WebApplicationInitializer;

@Component
public class WebAppInit implements WebApplicationInitializer,ApplicationListener<ApplicationContextEvent> {

	private Logger logger=LoggerFactory.getLogger(getClass());
	@Override
	public void onStartup(ServletContext servletContext) throws ServletException {
		Enumeration<String> names=servletContext.getInitParameterNames();
		while (names.hasMoreElements()) {
			String key = names.nextElement();
			logger.debug("初始化:{}={}",key,servletContext.getInitParameter(key));	
		}
		
	}
	@Override
	public void onApplicationEvent(ApplicationContextEvent event) {
		if(event instanceof ContextClosedEvent){
			logger.debug("发生事件:ContextClosedEvent");
		}else if(event instanceof ContextRefreshedEvent){
			logger.debug("发生事件:ContextRefreshedEvent");
		}else if(event instanceof ContextStoppedEvent){
			logger.debug("发生事件:ContextStoppedEvent");
		}else if(event instanceof ContextStartedEvent){
			logger.debug("发生事件:ContextStartedEvent");
		}else {
			logger.debug("发生事件:未知事件:{}",event.getClass().getName());
		}
	}
}
