package com.zdsoft.site.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

/**
 * 客户端(oauth2_client)
 * 用户表存储着认证/资源服务器的用户信息，即资源拥有者；比如用户名/密码；客户端表存储客户端的的客户端id及客户端安全key；在进行授权时使用。
 * @author yhm
 */
@Entity
@Table(name="oauth2_client")
public class Client {
	/**编号*/
	@Id
	@GenericGenerator(name = "systemUUID", strategy = "uuid")
	@GeneratedValue(generator = "systemUUID")
	@Column(length=32)
	private String id;
	
	/**客户端id*/
	@Column(length=128)
	private String client_id;
	
	/**客户端名称*/
	@Column(length=128)
	private String client_name;
	
	/**客户端安全key*/
	@Column(length=128)
	private String client_secret;
}
