<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>用户列表</title>
</head>
<body>
	<p>注入用户列表</p>
	<table>
		<thead>
			<tr>
				<th>编号</th>
				<th>账号</th>
				<th>名称</th>
			</tr>
		</thead>
		<tbody>
			<c:forEach items="${pager.records}" var="u">
				<tr>
					<td>${u.userId}</td>
					<td>${u.userCd}</td>
					<td>${u.userNm}</td>
					<td><a href="${pageContext.request.contextPath}/user/edit.do?id=${u.userId}">编辑</a></td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
</body>
</html>