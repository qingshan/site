package com.zdsoft.site.mina.model.customEnum;
/**
 * 机构代码(InstitutionCode) 银行编号 
 * @author cqyhm
 *
 */
public enum InstitutionCode {
	A0,//中行 
	A1,//工行 
	A2,//建行 
	A3,//农行 
	A4,//交行 
	A5,//招行 
	A6,//浦发 
	A7,//合行 
	A8,//光大 
	A9,//华夏 
	Aa,//兴业 
	Ab,//民生 
	Ac,//深发 
	Ad//中信 
}
