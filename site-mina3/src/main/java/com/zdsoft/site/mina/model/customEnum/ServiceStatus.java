package com.zdsoft.site.mina.model.customEnum;
/**
 * 服务商状态（ServiceStatus） 
 * @author cqyhm
 *
 */
public enum ServiceStatus {
	A0,//  正常 
	A1,//  注销 
	A2,//  签退 
	A3//  清算 
}
