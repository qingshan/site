package com.zdsoft.site.mina.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

import com.zdsoft.site.mina.model.customEnum.BalanceType;
/**
 * 7.9  余额(Balance) 
 * @author cqyhm
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class Balance {
	/**余额类型  BalanceType  [Type]  [0..1]  BalanceType  枚举  可选 */
	@XmlElement(name="Type")
	private BalanceType Type;
	
	/**余额  Balance  [Bal]  [1..1]  Amount  组件 必选 */
	@XmlElement(name="Bal")
	private Balance Balance;
	
	/**余额类型  BalanceType  [Type]  [0..1]  BalanceType  枚举  可选 */
	public BalanceType getType() {
		return Type;
	}
	/**余额类型  BalanceType  [Type]  [0..1]  BalanceType  枚举  可选 */
	public void setType(BalanceType Type) {
		this.Type = Type;
	}
	
	/**余额  Balance  [Bal]  [1..1]  Amount  组件 必选 */
	public Balance getBalance() {
		return Balance;
	}
	/**余额  Balance  [Bal]  [1..1]  Amount  组件 必选 */
	public void setBalance(Balance balance) {
		Balance = balance;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Balance [Type=").append(Type).append(", Balance=").append(Balance).append("]");
		return builder.toString();
	}
}
