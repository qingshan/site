package com.zdsoft.site.mina.server;

import java.io.IOException;
import java.net.InetSocketAddress;

import org.apache.mina.core.filterchain.DefaultIoFilterChainBuilder;
import org.apache.mina.core.service.IoHandlerAdapter;
import org.apache.mina.core.session.IdleStatus;
import org.apache.mina.core.session.IoSession;
import org.apache.mina.filter.codec.ProtocolCodecFilter;
import org.apache.mina.filter.codec.serialization.ObjectSerializationCodecFactory;
import org.apache.mina.transport.socket.SocketAcceptor;
import org.apache.mina.transport.socket.nio.NioSocketAcceptor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ObjectServerHanlder extends IoHandlerAdapter {
	
	private static Logger logger=LoggerFactory.getLogger(ObjectServerHanlder.class);
	private int count = 0;
	private static final int PORT = 8015;
	// 当一个新客户端连接后触发此方法.
	public void sessionCreated(IoSession session) {
		logger.debug("sessionCreated 新客户端连接");
	}

	// 当一个客端端连结进入时 @Override
	public void sessionOpened(IoSession session) throws Exception {
		count++;
		logger.debug("sessionOpened 第 {}个 client 登陆！address： :{}",count,session.getRemoteAddress());
	}

	// 当客户端发送的消息到达时:
	@Override
	public void messageReceived(IoSession session, Object message) throws Exception {
		// // 我们己设定了服务器解析消息的规则是一行一行读取,这里就可转为String:
		// String s = (String) message;
		// // Write the received data back to remote peer
		// logger.debug("收到客户机发来的消息: " + s);
		// // 测试将消息回送给客户端 session.write(s+count); count++;
		HimiObject ho = (HimiObject) message;
		logger.debug("接收到客户端的数据:{}",ho);
		ho.setId(count);
		ho.setName("serverHimi");
		session.write(ho);
	}

	// 当信息已经传送给客户端后触发此方法.
	@Override
	public void messageSent(IoSession session, Object message) {
		HimiObject ho = (HimiObject) message;
		logger.debug("发送到客户端的数据.{}",ho);
	}

	// 当一个客户端关闭时
	@Override
	public void sessionClosed(IoSession session) {
		logger.debug("sessionClosed");
	}

	// 当连接空闲时触发此方法.
	@Override
	public void sessionIdle(IoSession session, IdleStatus status) {
		logger.debug("sessionIdle 连接空闲");
	}

	// 当接口中其他方法抛出异常未被捕获时触发此方法
	@Override
	public void exceptionCaught(IoSession session, Throwable cause) {
		logger.debug("exceptionCaught 其他方法抛出异常");
	}

	public static void main(String[] args) {
		// 创建一个非阻塞的server端Socket ，用NIO
		SocketAcceptor acceptor = new NioSocketAcceptor();
		/*---------接收字符串---------*/
		// //创建一个接收数据过滤器
		// DefaultIoFilterChainBuilder chain = acceptor.getFilterChain();
		// //设定过滤器一行行(/r/n)的读取数据
		// chain.addLast("mychin", new ProtocolCodecFilter(new
		// TextLineCodecFactory() ));
		/*---------接收对象---------*/
		// 创建接收数据的过滤器
		DefaultIoFilterChainBuilder chain = acceptor.getFilterChain();
		// 设定这个过滤器将以对象为单位读取数据
		ProtocolCodecFilter filter = new ProtocolCodecFilter(new ObjectSerializationCodecFactory());
		chain.addLast("objectFilter", filter);

		// 设定服务器消息处理器
		acceptor.setHandler(new ObjectServerHanlder());
		// 服务器绑定的端口
		// 绑定端口，启动服务器
		try {
			acceptor.bind(new InetSocketAddress(PORT));
		} catch (IOException e) {
			logger.debug("Mina Server start for error! {}",PORT);
		}
		logger.debug("Mina Server 运行在端口:{}",PORT);
	}
}
