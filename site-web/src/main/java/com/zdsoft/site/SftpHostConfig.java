package com.zdsoft.site;

/**
 * 
 * 对账的sftp服务器的配置参数,从公共环境中读取
 * sftpHostConfig.host
 * sftpHostConfig.port
 * sftpHostConfig.user
 * sftpHostConfig.privateKey
 * sftpHostConfig.passwd
 * sftpHostConfig.remoteRoot
 * sftpHostConfig.localRoot
 * 
 * @author cqyhm
 *
 */
public class SftpHostConfig {
	
	/** ftp服务器地址 */
	private String host;

	/** ftp服务器端口 */
	private Integer port=22;

	/** ftp服务器访问用户 */
	private String user;

	/** sftp则为设置的私玥*/
	private String privateKey;

	/** ftp服务器访问密码,当存在私玥时则为私玥密码 */
	private String passwd;

	/** 远程主机的FTP主目录 */
	private String remoteRoot;

	/** FTP存放下载文件的本地根目录 */
	private String localRoot;

	public SftpHostConfig(){}
	
	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public Integer getPort() {
		return port;
	}

	public void setPort(Integer port) {
		this.port = port;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getPrivateKey() {
		return privateKey;
	}

	public void setPrivateKey(String privateKey) {
		this.privateKey = privateKey;
	}

	public String getPasswd() {
		return passwd;
	}

	public void setPasswd(String passwd) {
		this.passwd = passwd;
	}

	public String getRemoteRoot() {
		return remoteRoot;
	}

	public void setRemoteRoot(String remoteRoot) {
		this.remoteRoot = remoteRoot;
	}

	public String getLocalRoot() {
		return localRoot;
	}

	public void setLocalRoot(String localRoot) {
		this.localRoot = localRoot;
	}
}
