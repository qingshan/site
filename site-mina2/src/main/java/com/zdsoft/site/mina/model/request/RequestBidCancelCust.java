package com.zdsoft.site.mina.model.request;

import java.math.BigDecimal;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.zdsoft.site.mina.model.BidInfo;
import com.zdsoft.site.mina.model.Customer;
import com.zdsoft.site.mina.model.MessageHeader;
import com.zdsoft.site.mina.model.Reference;
import com.zdsoft.site.mina.model.TransMangAcc;
import com.zdsoft.site.mina.model.customEnum.BusinessType;
import com.zdsoft.site.mina.model.customEnum.CurrencyCode;
import com.zdsoft.site.mina.model.customEnum.PackageFlag;

/**
 * 6.1.16服务商客户标的投标撤销(42704) 说明： 1.本功能是服务商方发起客户对指定标的投标撤销，并由本系统进行监管账户余额处理
 * 2.投标金额为客户投资到标的的金额，投标撤销回退金额 = 投标金额 – 红包抵扣金额 3.支持重发操作 请求：
 * 
 * @author cqyhm
 *
 */
@XmlRootElement(name = "MsgText")
public class RequestBidCancelCust extends AbstractRequest {
	
	public RequestBidCancelCust(){}
	
	public RequestBidCancelCust(MessageHeader msgHdr){
		setMsgHdr(msgHdr);
	}
	
	/** 02 报文标志 PackageFlag <PkgFlag> [0..1] PackageFlag 枚举 */
	@XmlElement(name = "PkgFlag")
	private PackageFlag PkgFlag;

	/** 03.标的信息 BidInfo <BidInfo> [1..1] BidInfo 组件 */
	@XmlElement(name = "BidInfo")
	private BidInfo BidInfo;

	/** 04 客户信息 Customer <Cust> [1..1] Customer 组件 */
	@XmlElement(name = "Cust")
	private Customer Cust;

	/** 05 被撤销的流水号 CancelReference <CnRef> [1..1] Reference 组件 */
	@XmlElement(name = "CnRef")
	private Reference CnRef;

	/** 06.交易管理账户 ManageAccount <MgeAcct> [0..1] TransMangAcc 组件 */
	@XmlElement(name = "MgeAcct")
	private TransMangAcc MgeAcct;

	/** 07 投标份数 TransCount < TransCount> [0..1] Max60Text */
	@XmlElement(name = "TransCount")
	private Integer TransCount;

	/** 08 投标金额 TransferAmount <TrfAmt> [1..1] Amount */
	@XmlElement(name = "TrfAmt")
	private BigDecimal TrfAmt;

	/** 09 红包抵扣金额 RedPackBala <RedPackAmt> [0..1] Amount */
	@XmlElement(name = "RedPackAmt")
	private BigDecimal RedPackAmt;

	/** 10 手续费 FeeAmt <FeeAmt> [0..1] Amount */
	@XmlElement(name = "FeeAmt")
	private BigDecimal FeeAmt;

	/** 11 币种 Currency <Ccy> [1..1] CurrencyCode 枚举 */
	@XmlElement(name = "Ccy")
	private CurrencyCode Ccy;

	/** 12.业务类别 BusinessType <BusType> [0..1] BusinessType 枚举 */
	@XmlElement(name = "BusType")
	private BusinessType BusType;

	/** 13.短信验证码 MessageBox <MsgBox> [0..1] Max20Text */
	@XmlElement(name = "MsgBox")
	private String MsgBox;

	/** 14.摘要 Digest [Dgst] [0..1] Max128Text */
	@XmlElement(name = "Dgst")
	private String Dgst;

	/** 02 报文标志 PackageFlag <PkgFlag> [0..1] PackageFlag 枚举 */
	public PackageFlag getPkgFlag() {
		return PkgFlag;
	}

	/** 02 报文标志 PackageFlag <PkgFlag> [0..1] PackageFlag 枚举 */
	public void setPkgFlag(PackageFlag pkgFlag) {
		PkgFlag = pkgFlag;
	}

	/** 03.标的信息 BidInfo <BidInfo> [1..1] BidInfo 组件 */
	public BidInfo getBidInfo() {
		return BidInfo;
	}

	/** 03.标的信息 BidInfo <BidInfo> [1..1] BidInfo 组件 */
	public void setBidInfo(BidInfo bidInfo) {
		BidInfo = bidInfo;
	}

	/** 04 客户信息 Customer <Cust> [1..1] Customer 组件 */
	public Customer getCust() {
		return Cust;
	}

	/** 04 客户信息 Customer <Cust> [1..1] Customer 组件 */
	public void setCust(Customer cust) {
		Cust = cust;
	}

	/** 05 被撤销的流水号 CancelReference <CnRef> [1..1] Reference 组件 */
	public Reference getCnRef() {
		return CnRef;
	}

	/** 05 被撤销的流水号 CancelReference <CnRef> [1..1] Reference 组件 */
	public void setCnRef(Reference cnRef) {
		CnRef = cnRef;
	}

	/** 06.交易管理账户 ManageAccount <MgeAcct> [0..1] TransMangAcc 组件 */
	public TransMangAcc getMgeAcct() {
		return MgeAcct;
	}

	/** 06.交易管理账户 ManageAccount <MgeAcct> [0..1] TransMangAcc 组件 */
	public void setMgeAcct(TransMangAcc mgeAcct) {
		MgeAcct = mgeAcct;
	}

	/** 07 投标份数 TransCount < TransCount> [0..1] Max60Text */
	public Integer getTransCount() {
		return TransCount;
	}

	/** 07 投标份数 TransCount < TransCount> [0..1] Max60Text */
	public void setTransCount(Integer transCount) {
		TransCount = transCount;
	}

	/** 08 投标金额 TransferAmount <TrfAmt> [1..1] Amount */
	public BigDecimal getTrfAmt() {
		return TrfAmt;
	}

	/** 08 投标金额 TransferAmount <TrfAmt> [1..1] Amount */
	public void setTrfAmt(BigDecimal trfAmt) {
		TrfAmt = trfAmt;
	}

	/** 09 红包抵扣金额 RedPackBala <RedPackAmt> [0..1] Amount */
	public BigDecimal getRedPackAmt() {
		return RedPackAmt;
	}

	/** 09 红包抵扣金额 RedPackBala <RedPackAmt> [0..1] Amount */
	public void setRedPackAmt(BigDecimal redPackAmt) {
		RedPackAmt = redPackAmt;
	}

	/** 10 手续费 FeeAmt <FeeAmt> [0..1] Amount */
	public BigDecimal getFeeAmt() {
		return FeeAmt;
	}

	/** 10 手续费 FeeAmt <FeeAmt> [0..1] Amount */
	public void setFeeAmt(BigDecimal feeAmt) {
		FeeAmt = feeAmt;
	}

	/** 11 币种 Currency <Ccy> [1..1] CurrencyCode 枚举 */
	public CurrencyCode getCcy() {
		return Ccy;
	}

	/** 11 币种 Currency <Ccy> [1..1] CurrencyCode 枚举 */
	public void setCcy(CurrencyCode ccy) {
		Ccy = ccy;
	}

	/** 12.业务类别 BusinessType <BusType> [0..1] BusinessType 枚举 */
	public BusinessType getBusType() {
		return BusType;
	}

	/** 12.业务类别 BusinessType <BusType> [0..1] BusinessType 枚举 */
	public void setBusType(BusinessType busType) {
		BusType = busType;
	}

	/** 13.短信验证码 MessageBox <MsgBox> [0..1] Max20Text */
	public String getMsgBox() {
		return MsgBox;
	}

	/** 13.短信验证码 MessageBox <MsgBox> [0..1] Max20Text */
	public void setMsgBox(String msgBox) {
		MsgBox = msgBox;
	}

	/** 14.摘要 Digest [Dgst] [0..1] Max128Text */
	public String getDgst() {
		return Dgst;
	}

	/** 14摘要 Digest [Dgst] [0..1] Max128Text */
	public void setDgst(String digest) {
		Dgst = digest;
	}

}
