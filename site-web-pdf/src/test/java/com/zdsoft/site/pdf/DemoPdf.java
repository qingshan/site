package com.zdsoft.site.pdf;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StringUtils;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Image;
import com.itextpdf.text.pdf.AcroFields;
import com.itextpdf.text.pdf.AcroFields.Item;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfCopy;
import com.itextpdf.text.pdf.PdfImportedPage;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfStamper;

/**
 * http://www.cnblogs.com/julyluo/archive/2012/07/23/2605747.html
 * http://blog.chinaunix.net/uid-122937-id-3051170.html
 * http://www.open-open.com/34.htm
 * http://www.open-open.com/doc/view/f38ac665b7f14d4b816fc44b76926f14
 * @author Admin
 *
 */
public class DemoPdf {
	private Logger logger=LoggerFactory.getLogger(getClass());

	/**
	 * 【功能描述：给PDF 加水印功能，（文字水印和图片水印）】 
	 *  【功能详细描述：逐条详细描述功能】
	 * 
	 * @author 【lfssay】
	 * @see 【相关类/方法】
	 * @version 【类版本号, 2013-8-20 上午11:22:21】
	 * @since 【产品/模块版本】
	 */
	    public void main(String[] args) throws DocumentException, IOException {
	        addWaterMark("E:/tt/1.pdf", "E:/tt/129.pdf", "国家图书馆藏", "E:/tt/1.jpg", 400, 800, 200, 800);
	    }

	/**
	 * 
	 * 【功能描述：添加图片和文字水印】 【功能详细描述：功能详细描述】
	 * 
	 * @see 【类、类#方法、类#成员】
	 * @param srcFile
	 *            待加水印文件
	 * @param destFile
	 *            加水印后存放地址
	 * @param text
	 *            加水印的文本内容
	 * @param imgFile
	 *            加水印图片文件
	 * @param textWidth
	 *            文字横坐标
	 * @param textHeight
	 *            文字纵坐标
	 * @param imgWidth
	 *            图片横坐标
	 * @param imgHeight
	 *            图片纵坐标
	 * @throws IOException
	 * @throws DocumentException
	 */
	public void addWaterMark(String srcFile, String destFile, String text, String imgFile, int textWidth, int textHeight, int imgWidth, int imgHeight) throws IOException, DocumentException {
		// 待加水印的文件
		PdfReader reader = new PdfReader(srcFile);
		// 加完水印的文件
		PdfStamper stamper = new PdfStamper(reader, new FileOutputStream(destFile));

		int total = reader.getNumberOfPages() + 1;
		PdfContentByte content;
		String path = this.getClass().getResource("/").getPath();
		// 设置字体
		// BaseFont base = BaseFont.createFont("STSong-Light", "UniGB-UCS2-H",
		// BaseFont.EMBEDDED);
		// 加载字库来完成对字体的创建
		BaseFont font = BaseFont.createFont(path + "MSYH.TTF", BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
		// BaseFont base2 = BaseFont.createFont(BaseFont.HELVETICA,
		// BaseFont.WINANSI, BaseFont.EMBEDDED);
		// 水印文字
		String waterText = text;
		Image image = null;
		if (!StringUtils.isEmpty(imgFile)) {
			image = Image.getInstance(imgFile);
			image.setAbsolutePosition(imgWidth, imgHeight);
			// 设置图片的显示大小
			image.scaleToFit(100, 125);
		}
		int j = waterText.length(); // 文字长度
		char c = 0;
		int high = 0;// 高度
		// 循环对每页插入水印
		for (int i = 1; i < total; i++) {
			// 水印的起始
			high = 50;
			// 水印在之前文本之上
			content = stamper.getOverContent(i);
			if (image != null) {
				content.addImage(image);
			}

			if (!StringUtils.isEmpty(text)) {
				// 开始
				content.beginText();
				// 设置颜色 默认为蓝色
				content.setColorFill(BaseColor.BLUE);
				// content.setColorFill(Color.GRAY);
				// 设置字体及字号
				content.setFontAndSize(font, 38);
				// 设置起始位置
				// content.setTextMatrix(400, 880);
				content.setTextMatrix(textWidth, textHeight);
				// 开始写入水印
				content.showTextAligned(Element.ALIGN_LEFT, text, textWidth, textHeight, 45);
				// for (int k = 0; k < j; k++) {
				// content.setTextRise(14);
				// c = waterText.charAt(k);
				// // 将char转成字符串
				// content.showText(c + "");
				// high -= 5;
				// }
				content.endText();
			}
		}
		stamper.close();
		logger.info("%s%s%s",j,high,c);
		logger.info("%s添加水印到%s成功",srcFile,destFile);
	}

	public void test1(){
		try {
			int count = 8;// 总记录数
			int pageCount = 4;// 每页记录数
			int index = 1; // 表格序号
			int page = 0;// 总共页数
			/** 主要控制总共的页数 */
			if (count >= pageCount && count % pageCount == 0) {
				page = count / pageCount;
			} else {
				page = count / pageCount + 1;
			}
			String TemplatePDF = "c:/test/PdfTemplate.pdf";// 设置模板路径
			FileOutputStream fos = new FileOutputStream("c:/test/Pdf.pdf");// 需要生成PDF

			ByteArrayOutputStream baos[] = new ByteArrayOutputStream[page];// 用于存储每页生成PDF流
			/** 向PDF模板中插入数据 */
			for (int item = 0; item < page; item++) {
				baos[item] = new ByteArrayOutputStream();
				PdfReader reader = new PdfReader(TemplatePDF);
				PdfStamper stamp = new PdfStamper(reader, baos[item]);
				AcroFields form = stamp.getAcroFields();
				form.setField("DepartmnetNmae", "蓝飞");// 插入的数据都为字符类型
				form.setField("qq", "252462807");
				form.setField("pageNumber", "第" + (item + 1) + "页,共" + page + "页");
				if (count % pageCount != 0 && item == page - 1) {
					System.out.println("====pageCount+" + pageCount + "=====");
					pageCount = count % pageCount;
				}
				/** 因为PDF中的表格其实是众多的文本域组成,就是一个数组,所以把它循环出来就可以了 */
				for (int j = 0; j < pageCount; j++) {
					form.setField("ProjectTask[" + j + "]", index + "");
					form.setField("星期一[" + j + "]", "星期一[" + index + "]");
					form.setField("星期二[" + j + "]", "星期二[" + index + "]");
					form.setField("星期三[" + j + "]", "星期三[" + index + "]");
					form.setField("星期四[" + j + "]", "星期四[" + index + "]");
					form.setField("星期五[" + j + "]", "星期五[" + index + "]");
					form.setField("星期六[" + j + "]", "星期六[" + index + "]");
					form.setField("星期日[" + j + "]", "星期日[" + index + "]");
					form.setField("意见[" + j + "]", "同意[" + j + "]");
					index++;
				}
				stamp.setFormFlattening(true); // 千万不漏了这句啊, */
				stamp.close();
			}
			Document doc = new Document();
			PdfCopy pdfCopy = new PdfCopy(doc, fos);
			doc.open();
			PdfImportedPage impPage = null;
			/** 取出之前保存的每页内容 */
			for (int i = 0; i < page; i++) {
				impPage = pdfCopy.getImportedPage(new PdfReader(baos[i].toByteArray()), 1);
				pdfCopy.addPage(impPage);
			}
			doc.close();// 当文件拷贝 记得关闭doc
			logger.debug("根据模板生成成功");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}
	/**添加图片功能
	 **/
	public void addImg() throws Exception {
		PdfReader reader = new PdfReader("temp.pdf");
		PdfStamper stamp = new PdfStamper(reader, new FileOutputStream("model.pdf"));
		Image img = Image.getInstance("code.png"); // 使用png格式
		img.setAlignment(Image.LEFT | Image.TEXTWRAP);
		img.setBorderWidth(10);
		img.setAbsolutePosition(420, 240);
		img.scaleToFit(1000, 60);// 大小
		PdfContentByte over = stamp.getUnderContent(1); // overCount 与underCount
		over.addImage(img);
		stamp.close();
		reader.close();
	}
	public void test3(){
//		String inputFile = "conf/template/test.html";
//		String url = new File(inputFile).toURI().toURL().toString();
//		String outputFile = "firstdoc.pdf";
//		OutputStream os = new FileOutputStream(outputFile);
//		ITextRenderer renderer = new ITextRenderer();
//		renderer.setDocument(url);
//		// 解决中文支持问题
//		ITextFontResolver fontResolver = renderer.getFontResolver();
//		fontResolver.addFont("C:/Windows/Fonts/arialuni.ttf", BaseFont.IDENTITY_H, BaseFont.NOT_EMBEDDED);
//		// 解决图片的相对路径问题
//		renderer.getSharedContext().setBaseURL("file:/D:/Work/Demo2do/Yoda/branch/Yoda%20-%20All/conf/template/");
//		renderer.layout();
//		renderer.createPDF(os);
//
//		os.close();
	}
	/**
	 * @throws IOException
	 * @throws DocumentException
	 */
	@Test
	public void fillTemplate() throws IOException, DocumentException {
		String templateFile="c:/contract.pdf";
		PdfReader reader = new PdfReader(templateFile); // 模版文件目录
		PdfStamper ps = new PdfStamper(reader, new FileOutputStream("c:/fillTemplate.pdf")); // 生成的输出流
		
		AcroFields s = ps.getAcroFields();
		Map<String, Item> fieldMap = s.getFields(); // pdf表单相关信息展示
		for (Map.Entry<String, Item> entry : fieldMap.entrySet()) {
			String name = entry.getKey();           // name就是pdf模版中各个文本域的名字
			Item item = entry.getValue();
			logger.debug("{}={}",name,item);
		}
		s.setField("userNameA", "重庆正大软件");
		s.setField("userNameB", "重庆华日软件");
		ps.setFormFlattening(true); // 这句不能少
		ps.close();
		reader.close();
	}

	/**
	 * 多个PDF合并功能
	 * 
	 * @param files
	 *            多个PDF的文件路径
	 * @param os
	 *            生成的输出流
	 */
	public static void mergePdfFiles(String[] files, OutputStream os) {
		try {
			Document document = new Document(
					new PdfReader(files[0]).getPageSize(1));
			PdfCopy copy = new PdfCopy(document, os);
			document.open();
			for (int i = 0; i < files.length; i++) {
				PdfReader reader = new PdfReader(files[i]);
				int n = reader.getNumberOfPages();
				for (int j = 1; j <= n; j++) {
					document.newPage();
					PdfImportedPage page = copy.getImportedPage(reader, j);
					copy.addPage(page);
				}
			}
			document.close();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}
	/**
	 * 多个PDF合并功能
	 * 
	 * @param osList
	 * @param os
	 */
	public static void mergePdfFiles(List<ByteArrayOutputStream> osList,
			OutputStream os) {
		try {
			Document document = new Document(new PdfReader(osList.get(0)
					.toByteArray()).getPageSize(1));
			PdfCopy copy = new PdfCopy(document, os);
			document.open();
			for (int i = 0; i < osList.size(); i++) {
				PdfReader reader = new PdfReader(osList.get(i).toByteArray());
				int n = reader.getNumberOfPages();
				for (int j = 1; j <= n; j++) {
					document.newPage();
					PdfImportedPage page = copy.getImportedPage(reader, j);
					copy.addPage(page);
				}
			}
			document.close();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}
	/**
	 * 单个Pdf文件分割成N个文件
	 * 
	 * @param filepath
	 * @param N
	 */
	public static void partitionPdfFile(String filepath, int N) {
		Document document = null;
		PdfCopy copy = null;

		try {
			PdfReader reader = new PdfReader(filepath);
			int n = reader.getNumberOfPages();
			if (n < N) {
				System.out.println("The document does not have " + N
						+ " pages to partition !");
				return;
			}
			int size = n / N;
			String staticpath = filepath.substring(0,
					filepath.lastIndexOf("\\") + 1);
			String savepath = null;
			List<String> savepaths = new ArrayList<String>();
			for (int i = 1; i <= N; i++) {
				if (i < 10) {
					savepath = filepath.substring(
							filepath.lastIndexOf("\\") + 1,
							filepath.length() - 4);
					savepath = staticpath + savepath + "0" + i + ".pdf";
					savepaths.add(savepath);
				} else {
					savepath = filepath.substring(
							filepath.lastIndexOf("\\") + 1,
							filepath.length() - 4);
					savepath = staticpath + savepath + i + ".pdf";
					savepaths.add(savepath);
				}
			}

			for (int i = 0; i < N - 1; i++) {
				document = new Document(reader.getPageSize(1));
				copy = new PdfCopy(document, new FileOutputStream(
						savepaths.get(i)));
				document.open();
				for (int j = size * i + 1; j <= size * (i + 1); j++) {
					document.newPage();
					PdfImportedPage page = copy.getImportedPage(reader, j);
					copy.addPage(page);
				}
				document.close();
			}

			document = new Document(reader.getPageSize(1));
			copy = new PdfCopy(document, new FileOutputStream(
					savepaths.get(N - 1)));
			document.open();
			for (int j = size * (N - 1) + 1; j <= n; j++) {
				document.newPage();
				PdfImportedPage page = copy.getImportedPage(reader, j);
				copy.addPage(page);
			}
			document.close();

		} catch (IOException e) {
			e.printStackTrace();
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}
	
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		resp.setContentType("application/pdf");
		ServletOutputStream sos = resp.getOutputStream();
		FileInputStream in = new FileInputStream("f:/fillTemplate.pdf");
		byte data[] = new byte[1024];

		int len = 0;
		while ((len = in.read(data)) != -1) {
			sos.write(data, 0, len);
		}

		sos.flush();
		in.close();
		sos.close();
	}
	
	protected void doGetxxx(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		resp.setContentType("application/pdf");

		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		PdfReader reader = null;
		PdfStamper ps = null;
		try {
			reader = new PdfReader(""); // 模版文件目录
			ps = new PdfStamper(reader, baos);
			AcroFields s = ps.getAcroFields();
			s.setField("CUSTOMERNAME", "as该多好公司");
			s.setField("TEL", "123456asdzxc");
			s.setField("CONTACT", "我是联系人123");

			ps.setFormFlattening(true); // 这句不能少
			ps.close();
			reader.close();
		} catch (DocumentException e) {
			e.printStackTrace();
		}

		ServletOutputStream sos = resp.getOutputStream();
		baos.writeTo(sos);
		sos.flush();
		sos.close();
	}
	public void test2() {
//		Document document = new Document();
//		StyleSheet st = new StyleSheet();
//		st.loadTagStyle("body", "leading", "16,0");
//		PdfWriter.getInstance(document, new FileOutputStream("html2.pdf"));
//		document.open();
//		ArrayList p = HTMLWorker.parseToList(new FileReader("example.html"), st);
//		for (int k = 0; k < p.size(); ++k)
//			document.add((Element) p.get(k));
//		document.close();
	}
	public void testPrint(){
//		try {
//			Executable ex = new Executable();
//			ex.openDocument("c:/test/Pdf.pdf");
//			ex.printDocument("c:/test/Pdf.pdf");
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
	}
//	public void createPdf() {
//		// A4纸大小 // 左、右、上、下 // /* // 使用中文字体
//		Document document = new Document(PageSize.A4, 80, 79, 20, 45); 
//		BaseFont bfChinese = BaseFont.createFont("STSongStd-Light", "UniGB-UCS2-H", BaseFont.NOT_EMBEDDED); // 中文处理
//		Font subBoldFontChinese = new Font(bfChinese, 8, Font.BOLD); // 币种和租金金额的小一号字体
//		Font BoldChinese = new Font(bfChinese, 14, Font.BOLD); // 粗体
//		Font titleChinese = new Font(bfChinese, 20, Font.BOLD); // 模板抬头的字体
//		//Font subFontChinese = new Font(bfChinese, 12, Font.COURIER); // 币种和租金金额的小一号字体
//		//Font moneyFontChinese = new Font(bfChinese, 8, Font.COURIER); // 币种和租金金额的小一号字体
//		Font FontChinese = new Font(bfChinese, 14, Font.COURIER); // 其他所有文字字体
//		PdfWriter.getInstance(document, new FileOutputStream("D:/contract.pdf"));
//		document.open(); // 打开文档
//		// ------------开始写数据-------------------
//		Paragraph title = new Paragraph("起租通知书", titleChinese);// 抬头
//		title.setAlignment(Element.ALIGN_CENTER); // 居中设置
//		title.setLeading(1f);// 设置行间距//设置上面空白宽度
//		document.add(title);
//
//		title = new Paragraph("致：XXX公司", BoldChinese);// 抬头
//		title.setSpacingBefore(25f);// 设置上面空白宽度
//		document.add(title);
//
//		title = new Paragraph("         贵我双方签署的编号为 XXX有关起租条件已满足，现将租赁合同项下相关租赁要素明示如下：", FontChinese);
//		title.setLeading(22f);// 设置行间距
//		document.add(title);
//		int isExpress=6;
//		float[] widths = { 10f, 25f, 30f, 30f };// 设置表格的列宽和列数 默认是4列
//		if (isExpress == 5) { // 如果是明示就是6列
//			widths = new float[] { 8f, 15f, 19f, 19f, 19f, 20f };
//		} else if (isExpress == 6) { // 如果是业发事业部就是7列
//			widths = new float[] { 8f, 15f, 15f, 15f, 15f, 16f, 16f };
//		}
//
//		PdfPTable table = new PdfPTable(widths);// 建立一个pdf表格
//		table.setSpacingBefore(20f);// 设置表格上面空白宽度
//		table.setTotalWidth(500);// 设置表格的宽度
//		table.setWidthPercentage(100);// 设置表格宽度为%100
//		// table.getDefaultCell().setBorder(0);//设置表格默认为无边框
//
//		String[] tempValue = new String[]{"1","2011-07-07","2222","11.11","11.11","3000","9999"}; // 租金期次列表
//		int rowCount = 1; // 行计数器
//		PdfPCell cell = null;
//		// ---表头
//		cell = new PdfPCell(new Paragraph("期次", subBoldFontChinese));// 描述
//		cell.setFixedHeight(20);
//		cell.setHorizontalAlignment(Element.ALIGN_CENTER);// 设置内容水平居中显示
//		cell.setVerticalAlignment(Element.ALIGN_MIDDLE); // 设置垂直居中
//		table.addCell(cell);
//		cell = new PdfPCell(new Paragraph("租金日", subBoldFontChinese));// 描述
//		cell.setFixedHeight(20);
//		cell.setHorizontalAlignment(Element.ALIGN_CENTER);// 设置内容水平居中显示
//		cell.setVerticalAlignment(Element.ALIGN_MIDDLE); // 设置垂直居中
//		table.addCell(cell);
//		cell = new PdfPCell(new Paragraph("各期租金金额", subBoldFontChinese));// 描述
//		cell.setFixedHeight(20);
//		cell.setHorizontalAlignment(Element.ALIGN_CENTER);// 设置内容水平居中显示
//		cell.setVerticalAlignment(Element.ALIGN_MIDDLE); // 设置垂直居中
//		table.addCell(cell);
//
//		cell = new PdfPCell(new Paragraph("各期租金后\n剩余租金", subBoldFontChinese));// 描述
//		cell.setHorizontalAlignment(Element.ALIGN_CENTER);// 设置内容水平居中显示
//		cell.setVerticalAlignment(Element.ALIGN_MIDDLE); // 设置垂直居中
//		cell.setFixedHeight(20);
//		table.addCell(cell);
//
//		for (int j = 1; j < tempValue.length; j++) {
//			if (j % argument == 1) { // 第一列 日期
//				cell = new PdfPCell(new Paragraph(rowCount + "", moneyFontChinese));// 描述
//				cell.setFixedHeight(20);
//				cell.setHorizontalAlignment(Element.ALIGN_CENTER);// 设置内容水平居中显示
//				cell.setVerticalAlignment(Element.ALIGN_MIDDLE); // 设置垂直居中
//				table.addCell(cell);
//				rowCount++;
//			}
//			cell = new PdfPCell(new Paragraph(tempValue[j], moneyFontChinese));// 描述
//			cell.setFixedHeight(20);
//			cell.setHorizontalAlignment(Element.ALIGN_CENTER);// 设置内容水平居中显示
//			cell.setVerticalAlignment(Element.ALIGN_MIDDLE); // 设置垂直居中
//			table.addCell(cell);
//		}
//		document.add(table);
//
//		title = new Paragraph("                租金总额：XXX", FontChinese);
//		title.setLeading(22f);// 设置行间距
//		document.add(title);
//		title = new Paragraph("         特此通知！", FontChinese);
//		title.setLeading(22f);// 设置行间距
//		document.add(title);
//		// -------此处增加图片和日期，因为图片会遇到跨页的问题，图片跨页，图片下方的日期就会脱离图片下方会放到上一页。
//		// 所以必须用表格加以固定的技巧来实现
//		float[] widthes = { 50f };// 设置表格的列宽和列数
//		PdfPTable hiddenTable = new PdfPTable(widthes);// 建立一个pdf表格
//		hiddenTable.setSpacingBefore(11f); // 设置表格上空间
//		hiddenTable.setTotalWidth(500);// 设置表格的宽度
//		hiddenTable.setWidthPercentage(100);// 设置表格宽度为%100
//		hiddenTable.getDefaultCell().disableBorderSide(1);
//		hiddenTable.getDefaultCell().disableBorderSide(2);
//		hiddenTable.getDefaultCell().disableBorderSide(4);
//		hiddenTable.getDefaultCell().disableBorderSide(8);
//
//		Image upgif = Image.getInstance("D:/opt/yd_apps/rim/uploadfolder/stamp1.jpg");
//		upgif.scalePercent(7.5f);// 设置缩放的百分比%7.5
//		upgif.setAlignment(Element.ALIGN_RIGHT);
//
//		cell = new PdfPCell(new Paragraph("", FontChinese));// 描述
//		cell.setHorizontalAlignment(Element.ALIGN_RIGHT);// 设置内容水平居中显示
//		cell.addElement(upgif);
//		cell.setPaddingTop(0f); // 设置内容靠上位置
//		cell.setPaddingBottom(0f);
//		cell.setPaddingRight(20f);
//		cell.setBorder(Rectangle.NO_BORDER);// 设置单元格无边框
//		hiddenTable.addCell(cell);
//
//		cell = new PdfPCell(new Paragraph("XX 年 XX 月 XX 日                    ", FontChinese));// 金额
//		cell.setHorizontalAlignment(Element.ALIGN_RIGHT);// 设置内容水平居中显示
//		cell.setPaddingTop(0f);
//		cell.setPaddingRight(20f);
//		cell.setBorder(Rectangle.NO_BORDER);
//		hiddenTable.addCell(cell);
//		document.add(hiddenTable);
//		logger.debug("拼装起租通知书结束...");
//		document.close();
//	}
	/**
     * 填充PDF模板
     * 
     * @throws IOException
     * @throws DocumentException
     */
//    public static void fillTemplatePDF(Session session,lotus.domino.Document doc,java.io.InputStream isData,String gwType)
//            throws IOException, DocumentException, Exception {
//        PdfReader reader = new PdfReader(isData);
//        PdfStamper stamper = new PdfStamper(reader, new FileOutputStream(String.format(null, TMP_RESULT_MAIN, null)));
//        fill(session,doc, stamper.getAcroFields(),gwType);
//        stamper.setFormFlattening(true);
//        stamper.close();
//    }
//    /**
//     * 将模板中的表单字段赋值
//     */
//    public static void fill(Session session,lotus.domino.Document doc, AcroFields form,String gwType)
//            throws IOException, DocumentException, Exception {
//        String strFieldName = "";
//        //使用中文字体 
//        BaseFont bf = BaseFont.createFont("STSongStd-Light",  "UniGB-UCS2-H",
//                false);
////        BaseFont bf = BaseFont.createFont( "simsun.ttc,0", BaseFont.IDENTITY_H,
////                BaseFont.NOT_EMBEDDED);
//        for (Iterator it = form.getFields().keySet().iterator(); it.hasNext();) {
//            strFieldName = it.next().toString();
//            form.setFieldProperty(strFieldName, "textfont", bf, null);
//            if (doc.hasItem(strFieldName)) {
//                Item item = doc.getFirstItem(strFieldName);
//                Vector allvalues = doc.getItemValue(strFieldName);
//                String result = "";
//                if (allvalues.size() > 0) {
//                    int itemType = item.getType();
//                    if(itemType == Item.AUTHORS || itemType==Item.NAMES || itemType==Item.READERS){
//                        Name tmpName = null;
//                        StringBuffer sb = new StringBuffer();
//                        for (int i = 0; i < allvalues.size(); i++) {
//                            tmpName = session.createName((String) allvalues.get(i));
//                            sb.append(tmpName.getCommon()).append(",");
//                        }
//
//                        result = sb.substring(0, sb.lastIndexOf(","));
//                    } else {
//                        
//                        StringBuffer sb = new StringBuffer();
//                        for (int i = 0; i < allvalues.size(); i++) {
//                            if(((String) allvalues.get(i)).indexOf("<tr><td>") != -1){
//                                
//                            }else{
//                            sb.append(allvalues.get(i)).append(",");
//                            }
//                        }
//                        result = sb.substring(0, sb.lastIndexOf(","));
//                    }
//                    form.setField(strFieldName, result);
//                }
//            }
//        }
//        form.setField("Title", gwType);  //设置PDF文档的Title域
//    }
//	但有一个问题是：表单内容中如果有图片，那如何插入到PDF模板中的域中呢？
//	 form.setField(strFieldName, result);
//	这个方法是只能将字符串放入域中。希望高手能给指点一下
}
