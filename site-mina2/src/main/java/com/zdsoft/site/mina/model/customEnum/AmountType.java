package com.zdsoft.site.mina.model.customEnum;

import javax.xml.bind.annotation.XmlEnumValue;

/**
 * 金额类型(AmountType)
 * 
 * @author cqyhm
 *
 */
public enum AmountType {
	/** 金额类型 "00", "追保" */
	@XmlEnumValue("00") A00("00", "追保"),

	/** 金额类型"01", "批划" */
	@XmlEnumValue("01") A01("01", "批划");

	private String value;
	private String name;

	private AmountType(String value, String name) {
		this.value = value;
		this.name = name;
	}

	@Override
	public String toString() {
		return value;
	}

	public String getValue() {
		return value;
	}

	public String getName() {
		return name;
	}
}
