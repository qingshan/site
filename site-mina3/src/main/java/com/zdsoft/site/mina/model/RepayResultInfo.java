package com.zdsoft.site.mina.model;

import com.zdsoft.site.mina.model.customEnum.DealStatus;

/**
 * 7.29  兑付结果信息（RepayResultInfo） 
 * @author cqyhm
 *
 */
public class RepayResultInfo {
//	兑付流水号  RepayBillSerial  < RepayBillSeril>  [0..1]  Max20Text   
	private String RepayBillSerial;
	
//	兑付状态  RepayStatus  < RepayStatus>  [1..1]  DealStatus  枚举
	private DealStatus RepayStatus;
	
//	兑付信息  RepayInformation  <RepayInfo>  [1..1]  Max128Text   
	private String RepayInformation;
	
//	备注  Remark  < Remark >  [0..1]  Max128Text  
	private String Remark;

	public String getRepayBillSerial() {
		return RepayBillSerial;
	}

	public void setRepayBillSerial(String repayBillSerial) {
		RepayBillSerial = repayBillSerial;
	}

	public DealStatus getRepayStatus() {
		return RepayStatus;
	}

	public void setRepayStatus(DealStatus repayStatus) {
		RepayStatus = repayStatus;
	}

	public String getRepayInformation() {
		return RepayInformation;
	}

	public void setRepayInformation(String repayInformation) {
		RepayInformation = repayInformation;
	}

	public String getRemark() {
		return Remark;
	}

	public void setRemark(String remark) {
		Remark = remark;
	}
}
