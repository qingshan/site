package com.zdsoft.site.thread.mActiveObject.A2;

import com.zdsoft.site.thread.mActiveObject.A2.activeobject.ActiveObject;
import com.zdsoft.site.thread.mActiveObject.A2.activeobject.ActiveObjectFactory;

public class Main {
    public static void main(String[] args) {
        ActiveObject activeObject = ActiveObjectFactory.createActiveObject();
        new AddClientThread("Diana", activeObject).start();
    }
}
