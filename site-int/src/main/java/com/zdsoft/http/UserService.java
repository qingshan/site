/*
 * Copyright 2002-2012 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zdsoft.http;

import java.util.Map;

import com.zdsoft.rock.common.model.domain.ResponseMsg;

/**
 * 	连个方法都是访问同一个链接(参数通过两种方式发送都是一样的)
 * @author cqyhm
 *
 */
public interface UserService {
	//多个参数目前还测试不行,只有1个参数可以加到消息负载上
	public ResponseMsg<String> save(String userCd,String userNm);
	//只能传递1个参数
	public ResponseMsg<String> save(User user);
	//只能传递1个参数
	public ResponseMsg<String> save(Map<String, Object> Param);

}
