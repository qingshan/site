package com.zdsoft.site.mina.client;

import java.net.InetSocketAddress;
import java.nio.charset.Charset;

import org.apache.mina.core.future.ConnectFuture;
import org.apache.mina.core.future.IoFutureListener;
import org.apache.mina.core.service.IoHandlerAdapter;
import org.apache.mina.core.session.IdleStatus;
import org.apache.mina.core.session.IoSession;
import org.apache.mina.filter.codec.ProtocolCodecFilter;
import org.apache.mina.filter.codec.textline.TextLineCodecFactory;
import org.apache.mina.transport.socket.nio.NioSocketConnector;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TimeClientHandler extends IoHandlerAdapter {
	
    private static Logger logger=LoggerFactory.getLogger(TimeClientHandler.class);
    private static final int PORT = 8015;
    
    public void messageReceived(IoSession session, Object message) throws Exception {
        String content = message.toString();
        logger.debug("客户端接收到的消息是 : {}",content);
    }

    public void messageSent(IoSession session, Object message) throws Exception {
    	logger.debug("messageSent -> ：{}",message);
    }
    @Override
	public void sessionCreated(IoSession session) throws Exception {
    	logger.debug("sessionCreated client");
	}

	@Override
	public void sessionOpened(IoSession session) throws Exception {
		logger.debug("sessionOpened client");
	}

	@Override
	public void sessionClosed(IoSession session) throws Exception {
		logger.debug("sessionClosed client");
	}

	@Override
	public void sessionIdle(IoSession session, IdleStatus status) throws Exception {
		logger.debug("sessionIdle client");
	}

	@Override
	public void exceptionCaught(IoSession session, Throwable cause) throws Exception {
		logger.debug("exceptionCaught client");
	}

	@Override
	public void inputClosed(IoSession session) throws Exception {
		super.inputClosed(session);
		logger.debug("inputClosed client");
	}
	
	public static void main(String[] args){
        // 创建客户端连接器.
    	NioSocketConnector connector = new NioSocketConnector();
        //connector.getFilterChain().addLast("logger", new LoggingFilter());
        connector.getFilterChain().addLast("codec", 
                new ProtocolCodecFilter(new TextLineCodecFactory(Charset.forName("UTF-8"))));
        //自定义过滤器
        //connector.getFilterChain().addLast("minaTimeFilter", new ReferenceCountingFilter(new MinaTimeFilter()));
        // 设置连接超时检查时间
        connector.setConnectTimeoutCheckInterval(30);
        connector.setHandler(new TimeClientHandler());
        // 建立连接
        ConnectFuture cf = connector.connect(new InetSocketAddress("localhost", PORT));
//        // 等待连接创建完成(变异步为同步)
        cf.awaitUninterruptibly();
//        cf.getSession().write("Hi Server!");
//        cf.getSession().write("quit");
//        // 等待连接断开
//        cf.getSession().getCloseFuture().awaitUninterruptibly();
//        // 释放连接
//        connector.dispose();
        
        cf.addListener(new IoFutureListener<ConnectFuture>() {
			@Override
			public void operationComplete(ConnectFuture future) {
				IoSession session=future.getSession();
				logger.debug("获取连接对应的session={}",session.getId());
				session.write("Hi Server!\n\r this one line");
				session.write("quit");
				session.getCloseFuture().awaitUninterruptibly();
				logger.debug("数据发送完毕");
			}
		});
        logger.debug("客户代码全部执行完毕,但异步结果还在执行");
    }
}