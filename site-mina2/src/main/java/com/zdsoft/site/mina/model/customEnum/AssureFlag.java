package com.zdsoft.site.mina.model.customEnum;

import javax.xml.bind.annotation.XmlEnumValue;

/**
 * 担保标志（AssureFlag）
 * 
 * @author cqyhm
 *
 */
public enum AssureFlag {
	/**担保标志 "0", "无担保"*/
	@XmlEnumValue("0") A0("0", "无担保"),

	/**担保标志 "1", "有担保"*/
	@XmlEnumValue("1") A1("1", "有担保");

	private String value;
	private String name;

	private AssureFlag(String value, String name) {
		this.value = value;
		this.name = name;
	}

	@Override
	public String toString() {
		return value;
	}

	public String getValue() {
		return value;
	}

	public String getName() {
		return name;
	}

}
