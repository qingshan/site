package com.jfinal.cms.controller.validate;

import com.jfinal.cms.model.User;
import com.jfinal.core.Controller;
import com.jfinal.validate.Validator;

public class UserValidator extends Validator{

	@Override
	protected void validate(Controller c) {
		validateRequiredString("user.username", "usernameMsg", "请输入用户名称!");
		validateRequiredString("user.plainPassword", "passwordMsg", "请输入密码!");
	}

	@Override
	protected void handleError(Controller c) {
		c.keepModel(User.class);
	}
}
