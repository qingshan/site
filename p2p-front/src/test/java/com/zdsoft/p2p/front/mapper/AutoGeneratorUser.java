package com.zdsoft.p2p.front.mapper;

import org.junit.Test;

import com.baomidou.mybatisplus.annotations.IdType;
import com.baomidou.mybatisplus.generator.AutoGenerator;
import com.baomidou.mybatisplus.generator.ConfigGenerator;
import com.zdsoft.p2p.front.ConfigGeneratorTest;

public class AutoGeneratorUser extends ConfigGeneratorTest{

	@Test
	public void test() {
		ConfigGenerator cg = getConfigGenerator();

		/* mysql 数据库相关配置 */
		cg.setDbDriverName("com.mysql.jdbc.Driver");
		cg.setDbUrl("jdbc:mysql://192.168.34.115:3306/p2p20?characterEncoding=utf8");
		cg.setDbUser("root");
		cg.setDbPassword("p2p_zdsoft!");
		/*
		 * 表主键 ID 生成类型, 自增该设置无效。
		 * <p>
		 * IdType.AUTO 			数据库ID自增
		 * IdType.INPUT			用户输入ID
		 * IdType.ID_WORKER		全局唯一ID，内容为空自动填充（默认配置）
		 * IdType.UUID			全局唯一ID，内容为空自动填充（默认配置）
		 * </p>
		 */
		cg.setIdType(IdType.UUID);
		AutoGenerator.run(cg);


	}

}
