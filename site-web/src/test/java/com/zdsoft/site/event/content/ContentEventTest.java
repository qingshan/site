package com.zdsoft.site.event.content;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.FutureTask;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.concurrent.ListenableFutureCallback;
import org.springframework.util.concurrent.ListenableFutureTask;

import com.zdsoft.p2p.user.domain.User;
import com.zdsoft.p2p.user.service.UserService;
import com.zdsoft.rock.common.AppException;
import com.zdsoft.site.event.TransactionEventPublisher;

/**
 * @TransactionalEventListener
 * @EventListener
 * @author cqyhm
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:applicationContext.xml" })
public class ContentEventTest {

	private Logger logger = LoggerFactory.getLogger(getClass());

	//@Autowired
	//private ApplicationContext applicationContext;

	@Autowired
	private TransactionEventPublisher publisher;
	
	@Autowired
	private UserService userService;

	@Test
	public void testPublishEvent2(){
		logger.debug("登录成功");
		logger.debug("【1000】事件发布开始.");
		//TransactionEventPublisher pub=new TransactionEventPublisher(publisher);
		// applicationContext.publishEvent(new
		// ContentEvent("很快就要上线了"));//触发事件{xx-net:maplehtpc}
		User user=new User("zhangsan", "张三");
		userService.save(user);
		logger.debug("保存用户:{}",user);
		publisher.publishEvent(new ContentEvent(user));// 触发事件
		logger.debug("【1000】事件发布完成.");
		try {
			Thread.sleep(5000L); // 所有事件都是同步事件则不需要延时	
		} catch (Exception e) {
			logger.error("线程休眠5秒异常.");
		}
		
	}
	@Test
	public void test1() {
		//Linux下返回：/tmp
		//windows下返回: C:\Users\ADMINI~1\AppData\Local\Temp\
		logger.debug("{}", System.getProperty("java.io.tmpdir"));
	}

	@Test
	public void testFutureTask() throws Exception {
		final boolean failflag=true;
		
		ListenableFutureTask<String> task = new ListenableFutureTask<String>(new Callable<String>() {

			@Override
			public String call() throws Exception {
				logger.debug("执行方法");
				try {
					Thread.sleep(1000L);
					logger.error("等待1000毫秒完毕");
				} catch (Exception e) {
					logger.error("等待1000毫秒异常{}", e.getMessage());
				}
				if(failflag){
					throw new AppException("1000", "方法执行失败");
				}else{
					return "方法执行成功";
				}
			}

		});

		task.addCallback(new ListenableFutureCallback<String>() {
			@Override
			public void onSuccess(String result) {
				logger.debug("成功回调1");
			}

			@Override
			public void onFailure(Throwable t) {
				logger.debug("失败回调1:{}", t.getMessage());
			}
		});

		task.addCallback(new ListenableFutureCallback<String>() {
			@Override
			public void onSuccess(String result) {
				logger.debug("成功回调2");
			}

			@Override
			public void onFailure(Throwable t) {
				logger.debug("失败回调2:{}", t.getMessage());
			}
		});

		ExecutorService executorService = Executors.newSingleThreadExecutor();
		executorService.submit(task);
		try {
			String result = task.get();// 执行结果
			logger.debug("{}", result);
		} catch (Exception e) {
			logger.debug(e.getMessage());
		}

	}

	/**
	 * Thread、Runnable、Callable，其中Runnable实现的是void run()方法，Callable实现的是 V
	 * call()方法，并且可以返回执行结果， 其中Runnable可以提交给Thread来包装下，直接启动一个线程来执行，
	 * 而Callable则一般都是提交给ExecuteService来执行。
	 * 简单来说，Executor就是Runnable和Callable的调度容器，
	 * Future就是对于具体的调度任务的执行结果进行查看，最为关键的是Future可以检查对应的任务是否已经完成，
	 * 也可以阻塞在get方法上一直等待任务返回结果。
	 * Runnable和Callable的差别就是Runnable是没有结果可以返回的，就算是通过Future也看不到任务调度的结果的。
	 * 
	 * 通过简单的测试程序来试验Runnable、Callable通过Executor来调度的时候与Future的关系
	 */
	@Test
	public void test22() {

		// 创建一个执行任务的服务
		ExecutorService executor = Executors.newFixedThreadPool(3);
		try {
			// 1.Runnable通过Future返回结果为空
			// 创建一个Runnable，来调度，等待任务执行完毕，取得返回结果
			Future<?> runnable1 = executor.submit(new Runnable() {
				@Override
				public void run() {
					logger.debug("runnable1 running.");
				}
			});
			logger.debug("Runnable1:" + runnable1.get());

			// 2.Callable通过Future能返回结果
			// 提交并执行任务，任务启动时返回了一个 Future对象，
			// 如果想得到任务执行的结果或者是异常可对这个Future对象进行操作
			Future<String> future1 = executor.submit(new Callable<String>() {
				@Override
				public String call() throws Exception {
					return "result=task1";
				}
			});
			// 获得任务的结果，如果调用get方法，当前线程会等待任务执行完毕后才往下执行
			logger.debug("task1: " + future1.get());

			// 3. 对Callable调用cancel可以对对该任务进行中断
			// 提交并执行任务，任务启动时返回了一个 Future对象，
			// 如果想得到任务执行的结果或者是异常可对这个Future对象进行操作
			Future<String> future2 = executor.submit(new Callable<String>() {
				@Override
				public String call() throws Exception {
					try {
						while (true) {
							logger.debug("task2 running.");
							Thread.sleep(50);
						}
					} catch (InterruptedException e) {
						logger.debug("Interrupted task2.");
					}
					return "task2=false";
				}
			});

			// 等待5秒后，再停止第二个任务。因为第二个任务进行的是无限循环
			Thread.sleep(10);
			logger.debug("task2 cancel: " + future2.cancel(true));

			// 4.用Callable时抛出异常则Future什么也取不到了
			// 获取第三个任务的输出，因为执行第三个任务会引起异常
			// 所以下面的语句将引起异常的抛出
			Future<String> future3 = executor.submit(new Callable<String>() {

				@Override
				public String call() throws Exception {
					throw new Exception("task3 throw exception!");
				}

			});
			logger.debug("task3: " + future3.get());
		} catch (Exception e) {
			logger.debug(e.toString());
		}
		// 停止任务执行服务
		executor.shutdownNow();
	}

	/**
	 * FutureTask则是一个RunnableFuture<V>，即实现了Runnbale又实现了Futrue
	 * <V>这两个接口，另外它还可以包装Runnable和Callable<V>，
	 * 所以一般来讲是一个符合体了，它可以通过Thread包装来直接执行，也可以提交给ExecuteService来执行，并且还可以通过v
	 * get()返回执行结果， 在线程体没有执行完成的时候，主线程一直阻塞等待，执行完则直接返回结果。
	 */
	@Test
	public void test33() {
		Callable<String> task = new Callable<String>() {
			public String call() {
				logger.debug("Sleep start.");
				try {
					Thread.sleep(1000 * 10);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				logger.debug("Sleep end.");
				return "time=" + System.currentTimeMillis();
			}
		};

		// 直接使用Thread的方式执行
		FutureTask<String> ft = new FutureTask<String>(task);
		Thread t = new Thread(ft);
		t.start();
		try {
			logger.debug("waiting execute result");
			logger.debug("result = {}", ft.get());
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (ExecutionException e) {
			e.printStackTrace();
		}

		// 使用Executors来执行
		logger.debug("=========");
		FutureTask<String> ft2 = new FutureTask<String>(task);
		Executors.newSingleThreadExecutor().submit(ft2);
		try {
			logger.debug("waiting execute result");
			logger.debug("result = {}", ft2.get());
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (ExecutionException e) {
			e.printStackTrace();
		}
	}
	/**
	 * 在Java中，如果需要设定代码执行的最长时间，即超时，可以用Java线程池ExecutorService类配合Future接口来实现。
	Future接口是Java标准API的一部分，在java.util.concurrent包中。Future接口是Java线程Future模式的实 现，可以来进行异步计算。
	Future模式可以这样来描述：我有一个任务，提交给了Future，Future替我完成这个任务。期间我自己可以去做任何想做的事情。
	一段时 间之后，我就便可以从Future那儿取出结果。就相当于下了一张订货单，一段时间后可以拿着提订单来提货，这期间可以干别的任何事情。
	其中Future 接口就是订货单，真正处理订单的是Executor类，它根据Future接口的要求来生产产品。
	Future接口提供方法来检测任务是否被执行完，等待任务执行完获得结果，也可以设置任务执行的超时时间。这个设置超时的方法就是实现Java程 序执行超时的关键。
	Future接口是一个泛型接口，严格的格式应该是Future<V>，其中V代表了Future执行的任务返回值的类型。 Future接口的方法介绍如下：
    boolean cancel (boolean mayInterruptIfRunning) 取消任务的执行。参数指定是否立即中断任务执行，或者等等任务结束
    boolean isCancelled () 任务是否已经取消，任务正常完成前将其取消，则返回 true
    boolean isDone () 任务是否已经完成。需要注意的是如果任务正常终止、异常或取消，都将返回true
    V get () throws InterruptedException, ExecutionException  等待任务执行结束，然后获得V类型的结果。
    			InterruptedException 线程被中断异常， ExecutionException任务执行异常，如果任务被取消，还会抛出CancellationException
    V get (long timeout, TimeUnit unit) throws InterruptedException, ExecutionException, 
    			TimeoutException 同上面的get功能一样，多了设置超时时间。参数timeout指定超时时间，uint指定时间的单位，
    			在枚举类TimeUnit中有相关的定义。如果计 算超时，将抛出TimeoutException
	Future的实现类有java.util.concurrent.FutureTask<V>即 javax.swing.SwingWorker<T,V>。通常使用FutureTask来处理我们的任务。
	FutureTask类同时又 实现了Runnable接口，所以可以直接提交给Executor执行。使用FutureTask实现超时执行的代码如下：
	 */
	@Test
	public void test44(){
		ExecutorService executor = Executors.newSingleThreadExecutor();  
		FutureTask<String> futureTask =new FutureTask<String>(new Callable<String>() {//使用Callable接口作为构造参数  
		         public String call() {  
		           //真正的任务在这里执行，这里的返回值类型为String，可以为任意类型
		           logger.debug("执行任务");
		           return "成功执行";
		       }});  
		executor.execute(futureTask);  
		//在这里可以做别的任何事情  
		try {  
			//取得结果，同时设置超时执行时间为5秒。同样可以用future.get()，不设置执行超时时间取得结果
		    String result = futureTask.get(5000, TimeUnit.MILLISECONDS);   
		    logger.debug("{}",result);
		} catch (InterruptedException e) {  
		    futureTask.cancel(true);  
		} catch (ExecutionException e) {  
		    futureTask.cancel(true);  
		} catch (TimeoutException e) {  
		    futureTask.cancel(true);  
		} finally {  
		    executor.shutdown();  
		} 
	}
	@Test
	public void test55(){
	    ExecutorService executor = Executors.newSingleThreadExecutor(); 
	    Future<String> future = executor.submit(new Callable<String>(){//使用Callable接口作为构造参数  
	           public String call() {  
	               //真正的任务在这里执行，这里的返回值类型为String，可以为任意类型
	        	   return "成功执行";
	           }
	    });  
	    //在这里可以做别的任何事情  
	    //同上面取得结果的代码 
	    try {
		    logger.debug("执行结果:{}",future.get());
		} catch (Exception e) {
			logger.error("{}",e.getMessage());
		}
	}
}
