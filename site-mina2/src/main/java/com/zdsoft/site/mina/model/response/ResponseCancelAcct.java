package com.zdsoft.site.mina.model.response;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.springframework.beans.BeanUtils;

import com.zdsoft.site.mina.model.Account;
import com.zdsoft.site.mina.model.Customer;
import com.zdsoft.site.mina.model.Reference;
import com.zdsoft.site.mina.utils.JaxbUtil;
/**
 * 6.1.4　客户撤销银行账号(46703)
 * 
 */	 
@XmlRootElement(name="MsgText")
public class ResponseCancelAcct extends AbstractResponse {
	/**银行流水号  BankReference  [BkRef]  [1..1]  Reference  组件 */
	@XmlElement(name="BkRef")
	private Reference BkRef;
	
	/**客户信息  Customer  [Cust]  [0..1]  Customer  组件 */
	@XmlElement(name="Cust")
	private Customer Cust;
	
	/**银行账户  BankAccount  [BkAcct]  [0..1]  Account  组件*/ 
	@XmlElement(name="BkAcct")
	private Account BkAcct;
	
	/**交易管理账户  ManageAccount  [ MgeAcct]  [0..1]  Account  组件*/
	@XmlElement(name="MgeAcct")
	private Account MgeAcct;

	public ResponseCancelAcct(){}
	
	public ResponseCancelAcct(String xml){
		ResponseCancelAcct response=JaxbUtil.converyToJavaBean(xml, this.getClass());
		BeanUtils.copyProperties(response, this);
	}
	/**银行流水号  BankReference  [BkRef]  [1..1]  Reference  组件 */
	public Reference getBkRef() {
		return BkRef;
	}
	/**银行流水号  BankReference  [BkRef]  [1..1]  Reference  组件 */
	public void setBkRef(Reference reference) {
		BkRef = reference;
	}
	/**客户信息  Customer  [Cust]  [0..1]  Customer  组件 */
	public Customer getCust() {
		return Cust;
	}
	/**客户信息  Customer  [Cust]  [0..1]  Customer  组件 */
	public void setCust(Customer customer) {
		Cust = customer;
	}
	/**银行账户  BankAccount  [BkAcct]  [0..1]  Account  组件*/ 
	public Account getBkAcct() {
		return BkAcct;
	}
	/**银行账户  BankAccount  [BkAcct]  [0..1]  Account  组件*/ 
	public void setBkAcct(Account bankAccount) {
		BkAcct = bankAccount;
	}
	/**交易管理账户  ManageAccount  [ MgeAcct]  [0..1]  Account  组件*/
	public Account getMgeAcct() {
		return MgeAcct;
	}
	/**交易管理账户  ManageAccount  [ MgeAcct]  [0..1]  Account  组件*/
	public void setMgeAcct(Account manageAccount) {
		MgeAcct = manageAccount;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ResponseCancelAcct[")		
				.append("").append(getMsgHdr())
				.append("").append(getRst())
				.append(", BkRef=").append(BkRef)
				.append(", Cust=").append(Cust)
				.append(", BkAcct=").append(BkAcct)
				.append(", MgeAcct=").append(MgeAcct)
			    .append("]");
		return builder.toString();
	}
}
