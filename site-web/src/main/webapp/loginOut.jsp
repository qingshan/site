<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>退出登陆</title>
</head>
<body>
<p>当前会话【<%=session.getId()%>】</p>
<p>
<%
	session.invalidate();
	response.sendRedirect("index.jsp");
%>	
</p>
</body>
</html>