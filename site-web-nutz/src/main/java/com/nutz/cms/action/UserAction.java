package com.nutz.cms.action;

import org.nutz.ioc.loader.annotation.Inject;
import org.nutz.ioc.loader.annotation.IocBean;
import org.nutz.mvc.annotation.At;
import org.nutz.mvc.annotation.Fail;
import org.nutz.mvc.annotation.Ok;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.nutz.cms.service.UserService;

@IocBean
@At("/user")
@Ok("json")
@Fail("http:500")
public class UserAction {
	
	private Logger logger=LoggerFactory.getLogger(getClass());
    @Inject
    private UserService userService;
    
    @At("/id.html")
    public int count() {
    	logger.debug("访问控制器");
        return userService.count();
    }
}
