package com.zdsoft.site.mina.state;

import org.apache.mina.statemachine.StateMachine;
import org.apache.mina.statemachine.StateMachineFactory;
import org.apache.mina.statemachine.StateMachineProxyBuilder;
import org.apache.mina.statemachine.annotation.State;
import org.apache.mina.statemachine.annotation.Transition;

public class GameHandler {
	@State
	public static final String Empty = "empty";
	@State
	public static final String Start = "start";
	@State
	public static final String Play = "play";
	@State
	public static final String Stop = "stop";

	@Transition(on = "start", in = Empty, next = Start)
	public void startGame() {
		System.out.println("Start the game!");
	}

	@Transition(on = "play", in = Start, next = Play)
	public void playGame() {
		System.out.println("playing the game!!");
	}

	@Transition(on = "stop", in = Play, next = Stop)
	public void stopGame() {
		System.out.println("Stop the game!");
	}

	public static void main(String[] args) {
		GameHandler gh = new GameHandler();
		StateMachine sm = StateMachineFactory.getInstance(Transition.class).create(GameHandler.Empty, gh);
		PlayGame pg = new StateMachineProxyBuilder().create(PlayGame.class, sm);
		pg.start();
		pg.play();
		pg.stop();
	}
}
