package com.zdsoft.site.thread;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ObjectTest {

	private static Logger logger = LoggerFactory.getLogger(ObjectTest.class);
	public static Object object = new Object();
	@Test
	public void test() throws InterruptedException {
		Thread1 thread1 = new Thread1();
        Thread2 thread2 = new Thread2();
        thread1.start();
        thread2.start();     
        
        //Thread.sleep(3000);
        thread1.join();
        thread2.join();
        logger.debug("线程执行完毕");
	}
	static class Thread1 extends Thread {
		@Override
		public void run() {
			synchronized (object) {
				try {
					object.wait();
				} catch (InterruptedException e) {
				}
				logger.debug("线程{}获取到了锁",Thread.currentThread().getName());
			}
		}
	}

	static class Thread2 extends Thread {
		@Override
		public void run() {
			synchronized (object) {
				object.notify();
				logger.debug("线程{}调用了object.notify()",Thread.currentThread().getName());
			}
			logger.debug("线程{}释放了锁",Thread.currentThread().getName());
		}
	}
}
