package com.zdsoft.site.service;

import java.util.List;

import com.zdsoft.p2p.user.domain.Client;

public interface ClientService {
	/** 创建客户端 */
	public Client create(Client client);

	/** 更新客户端 */
	public Client update(Client client);

	/** 删除客户端 */
	public void delete(Long clientId);

	/** 根据id查找客户端 */
	public Client findOne(Long clientId);

	/** 查找所有 */
	public List<Client> findAll();

	/** 根据客户端id查找客户端 */
	public Client findById(String clientId);

	/** 根据客户端安全KEY查找客户端 */
	public Client findBySecret(String clientSecret);
}
