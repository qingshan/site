package com.zdsoft.site.mina.model.response;

import java.math.BigDecimal;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.springframework.beans.BeanUtils;

import com.zdsoft.site.mina.model.Account;
import com.zdsoft.site.mina.model.Reference;
import com.zdsoft.site.mina.model.TransMangAcc;
import com.zdsoft.site.mina.model.customEnum.CurrencyCode;
import com.zdsoft.site.mina.utils.JaxbUtil;

/**
 * 6.1.7 服务商发起网银支付入金(48705) 1，回调URL指支付完成时跳转的网址 2，服务商可以直接跳转至应答报文的“支付网关页面”进入网关支付
 * 应答：
 * 
 * @author cqyhm
 *
 */
@XmlRootElement(name = "MsgText")
public class ResponseOnlinePayment extends AbstractResponse {

	/** 03. 银行流水号 BankReference [BkRef] [1..1] Reference 组件 */
	@XmlElement(name = "BkRef")
	private Reference BkRef;

	/** 04. 银行账户 BankAccount [BkAcct] [0..1] Account 组件 */
	@XmlElement(name = "BkAcct")
	private Account BkAcct;

	/** 05. 交易管理账户 ManageAccount [ MgeAcct] [0..1] TransMangAcc 组件 */
	@XmlElement(name = "MgeAcct")
	private TransMangAcc MgeAcct;

	/** 06. 币种 Currency [Ccy] [0..1] CurrencyCode 枚举 */
	@XmlElement(name = "Ccy")
	private CurrencyCode Ccy;

	/** 07. 转帐金额 TransferAmount [TrfAmt] [0..1] Amount */
	@XmlElement(name = "TrfAmt")
	private BigDecimal TrfAmt;

	/** 08. 成功转帐金额 SuccessTransferAmount [SucTrfAmt] [0..1] Amount */
	@XmlElement(name = "SucTrfAmt")
	private BigDecimal SucTrfAmt;

	/** 09.支付网关页面 PayWebView < PayView > [1..1] Max2048Text */
	@XmlElement(name = "PayView")
	private String PayView;

	/** 10. 摘要 Digest [Dgst] [0..1] Max128Text */
	@XmlElement(name = "Dgst")
	private String Dgst;

	public ResponseOnlinePayment() {
	}

	public ResponseOnlinePayment(String xml) {
		ResponseOnlinePayment response = JaxbUtil.converyToJavaBean(xml, this.getClass());
		BeanUtils.copyProperties(response, this);
	}

	/** 银行流水号 BankReference [BkRef] [1..1] Reference 组件 */
	public Reference getBkRef() {
		return BkRef;
	}

	/** 银行流水号 BankReference [BkRef] [1..1] Reference 组件 */
	public void setBkRef(Reference bkRef) {
		BkRef = bkRef;
	}

	/** 银行账户 BankAccount [BkAcct] [0..1] Account 组件 */
	public Account getBkAcct() {
		return BkAcct;
	}

	/** 银行账户 BankAccount [BkAcct] [0..1] Account 组件 */
	public void setBkAcct(Account bkAcct) {
		BkAcct = bkAcct;
	}

	/** 交易管理账户 ManageAccount [ MgeAcct] [0..1] TransMangAcc 组件 */
	public TransMangAcc getMgeAcct() {
		return MgeAcct;
	}

	/** 交易管理账户 ManageAccount [ MgeAcct] [0..1] TransMangAcc 组件 */
	public void setMgeAcct(TransMangAcc mgeAcct) {
		MgeAcct = mgeAcct;
	}

	/** 币种 Currency [Ccy] [0..1] CurrencyCode 枚举 */
	public CurrencyCode getCcy() {
		return Ccy;
	}

	/** 币种 Currency [Ccy] [0..1] CurrencyCode 枚举 */
	public void setCcy(CurrencyCode ccy) {
		Ccy = ccy;
	}

	/** 转帐金额 TransferAmount [TrfAmt] [0..1] Amount */
	public BigDecimal getTrfAmt() {
		return TrfAmt;
	}

	/** 转帐金额 TransferAmount [TrfAmt] [0..1] Amount */
	public void setTrfAmt(BigDecimal trfAmt) {
		TrfAmt = trfAmt;
	}

	/** 成功转帐金额 SuccessTransferAmount [SucTrfAmt] [0..1] Amount */
	public BigDecimal getSucTrfAmt() {
		return SucTrfAmt;
	}

	/** 成功转帐金额 SuccessTransferAmount [SucTrfAmt] [0..1] Amount */
	public void setSucTrfAmt(BigDecimal sucTrfAmt) {
		SucTrfAmt = sucTrfAmt;
	}

	/** 摘要 Digest [Dgst] [0..1] Max128Text */
	public String getDgst() {
		return Dgst;
	}

	/** 摘要 Digest [Dgst] [0..1] Max128Text */
	public void setDgst(String dgst) {
		Dgst = dgst;
	}

	/** 09.支付网关页面 PayWebView < PayView > [1..1] Max2048Text */
	public String getPayView() {
		return PayView;
	}

	/** 09.支付网关页面 PayWebView < PayView > [1..1] Max2048Text */
	public void setPayView(String payView) {
		PayView = payView;
	}
}
