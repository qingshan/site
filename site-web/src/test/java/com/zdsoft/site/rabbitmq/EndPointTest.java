package com.zdsoft.site.rabbitmq;

import java.io.IOException;
import java.util.HashMap;
import java.util.concurrent.TimeoutException;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class EndPointTest {
	private Logger logger=LoggerFactory.getLogger(getClass());
	@Test
	public void test() throws IOException, TimeoutException, InterruptedException {
		QueueConsumer consumer = new QueueConsumer("A1");
        Thread consumerThread = new Thread(consumer);
        consumerThread.start();
         
        Producer producer = new Producer("A1");
         
        for (int i = 0; i < 1000000; i++) {
            HashMap<String,Integer> message = new HashMap<String,Integer>();
            message.put("message number", i);
            producer.sendMessage(message);
            logger.debug("Message Number {} sent.",i);
        }
        Thread.sleep(1000L);
	}

}
