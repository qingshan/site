package com.zdsoft.site.mina.model.request;

import java.math.BigDecimal;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.zdsoft.site.mina.model.Account;
import com.zdsoft.site.mina.model.Customer;
import com.zdsoft.site.mina.model.MessageHeader;
import com.zdsoft.site.mina.model.TransMangAcc;
import com.zdsoft.site.mina.model.customEnum.BusinessType;
import com.zdsoft.site.mina.model.customEnum.CurrencyCode;

/**
 * 6.1.5 服务商发起快捷支付入金(48703) –发送短信验证
 * 
 * @author cqyhm
 *
 */
@XmlRootElement(name = "MsgText")
public class RequestQuickPayment extends AbstractRequest {

	public RequestQuickPayment(){}
	
	public RequestQuickPayment(MessageHeader msgHdr){
		setMsgHdr(msgHdr);
	}
	
	/** 客户信息 Customer [Cust] [0..1] Customer 组件 */
	@XmlElement(name = "Cust")
	private Customer Cust;
	/** 银行账户 BankAccount [BkAcct] [1..1] Account 组件 */
	@XmlElement(name = "BkAcct")
	private Account BkAcct;
	/** 交易管理账户 ManageAccount [MgeAcct] [1..1] TransMangAcc 组件 */
	@XmlElement(name = "MgeAcct")
	private TransMangAcc MgeAcct;
	/** 服务商汇总账户 ServiceAccount [SrvAcct] [0..1] Account 组件 */
	@XmlElement(name = "SrvAcct")
	private Account SrvAcct;
	/** 币种 Currency [Ccy] [1..1] CurrencyCode 枚举 */
	@XmlElement(name = "Ccy")
	private CurrencyCode Ccy;
	/** 业务类别 BusinessType [BusType] [0..1] BusinessType 枚举 */
	@XmlElement(name = "BusType")
	private BusinessType BusType;
	/** 转帐金额 TransferAmount [TrfAmt] [1..1] Amount */
	@XmlElement(name = "TrfAmt")
	private BigDecimal TrfAmt;
	/** 摘要 Digest [Dgst] [0..1] Max128Text */
	@XmlElement(name = "Dgst")
	private String Dgst;

	/** 客户信息 Customer [Cust] [0..1] Customer 组件 */
	public Customer getCust() {
		return Cust;
	}

	/** 客户信息 Customer [Cust] [0..1] Customer 组件 */
	public void setCust(Customer cust) {
		Cust = cust;
	}

	/** 银行账户 BankAccount [BkAcct] [1..1] Account 组件 */
	public Account getBkAcct() {
		return BkAcct;
	}

	/** 银行账户 BankAccount [BkAcct] [1..1] Account 组件 */
	public void setBkAcct(Account bkAcct) {
		BkAcct = bkAcct;
	}

	/** 交易管理账户 ManageAccount [MgeAcct] [1..1] TransMangAcc 组件 */
	public TransMangAcc getMgeAcct() {
		return MgeAcct;
	}

	/** 交易管理账户 ManageAccount [MgeAcct] [1..1] TransMangAcc 组件 */
	public void setMgeAcct(TransMangAcc mgeAcct) {
		MgeAcct = mgeAcct;
	}

	/** 服务商汇总账户 ServiceAccount [SrvAcct] [0..1] Account 组件 */
	public Account getSrvAcct() {
		return SrvAcct;
	}

	/** 服务商汇总账户 ServiceAccount [SrvAcct] [0..1] Account 组件 */
	public void setSrvAcct(Account srvAcct) {
		SrvAcct = srvAcct;
	}

	/** 币种 Currency [Ccy] [1..1] CurrencyCode 枚举 */
	public CurrencyCode getCcy() {
		return Ccy;
	}

	/** 币种 Currency [Ccy] [1..1] CurrencyCode 枚举 */
	public void setCcy(CurrencyCode ccy) {
		Ccy = ccy;
	}

	/** 业务类别 BusinessType [BusType] [0..1] BusinessType 枚举 */
	public BusinessType getBusType() {
		return BusType;
	}

	/** 业务类别 BusinessType [BusType] [0..1] BusinessType 枚举 */
	public void setBusType(BusinessType busType) {
		BusType = busType;
	}

	/** 转帐金额 TransferAmount [TrfAmt] [1..1] Amount */
	public BigDecimal getTrfAmt() {
		return TrfAmt;
	}

	/** 转帐金额 TransferAmount [TrfAmt] [1..1] Amount */
	public void setTrfAmt(BigDecimal trfAmt) {
		TrfAmt = trfAmt;
	}

	/** 摘要 Digest [Dgst] [0..1] Max128Text */
	public String getDgst() {
		return Dgst;
	}

	/** 摘要 Digest [Dgst] [0..1] Max128Text */
	public void setDgst(String dgst) {
		Dgst = dgst;
	}
}
