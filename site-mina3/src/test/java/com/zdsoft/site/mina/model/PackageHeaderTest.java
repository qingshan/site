package com.zdsoft.site.mina.model;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.zdsoft.site.mina.model.customEnum.InstructionCode;

public class PackageHeaderTest {

	private Logger logger=LoggerFactory.getLogger(getClass());
	@Test
	public void test() {
		PackageHeader header=new PackageHeader();
		
		header.setBodyLength(0);
		header.setFlag("012");
		header.setInstId("P8888000");
		header.setInstrCd(InstructionCode.A46700.getValue());
		
		logger.debug("{}",header);
	}

}
