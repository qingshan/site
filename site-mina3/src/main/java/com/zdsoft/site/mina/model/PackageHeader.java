package com.zdsoft.site.mina.model;

/**
 *	请求包头部
 *	以上包头总长为 57字节长度请求与应答时均固定不变。 
 *	通讯时可以先收取前 9 字节判断后再做后续包的收取，也可先固定收取 57字节通讯头长度再根据
 *	通讯头第三个域里的长度收取 xml报文的长度。
 * 	@author cqyhm
 *
 */
public class PackageHeader {
	/**1 字节报文性质 “X”-表示xml类型报文。  */
	private String type="X";
	/**3字节为通讯头的全长,一般固定为“057”。 */
	private String headLength="057";
	/**5字节为其后的XML数据报文全长，不足左补零。不包含包头的 57字节固定长度。*/
	private String bodyLength="0";
	/**8字节为监管系统对连接的服务商或交易所的编号用于确定是哪家接入服务商。 */
	private String InstId="";
	/**5字节为托管系统内部交易编码。*/
	private String InstrCd="";
	/**3字节加密标志 
      *加密情况标志固定 3字节。密码加密标志：表示通讯加密方式，固定 3字节长度，第一字节：
      *密码加密标志;第二字节：MAC加密标志;第三字节：报文加密标志。*/
	private String flag="";
	/**32字节MAC值不足为左补空格。 */
	private String mac="0";
	/**1 字节报文性质 “X”-表示xml类型报文。  */
	public String getType() {
		return type;
	}
	/**1 字节报文性质 “X”-表示xml类型报文。  */
	public void setType(String type) {
		this.type = type;
	}
	/**3字节为通讯头的全长,一般固定为“057”。 */
	public String getHeadLength() {
		return headLength;
	}
	/**3字节为通讯头的全长,一般固定为“057”。 */
	public void setHeadLength(String headLength) {
		this.headLength = headLength;
	}
	/**3字节为通讯头的全长,一般固定为057。 */
	public void setHeadLength(Integer headLength) {
		setHeadLength(String.format("%03d", headLength));		
	}
	/**5字节为其后的XML数据报文全长，不足左补零。不包含包头的 57字节固定长度。*/
	public String getBodyLength() {
		return bodyLength;
	}
	/**5字节为其后的XML数据报文全长，不足左补零。不包含包头的 57字节固定长度。*/
	public void setBodyLength(String bodyLength) {
		this.bodyLength = bodyLength;
	}
	/**5字节为其后的XML数据报文全长，不足左补零。不包含包头的 57字节固定长度。*/
	public void setBodyLength(Integer bodyLength) {
		setBodyLength(String.format("%05d", bodyLength));
	}
	/**8字节为监管系统对连接的服务商或交易所的编号用于确定是哪家接入服务商。 */
	public String getInstId() {
		return InstId;
	}
	/**8字节为监管系统对连接的服务商或交易所的编号用于确定是哪家接入服务商。 */
	public void setInstId(String instId) {
		InstId = instId;
	}
	/**5字节为托管系统内部交易编码。*/
	public String getInstrCd() {
		return InstrCd;
	}
	/**5字节为托管系统内部交易编码。*/
	public void setInstrCd(String instrCd) {
		InstrCd = instrCd;
	}
	/**3字节加密标志 
     *加密情况标志固定 3字节。密码加密标志：表示通讯加密方式，固定 3字节长度，第一字节：
     *密码加密标志;第二字节：MAC加密标志;第三字节：报文加密标志。*/
	public String getFlag() {
		return flag;
	}
	/**3字节加密标志 
     *加密情况标志固定 3字节。密码加密标志：表示通讯加密方式，固定 3字节长度，第一字节：
     *密码加密标志;第二字节：MAC加密标志;第三字节：报文加密标志。*/
	public void setFlag(String flag) {
		this.flag = flag;
	}
	/**32字节MAC值不足为左补空格。 */
	public String getMac() {
		return mac;
	}
	/**32字节MAC值不足为左补空格。 */
	public void setMac(String mac) {
		if(mac!=null && mac.length()<32){
			//TODO 左边补足空格	
		}
		this.mac = mac;
	}
	public String toString(){
		return String.format("%s%s%s%s%s%s%32s", type,headLength,bodyLength,InstId,InstrCd,flag,mac);
	}
}
