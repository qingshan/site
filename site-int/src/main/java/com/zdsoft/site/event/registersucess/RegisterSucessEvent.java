package com.zdsoft.site.event.registersucess;

import org.springframework.context.ApplicationEvent;

import com.zdsoft.site.domain.User;

/**
 * 注册成功事件定义
 */
public class RegisterSucessEvent extends ApplicationEvent {

	private static final long serialVersionUID = -2579365084101829875L;

	public RegisterSucessEvent(User user) {
        super(user);
    }
}
