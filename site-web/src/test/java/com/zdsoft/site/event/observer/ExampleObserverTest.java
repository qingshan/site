package com.zdsoft.site.event.observer;

import org.junit.Test;

public class ExampleObserverTest {

	@Test
	public void testUpdate() {
		ExampleObservable example = new ExampleObservable();
		//添加观察者
        example.addObserver(new ExampleObserver());//给example这个被观察者添加观察者，允许添加多個观察者
        example.addObserver(new ExampleObserver());//给example这个被观察者添加观察者，允许添加多個观察者
        example.addObserver(new ExampleObserver());//给example这个被观察者添加观察者，允许添加多個观察者
        
        example.setData(2);
        example.setData(-5);
        example.setData(9999);
	}

}
