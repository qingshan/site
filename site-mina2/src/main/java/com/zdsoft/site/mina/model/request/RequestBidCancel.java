package com.zdsoft.site.mina.model.request;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.zdsoft.site.mina.model.BidInfo;
import com.zdsoft.site.mina.model.MessageHeader;
import com.zdsoft.site.mina.model.TransMangAcc;
import com.zdsoft.site.mina.model.customEnum.BusinessType;
import com.zdsoft.site.mina.model.customEnum.CurrencyCode;

/**
 * 6.1.13服务商发起标的撤销(42702)（含流标）
 * 说明： 
 * 	1.本功能是服务商方对已登记的标的进行撤销处理。
 * 	2.对于募集期结束后仍未满标的情况可进行标的流标处理，并更新客户的资金余额。 
 * 请求：
 * 
 * @author cqyhm
 *
 */
@XmlRootElement(name = "MsgText")
public class RequestBidCancel extends AbstractRequest {

	public RequestBidCancel(){}
	
	public RequestBidCancel(MessageHeader msgHdr){
		setMsgHdr(msgHdr);
	}
	/** 02.标的信息 BidInfo <BidInfo> [1..1] BidInfo 组件 */
	@XmlElement(name = "BidInfo")
	private BidInfo BidInfo;

	/** 03.交易管理账户 ManageAccount <MgeAcct> [0..1] TransMangAcc 组件 */
	@XmlElement(name = "MgeAcct")
	private TransMangAcc MgeAcct;

	/** 04 币种 Currency <Ccy> [1..1] CurrencyCode 枚举 */
	@XmlElement(name = "Ccy")
	private CurrencyCode Ccy;

	/** 05.业务类别 BusinessType <BusType> [0..1] BusinessType 枚举 */
	@XmlElement(name = "BusType")
	private BusinessType BusType;

	/** 06.短信验证码 MessageBox <MsgBox> [0..1] Max20Text */
	@XmlElement(name = "MsgBox")
	private String MsgBox;

	/** 07.摘要 Digest [Dgst] [0..1] Max128Text */
	@XmlElement(name = "Dgst")
	private String Dgst;

	/** 02.标的信息 BidInfo <BidInfo> [1..1] BidInfo 组件 */
	public BidInfo getBidInfo() {
		return BidInfo;
	}

	/** 02.标的信息 BidInfo <BidInfo> [1..1] BidInfo 组件 */
	public void setBidInfo(BidInfo bidInfo) {
		BidInfo = bidInfo;
	}

	/** 03.交易管理账户 ManageAccount <MgeAcct> [0..1] TransMangAcc 组件 */
	public TransMangAcc getMgeAcct() {
		return MgeAcct;
	}

	/** 03.交易管理账户 ManageAccount <MgeAcct> [0..1] TransMangAcc 组件 */
	public void setMgeAcct(TransMangAcc mgeAcct) {
		MgeAcct = mgeAcct;
	}

	/** 04 币种 Currency <Ccy> [1..1] CurrencyCode 枚举 */
	public CurrencyCode getCcy() {
		return Ccy;
	}

	/** 04 币种 Currency <Ccy> [1..1] CurrencyCode 枚举 */
	public void setCcy(CurrencyCode ccy) {
		Ccy = ccy;
	}

	/** 05.业务类别 BusinessType <BusType> [0..1] BusinessType 枚举 */
	public BusinessType getBusType() {
		return BusType;
	}

	/** 05.业务类别 BusinessType <BusType> [0..1] BusinessType 枚举 */
	public void setBusType(BusinessType busType) {
		BusType = busType;
	}

	/** 06.短信验证码 MessageBox <MsgBox> [0..1] Max20Text */
	public String getMsgBox() {
		return MsgBox;
	}

	/** 06.短信验证码 MessageBox <MsgBox> [0..1] Max20Text */
	public void setMsgBox(String msgBox) {
		MsgBox = msgBox;
	}

	/** 07.摘要 Digest [Dgst] [0..1] Max128Text */
	public String getDgst() {
		return Dgst;
	}

	/** 07摘要 Digest [Dgst] [0..1] Max128Text */
	public void setDgst(String digest) {
		Dgst = digest;
	}
}
