package com.zdsoft.site.mina.model.customEnum;

import javax.xml.bind.annotation.XmlEnumValue;

/**
 * 银行卡绑定状态（BankBindStatus）
 * 
 * @author cqyhm
 *
 */
public enum BankBindStatus {
	/** 银行卡绑定状态"0", "未绑卡" */
	@XmlEnumValue("0") A0("0", "未绑卡"),

	/** 银行卡绑定状态"1", "绑定处理中" */
	@XmlEnumValue("1") A1("1", "绑定处理中"),

	/** 银行卡绑定状态"2", "绑定成功 " */
	@XmlEnumValue("2") A2("2", "绑定成功 "),

	/** 银行卡绑定状态"3", "已解绑" */
	@XmlEnumValue("3") A3("3", "已解绑"),

	/** 银行卡绑定状态"4", "绑定失败 " */
	@XmlEnumValue("4") A4("4", "绑定失败 ");

	private String value;
	private String name;

	private BankBindStatus(String value, String name) {
		this.value = value;
		this.name = name;
	}

	@Override
	public String toString() {
		return value;
	}

	public String getValue() {
		return value;
	}

	public String getName() {
		return name;
	}
}
