package com.zdsoft.site.thread.mActiveObject.Sample.activeobject;

public interface ActiveObject {
    public abstract Result makeString(int count, char fillchar);
    public abstract void displayString(String string);
}
