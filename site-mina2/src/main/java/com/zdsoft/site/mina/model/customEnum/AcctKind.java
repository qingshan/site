package com.zdsoft.site.mina.model.customEnum;

import javax.xml.bind.annotation.XmlEnumValue;

/**
 * 账户类型（AcctKind）
 * 
 * @author cqyhm
 *
 */
public enum AcctKind {
	/** 账户类型 "K", "收益子账户 " */
	@XmlEnumValue("K") AK("K", "收益子账户 "),

	/** 账户类型 "r", "红包子账户 */
	@XmlEnumValue("r") Ar("r", "红包子账户");

	private String value;
	private String name;

	private AcctKind(String value, String name) {
		this.value = value;
		this.name = name;
	}

	@Override
	public String toString() {
		return value;
	}

	public String getValue() {
		return value;
	}

	public String getName() {
		return name;
	}
}
