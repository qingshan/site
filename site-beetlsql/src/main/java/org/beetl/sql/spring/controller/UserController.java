package org.beetl.sql.spring.controller;

import org.beetl.sql.spring.domain.User;
import org.beetl.sql.spring.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 * Created by mikey.zhaopeng on 2015/6/2.
 */
@Controller
public class UserController {

	@Autowired
	private UserService userService;
	
    @RequestMapping(value = "/index.html", method = RequestMethod.GET)
    public ModelAndView index() {
    	ModelAndView view = new ModelAndView("/index.html");
    	int total = userService.total(new User());
    	view.addObject("total",total);
        return view;
    }
}
