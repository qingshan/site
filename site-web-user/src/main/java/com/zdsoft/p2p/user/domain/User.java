package com.zdsoft.p2p.user.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

import com.zdsoft.rock.common.model.domain.StaticEntity;

@Entity
public class User extends StaticEntity{

	private static final long serialVersionUID = 7144357400961889050L;
	@Id
	@GenericGenerator(name = "systemUUID", strategy = "uuid")
	@GeneratedValue(generator = "systemUUID")
	@Column(length = 32)
	private String userId;
	
	@NotBlank(message="{user.code.NotNull}")
	@Length(max=64,min=6,message="{user.code.length}")
	@Column(length=64)
	private String userCd;
	
	@NotBlank(message="{user.name.NotNull}")
	@Column(length=128)
	private String userNm;
	
	public User(){}
	
	public User(String userId, String userCd, String userNm) {
		super();
		this.userId = userId;
		this.userCd = userCd;
		this.userNm = userNm;
	}
	public User(String userCd, String userNm){
		this("",userCd,userNm);
	}
	
	@Override
	public String getId() {
		return getUserId();
	}
	@Override
	public void setId(String id) {
		setUserId(id);
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getUserCd() {
		return userCd;
	}

	public void setUserCd(String userCd) {
		this.userCd = userCd;
	}

	public String getUserNm() {
		return userNm;
	}

	public void setUserNm(String userNm) {
		this.userNm = userNm;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("User [userId=");
		builder.append(userId);
		builder.append(", userCd=");
		builder.append(userCd);
		builder.append(", userNm=");
		builder.append(userNm);
		builder.append("]");
		return builder.toString();
	}

}
