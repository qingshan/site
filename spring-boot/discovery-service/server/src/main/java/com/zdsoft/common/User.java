package com.zdsoft.common;

import java.io.Serializable;

public class User implements Serializable {

	private static final long serialVersionUID = -4285337642212111798L;
	private String userId;
	private String userCd;
	private String userNm;
	
	public User(){}
	
	public User(String userCd, String userNm) {
		super();
		this.userCd = userCd;
		this.userNm = userNm;
	}
	
	public User(String userId, String userCd, String userNm) {
		super();
		this.userId = userId;
		this.userCd = userCd;
		this.userNm = userNm;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getUserCd() {
		return userCd;
	}
	public void setUserCd(String userCd) {
		this.userCd = userCd;
	}
	public String getUserNm() {
		return userNm;
	}
	public void setUserNm(String userNm) {
		this.userNm = userNm;
	}
}
