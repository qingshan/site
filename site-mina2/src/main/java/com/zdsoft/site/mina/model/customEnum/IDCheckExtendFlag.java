package com.zdsoft.site.mina.model.customEnum;

import javax.xml.bind.annotation.XmlEnumValue;

/**
 * 7.30 核查扩展标识(IDCheckExtendFlag) 身份核验结果
 * 
 * @author cqyhm
 *
 */
public enum IDCheckExtendFlag {
	/** 身份核验结果 "1",需要提供照片模式 目前只支持提供照片模式 */
	@XmlEnumValue("1") A1("1", "需要提供照片模式"),

	/** 身份核验结果"0","不需要提供照片模式" */
	@XmlEnumValue("0") A0("0", "不需要提供照片模式");

	private String value;
	private String name;

	private IDCheckExtendFlag(String value, String name) {
		this.value = value;
		this.name = name;
	}

	public String getValue() {
		return value;
	}

	public String getName() {
		return name;
	}

	@Override
	public String toString() {
		return value;
	}
}
