package com.zdsoft.site.mina.model.request;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.zdsoft.site.mina.model.BidInfo;
import com.zdsoft.site.mina.model.Customer;
import com.zdsoft.site.mina.model.MessageHeader;
import com.zdsoft.site.mina.model.TransMangAcc;
import com.zdsoft.site.mina.model.customEnum.BusinessType;
import com.zdsoft.site.mina.model.customEnum.CurrencyCode;

/**
 *6.1.12服务商发起标的建立(42701)
 *	说明：
 *	1.本功能是服务商方对标的进行审核通过后，发送本系统进行登记处理。
 * @author cqyhm
 *
 */
@XmlRootElement(name = "MsgText")
public class RequestBidCreate extends AbstractRequest {
	
	public RequestBidCreate(){}
	
	public RequestBidCreate(MessageHeader msgHdr){
		setMsgHdr(msgHdr);
	}
	/**02.标的信息	BidInfo	<BidInfo>	[1..1]	BidInfo	组件*/
	@XmlElement(name="BidInfo")
	private BidInfo BidInfo;
	
	/**03.交易管理账户	ManageAccount	<MgeAcct>	[0..1]	TransMangAcc	组件*/
	@XmlElement(name="MgeAcct")
	private TransMangAcc MgeAcct;
	
	/**04.客户信息	Customer	<Cust>	[1..1]	Customer	组件*/
	@XmlElement(name="Cust")
	private Customer Cust;
	
	/**05	币种	Currency	<Ccy>	[1..1]	CurrencyCode	枚举*/
	@XmlElement(name="Ccy")
	private CurrencyCode Ccy;	
	
	/**06.业务类别	BusinessType	<BusType>	[0..1]	BusinessType	枚举*/
	@XmlElement(name="BusType")
	private BusinessType BusType;
	
	/**07.短信验证码	MessageBox	<MsgBox>	[0..1]	Max20Text	*/
	@XmlElement(name="MsgBox")
	private String MsgBox;
	
	/** 08.摘要 Digest [Dgst] [0..1] Max128Text */
	@XmlElement(name = "Dgst")
	private String Dgst;

	
	/**02.标的信息	BidInfo	<BidInfo>	[1..1]	BidInfo	组件*/
	public BidInfo getBidInfo() {
		return BidInfo;
	}
	/**02.标的信息	BidInfo	<BidInfo>	[1..1]	BidInfo	组件*/
	public void setBidInfo(BidInfo bidInfo) {
		BidInfo = bidInfo;
	}
	/**03.交易管理账户	ManageAccount	<MgeAcct>	[0..1]	TransMangAcc	组件*/
	public TransMangAcc getMgeAcct() {
		return MgeAcct;
	}
	/**03.交易管理账户	ManageAccount	<MgeAcct>	[0..1]	TransMangAcc	组件*/
	public void setMgeAcct(TransMangAcc mgeAcct) {
		MgeAcct = mgeAcct;
	}
	/**04.客户信息	Customer	<Cust>	[1..1]	Customer	组件*/
	public Customer getCust() {
		return Cust;
	}
	/**04.客户信息	Customer	<Cust>	[1..1]	Customer	组件*/
	public void setCust(Customer cust) {
		Cust = cust;
	}
	/**05	币种	Currency	<Ccy>	[1..1]	CurrencyCode	枚举*/
	public CurrencyCode getCcy() {
		return Ccy;
	}

	/**05	币种	Currency	<Ccy>	[1..1]	CurrencyCode	枚举*/
	public void setCcy(CurrencyCode ccy) {
		Ccy = ccy;
	}
	/**06.业务类别	BusinessType	<BusType>	[0..1]	BusinessType	枚举*/
	public BusinessType getBusType() {
		return BusType;
	}
	/**06.业务类别	BusinessType	<BusType>	[0..1]	BusinessType	枚举*/
	public void setBusType(BusinessType busType) {
		BusType = busType;
	}
	/**07.短信验证码	MessageBox	<MsgBox>	[0..1]	Max20Text	*/
	public String getMsgBox() {
		return MsgBox;
	}
	/**07.短信验证码	MessageBox	<MsgBox>	[0..1]	Max20Text	*/
	public void setMsgBox(String msgBox) {
		MsgBox = msgBox;
	}
	/** 08.摘要 Digest [Dgst] [0..1] Max128Text */
	public String getDgst() {
		return Dgst;
	}

	/** 08.摘要 Digest [Dgst] [0..1] Max128Text */
	public void setDgst(String digest) {
		Dgst = digest;
	}
}
