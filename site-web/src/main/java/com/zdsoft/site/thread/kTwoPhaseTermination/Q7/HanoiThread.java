package com.zdsoft.site.thread.kTwoPhaseTermination.Q7;

public class HanoiThread extends Thread {
    // 竒癳沧ゎ叫―玥true
    private volatile boolean shutdownRequested = false;
    // 癳沧ゎ叫―ㄨ
    private volatile long requestedTimeMillis = 0;

    // 沧ゎ叫―
    public void shutdownRequest() {
        requestedTimeMillis = System.currentTimeMillis();
        shutdownRequested = true;
        interrupt();
    }

    // 耞沧ゎ叫―琌竒癳
    public boolean isShutdownRequested() {
        return shutdownRequested;
    }

    // 笆
    public void run() {
        try {
            for (int level = 0; !shutdownRequested; level++) {
                System.out.println("==== Level " + level + " ====");
                doWork(level, 'A', 'B', 'C');
                System.out.println("");
            }
        } catch (InterruptedException e) {
        } finally {
            doShutdown();
        }
    }

    // 穨
    private void doWork(int level, char posA, char posB, char posC) throws InterruptedException {
        if (level > 0) {
            doWork(level - 1, posA, posC, posB);
            System.out.print(posA + "->" + posB + " ");
            doWork(level - 1, posC, posB, posA);
        }
    }

    // 沧ゎ矪瞶
    private void doShutdown() {
        long time = System.currentTimeMillis() - requestedTimeMillis;
        System.out.println("doShutdown: Latency = " + time + " msec.");
    }
}
