package com.zdsoft.site.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

import com.zdsoft.site.domain.Member;
import com.zdsoft.site.domain.User;
import com.zdsoft.site.event.registersucess.RegisterSucessEvent;
import com.zdsoft.site.service.MemberService;


@Service
public class MemberServiceImpl implements MemberService {
	
	private final Logger logger=LoggerFactory.getLogger(getClass());
	@Autowired
    private ApplicationContext applicationContext;
	
	@Override
	@Cacheable("member")
	public Member findById(String id){
		logger.debug("直接从数据库中获取数据");
		return new Member(id,"cqyhm","获取参数");
	}
	
	@Override
	@CachePut("member")
	public void save(Member member){
		logger.debug("放入缓存");
	}
	
	@Override
	@CacheEvict("member")
	public void delete(Member member){
		logger.debug("从缓存删除");
	}
	
	@Override
	public void register(String username, String password) {
    	logger.debug("[{}]注册成功！",username);
    	//触发事件
    	applicationContext.publishEvent(new RegisterSucessEvent(new User(username, password)));
    }
}
