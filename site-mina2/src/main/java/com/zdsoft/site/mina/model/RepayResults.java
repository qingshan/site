package com.zdsoft.site.mina.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

/**
 * 7.28 兑付结果集合（RepayResults）
 * 
 * @author cqyhm
 *
 */

@XmlAccessorType(XmlAccessType.FIELD)
public class RepayResults {

	/** 兑付结果信息 RepayBillInfo [RepayResultInfo] [0..1] RepayResultInfo 组件 */
	private RepayResultInfo RepayBillInfo1;
	/** 兑付结果信息 RepayBillInfo [RepayResultInfo] [0..1] RepayResultInfo 组件 */
	private RepayResultInfo RepayBillInfo2;

	/** 兑付结果信息 RepayBillInfo [RepayResultInfo] [0..1] RepayResultInfo 组件 */
	public RepayResultInfo getRepayBillInfo1() {
		return RepayBillInfo1;
	}

	/** 兑付结果信息 RepayBillInfo [RepayResultInfo] [0..1] RepayResultInfo 组件 */
	public void setRepayBillInfo1(RepayResultInfo repayBillInfo1) {
		RepayBillInfo1 = repayBillInfo1;
	}

	/** 兑付结果信息 RepayBillInfo [RepayResultInfo] [0..1] RepayResultInfo 组件 */
	public RepayResultInfo getRepayBillInfo2() {
		return RepayBillInfo2;
	}

	/** 兑付结果信息 RepayBillInfo [RepayResultInfo] [0..1] RepayResultInfo 组件 */
	public void setRepayBillInfo2(RepayResultInfo repayBillInfo2) {
		RepayBillInfo2 = repayBillInfo2;
	}
}
