package com.zdsoft.site.mina.model.response;

import java.math.BigDecimal;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.springframework.beans.BeanUtils;

import com.zdsoft.site.mina.model.TransMangAcc;
import com.zdsoft.site.mina.model.customEnum.CurrencyCode;
import com.zdsoft.site.mina.utils.JaxbUtil;

/**
 * 6.1.19.服务商发起借款人还款(42707)
 * 说明：
 * 1.本功能是服务商方发起借款人还款，并由本系统进行监管账户余额处理
 * 2.支持重发操作
 * 应答：
 * 
 * @author cqyhm
 *
 */
@XmlRootElement(name = "MsgText")
public class ResponseRepayment extends AbstractResponse {

	/**03.交易管理账户	ManageAccount	<MgeAcct>	[1..1]	TransMangAcc	组件*/
	@XmlElement(name="MgeAcct")
	private TransMangAcc MgeAcct;
	
	/**04.币种	Currency	<Ccy>	[0..1]	CurrencyCode	枚举 */
	@XmlElement(name="Ccy")
	private CurrencyCode Ccy;
	
	/**05.还款金额	TransferAmount	<TrfAmt>	[0..1]	Amount	*/
	@XmlElement(name="TrfAmt")
	private BigDecimal TrfAmt;
	
	/**06.成功还款金额	SuccessTransferAmount	<SucTrfAmt>	[0..1]	Amount*/
	@XmlElement(name="SucTrfAmt")
	private BigDecimal SucTrfAmt;
	
	/**07.手续费	FeeAmount	<FeeAmt>	[0..1]	Amount	*/
	@XmlElement(name="FeeAmt")
	private BigDecimal FeeAmt;
	
	/**08.摘要	Digest	<Dgst>	[0..1]	Max128Text	*/
	@XmlElement(name="Dgst")
	private String Dgst;
	
	public ResponseRepayment() {
	}

	public ResponseRepayment(String xml) {
		ResponseRepayment response = JaxbUtil.converyToJavaBean(xml, this.getClass());
		BeanUtils.copyProperties(response, this);
	}
	/**03.交易管理账户	ManageAccount	<MgeAcct>	[1..1]	TransMangAcc	组件*/
	public TransMangAcc getMgeAcct() {
		return MgeAcct;
	}
	/**03.交易管理账户	ManageAccount	<MgeAcct>	[1..1]	TransMangAcc	组件*/
	public void setMgeAcct(TransMangAcc mgeAcct) {
		MgeAcct = mgeAcct;
	}
	
	/**04.币种	Currency	<Ccy>	[0..1]	CurrencyCode	枚举 */
	public CurrencyCode getCcy() {
		return Ccy;
	}
	
	/**04.币种	Currency	<Ccy>	[0..1]	CurrencyCode	枚举 */
	public void setCcy(CurrencyCode ccy) {
		Ccy = ccy;
	}

	/**05.还款金额	TransferAmount	<TrfAmt>	[0..1]	Amount	*/
	public BigDecimal getTrfAmt() {
		return TrfAmt;
	}

	/**05.还款金额	TransferAmount	<TrfAmt>	[0..1]	Amount	*/
	public void setTrfAmt(BigDecimal trfAmt) {
		TrfAmt = trfAmt;
	}

	/**06.成功还款金额	SuccessTransferAmount	<SucTrfAmt>	[0..1]	Amount*/
	public BigDecimal getSucTrfAmt() {
		return SucTrfAmt;
	}

	/**06.成功还款金额	SuccessTransferAmount	<SucTrfAmt>	[0..1]	Amount*/
	public void setSucTrfAmt(BigDecimal sucTrfAmt) {
		SucTrfAmt = sucTrfAmt;
	}

	/**07.手续费	FeeAmount	<FeeAmt>	[0..1]	Amount	*/
	public BigDecimal getFeeAmt() {
		return FeeAmt;
	}

	/**07.手续费	FeeAmount	<FeeAmt>	[0..1]	Amount	*/
	public void setFeeAmt(BigDecimal feeAmt) {
		FeeAmt = feeAmt;
	}

	/**08.摘要	Digest	<Dgst>	[0..1]	Max128Text	*/
	public String getDgst() {
		return Dgst;
	}

	/**08.摘要	Digest	<Dgst>	[0..1]	Max128Text	*/
	public void setDgst(String dgst) {
		Dgst = dgst;
	}

}
