package com.zdsoft.site.mina.model.customEnum;

import javax.xml.bind.annotation.XmlEnumValue;

/**
 * 调整类型（AdjustType）
 * 
 * @author cqyhm
 *
 */
public enum AdjustType {
	/** 调整类型 "0", "红冲" */
	@XmlEnumValue("0") A0("0", "红冲"),

	/** 调整类型 "1", "蓝补" */
	@XmlEnumValue("1") A1("1", "蓝补");
	private String value;
	private String name;

	private AdjustType(String value, String name) {
		this.value = value;
		this.name = name;
	}

	@Override
	public String toString() {
		return value;
	}

	public String getValue() {
		return value;
	}

	public String getName() {
		return name;
	}
}
