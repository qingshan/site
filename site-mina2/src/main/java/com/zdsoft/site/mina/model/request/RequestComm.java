package com.zdsoft.site.mina.model.request;

import java.math.BigDecimal;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.zdsoft.site.mina.model.BidInfo;
import com.zdsoft.site.mina.model.MessageHeader;
import com.zdsoft.site.mina.model.TransMangAcc;
import com.zdsoft.site.mina.model.customEnum.AcctKind;
import com.zdsoft.site.mina.model.customEnum.BusinessType;
import com.zdsoft.site.mina.model.customEnum.CurrencyCode;
import com.zdsoft.site.mina.model.customEnum.PackageFlag;
import com.zdsoft.site.mina.model.customEnum.SendType;

/**
 * 6.1.20.服务商发起客户佣金派送(48709)
 * 说明：
 * 1.本功能是服务商方发起的给指定客户派发推荐奖金等派发资金，并由本系统进行监管账户余额处理
 * 2.支持重发操作
 *  请求：
 * 
 * @author cqyhm
 *
 */
@XmlRootElement(name = "MsgText")
public class RequestComm extends AbstractRequest {

	public RequestComm(){}
	
	public RequestComm(MessageHeader msgHdr){
		setMsgHdr(msgHdr);
	}
	/** 02 报文标志 PackageFlag <PkgFlag> [0..1] PackageFlag 枚举 */
	@XmlElement(name = "PkgFlag")
	private PackageFlag PkgFlag;

	/** 03.标的信息 BidInfo <BidInfo> [1..1] BidInfo 组件 */
	@XmlElement(name = "BidInfo")
	private BidInfo BidInfo;

	/** 04.交易管理账户 ManageAccount <MgeAcct> [0..1] TransMangAcc 组件 */
	@XmlElement(name = "MgeAcct")
	private TransMangAcc MgeAcct;

	/** 05. 派送账户类型 AcctKind < AcctKind> [1..1] AcctKind 枚举 */
	@XmlElement(name = "AcctKind")
	private AcctKind AcctKind;

	/** 06. 派送类型 SendType <SendType> [0..1] SendType 枚举 */
	@XmlElement(name = "SendType")
	private SendType SendType;

	/** 07 币种 Currency <Ccy> [1..1] CurrencyCode 枚举 */
	@XmlElement(name = "Ccy")
	private CurrencyCode Ccy;

	/** 08.业务类别 BusinessType <BusType> [0..1] BusinessType 枚举 */
	@XmlElement(name = "BusType")
	private BusinessType BusType;

	/** 09. 派送金额 TransferAmount <TrfAmt> [1..1] Amount */
	@XmlElement(name = "TrfAmt")
	private BigDecimal TrfAmt;

	/** 10.摘要 Digest [Dgst] [0..1] Max128Text */
	@XmlElement(name = "Dgst")
	private String Dgst;

	/** 02 报文标志 PackageFlag <PkgFlag> [0..1] PackageFlag 枚举 */
	public PackageFlag getPkgFlag() {
		return PkgFlag;
	}

	/** 02 报文标志 PackageFlag <PkgFlag> [0..1] PackageFlag 枚举 */
	public void setPkgFlag(PackageFlag pkgFlag) {
		PkgFlag = pkgFlag;
	}

	/** 03.标的信息 BidInfo <BidInfo> [1..1] BidInfo 组件 */
	public BidInfo getBidInfo() {
		return BidInfo;
	}

	/** 03.标的信息 BidInfo <BidInfo> [1..1] BidInfo 组件 */
	public void setBidInfo(BidInfo bidInfo) {
		BidInfo = bidInfo;
	}

	/** 04.交易管理账户 ManageAccount <MgeAcct> [0..1] TransMangAcc 组件 */
	public TransMangAcc getMgeAcct() {
		return MgeAcct;
	}

	/** 04.交易管理账户 ManageAccount <MgeAcct> [0..1] TransMangAcc 组件 */
	public void setMgeAcct(TransMangAcc mgeAcct) {
		MgeAcct = mgeAcct;
	}

	/** 05. 派送账户类型 AcctKind < AcctKind> [1..1] AcctKind 枚举 */
	public AcctKind getAcctKind() {
		return AcctKind;
	}

	/** 05. 派送账户类型 AcctKind < AcctKind> [1..1] AcctKind 枚举 */
	public void setAcctKind(AcctKind acctKind) {
		AcctKind = acctKind;
	}

	/** 06. 派送类型 SendType <SendType> [0..1] SendType 枚举 */
	public SendType getSendType() {
		return SendType;
	}

	/** 06. 派送类型 SendType <SendType> [0..1] SendType 枚举 */
	public void setSendType(SendType sendType) {
		SendType = sendType;
	}

	/** 07 币种 Currency <Ccy> [1..1] CurrencyCode 枚举 */
	public CurrencyCode getCcy() {
		return Ccy;
	}

	/** 07 币种 Currency <Ccy> [1..1] CurrencyCode 枚举 */
	public void setCcy(CurrencyCode ccy) {
		Ccy = ccy;
	}

	/** 08.业务类别 BusinessType <BusType> [0..1] BusinessType 枚举 */
	public BusinessType getBusType() {
		return BusType;
	}

	/** 08.业务类别 BusinessType <BusType> [0..1] BusinessType 枚举 */
	public void setBusType(BusinessType busType) {
		BusType = busType;
	}

	/** 09. 派送金额 TransferAmount <TrfAmt> [1..1] Amount */
	public BigDecimal getTrfAmt() {
		return TrfAmt;
	}

	/** 09. 派送金额 TransferAmount <TrfAmt> [1..1] Amount */
	public void setTrfAmt(BigDecimal trfAmt) {
		TrfAmt = trfAmt;
	}

	/** 10.摘要 Digest [Dgst] [0..1] Max128Text */
	public String getDgst() {
		return Dgst;
	}

	/** 10摘要 Digest [Dgst] [0..1] Max128Text */
	public void setDgst(String digest) {
		Dgst = digest;
	}

}
