package com.zdsoft.site.mina.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * 银行流水信息集合（BankSerialsInfo）
 * 
 * @author cqyhm
 *
 */

@XmlAccessorType(XmlAccessType.FIELD)
public class BankSerialsInfo {
	/** 银行流水信息 BankSerial1 [BankSerial] [1..1] BankSerial 组件 */
	@XmlElement(name = "BankSerial")
	private BankSerial BankSerial1;

	/** 银行流水信息 BankSerial2 [BankSerial] [1..1] BankSerial 组件 */
	@XmlElement(name = "BankSerial")
	private BankSerial BankSerial2;

	/** 银行流水信息 BankSerial1 [BankSerial] [1..1] BankSerial 组件 */
	public BankSerial getBankSerial1() {
		return BankSerial1;
	}

	/** 银行流水信息 BankSerial1 [BankSerial] [1..1] BankSerial 组件 */
	public void setBankSerial1(BankSerial bankSerial1) {
		BankSerial1 = bankSerial1;
	}

	/** 银行流水信息 BankSerial2 [BankSerial] [1..1] BankSerial 组件 */
	public BankSerial getBankSerial2() {
		return BankSerial2;
	}

	/** 银行流水信息 BankSerial2 [BankSerial] [1..1] BankSerial 组件 */
	public void setBankSerial2(BankSerial bankSerial2) {
		BankSerial2 = bankSerial2;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("BankSerialsInfo [BankSerial1=").append(BankSerial1).append(", BankSerial2=").append(BankSerial2).append("]");
		return builder.toString();
	}
}
