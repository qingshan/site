package com.zdsoft.site.mina.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * 返回结果（ReturnResult） 
 * @author cqyhm
 */

@XmlAccessorType(XmlAccessType.FIELD)
public class ReturnResult {
	/**返回码  Code  [Code]  [1..1]  ReturnCode  枚举  必选*/
	@XmlElement(name="Code")
	private String Code;
	
	/**返回信息  Info  [Info]  [0..1]  Max100Text  可选*/
	@XmlElement(name="Info")
	private String Info;
	
	/**返回码  Code  [Code]  [1..1]  ReturnCode  枚举  必选*/
	public String getCode() {
		return Code;
	}
	/**返回码  Code  [Code]  [1..1]  ReturnCode  枚举  必选*/
	public void setCode(String code) {
		this.Code = code;
	}
	/**返回信息  Info  [Info]  [0..1]  Max100Text  可选*/
	public String getInfo() {
		return Info;
	}
	/**返回信息  Info  [Info]  [0..1]  Max100Text  可选*/
	public void setInfo(String info) {
		this.Info = info;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ReturnResult [code=").append(Code).append(", info=").append(Info).append("]");
		return builder.toString();
	} 
}
