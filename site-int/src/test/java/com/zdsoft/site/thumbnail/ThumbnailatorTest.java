package com.zdsoft.site.thumbnail;

import static org.junit.Assert.assertNotNull;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import javax.imageio.ImageIO;

import org.junit.Test;

import net.coobird.thumbnailator.Thumbnails;
import net.coobird.thumbnailator.geometry.Positions;
import net.coobird.thumbnailator.name.Rename;
/**
 * 缩率图生成工具
 * @author cqyhm
 * 支持：图片缩放，区域裁剪，水印，旋转，保持比例。
 * 需要注意的是，对于CMYK模式的图像，由于JDK的Bug，目前还不能够处理，会出以下异常：
 * javax.imageio.IIOException: Unsupported Image Type 
 * javax.imageio.IIOException: Incompatible color conversion 
 * java.lang.IllegalArgumentException: Numbers of source Raster bands and source color space components do not match 
 * 这些问题可以JAI.create()来代替ImageIO.read()解决。而高清图的内存溢出OOM问题只能使用ImageMagick转换了。 
 * 若png、gif格式图片中含有透明背景，使用该工具压缩处理后背景会变成黑色，这是Thumbnailator的一个bug，预计后期版本会解决。
 */
public class ThumbnailatorTest {
	//01.指定大小进行缩放
	@Test
	public void testA1() throws IOException{
	    //size(宽度, 高度)  
	    /*   
	     * 若图片横比200小，高比300小，不变   
	     * 若图片横比200小，高比300大，高缩小到300，图片比例不变   
	     * 若图片横比200大，高比300小，横缩小到200，图片比例不变   
	     * 若图片横比200大，高比300大，图片按比例缩小，横为200或高为300   
	     */   
	    Thumbnails.of("images/a380_1280x1024.jpg")   
	            .size(200, 300)  
	            .toFile("c:/a380_200x300.jpg");  
	      
	    Thumbnails.of("images/a380_1280x1024.jpg")   
	            .size(2560, 2048)   
	            .toFile("c:/a380_2560x2048.jpg");  
	}
	//02.按照比例进行缩放
	@Test
	public void testA2() throws IOException{
	    //scale(比例)  
	    Thumbnails.of("images/a380_1280x1024.jpg")   
	            .scale(0.25f)  
	            .toFile("c:/a380_25%.jpg");  
	      
	    Thumbnails.of("images/a380_1280x1024.jpg")   
	            .scale(1.10f)  
	            .toFile("c:/a380_110%.jpg");  
	}
	//03.不按照比例，指定大小进行缩放
	@Test
	public void testA3() throws IOException{
	    //keepAspectRatio(false) 默认是按照比例缩放的  
	    Thumbnails.of("images/a380_1280x1024.jpg")   
	            .size(200, 200)   
	            .keepAspectRatio(false)   
	            .toFile("c:/a380_200x200.jpg");  
	}
	//04.旋转
	@Test
	public void testA4() throws IOException{
	    //rotate(角度),正数：顺时针 负数：逆时针  
	    Thumbnails.of("images/a380_1280x1024.jpg")   
	            .size(1280, 1024)  
	            .rotate(90)   
	            .toFile("c:/a380_rotate+90.jpg");   
	      
	    Thumbnails.of("images/a380_1280x1024.jpg")   
	            .size(1280, 1024)  
	            .rotate(-90)   
	            .toFile("c:/a380_rotate-90.jpg");   
	}
	//05.水印
	@Test
	public void testA5() throws IOException{
	    //watermark(位置，水印图，透明度)  
	    Thumbnails.of("images/a380_1280x1024.jpg")   
	            .size(1280, 1024)  
	            .watermark(Positions.BOTTOM_RIGHT, ImageIO.read(new File("images/watermark.png")), 0.5f) 
	            .outputQuality(0.8f)   
	            .toFile("c:/a380_watermark_bottom_right.jpg");  
	    //outputQuality：输出的图片质量，范围：0.0~1.0，1为最高质量。注意使用该方法时输出的图片格式必须为jpg（即outputFormat("jpg")。
	    //其他格式我没试过，感兴趣的自己可以试试）。否则若是输出png格式图片，则该方法作用无效【这其实应该算是bug】
	    Thumbnails.of("images/a380_1280x1024.jpg")   
	            .size(1280, 1024)  
	            .watermark(Positions.CENTER, ImageIO.read(new File("images/watermark.png")), 0.5f)   
	            .outputQuality(0.8f)   
	            .toFile("c:/a380_watermark_center.jpg");  
	}
	//06.裁剪
	@Test
	public void testA6() throws IOException{
	    //sourceRegion()  
	      
	    //图片中心400*400的区域  
	    Thumbnails.of("images/a380_1280x1024.jpg")  
	            .sourceRegion(Positions.CENTER, 400,400)  
	            .size(200, 200)  
	            .keepAspectRatio(false)   
	            .toFile("c:/a380_region_center.jpg");  
	      
	    //图片右下400*400的区域  
	    Thumbnails.of("images/a380_1280x1024.jpg")  
	            .sourceRegion(Positions.BOTTOM_RIGHT, 400,400)  
	            .size(200, 200)  
	            .keepAspectRatio(false)   
	            .toFile("c:/a380_region_bootom_right.jpg");  
	    //指定坐标  
	    Thumbnails.of("images/a380_1280x1024.jpg")  
	            .sourceRegion(600, 500, 400, 400)  
	            .size(200, 200)  
	            .keepAspectRatio(false)   
	            .toFile("c:/a380_region_coord.jpg");  
	}
	//07.转化图像格式
	@Test
	public void testA7() throws IOException{
	    //outputFormat(图像格式)  
//	    Thumbnails.of("original.png")   
//	            .size(1280, 1024)  
//	            .outputFormat("png")   
//	            .toFile("a:/original.png");   
	    
	    //outputFormat：输出的图片格式。
	    Thumbnails.of("a:/1.jpg")   
	            .size(1024, 768) 
	            //.scale(0.5f)
	            //.outputQuality(1f)
	            .toFile("a:/1.jpg");   	
	}
	//08.输出到OutputStream
	@Test
	public void testA8() throws IOException{
	    //toOutputStream(流对象)  
	    OutputStream os = new FileOutputStream("c:/a380_1280x1024_OutputStream.png");  
	    Thumbnails.of("images/a380_1280x1024.jpg")   
	            .size(1280, 1024)  
	            .toOutputStream(os);  
	}
	//09.输出到BufferedImage
	@Test
	public void testA9() throws IOException{
		//asBufferedImage() 返回BufferedImage  
		BufferedImage thumbnail = Thumbnails.of("images/a380_1280x1024.jpg")   
		        .size(1280, 1024)  
		        .asBufferedImage();  
		ImageIO.write(thumbnail, "jpg", new File("c:/a380_1280x1024_BufferedImage.jpg"));  
	}
////////////////////////////////////////////////////////////////////////////////////////////	
	@Test
	public void test() throws IOException {
		//生成指定大小的缩率图
		Thumbnails.of(getClass().getResourceAsStream("/original.png")).size(80, 80).toFile("c:/original80.png");
		//生成一定比例的缩略图
		Thumbnails.of(getClass().getResourceAsStream("/original.png")).scale(0.25f).toFile("c:/original25.png");
	}
	
	@Test
	public void test1() throws IOException{
		BufferedImage originalImage = ImageIO.read(new File("original.png"));
		//按一定的比例生成缩略图..生成缩略图的大小是原来的25%
		BufferedImage thumbnail = Thumbnails.of(originalImage).scale(0.25f).asBufferedImage();
		assertNotNull(thumbnail);
	}
	@Test
	public void test2() throws IOException{
		//把生成的图片输出到输出流（OutPutStream）中
		OutputStream os=new FileOutputStream(new File(""));
		Thumbnails.of("large-picture.jpg")
		        .size(200, 200)
		        .outputFormat("png")
		        .toOutputStream(os);
	}
	@Test
	public void test3() throws IOException{
		//这段代码是从original.jpg这张图片生成最大尺寸160*160，顺时针旋转90°，水印放在右下角，50%的透明度，80%的质量压缩的缩略图。
		Thumbnails.of(new File("original.jpg"))
        .size(160, 160)
        .rotate(90)
        .watermark(Positions.BOTTOM_RIGHT, ImageIO.read(new File("watermark.png")), 0.5f)
        .outputQuality(0.8f)
        .toFile(new File("image-with-watermark.jpg"));
	}
	@Test
	public void test4() throws IOException{
		Thumbnails.of(new File("original.jpg"))
        .size(160, 160)
        .toFile(new File("thumbnail.jpg"));
		//最后一行的toFile()方法还接受一个String类型的参数，如下面的代码和上面的作用的一样的：
		Thumbnails.of("original.jpg")
        .size(160, 160)
        .toFile("thumbnail.jpg"); 
	}
	@Test
	public void test5() throws IOException{
		 //支持生成经过旋转后的缩略图：
		 for (int i : new int[] {0, 90, 180, 270, 45}) {
		    Thumbnails.of(new File("coobird.png"))
		            .size(100, 100)
		            .rotate(i)
		            .toFile(new File("image-rotated-" + i + ".png")); 
		}
	}
	
	@Test
	public void test6() throws IOException{
		//文件夹都需要生成缩略图
		Thumbnails.of(new File("path/to/directory")
		.listFiles())         
		.size(640, 480)         
		.outputFormat("jpg")         
		.toFiles(Rename.PREFIX_DOT_THUMBNAIL); 
	}
	
}
