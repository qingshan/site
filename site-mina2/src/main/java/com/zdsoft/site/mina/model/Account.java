package com.zdsoft.site.mina.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

import com.zdsoft.site.mina.model.customEnum.AccountStatus;
import com.zdsoft.site.mina.model.customEnum.AccountType;
import com.zdsoft.site.mina.model.customEnum.BankFlag;
import com.zdsoft.site.mina.model.customEnum.BankId;
import com.zdsoft.site.mina.model.customEnum.CardType;

/**
 *  7.7   账户（Account）  
 * @author cqyhm
 * amount 金额总的最大长度 18位，小数位长度最大为 2 
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class Account {
	/**银行管理账号 账号  Id  [Id]  [1..1]  Max32Text 必填 */
	@XmlElement(name="Id")
	private String Id;
	
	/**户主名称  Name  [Name]  [1..1]  Max60Text 可选*/
	@XmlElement(name="Name")
	private String Name;
	
	/**账户类别  Type  [Type]  [0..1]  Type  枚举  可选。*/
	@XmlElement(name="Type")
	private AccountType Type;
	
	/**账户状态  AccountStatus  [Status]  [0..1]  AccountStatus  枚举*/
	@XmlElement(name="Status")
	private AccountStatus Status;
	
	/**银行标志  BkFlag  [BkFlag]  [0..1]  BkFlag  枚举  可选，默认为本行*/
	@XmlElement(name="BkFlag")
	private BankFlag BkFlag;
	
	/**银行编号  BkId  [ BkId]  [1..1]  BkId  枚举*/
	@XmlElement(name="BkId")
	private BankId BkId;
	
	/**卡类型  CardType  [CardType]  [1..1]  CardType  枚举*/
	@XmlElement(name="CardType")
	private CardType CardType;
	
	/**归属联行号  BkCode  [BkCode]  [1..1]  Max16Text*/
	@XmlElement(name="BkCode")
	private String BkCode;
	
	/**归属行行名  BkName  [BkName]  [1..1]  Max246Tex*/
	@XmlElement(name="BkName")
	private String BkName;
	
	/**密码  Pwd  [Pwd]  [0..1]  Pwd  组件  可选*/
	@XmlElement(name="Pwd")
	private Password Pwd;
	
	/**开户日期  RegDt  [RegDt]  [0..1]  Date   YYYYMMDD  可选*/
	@XmlElement(name="RegDt")
	private String RegDt;
	
	/**有效日期  VldDt  [VldDt]  [0..1]  Date YYYYMMDD  可选*/
	@XmlElement(name="VldDt")
	private String VldDt;
	
	/**信用卡背后末3位数 CVN2  [CVN2]  [0..1]  Max3Tex信用卡支付  必填*/
	@XmlElement(name="CVN2")
	private String CVN2;
	
	/**预留手机号  TelNo  [ TelNo]  [1..1]  Max16Text*/
	@XmlElement(name="TelNo")
	private String TelNo;
	
	/**银行管理账号 账号  Id  [Id]  [1..1]  Max32Text 必填 */
	public String getId() {
		return Id;
	}
	/**银行管理账号 账号  Id  [Id]  [1..1]  Max32Text 必填 */
	public void setId(String Id) {
		this.Id = Id;
	}
	/**户主名称  Name  [Name]  [1..1]  Max60Text 可选*/
	public String getName() {
		return Name;
	}
	/**户主名称  Name  [Name]  [1..1]  Max60Text 可选*/
	public void setName(String Name) {
		this.Name = Name;
	}
	/**账户类别  Type  [Type]  [0..1]  Type  枚举  可选。*/
	public AccountType getType() {
		return Type;
	}
	/**账户类别  Type  [Type]  [0..1]  Type  枚举  可选。*/
	public void setType(AccountType Type) {
		this.Type = Type;
	}
	/**账户状态  Status  [Status]  [0..1]  Status  枚举*/
	public AccountStatus getStatus() {
		return Status;
	}
	/**账户状态  Status  [Status]  [0..1]  Status  枚举*/
	public void setStatus(AccountStatus Status) {
		this.Status = Status;
	}
	/**银行标志  BkFlag  [BkFlag]  [0..1]  BkFlag  枚举  可选，默认为本行*/
	public BankFlag getBkFlag() {
		return BkFlag;
	}
	/**银行标志  BkFlag  [BkFlag]  [0..1]  BkFlag  枚举  可选，默认为本行*/
	public void setBkFlag(BankFlag BkFlag) {
		this.BkFlag = BkFlag;
	}
	/**银行编号  BkId  [ BkId]  [1..1]  BankId  枚举*/
	public BankId getBkId() {
		return BkId;
	}
	/**银行编号  BkId  [ BkId]  [1..1]  BankId  枚举*/
	public void setBkId(BankId BkId) {
		this.BkId = BkId;
	}
	/**卡类型  CardType  [CardType]  [1..1]  CardType  枚举*/
	public CardType getCardType() {
		return CardType;
	}
	/**卡类型  CardType  [CardType]  [1..1]  CardType  枚举*/
	public void setCardType(CardType cardType) {
		CardType = cardType;
	}
	/**归属联行号  BkCode  [BkCode]  [1..1]  Max16Text*/
	public String getBkCode() {
		return BkCode;
	}
	/**归属联行号  BkCode  [BkCode]  [1..1]  Max16Text*/
	public void setBkCode(String BkCode) {
		this.BkCode = BkCode;
	}
	/**归属行行名  BkName  [BkName]  [1..1]  Max246Tex*/
	public String getBkName() {
		return BkName;
	}
	/**归属行行名  BkName  [BkName]  [1..1]  Max246Tex*/
	public void setBkName(String BkName) {
		this.BkName = BkName;
	}
	/**密码  Pwd  [Pwd]  [0..1]  Pwd  组件  可选*/
	public Password getPwd() {
		return Pwd;
	}
	/**密码  Pwd  [Pwd]  [0..1]  Pwd  组件  可选*/
	public void setPwd(Password Pwd) {
		this.Pwd = Pwd;
	}
	/**开户日期  RegDt  [RegDt]  [0..1]  Date   YYYYMMDD  可选*/
	public String getRegDt() {
		return RegDt;
	}
	/**开户日期  RegDt  [RegDt]  [0..1]  Date   YYYYMMDD  可选*/
	public void setRegDt(String RegDt) {
		this.RegDt = RegDt;
	}
	/**有效日期  VldDt  [VldDt]  [0..1]  Date YYYYMMDD  可选*/
	public String getVldDt() {
		return VldDt;
	}
	/**有效日期  VldDt  [VldDt]  [0..1]  Date YYYYMMDD  可选*/
	public void setVldDt(String VldDt) {
		this.VldDt = VldDt;
	}
	/**信用卡背后末3位数 CVN2  [CVN2]  [0..1]  Max3Tex信用卡支付  必填*/
	public String getCVN2() {
		return CVN2;
	}
	/**信用卡背后末3位数 CVN2  [CVN2]  [0..1]  Max3Tex信用卡支付  必填*/
	public void setCVN2(String cVN2) {
		CVN2 = cVN2;
	}
	/**预留手机号  TelNo  [ TelNo]  [1..1]  Max16Text*/
	public String getTelNo() {
		return TelNo;
	}
	/**预留手机号  TelNo  [ TelNo]  [1..1]  Max16Text*/
	public void setTelNo(String telNo) {
		TelNo = telNo;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Account [Id=").append(Id).append(", Name=").append(Name).append(", Type=").append(Type).append(", Status=").append(Status).append(", BkFlag=").append(BkFlag).append(", BkId=").append(BkId)
				.append(", CardType=").append(CardType).append(", BkCode=").append(BkCode).append(", BkName=").append(BkName).append(", Pwd=").append(Pwd).append(", RegDt=").append(RegDt).append(", VldDt=").append(VldDt).append(", CVN2=").append(CVN2)
				.append(", TelNo=").append(TelNo).append("]");
		return builder.toString();
	}
}
