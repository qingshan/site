package com.zdsoft.site.mina.model.customEnum;

import javax.xml.bind.annotation.XmlEnumValue;

/**
 * 7.31 文件传输类型(FileTransFlag)
 * 
 * @author cqyhm
 *
 */
public enum FileTransFlag {
	/**文件传输类型  "0":下载客户端从服务器获取文件*/
	@XmlEnumValue("0")
	A0("0","下载"), 
	
	/**文件传输类型  "1":上传客户端发文件到服务器*/
	@XmlEnumValue("1")
	A1("1","上传");

	private String value;
	private String name;

	private FileTransFlag(String value, String name) {
		this.value = value;
		this.name = name;
	}

	public String getValue() {
		return value;
	}

	public String getName() {
		return name;
	}

	@Override
	public String toString() {
		return value;
	}
}
