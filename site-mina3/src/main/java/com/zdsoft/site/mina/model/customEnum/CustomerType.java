package com.zdsoft.site.mina.model.customEnum;
/**
 * 客户类型（CustomerType） 
 * @author cqyhm
 *
 */
public enum CustomerType {
	A0,//  个人 
	A1 // 机构 
}
