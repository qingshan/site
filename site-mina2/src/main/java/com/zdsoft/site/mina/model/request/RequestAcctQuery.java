package com.zdsoft.site.mina.model.request;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.zdsoft.site.mina.model.Customer;
import com.zdsoft.site.mina.model.MessageHeader;
import com.zdsoft.site.mina.model.TransMangAcc;
import com.zdsoft.site.mina.model.customEnum.BusinessType;
import com.zdsoft.site.mina.model.customEnum.CurrencyCode;

/**
 *6.1.21　管理账户信息查询(46720)
 *说明：
 *	1.适用于服务商发起的对客户管理账户信息查询功能。
 *	2.可以查询客户托管账户状态，绑卡信息，余额信息等信息
 * @author cqyhm
 *
 */
@XmlRootElement(name = "MsgText")
public class RequestAcctQuery extends AbstractRequest {
	/** 02.客户信息 Customer <Cust> [1..1] Customer 组件 */
	@XmlElement(name = "Cust")
	private Customer Cust;

	/** 04.交易管理账户 ManageAccount <MgeAcct> [1..1] TransMangAcc 组件 */
	@XmlElement(name = "MgeAcct")
	private TransMangAcc MgeAcct;

	/** 05 币种 Currency <Ccy> [1..1] CurrencyCode 枚举 */
	@XmlElement(name = "Ccy")
	private CurrencyCode Ccy=CurrencyCode.A0;

	/** 06.业务类别 BusinessType <BusType> [0..1] BusinessType 枚举 */
	@XmlElement(name = "BusType")
	private BusinessType BusType=BusinessType.A4;

	/** 07.摘要 Digest [Dgst] [0..1] Max128Text */
	@XmlElement(name = "Dgst")
	private String Dgst;

	
	public RequestAcctQuery(){}
	
	public RequestAcctQuery(MessageHeader msgHdr){
		setMsgHdr(msgHdr);
	}
	
	/** 02.客户信息 Customer <Cust> [1..1] Customer 组件 */
	public Customer getCust() {
		return Cust;
	}

	/** 02.客户信息 Customer <Cust> [1..1] Customer 组件 */
	public void setCust(Customer cust) {
		Cust = cust;
	}

	/** 04.交易管理账户 ManageAccount <MgeAcct> [1..1] TransMangAcc 组件 */
	public TransMangAcc getMgeAcct() {
		return MgeAcct;
	}

	/** 04.交易管理账户 ManageAccount <MgeAcct> [1..1] TransMangAcc 组件 */
	public void setMgeAcct(TransMangAcc mgeAcct) {
		MgeAcct = mgeAcct;
	}

	/** 05 币种 Currency <Ccy> [1..1] CurrencyCode 枚举 */
	public CurrencyCode getCcy() {
		return Ccy;
	}

	/** 05 币种 Currency <Ccy> [1..1] CurrencyCode 枚举 */
	public void setCcy(CurrencyCode ccy) {
		this.Ccy = ccy;
	}

	/** 06.业务类别 BusinessType <BusType> [0..1] BusinessType 枚举 */
	public BusinessType getBusType() {
		return BusType;
	}

	/** 06.业务类别 BusinessType <BusType> [0..1] BusinessType 枚举 */
	public void setBusType(BusinessType busType) {
		BusType = busType;
	}

	/** 07.摘要 Digest [Dgst] [0..1] Max128Text */
	public String getDgst() {
		return Dgst;
	}

	/** 07.摘要 Digest [Dgst] [0..1] Max128Text */
	public void setDgst(String digest) {
		Dgst = digest;
	}

	
}
