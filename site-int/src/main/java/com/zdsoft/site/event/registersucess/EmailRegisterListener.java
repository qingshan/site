package com.zdsoft.site.event.registersucess;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import com.zdsoft.site.domain.User;

/**
 * 注册成功发送邮件
 */
@Component
public class EmailRegisterListener implements ApplicationListener<RegisterSucessEvent> {
	
	private Logger logger=LoggerFactory.getLogger(getClass());
    @Async
    @Override
    public void onApplicationEvent(final RegisterSucessEvent event) {
    	try{
    		Thread.sleep(1000L);
    		logger.debug("注册成功,发送确认邮件给:[{}][{}]",Thread.currentThread().getId(),((User)event.getSource()).getUsername());
    	}catch(Exception e){
    		logger.debug("",e.getMessage());
    	}
    	
    	
    }
}
