package com.zdsoft.site.pdf;

import static org.junit.Assert.assertNotNull;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.AcroFields;
import com.itextpdf.text.pdf.AcroFields.FieldPosition;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.ColumnText;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfCopy;
import com.itextpdf.text.pdf.PdfImportedPage;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfStamper;
import com.itextpdf.text.pdf.PdfWriter;


/**
 * itext5首先生成PDF文件有两种方式，
 * 1：利用Adobe Acrobat 8 Professional专业版来制作PDF模板 
 * 2：就是用PdfWriter去生成。我个人建议第二种，因为模板定义的文本域是要框定长宽的，
 * 	   而业务数据往往不规则就会出现有的内容放不下有的内容就位置太大，无法动态去换行和收缩控制
 *   但第二种去手动生成可以通过API属性来排版和自动换行等灵活的效果
 * @author Admin
 *
 */
public class PdfExportTest {

	private static Logger logger = LoggerFactory.getLogger(PdfExportTest.class);
	
	/**
	 * 创建pdf文档
	 * @throws DocumentException
	 * @throws IOException
	 */
	@Test
	public void test1() throws DocumentException, IOException {
		Document doc = new Document();
		assertNotNull(doc);
		PdfWriter.getInstance(doc, new FileOutputStream("c:/gitHello.pdf"));
		doc.open();
		doc.add(new Paragraph("one"));
		BaseFont bf=BaseFont.createFont( "STSong-Light",   "UniGB-UCS2-H",   BaseFont.NOT_EMBEDDED);
		Font font=new Font(bf,12,Font.NORMAL);
		doc.add(new Paragraph("中文合同",font));
		doc.add(new Paragraph("two"));
		doc.close();
		logger.debug("生成文件成功");
	}

	public void test2() throws IOException, DocumentException {
		// 需要生成后的PDF
		FileOutputStream fos = new FileOutputStream("c:/Hello.pdf");
		// PDF模板路径
		String TemplatePDF = "c:/Hello.pdf";
		PdfReader reader = new PdfReader(TemplatePDF);
		PdfStamper stamp = new PdfStamper(reader, fos);
		AcroFields form = stamp.getAcroFields();
		assertNotNull(form);
	}
	@Test
	public void test4(){
		Document doc=new Document();
		try {
			PdfWriter.getInstance(doc,new FileOutputStream("c:/hello.pdf"));
			doc.open();
			BaseFont bf=BaseFont.createFont( "STSong-Light",   "UniGB-UCS2-H",   BaseFont.NOT_EMBEDDED);
			Font font=new Font(bf,12,Font.NORMAL);
			doc.add(new Paragraph("中文测试。。 Itext! ",font));
			doc.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void test3(){
		try {  
            int count = 8;// 总记录数  
            int pageCount = 4;// 每页记录数  
            int index = 1; // 表格序号  
            int page = 0;// 总共页数  
            /** 主要控制总共的页数*/  
            if (count >= pageCount && count % pageCount == 0) {  
                page = count / pageCount;  
            } else {  
                page = count / pageCount + 1;  
            }  
            String TemplatePDF = "c:/test/PdfTemplate.pdf";//设置模板路径  
            FileOutputStream fos = new FileOutputStream("c:/test/Pdf.pdf");//需要生成PDF  
              
            ByteArrayOutputStream baos[] = new ByteArrayOutputStream[page];//用于存储每页生成PDF流  
            /** 向PDF模板中插入数据 */  
            for (int item = 0; item < page; item++) {  
                baos[item] = new ByteArrayOutputStream();  
                PdfReader reader = new PdfReader(TemplatePDF);  
                PdfStamper stamp = new PdfStamper(reader, baos[item]);  
                AcroFields form = stamp.getAcroFields();  
                form.setField("DepartmnetNmae", "蓝飞");//插入的数据都为字符类型  
                form.setField("qq", "252462807");                 
                form.setField("pageNumber", "第" + (item + 1) + "页,共" + page  
                        + "页");  
                if (count % pageCount != 0 && item == page - 1) {  
                    System.out.println("====pageCount+" + pageCount + "=====");  
                    pageCount = count % pageCount;  
                }  
                /**因为PDF中的表格其实是众多的文本域组成,就是一个数组,所以把它循环出来就可以了*/  
                for (int j = 0; j < pageCount; j++) {  
                    form.setField("ProjectTask[" + j + "]", index + "");  
                    form.setField("星期一[" + j + "]", "星期一[" + index + "]");  
                    form.setField("星期二[" + j + "]", "星期二[" + index + "]");  
                    form.setField("星期三[" + j + "]", "星期三[" + index + "]");  
                    form.setField("星期四[" + j + "]", "星期四[" + index + "]");  
                    form.setField("星期五[" + j + "]", "星期五[" + index + "]");  
                    form.setField("星期六[" + j + "]", "星期六[" + index + "]");  
                    form.setField("星期日[" + j + "]", "星期日[" + index + "]");  
                    form.setField("意见[" + j + "]", "同意[" + j + "]");  
                    index++;  
                }  
                stamp.setFormFlattening(true); // 千万不漏了这句啊, */  
                stamp.close();  
            }  
            Document doc = new Document();  
            PdfCopy pdfCopy = new PdfCopy(doc, fos);  
            doc.open();  
            PdfImportedPage impPage = null;  
            /**取出之前保存的每页内容*/  
            for (int i = 0; i < page; i++) {  
                impPage = pdfCopy.getImportedPage(new PdfReader(baos[i]  
                        .toByteArray()), 1);  
                pdfCopy.addPage(impPage);  
            }  
            doc.close();//当文件拷贝  记得关闭doc  
        } catch (FileNotFoundException e) {  
            e.printStackTrace();  
        } catch (IOException e) {  
            e.printStackTrace();  
        } catch (DocumentException e) {  
            e.printStackTrace();  
        }  
	}
	/***
	 * 新建表格
	 * @throws DocumentException
	 * @throws IOException
	 */
	public PdfPTable getTableData() throws DocumentException, IOException{
		BaseFont baseFont=BaseFont.createFont("STSong-Light","UniGB-UCS2-H",BaseFont.NOT_EMBEDDED);
		Font fontChi = new Font(baseFont, 12, Font.NORMAL);
		PdfPTable table = new PdfPTable(4);
		table.setWidthPercentage(100);
		table.addCell(new Paragraph("学号", fontChi));
		PdfPCell cell = new PdfPCell(new Paragraph("00000001", fontChi));
		cell.setColspan(3);
		table.addCell(cell);
		table.addCell(new Paragraph("姓名", fontChi));
		table.addCell(new Paragraph("张三", fontChi));
		table.addCell(new Paragraph("总成绩", fontChi));
		table.addCell(new Paragraph("160", fontChi));
		table.addCell(new Paragraph("学号", fontChi));
		PdfPCell cell2 = new PdfPCell(new Paragraph("00000002", fontChi));
		cell2.setColspan(3);
		table.addCell(cell2);
		table.addCell(new Paragraph("姓名", fontChi));
		table.addCell(new Paragraph("李四", fontChi));
		table.addCell(new Paragraph("总成绩", fontChi));
		table.addCell(new Paragraph("167", fontChi));
		return table;
	}
	public void fillData(AcroFields fields, Map<String, String> data) throws IOException, DocumentException {
		for (String key : data.keySet()) {
			String value = data.get(key);
			fields.setField(key, value);
		}
	}
	/***/
	public Map<String, String> getMapData() {
		Map<String, String> data = new HashMap<String, String>();
		data.put("userNameB", "胡桃同学");
		data.put("userNameA", "杨华明");
		return data;
	}
	
	//根据pdf模板生成pdf文件
	@Test
	public void testTemplate() throws IOException, DocumentException {
		String fileName = "c:/template.pdf"; // pdf模板
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		PdfStamper ps = new PdfStamper(new PdfReader(fileName), bos);
		AcroFields fields = ps.getAcroFields();
		//增加替代字体
		BaseFont baseFont=BaseFont.createFont("STSong-Light","UniGB-UCS2-H",BaseFont.NOT_EMBEDDED);
		fields.addSubstitutionFont(baseFont);
		logger.debug("指定的数据填充预定的字段");
		fillData(fields, getMapData());
		fields.setField("pass", "中国");
		logger.debug("用指定的列表生成表格填充");
		//TODO 用指定的列表生成表格填充
		FieldPosition fp=ps.getAcroFields().getFieldPositions("userNameB").get(0);
		PdfContentByte canvas=ps.getOverContent(fp.page);
		ColumnText.showTextAligned(canvas, Element.ALIGN_LEFT, new Phrase("Hello People!"),fp.position.getLeft(),fp.position.getBottom()-10,fp.position.getRotation());
		ColumnText.showTextAligned(canvas, Element.ALIGN_LEFT, new Phrase("Hello People!"),fp.position.getLeft(),fp.position.getBottom()-30,fp.position.getRotation());
		ps.setFormFlattening(true);//表单扁平化处理(变成不可编辑)
		ps.close();
		FileOutputStream fos = new FileOutputStream("c:/contract.pdf");
		fos.write(bos.toByteArray());
		fos.close();
		logger.debug("导出模板");
	}
}
