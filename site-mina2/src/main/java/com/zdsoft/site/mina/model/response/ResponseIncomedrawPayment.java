package com.zdsoft.site.mina.model.response;

import java.math.BigDecimal;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.springframework.beans.BeanUtils;

import com.zdsoft.site.mina.model.Account;
import com.zdsoft.site.mina.model.Reference;
import com.zdsoft.site.mina.model.customEnum.CurrencyCode;
import com.zdsoft.site.mina.utils.JaxbUtil;

/**
 * 6.1.9服务商收益划出(44705)
 * 1.用于服务商在监管账户的收益划出，收款账号需要提前在银行登记维护，收益划出只能划出到预先登记的账户内。
 * 应答：
 * 
 * @author cqyhm
 *
 */
@XmlRootElement(name = "MsgText")
public class ResponseIncomedrawPayment extends AbstractResponse {
	/** 03. 银行流水号 BankReference [BkRef] [1..1] Reference 组件 */
	@XmlElement(name = "BkRef")
	private Reference BkRef;

	/** 04. 银行账户 BankAccount [BkAcct] [0..1] Account 组件 */
	@XmlElement(name = "BkAcct")
	private Account BkAcct;

	/** 05. 币种 Currency [Ccy] [0..1] CurrencyCode 枚举 */
	@XmlElement(name = "Ccy")
	private CurrencyCode Ccy;

	/** 06. 转帐金额 TransferAmount [TrfAmt] [0..1] Amount */
	@XmlElement(name = "TrfAmt")
	private BigDecimal TrfAmt;

	/** 07. 摘要 Digest [Dgst] [0..1] Max128Text */
	@XmlElement(name = "Dgst")
	private String Dgst;

	public ResponseIncomedrawPayment() {
	}

	public ResponseIncomedrawPayment(String xml) {
		ResponseIncomedrawPayment response = JaxbUtil.converyToJavaBean(xml, this.getClass());
		BeanUtils.copyProperties(response, this);
	}

	/** 银行流水号 BankReference [BkRef] [1..1] Reference 组件 */
	public Reference getBkRef() {
		return BkRef;
	}

	/** 银行流水号 BankReference [BkRef] [1..1] Reference 组件 */
	public void setBkRef(Reference bkRef) {
		BkRef = bkRef;
	}

	/** 银行账户 BankAccount [BkAcct] [0..1] Account 组件 */
	public Account getBkAcct() {
		return BkAcct;
	}

	/** 银行账户 BankAccount [BkAcct] [0..1] Account 组件 */
	public void setBkAcct(Account bkAcct) {
		BkAcct = bkAcct;
	}

	/** 币种 Currency [Ccy] [0..1] CurrencyCode 枚举 */
	public CurrencyCode getCcy() {
		return Ccy;
	}

	/** 币种 Currency [Ccy] [0..1] CurrencyCode 枚举 */
	public void setCcy(CurrencyCode ccy) {
		Ccy = ccy;
	}

	/** 转帐金额 TransferAmount [TrfAmt] [0..1] Amount */
	public BigDecimal getTrfAmt() {
		return TrfAmt;
	}

	/** 转帐金额 TransferAmount [TrfAmt] [0..1] Amount */
	public void setTrfAmt(BigDecimal trfAmt) {
		TrfAmt = trfAmt;
	}

	/** 摘要 Digest [Dgst] [0..1] Max128Text */
	public String getDgst() {
		return Dgst;
	}

	/** 摘要 Digest [Dgst] [0..1] Max128Text */
	public void setDgst(String dgst) {
		Dgst = dgst;
	}
}
