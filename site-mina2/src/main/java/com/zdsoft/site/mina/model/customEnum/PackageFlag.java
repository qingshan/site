package com.zdsoft.site.mina.model.customEnum;

import javax.xml.bind.annotation.XmlEnumValue;

/**
 * 报文标志，默认为正常
 * 
 * @author cqyhm
 *
 */
public enum PackageFlag {

	/** 报文标志，"N","正常" 默认为正常 */
	@XmlEnumValue("N") N("N", "正常"),

	/** 报文标志，"Q","查询 " 默认为正常 */
	@XmlEnumValue("Q") Q("Q", "查询 ");

	private String value;
	private String name;

	private PackageFlag(String value, String name) {
		this.value = value;
		this.name = name;
	}

	@Override
	public String toString() {
		return value;
	}

	public String getValue() {
		return value;
	}

	public String getName() {
		return name;
	}
}
