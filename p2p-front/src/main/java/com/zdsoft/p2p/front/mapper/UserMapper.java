package com.zdsoft.p2p.front.mapper;

import java.util.List;

import com.baomidou.mybatisplus.mapper.CommonMapper;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.pagination.Pagination;
import com.zdsoft.p2p.front.domain.User;

/**
 *
 * User 表数据库控制层接口
 *
 */
public interface UserMapper extends CommonMapper<User> {
	/**
	 * 自定义接口签名,查询所有的会员信息
	 * @return
	 */
	public List<User> findAll();
	/**
	 * 分页查询会员信息
	 * @param   pagination 传参数时必须参数必须是【RowBounds】类的子类,直接使用【RowBounds】是不行的
	 * 		   	Pagination和Page<?>
	 * @return  返回当前页的数据
	 */
	public List<User> findPager(Pagination page,EntityWrapper<User> user);
}