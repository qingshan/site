package com.zdsoft.site.mina.model.request;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.zdsoft.site.mina.model.Account;
import com.zdsoft.site.mina.model.Balance;
import com.zdsoft.site.mina.model.Customer;
import com.zdsoft.site.mina.model.MessageHeader;
import com.zdsoft.site.mina.model.TransMangAcc;
import com.zdsoft.site.mina.model.customEnum.BusinessType;
import com.zdsoft.site.mina.model.customEnum.CurrencyCode;

/**
 * 6.1.1 客户注册(46701)
 * 说明： 
 * 1.适用于服务商客户带卡指定监管银行签约和服务商客户预指定监管银行的签约。 
 * 2.如果请求报中没有银行卡号，则银行监管系统只创建银行帐号和交易管理账号的对应关系，客 户还需要进行增加银行卡绑定交易。
 * 3.实现客户单个币种的签约，不同币种需要进行多次本功能操作。
 * 4.服务商方根据应答包中的银行帐号确定和交易管理账号建立对应关系。
 * 5.银行方支持重复签约。
 * @author cqyhm
 *
 */
@XmlRootElement(name = "MsgText")
public class RequestRegist extends AbstractRequest {
	
	public RequestRegist(){}
	
	public RequestRegist(MessageHeader msgHdr){
		setMsgHdr(msgHdr);
	}
	
	/** 客户信息 Customer [Cust] [1..1] Customer 组件 */
	@XmlElement(name = "Cust")
	private Customer Cust;

	/** 银行账户 BankAccount [BkAcct] [0..1] Account 组件 */
	@XmlElement(name = "BkAcct")
	private Account BkAcct;

	/** 交易管理账户 ManageAccount [MgeAcct] [1..1] TransMangAcc 组件 */
	@XmlElement(name = "MgeAcct")
	private TransMangAcc MgeAcct;

	/** 币种 Currency [Ccy] [1..1] CurrencyCode 枚举 */
	@XmlElement(name = "Ccy")
	private CurrencyCode Ccy=CurrencyCode.A0;

	/** 业务类别 BusinessType [BusType] [0..1] BusinessType 枚举 */
	@XmlElement(name = "BusType")
	private BusinessType BusType=BusinessType.A4;

	/** 可用余额 AvailBalance [AvaiBal] [0..1] Balance 组件 */
	@XmlElement(name = "AvaiBal")
	private Balance AvaiBal;

	/** 支付待收余额 DhxBalance [DhxBal] [0..1] Balance 组件 */
	@XmlElement(name = "DhxBal")
	private Balance DhxBal;

	/** 投标冻结金额 FrozenBalance [FroBal] [0..1] Balance 组件 */
	@XmlElement(name = "FroBal")
	private Balance FroBal;

	/** 资产余额 AssetBalance [AssetBal] [0..1] Balance 组件 */
	@XmlElement(name = "AssetBal")
	private Balance AssetBal;

	/** 操作类型 OprateFlag [OPFlag] [0..1] 0 新客户 1 增加账号 */
	@XmlElement(name = "OPFlag")
	private String OPFlag;

	/** 短信验证码 MessageBox [MsgBox] [0..1] Max20Text */
	@XmlElement(name = "MsgBox")
	private String MsgBox;

	/** 摘要 Digest [Dgst] [0..1] Max128Text */
	@XmlElement(name = "Dgst")
	private String Dgst;


	/** 客户信息 Customer [Cust] [1..1] Customer 组件 */
	public Customer getCust() {
		return Cust;
	}

	/** 客户信息 Customer [Cust] [1..1] Customer 组件 */
	public void setCust(Customer customer) {
		Cust = customer;
	}

	/** 银行账户 BankAccount [BkAcct] [0..1] Account 组件 */
	public Account getBkAcct() {
		return BkAcct;
	}

	/** 银行账户 BankAccount [BkAcct] [0..1] Account 组件 */
	public void setBkAcct(Account BkAcct) {
		this.BkAcct = BkAcct;
	}

	/** 交易管理账户 ManageAccount [MgeAcct] [1..1] TransMangAcc 组件 */
	public TransMangAcc getMgeAcct() {
		return MgeAcct;
	}

	/** 交易管理账户 ManageAccount [MgeAcct] [1..1] TransMangAcc 组件 */
	public void setMgeAcct(TransMangAcc manageAccount) {
		MgeAcct = manageAccount;
	}

	/** 币种 Currency [Ccy] [1..1] CurrencyCode 枚举 */
	public CurrencyCode getCcy() {
		return Ccy;
	}

	/** 币种 Currency [Ccy] [1..1] CurrencyCode 枚举 */
	public void setCcy(CurrencyCode currencyCode) {
		Ccy = currencyCode;
	}

	/** 业务类别 BusinessType [BusType] [0..1] BusinessType 枚举 */
	public BusinessType getBusType() {
		return BusType;
	}

	/** 业务类别 BusinessType [BusType] [0..1] BusinessType 枚举 */
	public void setBusType(BusinessType businessType) {
		BusType = businessType;
	}

	/** 可用余额 AvailBalance [AvaiBal] [0..1] Balance 组件 */
	public Balance getAvaiBal() {
		return AvaiBal;
	}

	/** 可用余额 AvailBalance [AvaiBal] [0..1] Balance 组件 */
	public void setAvaiBal(Balance availBalance) {
		AvaiBal = availBalance;
	}

	/** 支付待收余额 DhxBalance [DhxBal] [0..1] Balance 组件 */
	public Balance getDhxBal() {
		return DhxBal;
	}

	/** 支付待收余额 DhxBalance [DhxBal] [0..1] Balance 组件 */
	public void setDhxBal(Balance dhxBalance) {
		DhxBal = dhxBalance;
	}

	/** 投标冻结金额 FrozenBalance [FroBal] [0..1] Balance 组件 */
	public Balance getFroBal() {
		return FroBal;
	}

	/** 投标冻结金额 FrozenBalance [FroBal] [0..1] Balance 组件 */
	public void setFroBal(Balance frozenBalance) {
		FroBal = frozenBalance;
	}

	/** 资产余额 AssetBalance [AssetBal] [0..1] Balance 组件 */
	public Balance getAssetBal() {
		return AssetBal;
	}

	/** 资产余额 AssetBalance [AssetBal] [0..1] Balance 组件 */
	public void setAssetBal(Balance assetBalance) {
		AssetBal = assetBalance;
	}

	/** 操作类型 OprateFlag [OPFlag] [0..1] 0 新客户 1 增加账号 */
	public String getOPFlag() {
		return OPFlag;
	}

	/** 操作类型 OprateFlag [OPFlag] [0..1] 0 新客户 1 增加账号 */
	public void setOPFlag(String oprateFlag) {
		OPFlag = oprateFlag;
	}

	/** 短信验证码 MessageBox [MsgBox] [0..1] Max20Text */
	public String getMsgBox() {
		return MsgBox;
	}

	/** 短信验证码 MessageBox [MsgBox] [0..1] Max20Text */
	public void setMsgBox(String messageBox) {
		MsgBox = messageBox;
	}

	/** 摘要 Digest [Dgst] [0..1] Max128Text */
	public String getDgst() {
		return Dgst;
	}

	/** 摘要 Digest [Dgst] [0..1] Max128Text */
	public void setDgst(String digest) {
		Dgst = digest;
	}
}
