package com.zdsoft.site.dao;

import org.springframework.stereotype.Repository;

import com.zdsoft.rock.common.model.dao.impl.AbstractDaoImpl;
import com.zdsoft.site.domain.Member;

@Repository
public class MemberDao extends AbstractDaoImpl<Member> {
}
