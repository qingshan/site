package com.zdsoft.site.mina.model;
/**
 * 银行流水信息集合（BankSerialsInfo） 
 * @author cqyhm
 *
 */
public class BankSerialsInfo {
//	银行流水信息  BankSerial1  <BankSerial>  [1..1]  BankSerial  组件 
	private BankSerial BankSerial1;
//	银行流水信息  BankSerial2  <BankSerial>  [1..1]  BankSerial  组件 
	private BankSerial BankSerial2;
	public BankSerial getBankSerial1() {
		return BankSerial1;
	}
	public void setBankSerial1(BankSerial bankSerial1) {
		BankSerial1 = bankSerial1;
	}
	public BankSerial getBankSerial2() {
		return BankSerial2;
	}
	public void setBankSerial2(BankSerial bankSerial2) {
		BankSerial2 = bankSerial2;
	}
}
