package com.zdsoft.site.mina.server;

import java.io.Serializable;

public class HimiObject implements Serializable {

	private static final long serialVersionUID = -2397992374939160688L;

	public HimiObject(){}
	
	public HimiObject(int id,String name){
		this.id=id;
		this.name=name;
	}

	private int id;

	private String name;

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("HimiObject [id=").append(id).append(", name=").append(name).append("]");
		return builder.toString();
	}

}
