package com.zdsoft.site.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.zdsoft.rock.common.controller.AbstractController;
import com.zdsoft.rock.common.model.domain.ResponseMsg;

@Controller
@RequestMapping("/member")
public class MemberController extends AbstractController{
	
	@ResponseBody
	@RequestMapping("/test1.html")
	public ResponseMsg<String> test1(){
		logger.debug("输出数据");
		return new ResponseMsg<String>();
	}
}
