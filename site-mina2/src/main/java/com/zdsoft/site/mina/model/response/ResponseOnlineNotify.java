package com.zdsoft.site.mina.model.response;

import javax.xml.bind.annotation.XmlRootElement;

/**
  *6.2.1.网银支付结果通知（43710）
  *	说明：
  *	1，存管系统接收到支付公司的网银充值结果通知后，调用此接口通知服务商
  * P2P平台应答
 * 
 * @author cqyhm
 *
 */
@XmlRootElement(name = "MsgText")
public class ResponseOnlineNotify extends AbstractResponse {
}
