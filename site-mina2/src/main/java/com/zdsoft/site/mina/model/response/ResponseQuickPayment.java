package com.zdsoft.site.mina.model.response;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.springframework.beans.BeanUtils;

import com.zdsoft.site.mina.utils.JaxbUtil;
/**
 *  * 6.1.5 服务商发起快捷支付入金(48703) –发送短信验证
 * @author cqyhm
 *
 */
@XmlRootElement(name = "MsgText")
public class ResponseQuickPayment extends AbstractResponse {
	/** 摘要 Digest <Dgst> [0..1] Max128Text */
	@XmlElement(name = "Dgst")
	private String Dgst;

	public ResponseQuickPayment(){}
	public ResponseQuickPayment(String xml) {
		ResponseQuickPayment response = JaxbUtil.converyToJavaBean(xml, this.getClass());
		BeanUtils.copyProperties(response, this);
	}
	
	/** 摘要 Digest <Dgst> [0..1] Max128Text */
	public String getDgst() {
		return Dgst;
	}

	/** 摘要 Digest <Dgst> [0..1] Max128Text */
	public void setDgst(String dgst) {
		Dgst = dgst;
	}
}
