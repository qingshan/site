<%@ page language="java" contentType="text/html; charset=UTF-8"   pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<link href="${ctx}/resources/extjs/resources/css/ext-all.css" type="text/css" rel="stylesheet" media="screen" />
<script type="text/javascript" src="${ctx}/resources/extjs/adapter/ext/ext-base.js"></script>
<script type="text/javascript" src="${ctx}/resources/extjs/ext-all.js"></script>
<script type="text/javascript" src="${ctx}/resources/extjs/ext-lang-zh_CN.js"></script>
</head>
<body>
<a id="walk" href="#">单击事件</a>
<script type="text/javascript">
	Ext.onReady(function(){
		Ext.MessageBox.alert("world","hello");
	});
	Person=function(name){
		this.name=name;
		this.addEvents("walk","eat","slow");
	}
	Ext.extend(Person,Ext.util.Observable,{
		info:function(event){
			return this.name+''+event+'ing.';
		}
	});
	var person=new Person("cqyhm");
	//绑定事件
	person.on("walk",function(){
		Ext.Msg.alert("绑定过得事件触发");
	});
	//on=addListener(),     //增加事件对应的监听函数
	//un()=removeListener(),//删除事件对应的监听函数
	//prugeListeners()      //删除所有的监听器
	Ext.get("walk").on('click',function(){
		person.fireEvent('walk');
	});
	//Ext.lib.Event==的所有底层调用方法
	// getX(),getY(),getXY()返回数组,发生的事件在页面的坐标位置
	// getTarget()返回事件的目标元素
	//on(),un()
	//preventDefault(),取消默认操作stopPropagation(),停止事件传播,stopEvent()会执行前面两个函数
	//onAvailable(id,fn,scope)等到id元素可用时才执行fn函数
	//resolveTextNode(node),如果node是一个文本节点，就返回上层节点，否则返回node本身
	//getRelatedTarget(),返回事件相关的html元素
	//Ext.EventManager或Ext.EventObject会用到上面的函数
	Ext.get("btn").on("click",function(){
		Ext.util.Observable.capture(person,function(){
			Ext.Msg.alert("捕获事件");
			return false;
		});
	})
</script>

</body>
</html>