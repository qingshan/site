package com.zdsoft.site;

import static org.junit.Assert.*;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.zdsoft.rock.common.util.HttpClientUtil;

public class HttpDoPostTest {

	private Logger logger=LoggerFactory.getLogger(getClass());
	@Test
	public void test() {
		Map<String, String> postParam=new HashMap<String, String>();
		postParam.put("a1", "11111");
		//postParam.put("a2", "");
		postParam.put("a3", "{\"user\":\"cqyhm\"}");
		String str=HttpClientUtil.doHttpPost("http://localhost:8780/site-web/template/testpost.html", postParam);
		assertNotNull(str);
		logger.debug(str);
	}
	@Test
	public void test1() {
		String str=HttpClientUtil.doHttpPost("http://localhost:8780/site-web/template/testpost.html?a1=11111&a3=user:cqyhm",null);
		assertNotNull(str);
		logger.debug(str);
	}
}
