package com.zdsoft.site.mina.model;
/**
 * 7.28  兑付结果集合（RepayResults） 
 * @author cqyhm
 *
 */
public class RepayResults {
//	兑付结果信息  RepayBillInfo  <RepayResultInfo>  [0..1]  RepayResultInfo  组件
	private RepayResultInfo  RepayBillInfo1;
//	兑付结果信息  RepayBillInfo  <RepayResultInfo>  [0..1]  RepayResultInfo  组件
	private RepayResultInfo  RepayBillInfo2;
	
	public RepayResultInfo getRepayBillInfo1() {
		return RepayBillInfo1;
	}
	public void setRepayBillInfo1(RepayResultInfo repayBillInfo1) {
		RepayBillInfo1 = repayBillInfo1;
	}
	public RepayResultInfo getRepayBillInfo2() {
		return RepayBillInfo2;
	}
	public void setRepayBillInfo2(RepayResultInfo repayBillInfo2) {
		RepayBillInfo2 = repayBillInfo2;
	}
}
