package com.zdsoft.site.mina.model.customEnum;

import javax.xml.bind.annotation.XmlEnumValue;

/**
 * 业务类别（BusinessType）
 * 
 * @author cqyhm
 *
 */
public enum BusinessType {
	
	/** 业务类别"4", "P2P资金存管" */
	@XmlEnumValue("4") A4("4", "P2P资金存管");
	
	private String value;
	private String name;

	private BusinessType(String value, String name) {
		this.value = value;
		this.name = name;
	}

	@Override
	public String toString() {
		return value;
	}

	public String getValue() {
		return value;
	}

	public String getName() {
		return name;
	}
}
