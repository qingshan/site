package com.zdsoft.site.mina.model.response;

import javax.xml.bind.annotation.XmlRootElement;

import org.springframework.beans.BeanUtils;

import com.zdsoft.site.mina.utils.JaxbUtil;
/**
 * 6.2.3    客户银行账号绑定 (46702)（建立绑定关系、验证并绑定）
	说明：
	P2P平台发起银行卡绑定关系建立短信验证并绑定请求到托管系统，请求报文包括：绑定流水号和短信验证码。
	托管系统受到请求后，将验证信息发到支付平台，由支付平台校验短信验证码是否超时、是否正确，发送银行绑定，
	最终返回商户短信验证状态及绑定交易状态。
 * 
*/	 
@XmlRootElement(name="MsgText")
public class ResponseBindCard2 extends ResponseBindCard {
	
	public ResponseBindCard2(){}
	
	public ResponseBindCard2(String xml){
		ResponseBindCard2 response=JaxbUtil.converyToJavaBean(xml, this.getClass());
		BeanUtils.copyProperties(response, this);
	}
}
