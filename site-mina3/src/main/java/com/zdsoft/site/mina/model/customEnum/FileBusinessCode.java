package com.zdsoft.site.mina.model.customEnum;
/**
 * 文件业务功能 
 * @author cqyhm
 *
 */
public enum FileBusinessCode {
	A0000,//  其他   
	A0001,//  出入金交易明细对账  btranYYYYMMDD.dat 
	A0016,//  还款兑付结果文件  srepayclechkYYYYMMDD.dat 
	A0017,//  核销不符文件  sverifychkYYYYMMDD.dat 
	A0018,// 标的投标记录明细文件 sinvestchkYYYYMMDD.dat 
	A0019,// 标的还款记录明细文件 srepaychkYYYYMMDD.dat 
	A0020,//  债权转让明细文件  screditchkYYYYMMDD.dat 
	A0021,//  历史资金流水明细  P2PZJLIST_YYYYMMDD_ZZZZZZZZ_1.dat 
	A0022,// 平台备付金派送明细文件 sdepitsndYYYYMMDD.dat 
	A0023//  余额文件  bbalaYYYYMMDD.dat 
}
