package com.zdsoft.site.mina.model.customEnum;
/**
 * 7.30  核查扩展标识(IDCheckExtendFlag) 
 * 身份核验结果 
 * @author cqyhm
 *
 */
public enum IDCheckExtendFlag {
	A1("1"),//需要提供照片模式  目前只支持提供照片模式 
	A0("0");//不需要提供照片模式
	private String value;
	
	private IDCheckExtendFlag(String value){
		this.value=value;
	}
	
	public String toString(){
		return value;
	}
}
