package org.beetl.sql.spring.service;

import org.beetl.sql.spring.domain.User;

public interface UserService {
	
	public void save(User user);
	
	public int total(User user);
}
