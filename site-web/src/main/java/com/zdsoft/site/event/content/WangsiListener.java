package com.zdsoft.site.event.content;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.event.SmartApplicationListener;
import org.springframework.stereotype.Component;

/**
 */
@Component
public class WangsiListener implements SmartApplicationListener  {

	private Logger logger=LoggerFactory.getLogger(getClass());
    @Override
    public void onApplicationEvent(final ApplicationEvent event) {
		try {
			Thread.sleep(1000L);	
		} catch (Exception e) {
			logger.debug("延时异常",e);
		}
        if(event instanceof ContentEvent) {
        	logger.debug("【1020】【WangsiListener】[{}]",event.getSource());
        	//throw new AppException("1020","【1020】【WangsiListener】异常");
        }       
    }
	@Override
	public int getOrder() {
		return 20;
	}
	@Override
	public boolean supportsEventType(Class<? extends ApplicationEvent> eventType) {
		return eventType == ContentEvent.class;
	}
	@Override
	public boolean supportsSourceType(Class<?> sourceType) {
		return true;
	}
}
