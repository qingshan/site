package com.zdsoft.common;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * 在zookeeper中使用ls /services/tomcat命令可以看到连接了的实例
 * 该项目提供微服务供其它微服务调用,其它微服务通过接口直接调用.
 * @author admin
 *
 */
@SpringBootApplication
@EnableDiscoveryClient
public class DiscoveryServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(DiscoveryServerApplication.class, args);
	}
}
