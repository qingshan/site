package com.zdsoft.site.mina.model.request;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.zdsoft.site.mina.model.Account;
import com.zdsoft.site.mina.model.Agent;
import com.zdsoft.site.mina.model.Customer;
import com.zdsoft.site.mina.model.MessageHeader;
import com.zdsoft.site.mina.model.TransMangAcc;
import com.zdsoft.site.mina.model.customEnum.BusinessType;
import com.zdsoft.site.mina.model.customEnum.CurrencyCode;

/**
 * 6.1.4　客户撤销银行账号(46703)
 * 
 * @author cqyhm
 *
 */
@XmlRootElement(name = "MsgText")
public class RequestCancelAcct extends AbstractRequest {

	
	public RequestCancelAcct(){}
	
	public RequestCancelAcct(MessageHeader msgHdr){
		setMsgHdr(msgHdr);
	}
	
	/** 客户信息 Customer [Cust] [1..1] Customer 组件 */
	@XmlElement(name = "Cust")
	private Customer Cust;
	
	/**经办人信息	Agent	[Agt]	[0..1]	Agent	组件*/
	@XmlElement(name="Agt")
	private Agent Agt;
	
	/** 银行账户 BankAccount [BkAcct] [0..1] Account 组件 */
	@XmlElement(name = "BkAcct")
	private Account BkAcct;

	/** 交易管理账户 ManageAccount [MgeAcct] [1..1] TransMangAcc 组件 */
	@XmlElement(name = "MgeAcct")
	private TransMangAcc MgeAcct;

	/** 币种 Currency [Ccy] [1..1] CurrencyCode 枚举 */
	@XmlElement(name = "Ccy")
	private CurrencyCode Ccy=CurrencyCode.A0;

	/** 业务类别 BusinessType [BusType] [0..1] BusinessType 枚举 */
	@XmlElement(name = "BusType")
	private BusinessType BusType=BusinessType.A4;

	/**操作标志	CancelFlag	<CnlFlag>	[0..1]	0-全解 1-指定账户	枚举*/
	private String CnlFlag;
	
	/** 短信验证码 MessageBox [MsgBox] [0..1] Max20Text */
	@XmlElement(name = "MsgBox")
	private String MsgBox;

	/** 摘要 Digest [Dgst] [0..1] Max128Text */
	@XmlElement(name = "Dgst")
	private String Dgst;


	/** 客户信息 Customer [Cust] [1..1] Customer 组件 */
	public Customer getCust() {
		return Cust;
	}

	/** 客户信息 Customer [Cust] [1..1] Customer 组件 */
	public void setCust(Customer customer) {
		Cust = customer;
	}

	/** 银行账户 BankAccount [BkAcct] [0..1] Account 组件 */
	public Account getBkAcct() {
		return BkAcct;
	}

	/** 银行账户 BankAccount [BkAcct] [0..1] Account 组件 */
	public void setBkAcct(Account BkAcct) {
		this.BkAcct = BkAcct;
	}

	/** 交易管理账户 ManageAccount [MgeAcct] [1..1] TransMangAcc 组件 */
	public TransMangAcc getMgeAcct() {
		return MgeAcct;
	}

	/** 交易管理账户 ManageAccount [MgeAcct] [1..1] TransMangAcc 组件 */
	public void setMgeAcct(TransMangAcc manageAccount) {
		MgeAcct = manageAccount;
	}

	/** 币种 Currency [Ccy] [1..1] CurrencyCode 枚举 */
	public CurrencyCode getCcy() {
		return Ccy;
	}

	/** 币种 Currency [Ccy] [1..1] CurrencyCode 枚举 */
	public void setCcy(CurrencyCode currencyCode) {
		Ccy = currencyCode;
	}

	/** 业务类别 BusinessType [BusType] [0..1] BusinessType 枚举 */
	public BusinessType getBusType() {
		return BusType;
	}

	/** 业务类别 BusinessType [BusType] [0..1] BusinessType 枚举 */
	public void setBusType(BusinessType businessType) {
		BusType = businessType;
	}

	/** 短信验证码 MessageBox [MsgBox] [0..1] Max20Text */
	public String getMsgBox() {
		return MsgBox;
	}

	/** 短信验证码 MessageBox [MsgBox] [0..1] Max20Text */
	public void setMsgBox(String messageBox) {
		MsgBox = messageBox;
	}

	/** 摘要 Digest [Dgst] [0..1] Max128Text */
	public String getDgst() {
		return Dgst;
	}

	/** 摘要 Digest [Dgst] [0..1] Max128Text */
	public void setDgst(String digest) {
		Dgst = digest;
	}
	/**经办人信息	Agent	[Agt]	[0..1]	Agent	组件*/
	public Agent getAgt() {
		return Agt;
	}
	/**经办人信息	Agent	[Agt]	[0..1]	Agent	组件*/
	public void setAgt(Agent agt) {
		Agt = agt;
	}
	/**操作标志	CancelFlag	<CnlFlag>	[0..1]	0-全解 1-指定账户	枚举*/
	public String getCnlFlag() {
		return CnlFlag;
	}
	/**操作标志	CancelFlag	<CnlFlag>	[0..1]	0-全解 1-指定账户	枚举*/
	public void setCnlFlag(String cnlFlag) {
		CnlFlag = cnlFlag;
	}
}
