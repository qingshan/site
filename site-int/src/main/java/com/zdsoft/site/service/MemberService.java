package com.zdsoft.site.service;

import com.zdsoft.site.domain.Member;

public interface MemberService {

	/**
	 * 查询用户Id
	 * @param id
	 * @return
	 */
	public Member findById(String id);

	public void save(Member member);

	public void delete(Member member);
	/**
	 * 会员注册
	 * @param username
	 * @param password
	 */
	public void register(String username, String password);

}
