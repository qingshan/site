package com.zdsoft.site.mina.model.response;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.springframework.beans.BeanUtils;

import com.zdsoft.site.mina.model.BidInfo;
import com.zdsoft.site.mina.model.TransMangAcc;
import com.zdsoft.site.mina.utils.JaxbUtil;

/**
 * 6.1.13服务商发起标的撤销(42702)（含流标）
 * 说明： 
 * 	1.本功能是服务商方对已登记的标的进行撤销处理。
 * 	2.对于募集期结束后仍未满标的情况可进行标的流标处理，并更新客户的资金余额。 
 * 应答：
 * 
 * @author cqyhm
 *
 */
@XmlRootElement(name = "MsgText")
public class ResponseBidCancel extends AbstractResponse {

	/**03.交易管理账户	ManageAccount	<MgeAcct>	[1..1]	TransMangAcc	组件*/
	@XmlElement(name="MgeAcct")
	private TransMangAcc MgeAcct;
	
	/**04.标的信息	BidInfo	<BidInfo>	[0..1]	BidInfo	组件*/
	@XmlElement(name="BidInfo")
	private BidInfo BidInfo;
	
	public ResponseBidCancel() {
	}

	public ResponseBidCancel(String xml) {
		ResponseBidCancel response = JaxbUtil.converyToJavaBean(xml, this.getClass());
		BeanUtils.copyProperties(response, this);
	}
	/**03.交易管理账户	ManageAccount	<MgeAcct>	[1..1]	TransMangAcc	组件*/
	public TransMangAcc getMgeAcct() {
		return MgeAcct;
	}
	/**03.交易管理账户	ManageAccount	<MgeAcct>	[1..1]	TransMangAcc	组件*/
	public void setMgeAcct(TransMangAcc mgeAcct) {
		MgeAcct = mgeAcct;
	}
	/**04.标的信息	BidInfo	<BidInfo>	[0..1]	BidInfo	组件*/
	public BidInfo getBidInfo() {
		return BidInfo;
	}
	/**04.标的信息	BidInfo	<BidInfo>	[0..1]	BidInfo	组件*/
	public void setBidInfo(BidInfo bidInfo) {
		BidInfo = bidInfo;
	}

}
