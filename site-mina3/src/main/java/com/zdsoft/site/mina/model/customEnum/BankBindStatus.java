package com.zdsoft.site.mina.model.customEnum;
/**
 * 银行卡绑定状态（BankBindStatus） 
 * @author cqyhm
 *
 */
public enum BankBindStatus {
	A0("0"),//未绑卡 
	A1("1"),//绑定处理中 
	A2("2"),//绑定成功 
	A3("3"),//已解绑 
	A4("4");//绑定失败 
	private String value;
	
	private BankBindStatus(String value){
		this.value=value;
	}
	@Override
	public String toString(){
		return value;
	}	
}
