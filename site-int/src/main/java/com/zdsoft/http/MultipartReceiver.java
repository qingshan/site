package com.zdsoft.http;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.integration.http.multipart.UploadedMultipartFile;
import org.springframework.util.LinkedMultiValueMap;
/**
 * 接收到请求
 * @author 杨华明
 *
 */
public class MultipartReceiver {
	
	private Logger logger=LoggerFactory.getLogger(MultipartReceiver.class);
	
	public void receive(LinkedMultiValueMap<String, Object> multipartRequest){
		logger.debug("### 成功接收  multipart request ###");
	    for (String elementName : multipartRequest.keySet()) {
	        if (elementName.equals("company")){
	        	logger.debug("{} - {}",elementName,((String[]) multipartRequest.getFirst("company"))[0]);
	        }
	        else if (elementName.equals("logo")){
	        	logger.debug("{} - UploadedMultipartFile: {}",elementName,((UploadedMultipartFile) multipartRequest.getFirst("logo")).getOriginalFilename());
	        }
	    }
	}
}
