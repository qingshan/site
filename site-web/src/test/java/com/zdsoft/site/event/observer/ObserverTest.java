package com.zdsoft.site.event.observer;

import org.junit.Test;

public class ObserverTest {

	@Test
	public void testUpdate() {
		ObserverA a = new ObserverA();
        ObserverB b = new ObserverB();
        a.addObserver(b);
        b.addObserver(a);
        
        b.setData(2);
	}

}
