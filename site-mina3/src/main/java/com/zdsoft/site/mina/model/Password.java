package com.zdsoft.site.mina.model;

import com.zdsoft.site.mina.model.customEnum.EncryMode;
import com.zdsoft.site.mina.model.customEnum.PasswordType;

/**
 * 密码（Password） 
 * @author cqyhm
 *
 */
public class Password {
//	密码类型  Type  <Type>  [0..1]  PasswordType  枚举
	private PasswordType type;
//	加密方式  EncryMode  <Enc>  [0..1]  EncryMode  枚举
	private EncryMode EncryMode;
//	密码  Password  <Pwd>  [0..1]  Max16Text
	private String Password;
	
	public PasswordType getType() {
		return type;
	}
	public void setType(PasswordType type) {
		this.type = type;
	}
	public EncryMode getEncryMode() {
		return EncryMode;
	}
	public void setEncryMode(EncryMode encryMode) {
		EncryMode = encryMode;
	}
	public String getPassword() {
		return Password;
	}
	public void setPassword(String password) {
		Password = password;
	}
}
