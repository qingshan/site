package com.zdsoft.site.mina.model;

import java.math.BigDecimal;

import com.zdsoft.site.mina.model.customEnum.AssureFlag;
import com.zdsoft.site.mina.model.customEnum.BidType;
import com.zdsoft.site.mina.model.customEnum.PayMethod;

/**
 * 标的信息（BidInfo） 
 * @author cqyhm
 *
 */
public class BidInfo {
//	标的编号  BidNo  <BidNo>  [1..1]  Max60Text  
	private String BidNo;
	
//	标的类型  BidType  <BidType>  [1..1]  BidType  枚举 
	private BidType BidType;
	
//	标的描述  BidDescribe  <BidDescri>  [0..1]  Max250Text  
	private String BidDescribe;
	
//	资金账号  ManageAccount  <MgeAcct>  [1..1]  Max20Text  
	private String ManageAccount;
	
//	标的总额  BidTotBala  < BidTotBala >  [1..1]  Amount  枚举 
	private BigDecimal BidTotBala;
	
//	预期年化收益  AnnualRate  <AnnuRate>  [1..1]  Amount   
	private BigDecimal AnnualRate;
	
//	产品期限  BidPeriod  <BidPeriod>  [0..1]  Max20Text  
	private String BidPeriod;
	
//	标的份数  TotalCount  < TotalCount >  [0..1]  Max20Text   
	private String TotalCount;
	
//	每份金额  PerBalance  < PerBalance>  [0..1]  Amount   
	private BigDecimal PerBalance;
	
//	担保标志  AssureFlag  <AssureFlag>  [0..1]  AssureFlag  枚举 
	private AssureFlag AssureFlag;
	
//	担保账号  AssureAccount  <AssureAcc>  [0..1]  Account   
	private Account AssureAccount;
	
//	担保公司编号  AssureComId  <AssureComId>  [0..1]  Max20Text   
	private String AssureComId;
//	担保公司名称  AssureComName  <AssComName>  [0..1]  Max60Text
	private String AssureComName;
	
//	申请日期  ApplyDate  < ApplyDate >  [0..1]  Date   
	private String ApplyDate;
//	建立日期  BuildDate  < BuildDate >  [0..1]  Date
	private String BuildDate;
//	募集起始日  RaiseBeginDate  <RaiseBegDate>  [1..1]  Date   
	private String RaiseBeginDate;
	
//	募集结束日  RaiseEndDate  <RaiseEndDate>  [1..1]  Date   
	private String RaiseEndDate;
	
//	约定开标日期  AgreeBuildDay  <AgreeBuildDate>  [0..1]  Date 
	private String AgreeBuildDay;
	
//	标的截止日期  BidBeginDate  < BidEndDate>  [0..1]  Date 
	private String BidBeginDate;
	
//	还款方式  PayMethod  <PayMeth>  [1..1]  PayMethod  枚举 
	private PayMethod PayMethod;
	
//	还款期限  RepayPeriod  <RepayPeri>  [0..1]  Max20Text  
	private String RepayPeriod;
	
//	应还资金总额  TotalPayBala  <TotPayTax>  [0..1]  Amount   
	private BigDecimal TotalPayBala;
//	应付利息总额  TotalTax  <TotTax>  [0..1]  Amount   
	private BigDecimal TotalTax;
//	月还金额  MonthlyBala  <MonthBala>  [0..1]  Amount   
	private BigDecimal MonthlyBala;
//	首次还款日期  FirstPayDay  <FirstPayDay>  [0..1]  Date   
	private String FirstPayDay;
//	提前还款费率  FeeRate  <FeeRate>  [0..1]  Amount  
	private BigDecimal FeeRate;
//	摘要  Digest  <Dgst>  [0..1]  Max128Text  
	private String Digest;
	public String getBidNo() {
		return BidNo;
	}
	public void setBidNo(String bidNo) {
		BidNo = bidNo;
	}
	public BidType getBidType() {
		return BidType;
	}
	public void setBidType(BidType bidType) {
		BidType = bidType;
	}
	public String getBidDescribe() {
		return BidDescribe;
	}
	public void setBidDescribe(String bidDescribe) {
		BidDescribe = bidDescribe;
	}
	public String getManageAccount() {
		return ManageAccount;
	}
	public void setManageAccount(String manageAccount) {
		ManageAccount = manageAccount;
	}
	public BigDecimal getBidTotBala() {
		return BidTotBala;
	}
	public void setBidTotBala(BigDecimal bidTotBala) {
		BidTotBala = bidTotBala;
	}
	public BigDecimal getAnnualRate() {
		return AnnualRate;
	}
	public void setAnnualRate(BigDecimal annualRate) {
		AnnualRate = annualRate;
	}
	public String getBidPeriod() {
		return BidPeriod;
	}
	public void setBidPeriod(String bidPeriod) {
		BidPeriod = bidPeriod;
	}
	public String getTotalCount() {
		return TotalCount;
	}
	public void setTotalCount(String totalCount) {
		TotalCount = totalCount;
	}
	public BigDecimal getPerBalance() {
		return PerBalance;
	}
	public void setPerBalance(BigDecimal perBalance) {
		PerBalance = perBalance;
	}
	public AssureFlag getAssureFlag() {
		return AssureFlag;
	}
	public void setAssureFlag(AssureFlag assureFlag) {
		AssureFlag = assureFlag;
	}
	public Account getAssureAccount() {
		return AssureAccount;
	}
	public void setAssureAccount(Account assureAccount) {
		AssureAccount = assureAccount;
	}
	public String getAssureComId() {
		return AssureComId;
	}
	public void setAssureComId(String assureComId) {
		AssureComId = assureComId;
	}
	public String getAssureComName() {
		return AssureComName;
	}
	public void setAssureComName(String assureComName) {
		AssureComName = assureComName;
	}
	public String getApplyDate() {
		return ApplyDate;
	}
	public void setApplyDate(String applyDate) {
		ApplyDate = applyDate;
	}
	public String getBuildDate() {
		return BuildDate;
	}
	public void setBuildDate(String buildDate) {
		BuildDate = buildDate;
	}
	public String getRaiseBeginDate() {
		return RaiseBeginDate;
	}
	public void setRaiseBeginDate(String raiseBeginDate) {
		RaiseBeginDate = raiseBeginDate;
	}
	public String getRaiseEndDate() {
		return RaiseEndDate;
	}
	public void setRaiseEndDate(String raiseEndDate) {
		RaiseEndDate = raiseEndDate;
	}
	public String getAgreeBuildDay() {
		return AgreeBuildDay;
	}
	public void setAgreeBuildDay(String agreeBuildDay) {
		AgreeBuildDay = agreeBuildDay;
	}
	public String getBidBeginDate() {
		return BidBeginDate;
	}
	public void setBidBeginDate(String bidBeginDate) {
		BidBeginDate = bidBeginDate;
	}
	public PayMethod getPayMethod() {
		return PayMethod;
	}
	public void setPayMethod(PayMethod payMethod) {
		PayMethod = payMethod;
	}
	public String getRepayPeriod() {
		return RepayPeriod;
	}
	public void setRepayPeriod(String repayPeriod) {
		RepayPeriod = repayPeriod;
	}
	public BigDecimal getTotalPayBala() {
		return TotalPayBala;
	}
	public void setTotalPayBala(BigDecimal totalPayBala) {
		TotalPayBala = totalPayBala;
	}
	public BigDecimal getTotalTax() {
		return TotalTax;
	}
	public void setTotalTax(BigDecimal totalTax) {
		TotalTax = totalTax;
	}
	public BigDecimal getMonthlyBala() {
		return MonthlyBala;
	}
	public void setMonthlyBala(BigDecimal monthlyBala) {
		MonthlyBala = monthlyBala;
	}
	public String getFirstPayDay() {
		return FirstPayDay;
	}
	public void setFirstPayDay(String firstPayDay) {
		FirstPayDay = firstPayDay;
	}
	public BigDecimal getFeeRate() {
		return FeeRate;
	}
	public void setFeeRate(BigDecimal feeRate) {
		FeeRate = feeRate;
	}
	public String getDigest() {
		return Digest;
	}
	public void setDigest(String digest) {
		Digest = digest;
	}
}
