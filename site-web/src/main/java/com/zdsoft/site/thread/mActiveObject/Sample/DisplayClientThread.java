package com.zdsoft.site.thread.mActiveObject.Sample;

import com.zdsoft.site.thread.mActiveObject.Sample.activeobject.ActiveObject;
import com.zdsoft.site.thread.mActiveObject.Sample.activeobject.Result;

public class DisplayClientThread extends Thread {
    private final ActiveObject activeObject;
    public DisplayClientThread(String name, ActiveObject activeObject) {
        super(name);
        this.activeObject = activeObject;
    }
    public void run() {
        try {
            for (int i = 0; true; i++) {
                // 没有传回值的呼叫
                String string = Thread.currentThread().getName() + " " + i;
                activeObject.displayString(string);
                Thread.sleep(200);
            }
        } catch (InterruptedException e) {
        }
    }
}
