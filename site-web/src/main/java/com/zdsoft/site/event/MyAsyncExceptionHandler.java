package com.zdsoft.site.event;

import java.lang.reflect.Method;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.aop.interceptor.AsyncUncaughtExceptionHandler;
import org.springframework.util.ErrorHandler;
/**
 * 事件调度的异常处理器
 * 1.AsyncUncaughtExceptionHandler 异步事件未捕获异常处理器
 * 2.ErrorHandler
 * @author admin
 *
 */
public class MyAsyncExceptionHandler implements AsyncUncaughtExceptionHandler,ErrorHandler {
	
	private Logger logger=LoggerFactory.getLogger(getClass());
	
	@Override
	public void handleUncaughtException(Throwable ex, Method method, Object... params) {
		logger.debug("message={},params={}",ex.getMessage(),params);
	}

	@Override
	public void handleError(Throwable t) {
		logger.debug("异常消息:{}",t.getMessage());
	}

}
