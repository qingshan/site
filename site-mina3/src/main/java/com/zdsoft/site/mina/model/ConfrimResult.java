package com.zdsoft.site.mina.model;

import com.zdsoft.site.mina.model.customEnum.ConfirmCode;

/**
 * 确认结果（confrimResult） 
 * @author cqyhm
 *
 */
public class ConfrimResult {
//	返回码  Code  <Code>  [1..1]  确认码  枚举
	private ConfirmCode code;
//	返回信息  Info  <Info>  [0..1]  Max128Text
	private String Info;
	
	public ConfirmCode getCode() {
		return code;
	}
	public void setCode(ConfirmCode code) {
		this.code = code;
	}
	public String getInfo() {
		return Info;
	}
	public void setInfo(String info) {
		Info = info;
	}
	
}
