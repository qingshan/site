package com.zdsoft.p2p.jms.service;

import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Session;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;
import org.springframework.stereotype.Service;

@Service("productService")
public class ProductServiceImpl {
	
	private Logger logger=LoggerFactory.getLogger(getClass());
	
	@Autowired
	private JmsTemplate jmsTemplate;
	
	@Autowired
	@Qualifier("queueDestination1")
	private Destination destination1;

	@Autowired
	@Qualifier("queueDestination2")
	private Destination destination2;
	
	public void sendMessage1(final String message){
		logger.debug("生产者发送消息");
		logger.debug("生产者发了一个消息");
		
		jmsTemplate.send(destination1, new MessageCreator() {
			@Override
			public Message createMessage(Session session) throws JMSException {
				return session.createTextMessage(message);
			}
		});
	}
	
	public void sendMessage2(final String message){
		logger.debug("生产者发送消息");
		logger.debug("生产者发了一个消息");
		
		jmsTemplate.send(destination2, new MessageCreator() {
			@Override
			public Message createMessage(Session session) throws JMSException {
				return session.createTextMessage(message);
			}
		});
	}
}
