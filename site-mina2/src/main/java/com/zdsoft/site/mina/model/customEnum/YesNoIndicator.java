package com.zdsoft.site.mina.model.customEnum;

import javax.xml.bind.annotation.XmlEnumValue;

/**
 * 是否标志(YesNoIndicator) 
 * @author cqyhm
 *
 */
public enum YesNoIndicator {
	/**Y=是*/
	@XmlEnumValue("Y")
	Y("Y","是"),
	/**N=否*/
	@XmlEnumValue("N")
	N("N","否");
	
	private String value;
	private String name;
	
	private YesNoIndicator(String value,String name){
		this.value=value;
		this.name=name;
	}
	
	public String getValue() {
		return value;
	}
	public String getName() {
		return name;
	}
	@Override
	public String toString(){
		return value;
	}
}
