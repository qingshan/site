package com.zdsoft.site.mina.model.customEnum;

import javax.xml.bind.annotation.XmlEnumValue;

/**
 * 汇钞标志(CashExCode)
 * 
 * @author cqyhm
 *
 */
public enum CashExCode {
	/** 汇钞标志 "1", "汇" */
	@XmlEnumValue("1") A1("1", "汇"),

	/** 汇钞标志 "2", "钞" */
	@XmlEnumValue("2") A2("2", "钞");

	private String value;
	private String name;

	private CashExCode(String value, String name) {
		this.value = value;
		this.name = name;
	}

	public String getValue() {
		return value;
	}

	public String getName() {
		return name;
	}

	@Override
	public String toString() {
		return value;
	}
}
