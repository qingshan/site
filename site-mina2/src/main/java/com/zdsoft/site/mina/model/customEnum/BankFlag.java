package com.zdsoft.site.mina.model.customEnum;

import javax.xml.bind.annotation.XmlEnumValue;

/**
 * 银行标志，默认为本行 
 * @author cqyhm
 *
 */
public enum BankFlag {
	
	/**"0","本行"*/
	@XmlEnumValue("0")
	A0("0","本行"),
	
	/**"1","他行"*/
	@XmlEnumValue("1")
	A1("1","他行");   

	private String value;
	private String name;
	
	private BankFlag(String value,String name){
		this.value=value;
		this.name=name;
	}
	
	public String getValue() {
		return value;
	}
	public String getName() {
		return name;
	}
	@Override
	public String toString(){
		return value;
	}
}
