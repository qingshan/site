package com.zdsoft.site.thread.mActiveObject.Sample;

import com.zdsoft.site.thread.mActiveObject.Sample.activeobject.ActiveObject;
import com.zdsoft.site.thread.mActiveObject.Sample.activeobject.ActiveObjectFactory;

public class Main {
    public static void main(String[] args) {
        ActiveObject activeObject = ActiveObjectFactory.createActiveObject();
        new MakerClientThread("Alice", activeObject).start();
        new MakerClientThread("Bobby", activeObject).start();
        new DisplayClientThread("Chris", activeObject).start();
    }
}
