<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>用户编辑</title>
</head>
<body>
	<spring:hasBindErrors name="user">
		<c:forEach items="${errors.fieldErrors}" var="error"> 
			<p>${error.defaultMessage}</p>
		</c:forEach>
	</spring:hasBindErrors>
	<p>编辑用户</p>
	<form action="${pageContext.request.contextPath}/user/save.do" method="post">
		<input type="hidden" name="userId" value="${user.userId}" />
		<div>账号<input type="text" name="userCd" value="${user.userCd}" /></div>
		<div>名称<input type="text" name="userNm" value="${user.userNm}"/></div>
		<div><input type="submit" value="保存" /></div>
	</form>
</body>
</html>