package com.zdsoft.site.mina.model.customEnum;

import javax.xml.bind.annotation.XmlEnumValue;

/**
 * 服务商状态（ServiceStatus）
 * 
 * @author cqyhm
 *
 */
public enum ServiceStatus {
	/** 服务商状态 "0","正常" */
	@XmlEnumValue("0") A0("0", "正常"),

	/** 服务商状态 "1","注销" */
	@XmlEnumValue("1") A1("1", "注销"),

	/** 服务商状态 "2","签退" */
	@XmlEnumValue("2") A2("2", "签退"),

	/** 服务商状态 "3","清算" */
	@XmlEnumValue("3") A3("3", "清算");

	private String value;
	private String name;

	private ServiceStatus(String value, String name) {
		this.value = value;
		this.name = name;
	}

	public String getValue() {
		return value;
	}

	public String getName() {
		return name;
	}

	@Override
	public String toString() {
		return value;
	}
}
