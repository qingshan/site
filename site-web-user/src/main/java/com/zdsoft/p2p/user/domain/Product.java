package com.zdsoft.p2p.user.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.GenericGenerator;

import com.zdsoft.rock.common.model.domain.StaticEntity;

@Entity
public class Product extends StaticEntity{

	private static final long serialVersionUID = 6401444175227515832L;

	@Id
	@GenericGenerator(name = "systemUUID", strategy = "uuid")
	@GeneratedValue(generator = "systemUUID")
	@Column(length = 32)
	private String id;
	
	@Column
	private String code;
	
	@Column
	private String name;
	
	@ManyToOne
	@JoinColumn(name="userId")
	private User user;
	
	@Override
	public String getId() {
		return null;
	}

	@Override
	public void setId(String id) {
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
	
}
