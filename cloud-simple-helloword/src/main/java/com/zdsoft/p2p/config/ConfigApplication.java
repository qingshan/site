package com.zdsoft.p2p.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@ComponentScan(basePackages={"com.zdsoft.p2p"})
@SpringBootApplication
public class ConfigApplication implements CommandLineRunner{
	
	private Logger logger=LoggerFactory.getLogger(getClass());
	
	public static void main(String[] args) {
		SpringApplication.run(ConfigApplication.class);
	}

	@Override
	public void run(String... args) throws Exception {
		logger.info("启动之后执行");
	}
}
