package com.jfinal.cms.service;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static org.junit.Assert.*;
import com.jfinal.aop.Duang;
import com.jfinal.cms.config.AppConfig;
import com.jfinal.cms.model.User;
import com.jfinal.ext.test.ControllerTestCase;

public class UserServiceTest extends ControllerTestCase<AppConfig>{
	private Logger logger=LoggerFactory.getLogger(getClass());

	@Test
	public void testFindById() {
		UserService service=Duang.duang(UserService.class);
		User u1=service.findById("1");
		assertNotNull(u1);
		logger.debug("{}",u1);
		
		User u2=service.findById("3");
		assertNull(u2);
	}

}
