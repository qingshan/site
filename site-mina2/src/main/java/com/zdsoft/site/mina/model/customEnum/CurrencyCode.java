package com.zdsoft.site.mina.model.customEnum;

import javax.xml.bind.annotation.XmlEnumValue;

/**
 * 货币代码（CurrencyCode）
 * 
 * @author cqyhm
 *
 */
public enum CurrencyCode {
	/** 货币代码 "0", "人民币" */
	@XmlEnumValue("0") A0("0", "人民币"),

	/** 货币代码 "1", "美元" */
	@XmlEnumValue("1") A1("1", "美元"),

	/** 货币代码 "2", "港元" */
	@XmlEnumValue("2") A2("2", "港元");
	private String value;
	private String name;

	private CurrencyCode(String value, String name) {
		this.value = value;
		this.name = name;
	}

	public String getValue() {
		return value;
	}

	public String getName() {
		return name;
	}

	@Override
	public String toString() {
		return value;
	}
}
