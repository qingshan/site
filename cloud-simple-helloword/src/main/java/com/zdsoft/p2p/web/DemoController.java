package com.zdsoft.p2p.web;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.zdsoft.p2p.domain.User;

@RestController
public class DemoController extends BaseController{
	
	@RequestMapping("/")
	public String home(){
		logger.info("访问主页");
		return "helloWord";
	}
	
	@RequestMapping("/user.do")
	public User findUser(){
		return new User("cqyhm", "杨华明");
	}
}
