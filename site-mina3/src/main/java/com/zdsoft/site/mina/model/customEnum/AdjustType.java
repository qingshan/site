package com.zdsoft.site.mina.model.customEnum;
/**
 * 调整类型（AdjustType） 
 * @author cqyhm
 *
 */
public enum AdjustType {
	A0,//  红冲 
	A1//  蓝补 
}
