package org.beetl.sql.spring.dao;

import org.beetl.sql.spring.domain.User;

public interface UserDao {
	public void save(User user);
	public int total(User user) ;
}
