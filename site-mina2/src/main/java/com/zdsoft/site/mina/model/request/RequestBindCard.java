package com.zdsoft.site.mina.model.request;

import javax.xml.bind.annotation.XmlRootElement;

import com.zdsoft.site.mina.model.Account;
import com.zdsoft.site.mina.model.Agent;
import com.zdsoft.site.mina.model.Customer;
import com.zdsoft.site.mina.model.MessageHeader;
import com.zdsoft.site.mina.model.TransMangAcc;
import com.zdsoft.site.mina.model.customEnum.BusinessType;
import com.zdsoft.site.mina.model.customEnum.CurrencyCode;

/**
 * 6.2.2 客户银行账号绑定 (48702)（建立绑定关系、发送短信验证） 说明：
 * 1.P2P平台发起银行卡绑定关系建立发送验证短信请求到托管系统，托管系统接收到交易请求后，返回短信发送的状态。
 * 2.绑定的是借记卡，请求报文包括：绑定流水号、银行ID、账户名称、账户号码、开户证件类型、证件号码、手机号 3.只允许借记卡绑定
 * 
 * @author cqyhm
 *
 */

@XmlRootElement(name = "MsgText")
public class RequestBindCard extends AbstractRequest {
	
	public RequestBindCard(){}
	
	public RequestBindCard(MessageHeader msgHdr){
		setMsgHdr(msgHdr);
	}
	
	/** 客户信息 Customer [Cust] [1..1] Customer 组件 */
	private Customer Cust;

	/** 经办人信息 Agent [Agt] [0..1] Agent 组件 */
	private Agent Agt;

	/** 银行账户 BankAccount [BkAcct] [1..1] Account 组件 */
	private Account BkAcct;

	/** 交易管理账户 ManageAccount [MgeAcct] [1..1] TransMangAcc 组件 */
	private TransMangAcc MgeAcct;

	/** 币种 Currency [Ccy] [1..1] CurrencyCode 枚举 */
	private CurrencyCode Ccy;

	/** 业务类别 BusinessType [BusType] [1..1] BusinessType 枚举 */
	private BusinessType BusType;

	/** 摘要 Digest [Dgst] [0..1] Max128Text */
	private String Dgst;

	/** 客户信息 Customer [Cust] [1..1] Customer 组件 */
	public Customer getCust() {
		return Cust;
	}

	/** 客户信息 Customer [Cust] [1..1] Customer 组件 */
	public void setCust(Customer cust) {
		Cust = cust;
	}

	/** 经办人信息 Agent [Agt] [0..1] Agent 组件 */
	public Agent getAgt() {
		return Agt;
	}

	/** 经办人信息 Agent [Agt] [0..1] Agent 组件 */
	public void setAgt(Agent agt) {
		Agt = agt;
	}

	/** 银行账户 BankAccount [BkAcct] [1..1] Account 组件 */
	public Account getBkAcct() {
		return BkAcct;
	}

	/** 银行账户 BankAccount [BkAcct] [1..1] Account 组件 */
	public void setBkAcct(Account bkAcct) {
		BkAcct = bkAcct;
	}

	/** 交易管理账户 ManageAccount [MgeAcct] [1..1] TransMangAcc 组件 */
	public TransMangAcc getMgeAcct() {
		return MgeAcct;
	}

	/** 交易管理账户 ManageAccount [MgeAcct] [1..1] TransMangAcc 组件 */
	public void setMgeAcct(TransMangAcc mgeAcct) {
		MgeAcct = mgeAcct;
	}

	/** 币种 Currency [Ccy] [1..1] CurrencyCode 枚举 */
	public CurrencyCode getCcy() {
		return Ccy;
	}

	/** 币种 Currency [Ccy] [1..1] CurrencyCode 枚举 */
	public void setCcy(CurrencyCode ccy) {
		Ccy = ccy;
	}

	/** 业务类别 BusinessType [BusType] [1..1] BusinessType 枚举 */
	public BusinessType getBusType() {
		return BusType;
	}

	/** 业务类别 BusinessType [BusType] [1..1] BusinessType 枚举 */
	public void setBusType(BusinessType busType) {
		BusType = busType;
	}

	/** 摘要 Digest [Dgst] [0..1] Max128Text */
	public String getDgst() {
		return Dgst;
	}

	/** 摘要 Digest [Dgst] [0..1] Max128Text */
	public void setDgst(String dgst) {
		Dgst = dgst;
	}
}
