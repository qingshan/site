package com.zdsoft.site.event;

import org.springframework.context.ApplicationEvent;

import com.zdsoft.site.domain.User;

public class LogoutSuccessEvent extends ApplicationEvent{

	private static final long serialVersionUID = -6659662690831675780L;
	
	public LogoutSuccessEvent(User user) {
		super(user);
	}
}
