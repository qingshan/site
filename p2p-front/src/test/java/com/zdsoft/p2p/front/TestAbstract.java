package com.zdsoft.p2p.front;

import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * 带启动spring容器的测试类的公共基类主要用于以下用途
 * 1.测试类需要使用spring容器注入对象时必须加载spring容器
 * 2.在测试类中需要使用日志工具输出提示信息
 * @author yhm
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({"classpath*:applicationContext.xml"})
//@ActiveProfiles("production")//载入开发环境的数据库developer/production
public abstract class TestAbstract extends TestCommon {
}
