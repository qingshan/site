package com.zdsoft.site.mina.model;

import com.zdsoft.site.mina.model.customEnum.CertificationType;

/**
 * 代理人信息（Agent） 
 * @author cqyhm
 *
 */
public class Agent {
//	代理人姓名  Name  <Name>  [0..1]  Max60Text   可选 
	private String Name;
	
//	证件类型  CertificationType  <CertType>  [0..1]  CertificationType  枚举  可选 
	private CertificationType CertificationType;
//	证件号码  CertificationIdentifier  <CertId>  [0..1]  Max30Text   可选 
	
	private String CertificationIdentifier;

	public String getName() {
		return Name;
	}

	public void setName(String name) {
		Name = name;
	}

	public CertificationType getCertificationType() {
		return CertificationType;
	}

	public void setCertificationType(CertificationType certificationType) {
		CertificationType = certificationType;
	}

	public String getCertificationIdentifier() {
		return CertificationIdentifier;
	}

	public void setCertificationIdentifier(String certificationIdentifier) {
		CertificationIdentifier = certificationIdentifier;
	}
	
}
