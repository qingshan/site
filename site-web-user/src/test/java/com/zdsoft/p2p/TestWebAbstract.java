package com.zdsoft.p2p;

import static org.junit.Assert.assertNotNull;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.ContextHierarchy;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.context.WebApplicationContext;

/**
 * 带启动spring容器的测试类的公共基类主要用于以下用途 1.测试类需要使用spring容器注入对象时必须加载spring容器
 * 2.在测试类中需要使用日志工具输出提示信息
 * 
 * @author yhm
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextHierarchy({  
    @ContextConfiguration(name = "parent", locations ={"classpath*:applicationContext.xml"}),  
    @ContextConfiguration(name = "child", locations = {"classpath*:spring-mvc.xml"})  
})
@WebAppConfiguration
@Rollback(false)
public abstract class TestWebAbstract extends TestCommon {
	
	//Web上下文环境
	@Autowired
	private WebApplicationContext wac;
	//mvc模拟对象
	protected MockMvc mockMvc;

	@Before
	public void beforeClass() throws Exception {
		assertNotNull(wac);
		mockMvc = webAppContextSetup(wac).build();
		logger.debug("测试父类");
	}
}
