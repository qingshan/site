package com.zdsoft.site.mina.model.request;

import java.math.BigDecimal;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.zdsoft.site.mina.model.Account;
import com.zdsoft.site.mina.model.MessageHeader;
import com.zdsoft.site.mina.model.customEnum.AccountType;
import com.zdsoft.site.mina.model.customEnum.BusinessType;
import com.zdsoft.site.mina.model.customEnum.CurrencyCode;
import com.zdsoft.site.mina.model.customEnum.PackageFlag;
import com.zdsoft.site.mina.model.customEnum.Route;
import com.zdsoft.site.mina.model.customEnum.RouteFlag;

/**
 * 6.1.9服务商收益划出(44705)
 * 1.用于服务商在监管账户的收益划出，收款账号需要提前在银行登记维护，收益划出只能划出到预先登记的账户内。
 * 请求：
 * 
 * @author cqyhm
 *
 */
@XmlRootElement(name = "MsgText")
public class RequestIncomedrawPayment extends AbstractRequest {
	
	public RequestIncomedrawPayment(){}
	
	public RequestIncomedrawPayment(MessageHeader msgHdr){
		setMsgHdr(msgHdr);
	}
	
	/**02.报文标志	PackageFlag	[PkgFlag]	[0..1]	PackageFlag	枚举*/
	@XmlElement(name="PkgFlag")
	private PackageFlag PkgFlag;
	/**03. 银行账户 BankAccount [BkAcct] [1..1] Account 组件 */
	@XmlElement(name = "BkAcct")
	private Account BkAcct;
	
	/**04. 服务商汇总账户 ServiceAccount [SrvAcct] [0..1] Account 组件 */
	@XmlElement(name = "SrvAcct")
	private Account SrvAcct;
	
	/**05.币种 Currency [Ccy] [1..1] CurrencyCode 枚举 */
	@XmlElement(name = "Ccy")
	private CurrencyCode Ccy;
	
	/**06. 业务类别 BusinessType [BusType] [0..1] BusinessType 枚举 */
	@XmlElement(name = "BusType")
	private BusinessType BusType;
	
	/**07. 转帐金额 TransferAmount [TrfAmt] [1..1] Amount */
	@XmlElement(name = "TrfAmt")
	private BigDecimal TrfAmt;
	
	/**03.账户类别	AccountType	<Type>	[1..1]	AccountType	枚举 */
	@XmlElement(name="Type")
	private AccountType Type;
	
	/**08.优先码	PriorityNo	<PriorityNo>	[0..1]	Max16Text*/
	@XmlElement(name="PriorityNo")
	private String PriorityNo;
	
	/**09.汇路	Route	<Route>	[0..1]	0本行  1跨行同城  2跨行二代支付  3 跨行大小额	枚举 */
	@XmlElement(name = "Route")
	private Route Route;
	
	/**10.汇款标识	RouteFlag	<RouteFlag>	[0..1]	0普通 1实时 2加急	枚举*/
	@XmlElement(name = "RouteFlag")
	private RouteFlag RouteFlag;
	
	/**11.短信验证码	MessageBox	[MsgBox]	[1..1]	Max20Text	*/
	@XmlElement(name="MsgBox")
	private String MsgBox;
	
	/**12. 摘要 Digest [Dgst] [0..1] Max128Text */
	@XmlElement(name = "Dgst")
	private String Dgst;


	/** 银行账户 BankAccount [BkAcct] [1..1] Account 组件 */
	public Account getBkAcct() {
		return BkAcct;
	}

	/** 银行账户 BankAccount [BkAcct] [1..1] Account 组件 */
	public void setBkAcct(Account bkAcct) {
		BkAcct = bkAcct;
	}
	/** 服务商汇总账户 ServiceAccount [SrvAcct] [0..1] Account 组件 */
	public Account getSrvAcct() {
		return SrvAcct;
	}

	/** 服务商汇总账户 ServiceAccount [SrvAcct] [0..1] Account 组件 */
	public void setSrvAcct(Account srvAcct) {
		SrvAcct = srvAcct;
	}

	/** 币种 Currency [Ccy] [1..1] CurrencyCode 枚举 */
	public CurrencyCode getCcy() {
		return Ccy;
	}

	/** 币种 Currency [Ccy] [1..1] CurrencyCode 枚举 */
	public void setCcy(CurrencyCode ccy) {
		Ccy = ccy;
	}

	/** 业务类别 BusinessType [BusType] [0..1] BusinessType 枚举 */
	public BusinessType getBusType() {
		return BusType;
	}

	/** 业务类别 BusinessType [BusType] [0..1] BusinessType 枚举 */
	public void setBusType(BusinessType busType) {
		BusType = busType;
	}

	/** 转帐金额 TransferAmount [TrfAmt] [1..1] Amount */
	public BigDecimal getTrfAmt() {
		return TrfAmt;
	}

	/** 转帐金额 TransferAmount [TrfAmt] [1..1] Amount */
	public void setTrfAmt(BigDecimal trfAmt) {
		TrfAmt = trfAmt;
	}

	/** 摘要 Digest [Dgst] [0..1] Max128Text */
	public String getDgst() {
		return Dgst;
	}

	/** 摘要 Digest [Dgst] [0..1] Max128Text */
	public void setDgst(String dgst) {
		Dgst = dgst;
	}
	/**报文标志	PackageFlag	[PkgFlag]	[0..1]	PackageFlag	枚举*/
	public PackageFlag getPkgFlag() {
		return PkgFlag;
	}
	/**报文标志	PackageFlag	[PkgFlag]	[0..1]	PackageFlag	枚举*/
	public void setPkgFlag(PackageFlag pkgFlag) {
		PkgFlag = pkgFlag;
	}
	/**短信验证码	MessageBox	[MsgBox]	[1..1]	Max20Text	*/
	public String getMsgBox() {
		return MsgBox;
	}
	/**短信验证码	MessageBox	[MsgBox]	[1..1]	Max20Text	*/
	public void setMsgBox(String msgBox) {
		MsgBox = msgBox;
	}
	
	/**11.汇路	Route	<Route>	[0..1]	0本行  1跨行同城  2跨行二代支付  3 跨行大小额	枚举 */
	public Route getRoute() {
		return Route;
	}
	/**11.汇路	Route	<Route>	[0..1]	0本行  1跨行同城  2跨行二代支付  3 跨行大小额	枚举 */
	public void setRoute(Route route) {
		Route = route;
	}
	/**12.汇款标识	RouteFlag	<RouteFlag>	[0..1]	0普通 1实时 2加急	枚举*/
	public RouteFlag getRouteFlag() {
		return RouteFlag;
	}
	/**12.汇款标识	RouteFlag	<RouteFlag>	[0..1]	0普通 1实时 2加急	枚举*/
	public void setRouteFlag(RouteFlag routeFlag) {
		RouteFlag = routeFlag;
	}
	/**03.账户类别	AccountType	<Type>	[1..1]	AccountType	枚举 */
	public AccountType getType() {
		return Type;
	}
	/**03.账户类别	AccountType	<Type>	[1..1]	AccountType	枚举 */
	public void setType(AccountType type) {
		Type = type;
	}
	/**08.优先码	PriorityNo	<PriorityNo>	[0..1]	Max16Text*/
	public String getPriorityNo() {
		return PriorityNo;
	}
	/**08.优先码	PriorityNo	<PriorityNo>	[0..1]	Max16Text*/
	public void setPriorityNo(String priorityNo) {
		PriorityNo = priorityNo;
	}
}
