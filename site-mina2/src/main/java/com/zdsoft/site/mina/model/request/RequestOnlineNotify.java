package com.zdsoft.site.mina.model.request;

import java.math.BigDecimal;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.zdsoft.site.mina.model.MessageHeader;
import com.zdsoft.site.mina.model.Reference;
import com.zdsoft.site.mina.model.customEnum.BusinessType;
import com.zdsoft.site.mina.model.customEnum.CurrencyCode;
import com.zdsoft.site.mina.model.customEnum.DealStatus;

/**
  *6.2.1.网银支付结果通知（43710）
  *	说明：
  *	1，存管系统接收到支付公司的网银充值结果通知后，调用此接口通知服务商
  * 托管平台请求
  *  
 * @author cqyhm
 *
 */
@XmlRootElement(name = "MsgText")
public class RequestOnlineNotify extends AbstractRequest {

	
	public RequestOnlineNotify(){}
	
	public RequestOnlineNotify(MessageHeader msgHdr){
		setMsgHdr(msgHdr);
	}
	
	/** 02 币种 Currency <Ccy> [1..1] CurrencyCode 枚举 */
	@XmlElement(name = "Ccy")
	private CurrencyCode Ccy;

	/** 03 业务类别 BusinessType <BusType> [0..1] BusinessType 枚举 */
	@XmlElement(name = "BusType")
	private BusinessType BusType;

	/** 04 原交易日 OriginalTransDate <OriTransDate> [1..1] Date */
	@XmlElement(name = "OriTransDate")
	private String OriTransDate;

	/** 05 原服务商流水 OriginalSecuReference <OriSecuRef> [1..1] Reference 组件 */
	@XmlElement(name = "OriSecuRef")
	private Reference OriSecuRef;

	/** 06 原主机流水 OriginalBankReference <OriBkRef> [1..1] Reference 组件 */
	@XmlElement(name = "OriBkRef")
	private Reference OriBkRef;

	/** 07 支付完成日期 PayDate < PayDate > [1..1 Date */
	@XmlElement(name = "PayDate")
	private String PayDate;

	/** 08 支付完成时间 PayTime < PayTime > [1..1 Time */
	@XmlElement(name = "PayTime")
	private String PayTime;

	/** 09 支付结果 TransResult <TransRst> [1..1] DealStatus 枚举 */
	@XmlElement(name = "TransRst")
	private DealStatus TransRst;

	/** 10 失败原因 TransResultDescription <TransRstDes> [0..1 Max128Text */
	@XmlElement(name = "TransRstDes")
	private String TransRstDes;

	/** 11 转帐金额 TransferAmount <TrfAmt> [1..1] Amount */
	@XmlElement(name = "TrfAmt")
	private BigDecimal TrfAmt;

	/** 12 成功转帐金额 SuccessTransferAmount <SucTrfAmt> [1..1] Amount */
	@XmlElement(name = "SucTrfAmt")
	private BigDecimal SucTrfAmt;

	/** 13 摘要 Digest <Dgst> [0..1] Max128Text */
	@XmlElement(name = "Dgst")
	private String Dgst;

	/** 14 保留字段 Reserve < Reserve > [0..1] Max128Text */
	@XmlElement(name = "Reserve")
	private String Reserve;

	/** 15 保留字段1 Reserve1 < Reserve1 > [0..1] Max128Text */
	@XmlElement(name = "Reserve1")
	private String Reserve1;

	/** 02 币种 Currency <Ccy> [1..1] CurrencyCode 枚举 */
	public CurrencyCode getCcy() {
		return Ccy;
	}

	/** 02 币种 Currency <Ccy> [1..1] CurrencyCode 枚举 */
	public void setCcy(CurrencyCode ccy) {
		Ccy = ccy;
	}

	/** 03 业务类别 BusinessType <BusType> [0..1] BusinessType 枚举 */
	public BusinessType getBusType() {
		return BusType;
	}

	/** 03 业务类别 BusinessType <BusType> [0..1] BusinessType 枚举 */
	public void setBusType(BusinessType busType) {
		BusType = busType;
	}

	/** 04 原交易日 OriginalTransDate <OriTransDate> [1..1] Date */
	public String getOriTransDate() {
		return OriTransDate;
	}

	/** 04 原交易日 OriginalTransDate <OriTransDate> [1..1] Date */
	public void setOriTransDate(String oriTransDate) {
		OriTransDate = oriTransDate;
	}

	/** 05 原服务商流水 OriginalSecuReference <OriSecuRef> [1..1] Reference 组件 */
	public Reference getOriSecuRef() {
		return OriSecuRef;
	}

	/** 05 原服务商流水 OriginalSecuReference <OriSecuRef> [1..1] Reference 组件 */
	public void setOriSecuRef(Reference oriSecuRef) {
		OriSecuRef = oriSecuRef;
	}

	/** 06 原主机流水 OriginalBankReference <OriBkRef> [1..1] Reference 组件 */
	public Reference getOriBkRef() {
		return OriBkRef;
	}

	/** 06 原主机流水 OriginalBankReference <OriBkRef> [1..1] Reference 组件 */
	public void setOriBkRef(Reference oriBkRef) {
		OriBkRef = oriBkRef;
	}

	/** 07 支付完成日期 PayDate < PayDate > [1..1 Date */
	public String getPayDate() {
		return PayDate;
	}

	/** 07 支付完成日期 PayDate < PayDate > [1..1 Date */
	public void setPayDate(String payDate) {
		PayDate = payDate;
	}

	/** 08 支付完成时间 PayTime < PayTime > [1..1 Time */
	public String getPayTime() {
		return PayTime;
	}

	/** 08 支付完成时间 PayTime < PayTime > [1..1 Time */
	public void setPayTime(String payTime) {
		PayTime = payTime;
	}

	/** 09 支付结果 TransResult <TransRst> [1..1] DealStatus 枚举 */
	public DealStatus getTransRst() {
		return TransRst;
	}

	/** 09 支付结果 TransResult <TransRst> [1..1] DealStatus 枚举 */
	public void setTransRst(DealStatus transRst) {
		TransRst = transRst;
	}

	/** 10 失败原因 TransResultDescription <TransRstDes> [0..1 Max128Text */
	public String getTransRstDes() {
		return TransRstDes;
	}

	/** 10 失败原因 TransResultDescription <TransRstDes> [0..1 Max128Text */
	public void setTransRstDes(String transRstDes) {
		TransRstDes = transRstDes;
	}

	/** 11 转帐金额 TransferAmount <TrfAmt> [1..1] Amount */
	public BigDecimal getTrfAmt() {
		return TrfAmt;
	}

	/** 11 转帐金额 TransferAmount <TrfAmt> [1..1] Amount */
	public void setTrfAmt(BigDecimal trfAmt) {
		TrfAmt = trfAmt;
	}

	/** 12 成功转帐金额 SuccessTransferAmount <SucTrfAmt> [1..1] Amount */
	public BigDecimal getSucTrfAmt() {
		return SucTrfAmt;
	}

	/** 12 成功转帐金额 SuccessTransferAmount <SucTrfAmt> [1..1] Amount */
	public void setSucTrfAmt(BigDecimal sucTrfAmt) {
		SucTrfAmt = sucTrfAmt;
	}

	/** 13 摘要 Digest <Dgst> [0..1] Max128Text */
	public String getDgst() {
		return Dgst;
	}

	/** 13 摘要 Digest <Dgst> [0..1] Max128Text */
	public void setDgst(String dgst) {
		Dgst = dgst;
	}

	/** 14 保留字段 Reserve < Reserve > [0..1] Max128Text */
	public String getReserve() {
		return Reserve;
	}

	/** 14 保留字段 Reserve < Reserve > [0..1] Max128Text */
	public void setReserve(String reserve) {
		Reserve = reserve;
	}

	/** 15 保留字段1 Reserve1 < Reserve1 > [0..1] Max128Text */
	public String getReserve1() {
		return Reserve1;
	}

	/** 15 保留字段1 Reserve1 < Reserve1 > [0..1] Max128Text */
	public void setReserve1(String reserve1) {
		Reserve1 = reserve1;
	}
	
}
