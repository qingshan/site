package cloud.simple.hello;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@SpringBootApplication
public class SampleController  {
	
	volatile static int n=0;
	volatile static int m=0;
	private Logger logger=LoggerFactory.getLogger(getClass());
	
	Executor executor = Executors.newFixedThreadPool(10); 

    @ResponseBody
    @RequestMapping(value = "/hello/{nPath}")
    public String home(@PathVariable(value = "nPath") int nPath ) {   
    	
    	final int sPath=nPath;
    	
    	Runnable task = new Runnable() {  
    	    @Override  
    	    public void run() {  
    	    	logger.debug("hello当前线程：{}",Thread.currentThread().getName());
    	    	logger.debug("hello请求数量：{}",(sPath));
    	    	try {
    				Thread.sleep(500);
    			} catch (InterruptedException e) {
    				e.printStackTrace();
    			}
    	    	logger.debug("hello处理完成：{}",(sPath));
    	    }  
    	};  
    	executor.execute(task); 
    	
    	
        return "Hello World!"+n;
    }
    
    @ResponseBody
    @RequestMapping(value = "/user")
    public String user() {    
    	
    	logger.debug("user当前线程：{}",Thread.currentThread().getName());
    	logger.debug("user请求数量：{}",(++m));
    	
    	logger.debug("user处理完成：{}",(m));
        return "Hello User!"+m;
    }

    public static void main(String[] args) throws Exception {
        SpringApplication.run(SampleController.class, args);
    }

}
