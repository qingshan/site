package com.zdsoft.site.mina.p2p;

import org.apache.mina.core.session.IoSession;

import com.zdsoft.site.mina.model.request.AbstractRequest;
import com.zdsoft.site.mina.model.response.ResponseBindCard;
/**
 * 6.2.3    客户银行账号绑定 (46702)（建立绑定关系、验证并绑定）
 * 	说明：
 * 	P2P平台发起银行卡绑定关系建立短信验证并绑定请求到托管系统，请求报文包括：绑定流水号和短信验证码。
 * 	托管系统受到请求后，将验证信息发到支付平台，由支付平台校验短信验证码是否超时、是否正确，发送银行绑定，
 * 	最终返回商户短信验证状态及绑定交易状态。
 *
 */
public class BindCardHandler extends P2PIoHandler {
	
	public BindCardHandler(AbstractRequest request) {
		super(request);
	}
	
	@Override
	public void messageSent(IoSession session, Object message) throws Exception {
		//发送的消息已经通过编码器编码为字符串
		logger.debug("1.3.发送数据到服务器：{}",message);
	}
	/**接收到的消息体为xml字符串*/
	@Override
	public void messageReceived(IoSession session, Object message) throws Exception {
		String text=message.toString();
		setResponse(new ResponseBindCard(text));
		logger.debug("2.4.接收服务端的数据:[{}]",text);
		ResponseBindCard response=(ResponseBindCard)getResponse();
		logger.debug("{}",response);
		logger.debug("{}",response.getRst());
	}
	@Override
	public String getInstrCd() {
		return "46702";
	}
}
