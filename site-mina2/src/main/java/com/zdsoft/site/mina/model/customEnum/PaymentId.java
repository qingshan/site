package com.zdsoft.site.mina.model.customEnum;

import javax.xml.bind.annotation.XmlEnumValue;

/**
 * 支付公司编号（PaymentId）
 * 
 * @author cqyhm
 *
 */
public enum PaymentId {
	/** 支付公司编号 "80080000","中金支付有限公司" */
	@XmlEnumValue("80080000") A80080000("80080000", "中金支付有限公司"),

	/** 支付公司编号 "80250000","连连银通电子支付有限公司" */
	@XmlEnumValue("80250000") A80250000("80250000", "连连银通电子支付有限公司"),

	/** 支付公司编号 "80360000","通联支付网络服务股份有限公司" */
	@XmlEnumValue("80360000") A80360000("80360000", "通联支付网络服务股份有限公司"),

	/** 支付公司编号 "80830000","银联商务有限公司" */
	@XmlEnumValue("80830000") A80830000("80830000", "银联商务有限公司");

	private String value;
	private String name;

	private PaymentId(String value, String name) {
		this.value = value;
		this.name = name;
	}

	@Override
	public String toString() {
		return value;
	}

	public String getValue() {
		return value;
	}

	public String getName() {
		return name;
	}
}
