package com.zdsoft.site.mina.model;

import com.zdsoft.site.mina.model.customEnum.AccountStatus;
import com.zdsoft.site.mina.model.customEnum.AccountType;

/**
 * 7.10  交易管理账号账户（TransMangAcc ） 
 * @author cqyhm
 *
 */
public class TransMangAcc {
//	账号  AccountIdentification  <Id>  [1..1]  Max32Text   
	private String AccountIdentification;
//	户主名称  AccountName  <Name>  [0..1]  Max60Text
	private String AccountName;
	
//	账户类别  AccountType  <Type>  [0..1]  AccountType  枚举 
	private AccountType AccountType;
//	账户状态  AccountStatus  <Status>  [0..1]  AccountStatus  枚举 
	private AccountStatus AccountStatus;
	
//	密码  Password  <Pwd>  [0..1]  Password  组件 
	private Password Password;
//	开户日期  RegisterDate  <RegDt>  [0..1]  Date
	private String RegisterDate;
	
//	有效日期  ValidDate  <VldDt>  [0..1]  Date   
	private String ValidDate;

	public String getAccountIdentification() {
		return AccountIdentification;
	}

	public void setAccountIdentification(String accountIdentification) {
		AccountIdentification = accountIdentification;
	}

	public String getAccountName() {
		return AccountName;
	}

	public void setAccountName(String accountName) {
		AccountName = accountName;
	}

	public AccountType getAccountType() {
		return AccountType;
	}

	public void setAccountType(AccountType accountType) {
		AccountType = accountType;
	}

	public AccountStatus getAccountStatus() {
		return AccountStatus;
	}

	public void setAccountStatus(AccountStatus accountStatus) {
		AccountStatus = accountStatus;
	}

	public Password getPassword() {
		return Password;
	}

	public void setPassword(Password password) {
		Password = password;
	}

	public String getRegisterDate() {
		return RegisterDate;
	}

	public void setRegisterDate(String registerDate) {
		RegisterDate = registerDate;
	}

	public String getValidDate() {
		return ValidDate;
	}

	public void setValidDate(String validDate) {
		ValidDate = validDate;
	}
	
}
