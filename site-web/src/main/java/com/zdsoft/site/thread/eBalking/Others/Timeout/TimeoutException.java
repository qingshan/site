package com.zdsoft.site.thread.eBalking.Others.Timeout;

public class TimeoutException extends InterruptedException {
    public TimeoutException(String msg) {
        super(msg);
    }
}
