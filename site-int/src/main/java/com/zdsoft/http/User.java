package com.zdsoft.http;

import java.io.Serializable;

public class User implements Serializable {

	private static final long serialVersionUID = 6299777889352761248L;
	
	private String userCd;
	private String userNm;

	public User(){
		this("", "");
	}
	
	public User(String userCd, String userNm) {
		this.userCd=userCd;
		this.userNm=userNm;
	}

	@Override
	public String toString() {
		return "User [userCd=" + userCd + ", userNm=" + userNm + "]";
	}

	public String getUserCd() {
		return userCd;
	}

	public void setUserCd(String userCd) {
		this.userCd = userCd;
	}

	public String getUserNm() {
		return userNm;
	}

	public void setUserNm(String userNm) {
		this.userNm = userNm;
	}

}
